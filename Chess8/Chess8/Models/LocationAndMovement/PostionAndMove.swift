//
//  PositionAndMove.swift
//  Chess8
//
//  Created by Don Clore on 4/4/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

enum WhiteOrBlack: String {
  case white = "White"
  case black = "Black"
}

// This ended up having to be a class, because I wanted to keep a singly linked list for the Knight case, so I could
// reconstuct an array of [Move] from a single Position item, if it had its chain of predecessors intact.
class Position: Hashable, Equatable {
  static func == (lhs: Position, rhs: Position) -> Bool {
    return lhs.x == rhs.x && lhs.y == rhs.y
  }
  
  static let maxX: Int = 7
  static let minX: Int = 0
  static let maxY: Int = 7
  static let minY: Int = 0
  
  let x: Int   // 0 relative.
  let y: Int
  let predecessor: Position?
  
  let color: WhiteOrBlack
  
  var valid: Bool {
    return x > -1 && x < 8 && y > -1 && y < 8
  }
        
  init(x: Int, y: Int, predecessor: Position? = nil) {
    self.x = x
    self.y = y
    self.predecessor = predecessor
    
    // Note to reviewers. There are many more 'elegant' ways to express this, but this is in keeping with my desire
    // for my code to be understandable by a 12 year old.  I fail at this a lot, but that's my goal. 
    if x.isEven {
      if y.isEven {
        color = .black
      } else {
        color = .white
      }
    } else {
      if y.isEven {
        color = .white
      } else {
        color = .black
      }
    }
  }
  
  func hash(into hasher: inout Hasher) {
    hasher.combine(x)
    hasher.combine(y)
  }
  
  private func generateDiagonalRays() -> [[Position]] {
    var rays: [[Position]] = []
    let directions = DiagonalDirection.allCases
    for direction in directions {
      if direction != .none {
        let ray = generateDiagonalRay(for: direction)
        if !rays.contains(ray) && ray.count > 1 {
          rays.append(ray)
        }
      }
    }
            
    return rays
  }
  
  private func generateDiagonalRay(for direction: DiagonalDirection) -> [Position] {
    var ray: [Position] = []
    let xMultiplier: Int
    let yMultiplier: Int
    
    switch direction {
      
    case .none:
      xMultiplier = 0
      yMultiplier = 0
      break
    case .upandright:
      xMultiplier = 1
      yMultiplier = 1
    case .upandleft:
      xMultiplier = -1
      yMultiplier = 1
    case .downandright:
      xMultiplier = 1
      yMultiplier = -1
      break
    case .downandleft:
      xMultiplier = -1
      yMultiplier = -1
    }
    
    var i = 1
    ray.append(self)
    repeat {
      
      let nextX: Int = x + (i * xMultiplier)
      let nextY: Int = y + (i * yMultiplier)
      let next = Position(x: nextX, y: nextY)
      guard next.valid else {
        // keep going until you hit the edge of chessboard.
        break
      }
      i += 1
      ray.append(next)
    } while true
    return ray
  }
  
  // Big N-squared fun.
  func findDiagonalPathIntersection(otherPosition: Position) -> Position? {
    let ourRays = generateDiagonalRays()
    let otherRays = otherPosition.generateDiagonalRays()

    for ray in ourRays {
      if ray.contains(otherPosition) {
        return otherPosition  // we're done.
      }
    }
    
    for ray in ourRays {      
      for otherRay in otherRays {
        let setOurs = Set<Position>(ray)
        let setOthers = Set<Position>(otherRay)
        
        let intersection = setOurs.intersection(setOthers)
        guard intersection.count > 0 else {
          continue
        }
        return intersection.first
      }
    }
    return nil
  }
}

// There is probably something in Foundation or the Swift stdlib that does this, but...it doesn't seem important to scour for it
// given the context of this assignment.
extension Int {
  var isEven: Bool {
    return isMultiple(of: 2) || isMultiple(of: -2) || self == 0
  }
}

struct Move: Hashable {
  let from: Position
  let to: Position
  
  var valid: Bool {
    return from.valid && to.valid
  }
}
