//
//  Chessboard.swift
//  Chess8
//
//  Created by Don Clore on 4/5/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation
/// Note to reader.  This is the very last thing I did.  I did this as an iOS app that does nothing, so if you wanted me to
// build some UI I could, but it could all easily be converted to a MacOS cmdline app, or a dylib or a static library, or
// you know....so for now, I'm exposing this singleton Chessboard object.
struct Chessboard {
  static let sharedInstance = Chessboard()
  
  private init() {}
  
  func movesFor(chessPieceType: ChessPieceType, from start: Position, to goal: Position) -> Result<[Move], RouteFinderError> {
    switch chessPieceType {
    case .queen:
      var queen = Queen(startPos: start, goal: goal)
      return queen.Run()
    case .king:
      var king = King(startPos: start, goal: goal)
//      var finder = RouteFinder(chessPiece: king, goal: goal)
//      return finder.Run()
      return king.Run()
    case .bishop:
      var bishop = Bishop(startPos: start, goal: goal)
      return bishop.Run()
    case .knight:
      var knight = Knight(startPos: start, goal: goal)
      return knight.Run()
    }
  }
}
