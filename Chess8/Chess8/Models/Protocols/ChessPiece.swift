//
//  ChessPiece.swift
//  Chess8
//
//  Created by Don Clore on 4/4/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

enum ChessPieceType: String {
  case queen = "Queen"
  case king = "King"
  case bishop = "Bishop"
  case knight = "Knight"
}

enum DiagonalDirection: CaseIterable {
  case none
  case upandright
  case upandleft
  case downandright
  case downandleft
}

// The idea was to try to extract out all the things in common that Chess pieces have to do, give them flexibility to
// do their case-specific stuff, and still work with generic Backtracker.   Which worked, until Knight...meh.
// It's all POP stuff, if we care about that kind of fancy lingo.  
protocol ChessPiece {
  var visited: Set<Position> { get set }
  var pieceType: ChessPieceType { get }
  var startPos: Position { get set }
  var goal: Position { get set }
  var currPos: Position { get set }
  mutating func getPossibleMoves() -> [Move]
  func isGoalFeasible(goal: Position) -> Bool
  func maybeMoveDiagonally() -> Move? 
  mutating func makeMove(move: Move)
  mutating func undoLastMove(oldPos: Position)
}

extension ChessPiece {
  mutating func undoLastMove(oldPos: Position) {
    currPos = oldPos
  }
  
  mutating func makeMove(move: Move) {
    currPos = move.to
  }
  
  func maybeMoveDiagonally() -> Move? {
    if let intersection = currPos.findDiagonalPathIntersection(otherPosition: goal) {
      if intersection.valid && !visited.contains(intersection) {
        let diagonalMove = Move(from: currPos, to: intersection)
        return diagonalMove
      }
    }
    return nil
  }
}

