//
//  Bishop.swift
//  Chess8
//
//  Created by Don Clore on 4/4/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

// Bishop is a subset of Queen, can only go diagonally, and it cannot ever reach a goal for a different colored square.
struct Bishop: ChessPiece {
  var visited: Set<Position> = Set<Position>()

  var pieceType: ChessPieceType {
    return .bishop
  }
  
  var startPos: Position
  var goal: Position
  var currPos: Position
  
  init(startPos: Position, goal: Position) {
    self.startPos = startPos
    self.goal = goal
    self.currPos = startPos
  }
  
  mutating func Run() -> Result<[Move], RouteFinderError> {
    guard startPos.color == goal.color else {
      return .failure(.impossible(reason: "Can't move from \(startPos.color.rawValue) to \(goal.color.rawValue)"))
    }
    var moves: [Move] = []
    guard let firstMove = maybeMoveDiagonally() else {
      // fatalError or assert()?  Both are arguable, depends on requirements.
      return .failure(.unexpected(reason: "should not be possible"))
    }
    moves.append(firstMove)
    if firstMove.to == goal {
      return .success(moves)
    }
    currPos = firstMove.to 
    guard let secondMove = maybeMoveDiagonally() else {
      return .failure(.unexpected(reason: "should not be possible"))
    }
    guard secondMove.to == goal else {
      return .failure(.unexpected(reason: "should not be possible"))
    }
    moves.append(secondMove)
    return .success(moves)
  }
    
  mutating func getPossibleMoves() -> [Move] {
    var moves: [Move] = []
    if let move = maybeMoveDiagonally() {
      if !visited.contains(move.to) {
        moves.append(move)
        visited.formUnion([move.to])
      }      
    }
    
    return moves
  }
  
  func isGoalFeasible(goal: Position) -> Bool {
    return goal.valid && goal.color == startPos.color
  }
}
