//
//  Queen.swift
//  Chess8
//
//  Created by Don Clore on 4/4/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

// A little more complicated than King in some ways, but easier in others.   It's a superset of Bishop, since it can
// go diagonally.  The most complicated thing was the diagonal ray logic.
struct Queen: ChessPiece {
  var visited: Set<Position> = Set<Position>()

  var pieceType: ChessPieceType {
    return .queen
  }
  var startPos: Position
  var goal: Position
  var currPos: Position

  init(startPos: Position, goal: Position) {
    self.startPos = startPos
    self.goal = goal
    self.currPos = startPos
  }
  
  mutating func Run() -> Result<[Move], RouteFinderError> {
    var moves: [Move] = []
    
    if startPos.x == goal.x || startPos.y == goal.y {
      // That's it.  One move.
      moves.append(Move(from: startPos, to: goal))
      return .success(moves)
    }
    
    if let diagonalMove = maybeMoveDiagonally() {
      if diagonalMove.to == goal {
        moves.append(diagonalMove)
        return .success(moves)
      }
    }
    
    let firstPosition = Position(x: startPos.x, y: goal.y, predecessor: startPos)
    let firstMove = Move(from: startPos, to: firstPosition)
    let secondPosition = Position(x: goal.x, y: goal.y, predecessor: firstPosition)
    let secondMove = Move(from: firstPosition, to: secondPosition)
    return .success([firstMove, secondMove])    
  }
  
  mutating func getPossibleMoves() -> [Move] {
    var moves: [Move] = []
    
    // Make a diagonal move?   This only happens if both xDelta and yDelta are nonzero, and it moves the same amount of x and y
    // in a number of increments equal to the smaller delta.
    let xDelta = goal.x - currPos.x
    let yDelta = goal.y - currPos.y
    
    if let diagonalMove = maybeMoveDiagonally() {
      moves.append(diagonalMove)
    }    
       
    // Make a horizontal move?   Just move horizontally across the board by the full xDelta
    if xDelta != 0 {
      let xPosition: Position = Position(x: goal.x, y: currPos.y)
      if xPosition.valid {
        let xMove = Move(from: currPos, to: xPosition)
        if !visited.contains(xPosition) {
          moves.append(xMove)
          visited.formUnion([xPosition])
        }
      }
    }
    
    // Similarly, a vertical move would be vertically up or down by the full yDelta
    if yDelta != 0 {
      let yPosition: Position = Position(x: currPos.x, y: goal.y)
      if yPosition.valid {
        let yMove = Move(from: currPos, to: yPosition)
        if !visited.contains(yPosition) {
          moves.append(yMove)
        }
      }
    }
    
    return moves
  }
  
  func isGoalFeasible(goal: Position) -> Bool {
    return goal.valid
  }
}
