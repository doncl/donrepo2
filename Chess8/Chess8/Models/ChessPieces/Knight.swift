//
//  Knight.swift
//  Chess8
//
//  Created by Don Clore on 4/4/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

// Man, I kept trying to force this square peg into a round hole, and make it work with my BackTracker<T> stuff, until I...finally
// realized...backtracker is depth first.   And I need breadth first for this.
// https://www.geeksforgeeks.org/minimum-steps-reach-target-knight/
// Anyway, this is just a very simple implementation tha kinda sort of resonates in spirit with the King, Queen, and Bishop,
// which were solvable with Backtracker.
struct Knight {
  var pieceType: ChessPieceType {
    return .knight
  }
  var visited: Set<Position> = Set<Position>()
  var startPos: Position
  var goal: Position
  var currPos: Position

  init(startPos: Position, goal: Position) {
    self.startPos = startPos
    self.goal = goal
    self.currPos = startPos
    self.visited.formUnion([startPos])
  }
  
  mutating func Run() -> Result<[Move], RouteFinderError> {
    var q: Queue<Position> = Queue<Position>()
    q.enqueue(startPos)
    
    visited.formUnion([startPos])
    
    let deltaTuples: [(x: Int, y: Int)] = [
      (x: 2, y: 1),
      (x: 2, y: -1),
      (x: 1, y: 2),
      (x: 1, y: -2),
      (x: -1, y: 2),
      (x: -1, y: -2),
      (x: -2, y: 1),
      (x: -2, y: -1),
    ]
    
    while !q.isEmpty {
      let current = q.dequeue()!
      
      if current == goal {
        let moves = constructMoves(from: current)
        return .success(moves)
      }
      
      for tuple in deltaTuples {
        let candidate = Position(x: current.x + tuple.x, y: current.y + tuple.y, predecessor: current)
        if candidate.valid && !visited.contains(candidate) {
          visited.formUnion([candidate])
          q.enqueue(candidate)
        }
      }
    }
    
    return .failure(.impossible(reason: "Couldn't reach goal"))
  }
  
  private func constructMoves(from position: Position) -> [Move] {
    var positions: [Position] = []
    var current = position
    positions.append(current)
    while current.predecessor != nil {
      current = current.predecessor!
      positions.insert(current, at: 0)
    }
    
    guard positions.count > 1 else {
      return []
    }
    var moves: [Move] = []
    for i in 0..<positions.count - 1 {
      let start = positions[i]
      let end = positions[i + 1]
      let move = Move(from: start, to: end)
      moves.append(move)
    }
    
    
    return moves
  }
}
