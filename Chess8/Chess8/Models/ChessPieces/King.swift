//
//  King.swift
//  Chess8
//
//  Created by Don Clore on 4/4/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

// Simplest case, in some senses.  Can only move one square at a time.  Not diagonally.
// Did this one first. 
struct King: ChessPiece {
  enum Directions {
    case left
    case right
    case up
    case down
  }
  
  var pieceType: ChessPieceType {
    return .king
  }
    
  var startPos: Position = Position(x: 3, y: 0)
  var currPos: Position
  
  var goal: Position
  
  var visited: Set<Position> = Set<Position>()
  
  init(startPos: Position, goal: Position) {
    self.startPos = startPos
    self.goal = goal
    currPos = startPos
  }
  
  func isGoalFeasible(goal: Position) -> Bool {
    return goal.valid
  }
  
  mutating func Run() -> Result<[Move], RouteFinderError> {
    guard goal.valid else {
      return .failure(.impossible(reason: "Can't get there from here."))
    }
    let xDir: Directions
    let yDir: Directions
    
    var xDelta = goal.x - startPos.x
    var yDelta = goal.y - startPos.y
    
    xDir = xDelta < 0 ? .left : .right
    yDir = yDelta < 0 ? .down : .up
    
    var moves: [Move] = []
    var direction: Directions = xDelta != 0 ? xDir : yDir
    var done: Bool = false
    repeat {
      switch direction {
      case .left:
        if xDelta != 0 {
          assert(xDelta < 0)
          let newPosition = Position(x: currPos.x - 1, y: currPos.y)
          assert(newPosition.valid)
          let move = Move(from: currPos, to: newPosition)
          currPos = newPosition
          moves.append(move)
          if newPosition == goal {
            done = true
          }
          xDelta += 1
        }
      case .right:
        if xDelta != 0 {
          assert(xDelta > 0)
          let newPosition = Position(x: currPos.x + 1, y: currPos.y)
          assert(newPosition.valid)
          let move = Move(from: currPos, to: newPosition)
          currPos = newPosition
          moves.append(move)
          if newPosition == goal {
            done = true
          }
          xDelta -= 1
        }
      case .up:
        if yDelta != 0 {
          assert(yDelta > 0)
          let newPosition = Position(x: currPos.x, y: currPos.y + 1)
          assert(newPosition.valid)
          let move = Move(from: currPos, to: newPosition)
          currPos = newPosition
          moves.append(move)
          if newPosition == goal {
            done = true
          }
          yDelta -= 1
        }
      case .down:
        if yDelta != 0 {
          assert(yDelta < 0)
          let newPosition = Position(x: currPos.x, y: currPos.y - 1)
          assert(newPosition.valid)
          let move = Move(from: currPos, to: newPosition)
          currPos = newPosition
          moves.append(move)
          if newPosition == goal {
            done = true
          }
          yDelta += 1
        }
      }
      
      if !done {
        // swap directions.
        if direction == xDir {
          direction = yDir
        } else {
          direction = xDir
        }
      }
    } while !done
    
    return .success(moves)
  }
  
  mutating func getPossibleMoves() -> [Move] {
    var moves: [Move] = []
    
    var directions: [Directions] = [Directions]()
      
    if goal.y > currPos.y {
      directions.append(.up)
    } else if goal.y < currPos.y {
      directions.append(.down)
    }
    
    if goal.x < currPos.x {
      directions.append(.left)
    } else if goal.x > currPos.x {
      directions.append(.right)
    }
    
    for direction in directions {
      switch direction {
      case .left:
        let leftPosition: Position = Position(x: currPos.x - 1, y: currPos.y)
        if leftPosition.valid {
          let leftMove = Move(from: currPos, to: leftPosition)
          if !visited.contains(leftPosition) {
            moves.append(leftMove)
            visited.formUnion([leftPosition])
          }
        }
      case .right:
        let rightPosition: Position = Position(x: currPos.x + 1, y: currPos.y)
        if rightPosition.valid {
          let rightMove = Move(from: currPos, to: rightPosition)
          if !visited.contains(rightPosition) {
            moves.append(rightMove)
            visited.formUnion([rightPosition])
          }
        }
      case .up:
        let forwardPosition: Position = Position(x: currPos.x, y: currPos.y + 1)
        if forwardPosition.valid {
          let forwardMove = Move(from: currPos, to: forwardPosition)
          if !visited.contains(forwardPosition) {
            moves.append(forwardMove)
            visited.formUnion([forwardPosition])
          }
        }
      case .down:
        let downPosition: Position = Position(x: currPos.x, y: currPos.y - 1)
        if downPosition.valid {
          let backMove = Move(from: currPos, to: downPosition)
          if !visited.contains(downPosition) {
            moves.append(backMove)
            visited.formUnion([downPosition])
          }
        }
      }
    }
    
    return moves
  }
}
