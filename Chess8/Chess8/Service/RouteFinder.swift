//
//  RouteFinder.swift
//  Chess8
//
//  Created by Don Clore on 4/4/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

enum RouteFinderError: Error {
  case impossible(reason: String)
  case unexpected(reason: String)
}

// concrete implementor of Backtracker protocol for use with Chess Pieces. 
struct RouteFinder: Backtracker {
  typealias entityType = Move
  
  var finished: Bool = false
  var start: Position
  var goal: Position
  var chessPiece: ChessPiece
  var solutions: [[Move]] = [[Move]]()
    
  init(chessPiece: ChessPiece, goal: Position) {
    self.start = chessPiece.startPos
    self.goal = goal
    self.chessPiece = chessPiece
  }
  
  mutating func Run() -> Result<[Move], RouteFinderError> {
    guard chessPiece.isGoalFeasible(goal: goal) else {
      let rawName = chessPiece.pieceType.rawValue
      return .failure(.impossible(reason: "Goal of \(goal) is not possible for chessPiece \(rawName) starting from \(start)"))
    }
    var a:[Move] = [Move]()
    backTrack(a: &a, k: 0)
    guard solutions.count > 0 else {
      return .failure(.unexpected(reason: "No solutions found"))
    }
    guard let shortest = solutions.min(by: {$0.count < $1.count}) else {
      return .failure(.unexpected(reason: "should not be possible"))
    }
    return .success(shortest)
  }
  
  func isSolution(a: [Move], k: Int) -> Bool {
    guard let lastMove = a.last else {
      return false
    }
    let solvedIt = lastMove.to == goal
    
    return solvedIt
  }
  
  mutating func processSolution(a: [Move], k: Int) {
    solutions.append(a)
  }
  
  mutating func constructCandidates(a: [Move], k: Int) -> [Move] {
    if solutions.count > 0 {
      if let _ = solutions.first(where: { $0.count < k}) {
        return []
      }
    }
    let candidates = chessPiece.getPossibleMoves()
    return candidates
  }
  
  mutating func makeMove(a: inout [Move], k: Int) {
    guard let nextMove = a.last else {
      return
    }
    chessPiece.makeMove(move: nextMove)
  }
  
  mutating func unmakeMove(a: inout [Move], k: Int) {
    let oldPos: Position
    if let lastMove = a.last {
      oldPos = lastMove.from
    } else {
      oldPos = chessPiece.startPos
    }
    chessPiece.undoLastMove(oldPos: oldPos)
  }
}
