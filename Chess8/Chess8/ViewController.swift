//
//  ViewController.swift
//  Chess8
//
//  Created by Don Clore on 4/3/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit


/// Doesn't do anything useful.  It's here in case we wanted to extend this to have some UI.  Please let me do this offline
/// though, not in the middle of a screenshare.
class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }
}

