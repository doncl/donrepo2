//
//  AppDelegate.swift
//  Chess8
//
//  Created by Don Clore on 4/3/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

/// So why is it called 'Chess8'?   Because....I don't play chess...at all.   And I didn't realize the 8x8 definition of the
/// problem is just the way all chessboards are, until I started looking online at pics of them to help me visualize the moves.
/// I don't play Go, or do crossword puzzles, either....I hope that doesn't mitigate against my qualifications.
/// https://www.quora.com/Are-there-really-productive-developers-who-just-dont-do-well-with-technical-interviews/answer/Don-Clore?__nsrc__=4&__snid3__=1468893085
/// My father was a Life Master in contract bridge, and one of his eternal disappointments was my extreme distaste for learning
/// the game :).   Oh, well.  

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    return true
  }

  // MARK: UISceneSession Lifecycle

  func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
    // Called when a new scene session is being created.
    // Use this method to select a configuration to create the new scene with.
    return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
  }

  func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    // Called when the user discards a scene session.
    // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
    // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
  }


}

