//
//  Backtracker.swift
//  Chess8
//
//  Created by Don Clore on 4/3/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

/// I dug up some code I was studying for interviews in 2012.  I originally wrote it in C#, it was like this:
/*
public abstract class Backtracker<T>
   {
       public bool Finished { get; set; }

       public abstract bool IsSolution(List<T> a, int k);

       public abstract void ProcessSolution(List<T> a, int k);

       public abstract IEnumerable<T> ConstructCandidates(List<T> a, int k);

       public virtual void MakeMove(List<T> a, int k)
       {
       }

       public virtual void UnmakeMove(List<T> a, int k)
       {
       }

       public void Backtrack(List<T> a, int k)
       {
           if (IsSolution(a, k))
               ProcessSolution(a, k);
           else
           {
               foreach (T c in ConstructCandidates(a, k))
               {
                   a.Add(c);
                   MakeMove(a, k);
                   Backtrack(a, k + 1);
                   UnmakeMove(a, k);
                   a.RemoveAt(a.Count - 1);
                   if (Finished)
                       return;
               }
           }
       }
   }
*/

// With all these mutating funcs, it might well have been a reference type, I guess.   I have no religion over whether its
// implemented by struct or class, I just generally start with structs, but...my principles are flexible.
// As the great Groucho Marx said, "These are my principles, and if you don't like them....I have others."
protocol Backtracker {
  associatedtype entityType
  
  var finished: Bool { get }
  func isSolution(a: [entityType], k: Int) -> Bool
  mutating func processSolution(a: [entityType], k: Int)
  mutating func constructCandidates(a: [entityType], k: Int) -> [entityType]
  
  mutating func makeMove(a: inout [entityType], k: Int)
  mutating func unmakeMove(a: inout [entityType], k: Int)
  
  mutating func backTrack(a: inout [entityType], k: Int)
}

extension Backtracker {
  mutating func backTrack(a: inout [entityType], k: Int) {
    if isSolution(a: a, k: k) {
      processSolution(a: a, k: k)
    } else {
      let candidates = constructCandidates(a: a, k: k)
      for candidate in candidates {
        a.append(candidate)
        makeMove(a: &a, k: k)
        backTrack(a: &a, k: k + 1)
        unmakeMove(a: &a, k: k)
        a.removeLast()
        if finished {
          return
        }
      }
    }
  }
}
