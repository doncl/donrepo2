//
//  Queue.swift
//  Chess8
//
//  Created by Don Clore on 4/5/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation
// From Wenderlich data structure book, I think....
// Probably would have been more sensible to do the Queue as a simple array, and just pop off the end.  I think the book
// presented queues as a protocol, and then this final version that uses the pair of arrays was presented as a sort of
// clever way to get O(1) performance out of it.  This wasn't really necessary for this homework, it was just what I remembered
// from the book, but you know, premature optimization is the root of all evil, and all that....
protocol QueueProtocol {
  associatedtype Element
  mutating func enqueue(_ element: Element) -> Bool
  mutating func dequeue() -> Element?
  var isEmpty: Bool { get }
  var count: Int { get }
  var peek: Element? { get }
}

struct Queue<T>: QueueProtocol {
  private var leftStack: [T] = []
  private var rightStack: [T] = []
  
  var isEmpty: Bool {
    return leftStack.isEmpty && rightStack.isEmpty
  }
  
  var peek: T? {
    return !leftStack.isEmpty ? leftStack.last : rightStack.first
  }
  
  var count: Int {
    return leftStack.count + rightStack.count
  }
  
  init() {}
  
  @discardableResult
  mutating func enqueue(_ element: T) -> Bool {
    rightStack.append(element)
    return true
  }
  
  mutating func dequeue() -> T? {
    if leftStack.isEmpty {
      leftStack = rightStack.reversed()
      rightStack.removeAll()
    }
    return leftStack.popLast()
  }
}
