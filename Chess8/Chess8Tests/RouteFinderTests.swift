//
//  RouteFinderTests.swift
//  Chess8Tests
//
//  Created by Don Clore on 4/4/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import XCTest
@testable import Chess8

///
///  Notes to reviewer:  I pretty much developed it using these tests, albeit it took me awhile to realize how to generate
///  test cases.  I realize that, per the assignment, if all we want is the *number* of moves, I didn't even need to do all
///  this work.  The test case generation code knows how many moves to expect.  I realize now that....the code I wrote is
///  more about generating the actual route (or an actual route, sometimes there are multiples that have the same length).
///

class RouteFinderTests: XCTestCase {
  private func generateRoutes(algo: ((Position, Position) -> Int)) -> [(start: Position, end: Position, length: Int)] {
    var routes: [(start: Position, end: Position, length: Int)] = []
    for i in 0..<8 {
      for j in 0..<8 {
        let startPos: Position = Position(x: i, y: j)
        for x in 0..<8 {
          for y in 0..<8 {
            
            if x == i && y == j {
              continue
            }
            let goal = Position(x: x, y: y)
            
            let expected = algo(startPos, goal)
            routes.append((start: startPos, end: goal, length: expected))
          }
        }
      }
    }
    return routes
  }
  
  func testKingRoutes() {
    let routes = generateRoutes(algo: { start, goal in
      return abs(goal.x - start.x) + abs(goal.y - start.y)
    })

    for (index, route) in routes.enumerated() {
      let result = Chessboard.sharedInstance.movesFor(chessPieceType: .king, from: route.start, to: route.end)
      
      switch result {
      case .success(let shortestRout):
        XCTAssertEqual(shortestRout.count, route.length)
      case .failure(let error):
        print("index = \(index)")
        XCTFail(error.localizedDescription)
      }
    }
  }
  
  func testQueenRoutes() {
    let queenAlgo: (Position, Position) -> Int = { start, goal in
      if start.x == goal.x || start.y == goal.y {
        return 1
      }

      if let intersection = start.findDiagonalPathIntersection(otherPosition: goal) {
        if intersection == goal {
          return 1
        }
        return 2
      }
      return 2
    }
    
    let queenRoutes = generateRoutes(algo: queenAlgo)

    for route in queenRoutes {
      let result = Chessboard.sharedInstance.movesFor(chessPieceType: .queen, from: route.start, to: route.end)
      switch result {
      case .success(let shortestRout):
        XCTAssertEqual(shortestRout.count, route.length)
      case .failure(let error):
        XCTFail(error.localizedDescription)
      }
    }
  }
  
  func testBishopRoutes() {
    let bishopAlgo: (Position, Position) -> Int = { start, goal in
      guard start.color == goal.color else {
        return -1
      }

      if let intersection = start.findDiagonalPathIntersection(otherPosition: goal) {
        if intersection == goal {
          return 1
        }
      }
      return 2
    }

    let bishopRoutes = generateRoutes(algo: bishopAlgo)
    
    for route in bishopRoutes {
      let result = Chessboard.sharedInstance.movesFor(chessPieceType: .bishop, from: route.start, to: route.end)
      switch result {
      case .success(let shortestRout):
        XCTAssertEqual(shortestRout.count, route.length)
      case .failure(let error):
        XCTAssertEqual(-1, route.length)
        switch error {
        case .impossible:
          // expected - this is a success case.
          break
        case .unexpected:
          XCTFail("must never happen")
        }
      }
    }
  }
    
  func testCompareKnightRouteMethods() {
    for i in 0..<8 {
      for j in 0..<8 {
        let knightPos: [Int] = [i, j]
        let startPos: Position = Position(x: i, y: j)
        for k in 0..<8 {
          for l in 0..<8 {
            if k == i && l == j {
              continue
            }
            let targetPos: [Int] = [k, l]
            let goal: Position = Position(x: k, y: l)

            let altSteps = minStepToReachTarget(knightPos: knightPos, targetPos: targetPos, n: 8)
            let result = Chessboard.sharedInstance.movesFor(chessPieceType: .knight, from: startPos, to: goal)
            switch result {
            case .success(let shortestRoute):
              XCTAssertEqual(shortestRoute.count, altSteps)
            case .failure(let error):
              switch error {
              case .impossible:
                XCTAssertEqual(altSteps, Int.max) // success case.
              case .unexpected(reason: let reason):
                XCTFail("Must never happen, reason \(reason)")
              }
            }
          }
        }
      }
    }
  }
}

// from https://www.geeksforgeeks.org/minimum-steps-reach-target-knight/
class Cell {
  var x: Int
  var y: Int
  var dis: Int
  var predecessor: Cell?
  
  init(x: Int, y: Int, dis: Int, predecessor: Cell?) {
    self.x = x
    self.y = y
    self.dis = dis
    self.predecessor = predecessor
  }
}

func inside(x: Int, y: Int, n: Int) -> Bool {
  if x >= 0 && x < n && y >= 0 && y < n {
    return true
  }
  return false
}

func minStepToReachTarget(knightPos: [Int], targetPos: [Int], n: Int) -> Int {
  // x and y direction, where a knight can move
  let dx: [Int] = [-2, -1, 1, 2, -2, -1, 1, 2]
  let dy: [Int] = [-1, -2, -2, -1, 1, 2, 2, 1]
  
  var q = Queue<Cell>()
  
  let newCell = Cell(x: knightPos[0], y: knightPos[1], dis: 0, predecessor: nil)
  q.enqueue(newCell)
  
  var t: Cell
  var x, y: Int
  var visit: [[Bool]] = [[Bool]]()
  for _ in 0..<n {
    let row: [Bool] = [Bool](repeating: false, count: n)
    visit.append(row)
  }
  
  visit[knightPos[0]][knightPos[1]] = true
  
  while q.count != 0 {
    t = q.dequeue()!
    
    // if current cell is equal to target cell, return its distance
    if t.x == targetPos[0] && t.y == targetPos[1] {
      return t.dis
    }
    
    // loop for all reachable states
    for i in 0..<8 {
      x = t.x + dx[i]
      y = t.y + dy[i]
      
      // If reachable state is not yet visited and inside board, push that state into queue.
      if inside(x: x, y: y, n: n) && !visit[x][y] {
        visit[x][y] = true
        let newCell = Cell(x: x, y: y, dis: t.dis + 1, predecessor: t)
        q.enqueue(newCell)
      }
    }
  }
  
  
  return Int.max
}


