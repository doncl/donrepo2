//
//  PositionTests.swift
//  Chess8Tests
//
//  Created by Don Clore on 4/4/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import XCTest
@testable import Chess8

class PositionTests: XCTestCase {  
  func testIsEven() {
    let start: Int = -100
    let end: Int = 100
    var even: Bool = (start % 2) == 0
    for i in start..<end {
      XCTAssertEqual(i.isEven, even)
      even = !even
    }
  }
  
  func testDiagonalIntersectionLogic() {
    let testTuples: [(first: Position, second: Position, intersection: Position?)] = [
      (first: Position(x: 5, y: 2), second: Position(x: 0, y: 7), intersection: Position(x: 0, y: 7)),
      (first: Position(x: 1, y: 0), second: Position(x: 0, y: 7), intersection: Position(x: 4, y: 3)),
      (first: Position(x: 1, y: 0), second: Position(x: 3, y: 0), intersection: Position(x: 2, y: 1)),
      (first: Position(x: 1, y: 0), second: Position(x: 4, y: 0), intersection: nil),
      (first: Position(x: 1, y: 0), second: Position(x: 3, y: 6), intersection: Position(x: 5, y: 4)),
      (first: Position(x: 1, y: 0), second: Position(x: 7, y: 6), intersection: Position(x: 7, y: 6)),
      (first: Position(x: 1, y: 0), second: Position(x: 4, y: 7), intersection: Position(x: 6, y: 5)),
      (first: Position(x: 1, y: 0), second: Position(x: 4, y: 6), intersection: nil),
      (first: Position(x: 1, y: 0), second: Position(x: 7, y: 4), intersection: Position(x: 6, y: 5)),
      (first: Position(x: 7, y: 0), second: Position(x: 0, y: 7), intersection: Position(x: 0, y: 7)),
      (first: Position(x: 7, y: 0), second: Position(x: 3, y: 0), intersection: Position(x: 5, y: 2)),
      (first: Position(x: 7, y: 0), second: Position(x: 0, y: 3), intersection: Position(x: 2, y: 5)),
      (first: Position(x: 7, y: 0), second: Position(x: 5, y: 4), intersection: Position(x: 4, y: 3)),
      (first: Position(x: 7, y: 0), second: Position(x: 7, y: 2), intersection: Position(x: 6, y: 1)),
      (first: Position(x: 7, y: 0), second: Position(x: 1, y: 2), intersection: Position(x: 3, y: 4)),
    ]
    
    for tuple in testTuples {
      let intersection = tuple.first.findDiagonalPathIntersection(otherPosition: tuple.second)
      XCTAssertEqual(intersection, tuple.intersection)
    }
  }
}
