//
//  Chess8Tests.swift
//  Chess8Tests
//
//  Created by Don Clore on 4/3/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import XCTest
@testable import Chess8

/// This was just to refamiliarize myself with the vagaries of the backtracking algorithm, I had implemented it for these
/// two cases some years ago (2012?), and I needed to get my arms around it again. 
class Chess8Tests: XCTestCase {
  func testStringCombos() {
    let s: String = "0123"
    var sc: StringCombos = StringCombos(input: s)
    let solutions = sc.Run()
    print(solutions)
  }
  
  func testStringPerms() {
    let s: String = "0123"
    var sp: StringPerms = StringPerms(input: s)
    let solutions = sp.Run()
    print(solutions)
  }
}

struct StringCombos: Backtracker {
  typealias entityType = Int
  
  var input: String
  var finished: Bool
  var solutions: [String]
  
  init(input: String) {
    self.input = input
    self.finished = false
    self.solutions = []
  }
  
  func isSolution(a: [Int], k: Int) -> Bool {
    return false
  }
  
  mutating func processSolution(a: [Int], k: Int) {
  }
  
  func constructCandidates(a: [Int], k: Int) -> [Int] {
    var result: [Int] = []
    
    if k == input.count {
      return result
    }
    
    var start: Int = k
    if a.count > 0 {
      start = a[a.count - 1]
    }
    
    for i in start..<input.count {
      if !a.contains(i) {
        result.append(i)
      }
    }
    return result
  }
  
  mutating func makeMove(a: inout [Int], k: Int) {
    var solution: String = ""
    for i in a {
      let index = input.index(input.startIndex, offsetBy: i)
      let character: Character = input[index]
      solution.append(character)
    }
    if !solution.isEmpty {
      solutions.append(solution)
    }
  }
  
  func unmakeMove(a: inout [Int], k: Int) {
    // do nothing
  }
  
  mutating func Run() -> [String] {
    var a: [Int] = [Int]()
    backTrack(a: &a, k: 0)
    return solutions
  }
}


struct StringPerms: Backtracker {
  typealias entityType = Int
  
  var input: String
  var finished: Bool
  var solutions: [String]
  
  init(input: String) {
    self.input = input
    self.finished = false
    self.solutions = []
  }

  func isSolution(a: [Int], k: Int) -> Bool {
    return k == input.count
  }
  
  mutating func processSolution(a: [Int], k: Int) {
    var solution: String = ""
    for i in a {
      let index = input.index(input.startIndex, offsetBy: i)
      let character: Character = input[index]
      solution.append(character)
    }
    if !solution.isEmpty {
      solutions.append(solution)
    }
  }
  
  func constructCandidates(a: [Int], k: Int) -> [Int] {
    var candidates: [Int] = [Int]()
    
    if k == input.count {
      return candidates
    }
    var inPerms: [Bool] = [Bool](repeating: false, count: input.count)
    for i in a {
      inPerms[i] = true
    }
    for i in 0..<input.count {
      if inPerms[i] == false {
        candidates.append(i)
      }
    }
    
    return candidates
  }
  
  mutating func makeMove(a: inout [Int], k: Int) {
    
  }
  
  mutating func unmakeMove(a: inout [Int], k: Int) {
    
  }
  
  mutating func Run() -> [String] {
    var a: [Int] = [Int]()
    backTrack(a: &a, k: 0)
    return solutions
  }
}
