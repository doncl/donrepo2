//
//  AlignmentGuidesAnimationSwiftUIApp.swift
//  AlignmentGuidesAnimationSwiftUI
//
//  Created by Don Clore on 2/18/21.
//

import SwiftUI

@main
struct AlignmentGuidesAnimationSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
