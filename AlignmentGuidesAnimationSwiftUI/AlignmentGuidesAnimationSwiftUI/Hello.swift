//
//  Hello.swift
//  AlignmentGuidesAnimationSwiftUI
//
//  Created by Don Clore on 2/18/21.
//

import SwiftUI

struct Hello: View {
    var body: some View {
        Group { Text("Hello").foregroundColor(.black) + Text(" World").foregroundColor(.clear) }.padding(20)
    }
}


struct Hello_Previews: PreviewProvider {
    static var previews: some View {
        Hello()
    }
}
