//
//  World.swift
//  AlignmentGuidesAnimationSwiftUI
//
//  Created by Don Clore on 2/18/21.
//

import SwiftUI


struct World: View {
    var body: some View {
        Group { Text("Hello").foregroundColor(.clear) + Text(" World").foregroundColor(.black) }.padding(20)
    }
}


struct World_Previews: PreviewProvider {
    static var previews: some View {
        World()
    }
}
