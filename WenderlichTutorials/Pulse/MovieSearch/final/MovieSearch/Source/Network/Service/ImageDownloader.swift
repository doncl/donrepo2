/// Copyright (c) 2022 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// This project and source code may use libraries or frameworks that are
/// released under various Open-Source licenses. Use of those libraries and
/// frameworks are governed by their own individual licenses.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import PulseCore

class ImageDownloader: NSObject {
  private let imageBaseURLString = "https://image.tmdb.org"

  let urlSession = URLSession(configuration: URLSessionConfiguration.default, delegate: nil, delegateQueue: nil)
  let logger = NetworkLogger()

  var imageDownloadCompletion: ((Result<UIImage, NetworkError>) -> Void)?

  func downloadImage(for imageType: ImageType, at path: String) {
    guard let url = try? url(for: imageType, at: path) else {
      return
    }

    let task = urlSession.dataTask(with: url)
    task.delegate = self
    task.resume()
  }

  private func url(for imageType: ImageType, at path: String) throws -> URL {
    let imagePathParam = imageType.pathParameter()
    guard let baseURL = URL(string: imageBaseURLString),
      var urlComponents = URLComponents(url: baseURL, resolvingAgainstBaseURL: false) else {
        throw NetworkError.invalidURL
      }

    urlComponents.path = "/t/p/\(imagePathParam)\(path)"

    let queryItems: [URLQueryItem] = [
      URLQueryItem(name: "api_key", value: APIKey.value)
    ]

    urlComponents.queryItems = queryItems

    guard let url = urlComponents.url else {
      throw NetworkError.invalidURL
    }

    return url
  }
}

extension ImageDownloader: URLSessionTaskDelegate, URLSessionDataDelegate {
  func urlSession(
    _ session: URLSession,
    dataTask: URLSessionDataTask,
    didReceive response: URLResponse,
    completionHandler: @escaping (URLSession.ResponseDisposition) -> Void
  ) {
    logger.logDataTask(dataTask, didReceive: response)

    if let response = response as? HTTPURLResponse,
      response.statusCode != 200 {
      imageDownloadCompletion?(.failure(.invalidResponseType))
    }

    completionHandler(.allow)
  }

  func urlSession(
    _ session: URLSession,
    task: URLSessionTask,
    didCompleteWithError error: Error?
    ) {
    logger.logTask(task, didCompleteWithError: error)
    imageDownloadCompletion?(.failure(NetworkError.invalidResponseType))
  }

  func urlSession(
    _ session: URLSession,
    task: URLSessionTask,
    didFinishCollecting metrics: URLSessionTaskMetrics
    ) {
    logger.logTask(task, didFinishCollecting: metrics)
  }

  func urlSession(
    _ session: URLSession,
    dataTask: URLSessionDataTask,
    didReceive data: Data
    ) {
    logger.logDataTask(dataTask, didReceive: data)
    guard let image = UIImage(data: data) else {
      imageDownloadCompletion?(.failure(.invalidParse))
      return
    }
    imageDownloadCompletion?(.success(image))
  }
}
