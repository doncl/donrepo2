//
//  main.swift
//  Permu
//
//  Created by Don Clore on 11/3/20.
//

import Foundation

extension String {
  func toStringCharArray() -> [String] {
    var ret: [String] = []
    forEach {
      let charString: String = String($0)
      ret.append(charString)
    }
    return ret
  }
}

func permu(string: String) -> [String] {
  var result: [String] = []
     
  func permus(curStr: String, bank: inout [String]) {
    // If we're up to the full length, record the result.
    if curStr.count == string.count  {
      result.append(curStr)
      return
    }
    
    var build = curStr
       
    let count = bank.count
    for i in 0..<count {
      // for each character in bank, extract the character, and remove it from the bank.
      let char = bank[i]
      bank.remove(at: i)
      
      // Add it to build
      build += char
      
      // recurse into the method, with the new build, and the bank that has one char removed.
      permus(curStr: build, bank: &bank)
      
      // add the character back to bank.
      bank.insert(char, at: i)
    }
  }
  
  var charArray: [String] = string.toStringCharArray()
  permus(curStr: "", bank: &charArray)
  
  return result
}

//let result = permu(string: "ron")
//
//print(result)


func powerset(word: String) -> [String] {
  var ret: [String] = []

  func helper(build: String, i: Int) {
    if i == word.count {
      guard !build.isEmpty else {
        return 
      }
      ret.append(build)
      return
    }
    
    // DO append the character
    let  index = word.index(word.startIndex, offsetBy: i)
    let char = word[index]
    let doAppend = build + String(char)
    helper(build: doAppend, i: i + 1)
    
    // DON'T append the character
    helper(build: build, i: i + 1)
  }

  helper(build: "", i: 0)
  return ret
}

let result = powerset(word: "ron")
print(result)
