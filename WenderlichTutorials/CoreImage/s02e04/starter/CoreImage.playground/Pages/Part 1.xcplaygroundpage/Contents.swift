import PlaygroundSupport
import UIKit
import CoreImage
import CoreImage.CIFilterBuiltins

let url = Bundle.main.url(forResource: "IMG_5276", withExtension: "HEIC")!
var options = [CIImageOption.applyOrientationProperty: true]
let image = CIImage(contentsOf: url, options: options)!

options[.auxiliaryPortraitEffectsMatte] = true
let matte = CIImage(contentsOf: url, options: options)!

let resized = image.transformed(by: CGAffineTransform(scaleX: 0.125, y: 0.125))
let matteResized = matte.transformed(by: CGAffineTransform(scaleX: 0.25, y: 0.25))

// Stringly-typed 'fluent' way
let background = resized.applyingFilter("CIVibrance", parameters: ["inputAmount": -0.8])
  .applyingFilter("CIDiscBlur", parameters: ["inputRadius": 8])
  .cropped(to: resized.extent)
  .applyingFilter("CIVignette", parameters: ["inputIntensity": 0.7, "inputRadius": 20])


let fgVibrance = CIFilter.vibrance()
fgVibrance.amount = 0.7
fgVibrance.inputImage = resized

let foreground = fgVibrance.outputImage

let compositeFilter = CIFilter.blendWithMask()
compositeFilter.inputImage = foreground
compositeFilter.backgroundImage = background
compositeFilter.maskImage = matteResized

let composite = compositeFilter.outputImage
