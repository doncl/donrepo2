//: [Previous](@previous)
import Foundation
import CoreImage
import CoreImage.CIFilterBuiltins

// Import the image, as before
let url = Bundle.main.url(forResource: "IMG_5276", withExtension: "HEIC")!
var options = [CIImageOption.applyOrientationProperty: true]
let image = CIImage(contentsOf: url, options: options)!

// Video 2: Masked Filtering
options[.auxiliaryPortraitEffectsMatte] = true
let matte = CIImage(contentsOf: url, options: options)!

let resized = image.transformed(by: CGAffineTransform(scaleX: 0.125, y: 0.125))
let matteResized = matte.transformed(by: CGAffineTransform(scaleX: 0.25, y: 0.25))

//let invertFilter = CIFilter.colorInvert()
//invertFilter.inputImage = matteResized
//
//let filter = CIFilter.maskedVariableBlur()
//filter.inputImage = resized
//filter.mask = invertFilter.outputImage
//filter.radius = 20
//filter.outputImage

// Video 3: Manually filtering the background

//let bgVibrance = CIFilter.vibrance()
//bgVibrance.amount = 0.8
//bgVibrance.inputImage = resized
//
//let bgDiscBlur = CIFilter.discBlur()
//bgDiscBlur.radius = 8
//bgDiscBlur.inputImage = bgVibrance.outputImage
//
//let bgVignette = CIFilter.vignette()
//bgVignette.intensity = 0.7
//bgVignette.radius = 20
//bgVignette.inputImage = bgDiscBlur.outputImage?.cropped(to: resized.extent)
//
//let background = bgVignette.outputImage

// Video 5: Here comes the sun...






let background = resized.applyingFilter("CIVibrance", parameters: ["inputAmount": -0.8])
  .applyingFilter("CIDiscBlur", parameters: ["inputRadius": 8])
  .cropped(to: resized.extent)
  .applyingFilter("CIVignette", parameters: ["inputIntensity": 0.7, "inputRadius": 20])

let fgVibrance = CIFilter.vibrance()
fgVibrance.amount = 0.7
fgVibrance.inputImage = resized
let foreground = fgVibrance.outputImage

let compositeFilter = CIFilter.blendWithMask()
compositeFilter.inputImage = foreground
compositeFilter.backgroundImage = background
compositeFilter.maskImage = matteResized

let composite = compositeFilter.outputImage







//: [Next](@next)
