import PlaygroundSupport
import UIKit
import CoreImage
import CoreImage.CIFilterBuiltins

let url = Bundle.main.url(forResource: "IMG_5276", withExtension: "HEIC")!
var options = [CIImageOption.applyOrientationProperty: true]
let image = CIImage(contentsOf: url, options: options)!

//options[.auxiliaryPortraitEffectsMatte] = true
//let matte = CIImage(contentsOf: url, options: options)!

let resized = image.transformed(by: CGAffineTransform(scaleX: 0.125, y: 0.125))
let matteResized = matte.transformed(by: CGAffineTransform(scaleX: 0.25, y: 0.25))
matteResized

let invertFilter = CIFilter.colorInvert()
invertFilter.inputImage = matteResized
invertFilter.outputImage

let filter = CIFilter.maskedVariableBlur()
filter.inputImage = resized
filter.mask = invertFilter.outputImage
filter.radius = 20
filter.outputImage


