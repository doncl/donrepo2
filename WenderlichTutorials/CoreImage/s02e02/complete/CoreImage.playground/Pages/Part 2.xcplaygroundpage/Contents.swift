//: [Previous](@previous)
import Foundation
import CoreImage
import CoreImage.CIFilterBuiltins

// Import the image, as before
let url = Bundle.main.url(forResource: "IMG_5276", withExtension: "HEIC")!
var options = [CIImageOption.applyOrientationProperty: true]
let image = CIImage(contentsOf: url, options: options)!

// Video 2: Masked Filtering
options[.auxiliaryPortraitEffectsMatte] = true
let matte = CIImage(contentsOf: url, options: options)!

let resized = image.transformed(by: CGAffineTransform(scaleX: 0.125, y: 0.125))
let matteResized = matte.transformed(by: CGAffineTransform(scaleX: 0.25, y: 0.25))

let invertFilter = CIFilter.colorInvert()
invertFilter.inputImage = matteResized

let filter = CIFilter.maskedVariableBlur()
filter.inputImage = resized
filter.mask = invertFilter.outputImage
filter.radius = 20
filter.outputImage









//: [Next](@next)
