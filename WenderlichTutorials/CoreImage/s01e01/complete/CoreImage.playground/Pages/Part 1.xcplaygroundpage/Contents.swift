import PlaygroundSupport
import UIKit

// Video 1: Importing a CIImage

let url = Bundle.main.url(forResource: "IMG_5276", withExtension: "HEIC")!
let image = CIImage(contentsOf: url, options: [CIImageOption.applyOrientationProperty: true])?.transformed(by: CGAffineTransform(scaleX: 0.125, y: 0.125))

let options = [
  CIImageOption.applyOrientationProperty: true,
  CIImageOption.auxiliaryDepth: true
]
let depthMap = CIImage(contentsOf: url, options: options)
