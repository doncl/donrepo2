import PlaygroundSupport
import UIKit
import CoreImage
import CoreImage.CIFilterBuiltins

let url = Bundle.main.url(forResource: "IMG_5276", withExtension: "HEIC")!
var options = [CIImageOption.applyOrientationProperty: true]
let image = CIImage(contentsOf: url, options: options)!

let resized = image.transformed(by: CGAffineTransform(scaleX: 0.125, y: 0.125))

let bgVibrance = CIFilter.vibrance()
bgVibrance.amount = 0.8
bgVibrance.inputImage = resized
bgVibrance.outputImage

let bgDiscBlur = CIFilter.discBlur()
bgDiscBlur.radius = 8
bgDiscBlur.inputImage = bgVibrance.outputImage

bgDiscBlur.outputImage

let bgVignette = CIFilter.vignette()
bgVignette.intensity = 0.7
bgVignette.radius = 20
bgVignette.inputImage = bgDiscBlur.outputImage?.cropped(to: resized.extent)

let background = bgVignette.outputImage

let background2 = resized.applyingFilter("CIVibrance", parameters: ["inputAmount": -0.8])
  .applyingFilter("CIDiscBlur", parameters: ["inputRadius": 8])
  .cropped(to: resized.extent)
  .applyingFilter("CIVignette", parameters: ["inputIntensity": 0.7, "inputRadius": 20])


