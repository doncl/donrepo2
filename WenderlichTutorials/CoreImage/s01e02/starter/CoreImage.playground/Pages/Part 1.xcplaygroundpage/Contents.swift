import PlaygroundSupport
import UIKit
import CoreImage
import CoreImage.CIFilterBuiltins

// Video 1: Importing a CIImage

let url = Bundle.main.url(forResource: "IMG_5276", withExtension: "HEIC")!
let image = CIImage(contentsOf: url, options: [CIImageOption.applyOrientationProperty: true])!.transformed(by: CGAffineTransform(scaleX: 0.125, y: 0.125))

let options = [
  CIImageOption.applyOrientationProperty: true,
  CIImageOption.auxiliaryDepth: true
]
let depthMap = CIImage(contentsOf: url, options: options)

//let filter = CIFilter(name: "CIVibrance", parameters: ["inputImage": image, "inputAmount": 0.9])
//filter?.outputImage

let filter = CIFilter.vibrance()
filter.amount = 0.9
filter.inputImage = image
filter.outputImage

