//
//  PassthroughFilter.swift
//  MetalFilters
//
//  Created by Sam Davies on 08/08/2019.
//  Copyright © 2019 Razeware LLC. All rights reserved.
//

import CoreImage

class PassthroughFilter: CIFilter {
  private lazy var kernel: CIKernel = {
    guard
      let url = Bundle.main.url(forResource: "default", withExtension: "metallib"),
      let data = try? Data(contentsOf: url) else {
        fatalError("Unable to load metallib")
    }
    
    guard let kernel = try? CIColorKernel(functionName: "passthroughFilterKernel", fromMetalLibraryData: data) else {
      fatalError("Unable to create the CIColorKernel for passthoughFilterKernel")
    }
    
    return kernel
  }()
  
  var inputImage: CIImage?
  
  override var outputImage: CIImage? {
    guard let inputImage = inputImage else { return .none }
    
    return kernel.apply(extent: inputImage.extent,
                        roiCallback: { (index, rect) -> CGRect in
                          return rect
    }, arguments: [inputImage])
  }
  
}
