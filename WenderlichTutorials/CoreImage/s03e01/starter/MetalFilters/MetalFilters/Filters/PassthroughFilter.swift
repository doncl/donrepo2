//
//  PassthroughFilter.swift
//  MetalFilters
//
//  Created by Don Clore on 3/6/22.
//  Copyright © 2022 Razeware LLC. All rights reserved.
//

import CoreImage

class PassthroughFilter: CIFilter {
  private lazy var kernel: CIKernel = {
    guard let url = Bundle.main.url(forResource: "default", withExtension: "metallib"), let data = try? Data(contentsOf: url) else {
      fatalError("Unable to laod metallib")
    }
    
    guard let kernel = try? CIColorKernel(functionName: "passthroughFilterKernel", fromMetalLibraryData: data) else {
      fatalError()
    }
    return kernel
  }()
  
  var inputImage: CIImage?
  
  override var outputImage: CIImage? {
    guard let inputImage = inputImage else {
      return nil
    }

    return kernel.apply(extent: inputImage.extent, roiCallback: { (index, rect) -> CGRect in
      return rect
    }, arguments: [inputImage])
  }
}
