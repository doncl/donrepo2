//
//  FilterKernels.metal
//  MetalFilters
//
//  Created by Don Clore on 3/6/22.
//  Copyright © 2022 Razeware LLC. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include <CoreImage/CoreImage.h>

extern "C" {
  namespace coreimage {
    float4 passthroughFilterKernel(sample_t s) {
      return s;
    }
  }
}
