//
//  FilterKernels.metal
//  MetalFilters
//
//  Created by Sam Davies on 08/08/2019.
//  Copyright © 2019 Razeware LLC. All rights reserved.
//

#include <metal_stdlib>
using namespace metal;

#include <CoreImage/CoreImage.h>

extern "C" {
  namespace coreimage {
    // KERNEL
    float4 passthroughFilterKernel(sample_t s) {
      return s;
    }
  }
}


