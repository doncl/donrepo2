//
//  MetalRenderView.swift
//  RealtimeFiltering
//
//  Created by Don Clore on 3/6/22.
//  Copyright © 2022 Razeware LLC. All rights reserved.
//

import MetalKit
import CoreImage

class MetalRenderView: MTKView {
  private lazy var commandQueue: MTLCommandQueue? = { [unowned self] in
    return self.device!.makeCommandQueue()
    
  }()
  
  private lazy var ciContext: CIContext = { [unowned self] in
    return CIContext(mtlDevice: self.device!)
  }()
  
  var image: CIImage? {
    didSet {
      renderImage()
    }
  }
  
  override init(frame frameRect: CGRect, device: MTLDevice?) {
    super.init(frame: frameRect, device: device)
    
    if super.device == nil {
      fatalError()
    }
    
    framebufferOnly = false
  }
  
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func renderImage() {
    guard let image = image else {
      return
    }
    
    let commandBuffer = commandQueue?.makeCommandBuffer()
    
    let destination = CIRenderDestination(width: Int(drawableSize.width),
                                          height: Int(drawableSize.height),
                                          pixelFormat: MTLPixelFormat.rgba8Unorm,
                                          commandBuffer: commandBuffer) { () -> MTLTexture in

      return self.currentDrawable!.texture
    }
    
    let transformedImage: CIImage = image.transformToOrigin(withSize: drawableSize)
    
    try! ciContext.startTask(toRender: transformedImage, to: destination)
    commandBuffer?.present(currentDrawable!)
    commandBuffer?.commit()
    draw()
  }
}
