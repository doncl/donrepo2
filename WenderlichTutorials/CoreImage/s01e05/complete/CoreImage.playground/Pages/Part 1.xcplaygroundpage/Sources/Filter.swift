import Foundation
import CoreImage
import CoreImage.CIFilterBuiltins

public func gloom(_ image: CIImage) -> CIImage {
  let filter = CIFilter.gloom()
  filter.radius = 10
  filter.intensity = 0.7
  filter.inputImage = image
  return filter.outputImage!
}

public func vignette(_ image: CIImage) -> CIImage {
  let filter = CIFilter.vignette()
  filter.intensity = 0.9
  filter.radius = 2
  filter.inputImage = image
  return filter.outputImage!
}

public func vibrance(_ image: CIImage) -> CIImage {
  let filter = CIFilter.vibrance()
  filter.amount = 0.9
  filter.inputImage = image
  return filter.outputImage!
}

public func filterChain(_ image: CIImage) -> CIImage {
  return vignette(gloom(vibrance(image)))
}

public func timeCode(_ closure: () -> ()) -> TimeInterval {
  let startDate = Date()
  closure()
  return Date().timeIntervalSince(startDate)
}
