import PlaygroundSupport
import UIKit
import CoreImage
import CoreImage.CIFilterBuiltins

// Video 1: Importing a CIImage

let url = Bundle.main.url(forResource: "IMG_5276", withExtension: "HEIC")!
let image = CIImage(contentsOf: url, options: [CIImageOption.applyOrientationProperty: true])!.transformed(by: CGAffineTransform(scaleX: 0.125, y: 0.125))

let options = [
  CIImageOption.applyOrientationProperty: true,
  CIImageOption.auxiliaryDepth: true
]
let depthMap = CIImage(contentsOf: url, options: options)

// Video 2: Filtering an image
//let filter = CIFilter(name: "CIVibrance", parameters: ["inputImage": image, "inputAmount": 0.9])
//filter?.outputImage

//let filter = CIFilter.vibrance()
//filter.amount = 0.9
//filter.inputImage = image
//filter.outputImage

// Video 3: Filter Chains

//let filter2 = CIFilter.gloom()
//filter2.radius = 10
//filter2.intensity = 0.7
//filter2.inputImage = filter.outputImage
//
//let filter3 = CIFilter.vignette()
//filter3.intensity = 0.9
//filter3.radius = 2
//filter3.inputImage = filter2.outputImage
//filter3.outputImage
//
//let finishedImage = filter3.outputImage?.cropped(to: image.extent)
//
//timeCode {
//  let image1 = vibrance(image)
//  let image2 = gloom(image1)
//  vignette(image2)
//}
//
//timeCode {
//  filterChain(image)
//}
//

let finishedImage = filterChain(image).cropped(to: image.extent)

//let uiImage = UIImage(ciImage: finishedImage)
//let imageView = UIImageView(image: uiImage)
//
//let imageView2 = UIImageView(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
////imageView2.image = uiImage
//imageView2.contentMode = .scaleAspectFit
//
//let context = CIContext()
//let cgImage = context.createCGImage(finishedImage, from: finishedImage.extent)
//imageView2.image = UIImage(cgImage: cgImage!)
//
//PlaygroundPage.current.liveView = imageView2

// Video 5: Saving images

finishedImage
let context = CIContext()
let jpegUrl = URL(fileURLWithPath: "/Users/sam/Documents/Shared Playground Data/filterOutput.jpeg")
try! context.writeJPEGRepresentation(of: finishedImage, to: jpegUrl, colorSpace: CGColorSpace(name: CGColorSpace.sRGB)!)

let heifUrl = URL(fileURLWithPath: "/Users/sam/Documents/Shared Playground Data/filterOutput.heic")
try! context.writeHEIFRepresentation(of: finishedImage, to: heifUrl, format: .RGBA8, colorSpace: CGColorSpace(name: CGColorSpace.sRGB)!, options: [CIImageRepresentationOption.depthImage: depthMap!])









