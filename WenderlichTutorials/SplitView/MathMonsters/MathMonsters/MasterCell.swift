//
//  MasterCell.swift
//  MathMonsters
//
//  Created by Don Clore on 9/23/20.
//

import UIKit

class MasterCell: UITableViewCell {
  static let id = "MasterCellID"
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
