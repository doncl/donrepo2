//
//  DetailVC.swift
//  MathMonsters
//
//  Created by Don Clore on 9/23/20.
//

import UIKit

class DetailVC: UIViewController {
  let container: UIView = UIView()
  let iconImageView: UIImageView = UIImageView()
  let monsterNameLabel: UILabel = UILabel()
  let descriptionLabel: UILabel = UILabel()
  let preferredKillLabel: UILabel = UILabel()
  let weaponImageView: UIImageView = UIImageView()
  
  var monster: Monster? {
    didSet {
      refreshUI()
    }
  }
       
  lazy var allChildViews: [UIView] = {
    return [iconImageView, monsterNameLabel, descriptionLabel, preferredKillLabel, weaponImageView]
  }()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white
    
    container.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(container)
  
    NSLayoutConstraint.activate([
      container.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      container.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
      container.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])

    allChildViews.forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      container.addSubview($0)
    }

    NSLayoutConstraint.activate([
      iconImageView.topAnchor.constraint(equalTo: container.topAnchor, constant: 8),
      iconImageView.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 20),
      iconImageView.widthAnchor.constraint(equalToConstant: 95),
      iconImageView.heightAnchor.constraint(equalToConstant: 95),
    ])
    
    NSLayoutConstraint.activate([
      monsterNameLabel.topAnchor.constraint(equalTo: iconImageView.topAnchor),
      monsterNameLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 8),
      monsterNameLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -8),
    ])
    
    monsterNameLabel.font = UIFont.boldSystemFont(ofSize: 30)
    monsterNameLabel.text = "Monster Name"
    
    NSLayoutConstraint.activate([
      descriptionLabel.leadingAnchor.constraint(equalTo: iconImageView.trailingAnchor, constant: 8),
      descriptionLabel.topAnchor.constraint(equalTo: monsterNameLabel.bottomAnchor),
      descriptionLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -8),
      
      preferredKillLabel.bottomAnchor.constraint(equalTo: iconImageView.bottomAnchor),
      preferredKillLabel.leadingAnchor.constraint(equalTo: descriptionLabel.leadingAnchor),
      preferredKillLabel.trailingAnchor.constraint(equalTo: descriptionLabel.trailingAnchor),
    ])
    
    [descriptionLabel, preferredKillLabel].forEach {
      $0.font = UIFont.systemFont(ofSize: 24)
    }
    
    descriptionLabel.text = "Description"
    preferredKillLabel.text = "Preferred way to Kill"
    
    NSLayoutConstraint.activate([
      weaponImageView.leadingAnchor.constraint(equalTo: preferredKillLabel.leadingAnchor),
      weaponImageView.topAnchor.constraint(equalTo: preferredKillLabel.bottomAnchor, constant: 8),
      weaponImageView.widthAnchor.constraint(equalToConstant: 70),
      weaponImageView.heightAnchor.constraint(equalToConstant: 70),
    ])
  }
  
  private func refreshUI() {
    loadViewIfNeeded()
    monsterNameLabel.text = monster?.name
    descriptionLabel.text = monster?.description
    iconImageView.image = monster?.icon
    weaponImageView.image = monster?.weapon.image
  }
}

extension DetailVC: MonsterSelectionDelegate {
  func monsterSelected(_ newMonster: Monster) {
    monster = newMonster
  }
}
