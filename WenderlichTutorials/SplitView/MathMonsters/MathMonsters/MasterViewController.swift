//
//  ViewController.swift
//  MathMonsters
//
//  Created by Don Clore on 9/23/20.
//

import UIKit

protocol MonsterSelectionDelegate: class {
  func monsterSelected(_ newMonster: Monster)
}

class MasterViewController: UIViewController {
  let table = UITableView(frame: CGRect.zero, style: .plain)
  weak var delegate: MonsterSelectionDelegate?
  
  let monsters = [
      Monster(name: "Cat-Bot", description: "MEE-OW",
              iconName: "meetcatbot", weapon: .sword),
      Monster(name: "Dog-Bot", description: "BOW-WOW",
              iconName: "meetdogbot", weapon: .blowgun),
      Monster(name: "Explode-Bot", description: "BOOM!",
              iconName: "meetexplodebot", weapon: .smoke),
      Monster(name: "Fire-Bot", description: "Will Make You Steamed",
              iconName: "meetfirebot", weapon: .ninjaStar),
      Monster(name: "Ice-Bot", description: "Has A Chilling Effect",
              iconName: "meeticebot", weapon: .fire),
      Monster(name: "Mini-Tomato-Bot", description: "Extremely Handsome",
              iconName: "meetminitomatobot", weapon: .ninjaStar)
    ]

  
  override func viewDidLoad() {
    title = "Monster List"
    super.viewDidLoad()
    // Do any additional setup after loading the view.
   
    table.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(table)
    NSLayoutConstraint.activate([
      table.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      table.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      table.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
    
    table.register(MasterCell.self, forCellReuseIdentifier: MasterCell.id)
    table.dataSource = self
    table.delegate = self 
    table.reloadData()
  }
}

extension MasterViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return monsters.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: MasterCell.id, for: indexPath) as? MasterCell else {
      fatalError("Don't have cell registration wired up correctly.")
    }
    let monster = monsters[indexPath.item]
    cell.textLabel?.text = monster.name 
    return cell 
  }
}

extension MasterViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let delegate = delegate else {
      return
    }
    let selectedMonster = monsters[indexPath.item]
    
    delegate.monsterSelected(selectedMonster)
    
    if let detailVC = delegate as? DetailVC, let split = splitViewController {
      split.showDetailViewController(detailVC, sender: nil)
    }
  }
}

extension MasterViewController: UISplitViewControllerDelegate {
  func splitViewController(_ splitViewController: UISplitViewController,
                           collapseSecondary secondaryViewController: UIViewController,
                           onto primaryViewController: UIViewController) -> Bool {

    guard let _ = secondaryViewController as? DetailVC else  {
      // Fallback to the default
      return false
    }

//    if let primaryAsNavController = primaryViewController as? UINavigationController {
//      primaryAsNavController.setNavigationBarHidden(false, animated: false)
//    }

    return true
  }
}
