//
//  SceneDelegate.swift
//  RWDevCon
//
//  Created by Don Clore on 9/23/20.
//  Copyright © 2020 Razeware LLC. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate, SplitViewHandler {
  var window: UIWindow?
  
  // UIWindowScene delegate
  
  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    guard let _ = scene as? UIWindowScene else { return }
    
    if let split = window?.rootViewController as? UISplitViewController {
      split.delegate = self
      setupSideBySideParameters(split: split)
    }
    
    if #available(iOS 14.0, *) {
      if let oldSVC = self.window?.rootViewController as? UISplitViewController {
        /* If you instantiate an UISplitViewController in this way the style will be undefined (classic). */
        let newSVC = UISplitViewController()
        newSVC.viewControllers = oldSVC.viewControllers
        /* Here you can align other properties if needed. */
        
        newSVC.delegate = self
        setupSideBySideParameters(split: newSVC)

        self.window?.rootViewController = newSVC
      }
    }
  }
}

@available(iOS 13.0, *)
extension SceneDelegate: UISplitViewControllerDelegate {
  func splitViewController(_ splitViewController: UISplitViewController,
                           collapseSecondary secondaryViewController: UIViewController,
                           onto primaryViewController: UIViewController) -> Bool {

    return handlecollapsing(secondaryViewController: secondaryViewController, onto: primaryViewController)
  }
}
