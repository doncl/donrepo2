//
//  ScheduleTableViewCell.swift
//  RWDevCon
//
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

import UIKit
import AddressBook
import MapKit
import Contacts

class RoomViewController: UIViewController {
  var room: Room!

  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var mapButton: UIButton!
  @IBOutlet weak var descriptionLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    imageView.image = UIImage(named: room.image)
    descriptionLabel.text = room.roomDescription

    if room.mapLongitude != 0 && room.mapLatitude != 0 {
      mapButton.isHidden = false
    } else {
      mapButton.isHidden = true
    }
  }

  @IBAction func mapButtonTapped(sender: AnyObject) {
    //let placemark = MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: room.mapLatitude, longitude: room.mapLongitude), addressDictionary: [kABPersonAddressStreetKey: room.mapAddress])
    let coord: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: room.mapLatitude, longitude: room.mapLongitude)
    let addressDictionary: [String: Any] = [
      CNPostalAddressStreetKey: room.mapAddress,
    ]
        
    let placemark = MKPlacemark(coordinate: coord, addressDictionary: addressDictionary)
    let mapItem = MKMapItem(placemark: placemark)
    mapItem.name = room.name

    MKMapItem.openMaps(with: [mapItem], launchOptions: [:])
  }
}
