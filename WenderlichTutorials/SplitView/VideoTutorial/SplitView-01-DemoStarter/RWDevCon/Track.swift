//
//  ScheduleTableViewCell.swift
//  RWDevCon
//
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

import Foundation
import CoreData

@objc(Track)
class Track: NSManagedObject {
  @NSManaged var trackId: Int32
  @NSManaged var name: String
  @NSManaged var sessions: NSSet

  class func trackByTrackId(trackId: Int, context: NSManagedObjectContext) -> Track? {
    let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Track")
    fetch.predicate = NSPredicate(format: "trackId = %@", argumentArray: [trackId])

    if let results = try? context.fetch(fetch) {
      if let result = results.first as? Track {
        return result
      }
    }

    return nil
  }

  class func trackByTrackIdOrNew(trackId: Int, context: NSManagedObjectContext) -> Track {
    if let track = trackByTrackId(trackId: trackId, context: context) {
      return track
    }
    let entity = NSEntityDescription.entity(forEntityName: "Track", in: context)!
    let track = Track(entity: entity, insertInto: context)
    return track
  }
}
