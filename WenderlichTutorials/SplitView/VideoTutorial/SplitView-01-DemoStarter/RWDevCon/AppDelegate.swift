//
//  AppDelegate.swift
//  RWDevCon
//
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

import Foundation
import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, SplitViewHandler {
  var window: UIWindow?
  lazy var coreDataStack = CoreDataStack()

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
    // global style
    application.statusBarStyle = UIStatusBarStyle.lightContent
    UIBarButtonItem.appearance().setTitleTextAttributes(
      [NSAttributedString.Key.font: UIFont(name: "AvenirNext-Regular", size: 17)!,
       NSAttributedString.Key.foregroundColor: UIColor.white], for: .normal)

    if #available(iOS 13.0, *) {
      
    } else {
      if let split = window?.rootViewController as? UISplitViewController {
        split.delegate = self
        setupSideBySideParameters(split: split)
      }
    }
    
    return true
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    coreDataStack.saveContext()
  }
}

extension AppDelegate: UISplitViewControllerDelegate {
  func splitViewController(_ splitViewController: UISplitViewController,
                           collapseSecondary secondaryViewController: UIViewController,
                           onto primaryViewController: UIViewController) -> Bool {
    
    return handlecollapsing(secondaryViewController: secondaryViewController, onto: primaryViewController)
  }
}

@available(iOS 13.0, *)
extension AppDelegate {
  func application(_ application: UIApplication,
                   configurationForConnecting connectingSceneSession: UISceneSession,
                   options: UIScene.ConnectionOptions) -> UISceneConfiguration {
      return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
  }
}
