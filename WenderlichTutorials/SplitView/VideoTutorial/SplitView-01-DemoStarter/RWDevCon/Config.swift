//
//  ScheduleTableViewCell.swift
//  RWDevCon
//
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

import Foundation
let SessionDataUpdatedNotification = NSNotification.Name(rawValue: "com.razeware.rwdevcon.notification.sessionDataUpdated")

class Config {
  class func applicationDocumentsDirectory() -> URL {
    let fileManager = FileManager.default

    let urls = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
    return urls[0]
  }
  
  class func userDefaults() -> UserDefaults {
    return UserDefaults.standard
  }

  class func favoriteSessions() -> [String: String] {
    if let favs = userDefaults().dictionary(forKey: "favoriteSessions") as? [String: String] {
      return favs
    }
    return [:]
  }

  class func registerFavorite(session: Session) {
    var favs = favoriteSessions()
    favs[session.startDateTimeString] = session.identifier

    userDefaults().setValue((favs as NSDictionary), forKey: "favoriteSessions")
    userDefaults().synchronize()
  }

  class func unregisterFavorite(session: Session) {
    var favs = favoriteSessions()
    favs[session.startDateTimeString] = nil

    userDefaults().setValue((favs as NSDictionary), forKey: "favoriteSessions")
    userDefaults().synchronize()
  }

}
