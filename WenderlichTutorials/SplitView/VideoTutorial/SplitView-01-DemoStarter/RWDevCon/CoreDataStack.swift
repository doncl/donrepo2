/*
* Copyright (c) 2014 Razeware LLC
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
*/

import CoreData

// A date before the bundled plist date
private let beginningOfTimeDate = NSDate(timeIntervalSince1970: 1417348800)

public class CoreDataStack {
  
  public let context: NSManagedObjectContext
  let psc: NSPersistentStoreCoordinator
  let model: NSManagedObjectModel
  let store: NSPersistentStore?
  
  public init() {
    let modelName = "RWDevCon"
    
    let bundle = Bundle.main
    let modelURL =
      bundle.url(forResource: modelName, withExtension:"momd")!
    model = NSManagedObjectModel(contentsOf: modelURL)!
    
    psc = NSPersistentStoreCoordinator(managedObjectModel: model)
    
    context = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
    context.persistentStoreCoordinator = psc
    
    let documentsURL = Config.applicationDocumentsDirectory()
    let storeURL = documentsURL.appendingPathComponent("\(modelName).sqlite")

    let options = [NSInferMappingModelAutomaticallyOption:true,
        NSMigratePersistentStoresAutomaticallyOption:true]

    var workingStore = try? psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: options)

    if workingStore == nil {
      do {
        try FileManager.default.removeItem(at: storeURL)
      } catch let error {
        print("Error removing persistent store: \(error.localizedDescription)")
        abort()
      }

      print("Model has changed, removing.")
      

      do {
        workingStore = try psc.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: options)
      } catch let error {
        print("Error adding persistent store: \(error.localizedDescription)")
        abort()
      }
    }

    store = workingStore

    // If 0 sessions, start with the bundled plist data
    if Session.sessionCount(context: context) == 0 {
      if let conferencePlist = Bundle.main.url(forResource: "RWDevCon2015", withExtension: "plist") {
        loadDataFromPlist(url: conferencePlist)
      }
    }
  }

  func loadDataFromPlist(url: URL) {
    if let data = NSDictionary(contentsOf: url) {
      typealias PlistDict = [String: NSDictionary]
      typealias PlistArray = [NSDictionary]

      let metadata: NSDictionary! = data["metadata"] as? NSDictionary
      let sessions: PlistDict! = data["sessions"] as? PlistDict
      let people: PlistDict! = data["people"] as? PlistDict
      let rooms: PlistArray! = data["rooms"] as? PlistArray
      let tracks: [String]! = data["tracks"] as? [String]

      if metadata == nil || sessions == nil || people == nil || rooms == nil || tracks == nil {
        return
      }

      let lastUpdated = metadata["lastUpdated"] as? NSDate ?? beginningOfTimeDate
      Config.userDefaults().set(lastUpdated, forKey: "lastUpdated")

      var allRooms = [Room]()
      var allTracks = [Track]()
      var allPeople = [String: Person]()

      for (identifier, dict) in rooms.enumerated() {
        var room = Room.roomByRoomIdOrNew(roomId: identifier, context: context)

        room.roomId = Int32(identifier)
        room.name = dict["name"] as? String ?? ""
        room.image = dict["image"] as? String ?? ""
        room.roomDescription = dict["roomDescription"] as? String ?? ""
        room.mapAddress = dict["mapAddress"] as? String ?? ""
        room.mapLatitude = dict["mapLatitude"] as? Double ?? 0
        room.mapLongitude = dict["mapLongitude"] as? Double ?? 0

        allRooms.append(room)
      }

      for (identifier, name) in tracks.enumerated() {
        let track = Track.trackByTrackIdOrNew(trackId: identifier, context: context)

        track.trackId = Int32(identifier)
        track.name = name

        allTracks.append(track)
      }

      for (identifier, dict) in people {
        let person = Person.personByIdentifierOrNew(identifier: identifier, context: context)

        person.identifier = identifier
        person.first = dict["first"] as? String ?? ""
        person.last = dict["last"] as? String ?? ""
        person.active = dict["active"] as? Bool ?? false
        person.twitter = dict["twitter"] as? String ?? ""
        person.bio = dict["bio"] as? String ?? ""

        allPeople[identifier] = person
      }

      for (identifier, dict) in sessions {
        let session = Session.sessionByIdentifierOrNew(identifier: identifier, context: context)

        session.identifier = identifier
        session.active = dict["active"] as? Bool ?? false
        session.date = dict["date"] as? NSDate ?? beginningOfTimeDate
        session.duration = Int32(dict["duration"] as? Int ?? 0)
        session.column = Int32(dict["column"] as? Int ?? 0)
        session.sessionNumber = dict["sessionNumber"] as? String ?? ""
        session.sessionDescription = dict["sessionDescription"] as? String ?? ""
        session.title = dict["title"] as? String ?? ""

        session.track = allTracks[dict["trackId"] as! Int]
        session.room = allRooms[dict["roomId"] as! Int]

        var presenters = [Person]()
        if let rawPresenters = dict["presenters"] as? [String] {
          for presenter in rawPresenters {
            if let person = allPeople[presenter] {
              presenters.append(person)
            }
          }
        }
        session.presenters = NSOrderedSet(array: presenters)
      }

      saveContext()

      NotificationCenter.default.post(name: SessionDataUpdatedNotification, object: self)
    }
  }

  func saveContext() {
    if context.hasChanges {
      do {
        try context.save()
      } catch let error {
        print("Could not save: \(error.localizedDescription)")
        abort()
      }
    }
  }
}

