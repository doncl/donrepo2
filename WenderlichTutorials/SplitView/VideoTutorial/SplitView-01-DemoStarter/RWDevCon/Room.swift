//
//  ScheduleTableViewCell.swift
//  RWDevCon
//
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

import Foundation
import CoreData

@objc(Room)
class Room: NSManagedObject {
  @NSManaged var roomId: Int32
  @NSManaged var name: String
  @NSManaged var roomDescription: String
  @NSManaged var sessions: NSSet
  @NSManaged var image: String
  @NSManaged var mapAddress: String
  @NSManaged var mapLatitude: Double
  @NSManaged var mapLongitude: Double

  class func roomByRoomId(roomId: Int, context: NSManagedObjectContext) -> Room? {
    let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "Room")
    fetch.predicate = NSPredicate(format: "roomId = %@", argumentArray: [roomId])

    if let result = try? context.fetch(fetch) {
      if let room = result.first as? Room {
        return room
      }
    }

    return nil
  }

  class func roomByRoomIdOrNew(roomId: Int, context: NSManagedObjectContext) -> Room {
    if let room = roomByRoomId(roomId: roomId, context: context) {
      return room
    }
 
    let entity = NSEntityDescription.entity(forEntityName: "Room", in: context)!
    let room = Room(entity: entity, insertInto: context)
    return room
  }
}
