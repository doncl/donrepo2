//
//  ScheduleTableViewCell.swift
//  RWDevCon
//
//  Copyright (c) 2015 Razeware LLC. All rights reserved.
//

import Foundation
import UIKit

let MyScheduleSomethingChangedNotification = NSNotification.Name(rawValue: "com.razeware.rwdevcon.notifications.myScheduleChanged")

class SessionViewController: UITableViewController {
  var coreDataStack: CoreDataStack!
  var session: Session!

  struct Sections {
    static let info = 0
    static let description = 1
    static let presenters = 2
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    if let split = splitViewController {
      navigationItem.leftBarButtonItem = split.displayModeButtonItem
      navigationItem.leftItemsSupplementBackButton = true 
    }
    coreDataStack = (UIApplication.shared.delegate as! AppDelegate).coreDataStack

    title = session?.title

    tableView.rowHeight = UITableView.automaticDimension
    tableView.estimatedRowHeight = 76

    navigationController?.navigationBar.barStyle = UIBarStyle.default
    navigationController?.navigationBar.setBackgroundImage(UIImage(named: "pattern-64tall"), for: UIBarMetrics.default)
    navigationController?.navigationBar.tintColor = UIColor.white
    navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "AvenirNext-Regular", size: 17)!, NSAttributedString.Key.foregroundColor: UIColor.white]
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)

    navigationController?.setNavigationBarHidden(false, animated: animated)
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

  // MARK: - Table view data source

  override func numberOfSections(in tableView: UITableView) -> Int {
    if session == nil {
      return 0
    } else {
      return 3
    }
  }

  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if section == Sections.info {
      return 3
    } else if section == Sections.description {
      return 1
    } else if section == Sections.presenters {
      return session.presenters.count
    }

    return 0
  }

  override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    if section == Sections.info {
      if session.sessionNumber == "" {
        return "Summary"
      } else {
        return "Session #\(session.sessionNumber)"
      }
    } else if section == Sections.description {
      return "Description"
    } else if section == Sections.presenters {
      if session.presenters.count == 1 {
        return "Presenter"
      } else if session.presenters.count > 1 {
        return "Presenters"
      }
    }
    return nil
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    if indexPath.section == Sections.info && indexPath.row == 3 {
      let cell = tableView.dequeueReusableCell(withIdentifier: "detailButton", for: indexPath) as! DetailTableViewCell

      cell.keyLabel.text = "My Schedule".uppercased()
      if session.isFavorite {
        cell.valueButton.setTitle("Remove from My Schedule", for: .normal)
      } else {
        cell.valueButton.setTitle("Add to My Schedule", for: .normal)
      }
      cell.valueButton.addTarget(self, action: #selector(myScheduleButton(sender:)), for: .touchUpInside)

      return cell
    } else if indexPath.section == Sections.info && indexPath.row == 2 {
      let cell = tableView.dequeueReusableCell(withIdentifier: "detailButton", for: indexPath) as! DetailTableViewCell

      cell.keyLabel.text = "Where".uppercased()
      cell.valueButton.setTitle(session.room.name, for: .normal)
      cell.valueButton.addTarget(self, action: #selector(roomDetails(sender:)), for: .touchUpInside)

      return cell
    } else if indexPath.section == Sections.info {
      let cell = tableView.dequeueReusableCell(withIdentifier: "detail", for: indexPath) as! DetailTableViewCell

      if indexPath.row == 0 {
        cell.keyLabel.text = "Track".uppercased()
        cell.valueLabel.text = session.track.name
      } else if indexPath.row == 1 {
        cell.keyLabel.text = "When".uppercased()
        cell.valueLabel.text = session.startDateTimeString
      }

      return cell
    } else if indexPath.section == Sections.description {
      let cell = tableView.dequeueReusableCell(withIdentifier: "label", for: indexPath) as! LabelTableViewCell
      cell.label.text = session.sessionDescription
      return cell
    } else if indexPath.section == Sections.presenters {
      let cell = tableView.dequeueReusableCell(withIdentifier: "presenter", for: indexPath) as! PresenterTableViewCell
      let presenter = session.presenters[indexPath.row] as! Person

      if let image = UIImage(named: presenter.identifier) {
        cell.squareImageView.image = image
      } else {
        cell.squareImageView.image = UIImage(named: "RW_logo")
      }
      cell.nameLabel.text = presenter.fullName
      cell.bioLabel.text = presenter.bio
      if presenter.twitter != "" {
        cell.twitterButton.isHidden = false
        cell.twitterButton.setTitle("@\(presenter.twitter)", for: .normal)
        cell.twitterButton.addTarget(self, action: #selector(twitterButton(sender:)), for: .touchUpInside)
      } else {
        cell.twitterButton.isHidden = true
      }

      return cell
    } else {
      assertionFailure("Unhandled session table view section")
      let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
      return cell
    }
  }

  @objc func roomDetails(sender: UIButton) {
    if let roomVC = storyboard?.instantiateViewController(withIdentifier: "RoomViewController") as? RoomViewController {
      roomVC.room = session.room
      roomVC.title = session.room.name
      navigationController?.pushViewController(roomVC, animated: true)
    }
  }

  @objc func myScheduleButton(sender: UIButton) {
    session.isFavorite = !session.isFavorite

    let indexSet: IndexSet = IndexSet(integer: Sections.info)
    tableView.reloadSections(indexSet, with: .automatic)
    if let session = session {
      NotificationCenter.default.post(name: MyScheduleSomethingChangedNotification, object: self, userInfo: ["session": session])
    }
  }

  @objc func twitterButton(sender: UIButton) {
    guard let title = sender.title(for: .normal) else {
      return
    }
    guard let url = URL(string:  "http://twitter.com/\(title)") else {
      return
    }
    UIApplication.shared.openURL(url)
  }
}
