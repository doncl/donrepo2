//
//  ViewController.swift
//  UseYourLoafSplitViewTemplate
//
//  Created by Don Clore on 9/24/20.
//

import UIKit

class RootViewController: UISplitViewController {
  let masterNav: UINavigationController = UINavigationController()
  let detailsNav: UINavigationController = UINavigationController()

  override func viewDidLoad() {
    super.viewDidLoad()
    viewControllers = [masterNav, detailsNav]
    #if targetEnvironment(macCatalyst)
    preferredDisplayMode = UISplitViewController.DisplayMode.allVisible
    #else
    preferredDisplayMode = UISplitViewController.DisplayMode.oneBesideSecondary
    #endif
    delegate = self
    
    let selector = MasterVC()
    let details = DetailsVC()
    
    masterNav.show(selector, sender: self)
    
    details.navigationItem.leftBarButtonItem = displayModeButtonItem
    details.navigationItem.leftItemsSupplementBackButton = true
    masterNav.setNavigationBarHidden(true, animated: false)
  }
}

extension RootViewController: UISplitViewControllerDelegate {
  func splitViewController(_ splitViewController: UISplitViewController,
                           collapseSecondary secondaryViewController: UIViewController,
                           onto primaryViewController: UIViewController) -> Bool {

    guard let _ = secondaryViewController as? DetailsVC else  {
      // Fallback to the default
      return true
    }

    if let primaryAsNavController = primaryViewController as? UINavigationController {
      primaryAsNavController.setNavigationBarHidden(false, animated: false)
    }

    return false
  }
}
