//
//  SelectorVC.swift
//  UseYourLoafSplitViewTemplate
//
//  Created by Don Clore on 9/24/20.
//

import UIKit

class MasterVC: UIViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .systemPink
  }  
}
