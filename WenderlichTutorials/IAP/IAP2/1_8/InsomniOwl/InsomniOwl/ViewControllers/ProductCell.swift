import UIKit
import StoreKit

class ProductCell: UITableViewCell {

  // MARK: - IBOutlets
  @IBOutlet weak var imgRandom: UIImageView!
  @IBOutlet weak var lblProductName: UILabel!
  @IBOutlet weak var lblPrice: UILabel!
  @IBOutlet weak var btnBuy: UIButton!
  @IBOutlet weak var imgCheckmark: UIImageView!
  
  var buyButtonHandler: ((_ product: SKProduct) -> ())?
  
  var product: SKProduct? {
    didSet {
      guard let product = product else {
        return
      }
      if btnBuy.allTargets.count == 0 {
        btnBuy.addTarget(self, action: #selector(ProductCell.buyButtonTapped(_:)), for: UIControl.Event.touchUpInside)
      }
      lblProductName.text = product.localizedTitle
      if product.productIdentifier == OwlProducts.randomProductID {
        imgRandom.isHidden = false
      } else {
        imgRandom.isHidden = true
      }
      
      if OwlProducts.store.isPurchased(product.productIdentifier) {
        btnBuy.isHidden = true
        imgCheckmark.isHidden = false
      } else if OwlProducts.productIDsNonRenewing.contains(product.productIdentifier) {
        btnBuy.isHidden = false
        imgCheckmark.isHidden = true
        if OwlProducts.daysRemainingOnSubscription() > 0 {
          btnBuy.setTitle("Renew", for: UIControl.State.normal)
          btnBuy.setImage(UIImage(named: "IconRenew")!, for: UIControl.State.normal)
        } else {
          btnBuy.setTitle("Buy", for: UIControl.State.normal)
          btnBuy.setImage(UIImage(named: "IconBuy")!, for: UIControl.State.normal)
        }
        ProductCell.priceFormatter.locale = product.priceLocale
        lblPrice.text = ProductCell.priceFormatter.string(from: product.price)
      } else {
        ProductCell.priceFormatter.locale = product.priceLocale
        lblPrice.text = ProductCell.priceFormatter.string(from: product.price)
        btnBuy.isHidden = false
        imgCheckmark.isHidden = true
        btnBuy.setTitle("Buy", for: UIControl.State.normal)
        btnBuy.setImage(UIImage(named: "IconBuy")!, for: UIControl.State.normal)
      }
    }
  }
  
  // MARK: - Properties
  static let priceFormatter: NumberFormatter = {
    let formatter = NumberFormatter()
    formatter.formatterBehavior = .behavior10_4
    formatter.numberStyle = .currency
    return formatter
  }()
}

extension ProductCell {
  @objc func buyButtonTapped(_ sender: UIButton) {
    guard let product = product, let buyButtonHandler = buyButtonHandler else {
      return
    }
    
    buyButtonHandler(product)
  }
}
