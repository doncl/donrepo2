import Foundation
import SwiftKeychainWrapper

let expirationDateKey = "ExpirationDate"
let remainingKey = "Remaining"
let lastRandomIndexKey = "LastRandomIndex"

class UserSettings {

  // MARK: - Properties
  static let shared = UserSettings()

  init() {
  }

  public var expirationDate: Date? {
    set {
      if let newDate = newValue {
        KeychainWrapper.standard.set(newDate.timeIntervalSince1970, forKey: expirationDateKey)
      }
    }
    get {
      if let expiration = KeychainWrapper.standard.double(forKey: expirationDateKey) {
        return Date(timeIntervalSince1970: expiration)
      }
      return nil
    }
  }

  public var randomRemaining: Int {
    set {
      KeychainWrapper.standard.set(newValue, forKey: remainingKey)
    }
    get {
      if let remaining = KeychainWrapper.standard.integer(forKey: remainingKey) {
        return remaining
      }
      return 0
    }
  }

  public var lastRandomIndex: Int {
    set {
      KeychainWrapper.standard.set(newValue, forKey: lastRandomIndexKey)
    }
    get {
      if let lastIndex = KeychainWrapper.standard.integer(forKey: lastRandomIndexKey) {
        return lastIndex
      }
      return 0
    }
  }

  public func increaseRandomExpirationDate(by months: Int) {
    let lastDate = expirationDate ?? Date()
    let newDate = Calendar.current.date(byAdding: .month, value: months, to: lastDate)
    expirationDate = newDate
  }

  public func increaseRandomRemaining(by times: Int) {
    let lastTimes = (randomRemaining < 0) ? 0 : randomRemaining
    randomRemaining = lastTimes + times
  }
}
