//
//  OwlProducts.swift
//  InsomniOwl
//
//  Created by Don Clore on 10/24/20.
//  Copyright © 2020 Razeware LLC. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper

public struct OwlProducts {
  static let productIdsNonConsumables: Set<ProductIdentifier> = [
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.CarefreeOwl",
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.GoodJobOwl",
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.CouchOwl",
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.NightOwl",
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.LonelyOwl",
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.ShyOwl",
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.CryingOwl",
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.GoodNightOwl",
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.InLoveOwl",
  ]
  
  static let randomProductID = "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.RandomOwls"
  static let productIDsConsumables: Set<ProductIdentifier> = [randomProductID]
  
  static let productIDsNonRenewing: Set<ProductIdentifier> = [
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.3MonthsOfRandom",
    "com.beerbarrelpokerstudios.video.tutorial.iap.insomniowldoncl.6monthsOfRandom",
  ]
  
  static let randomImages: [UIImage] = [
    UIImage(named: "CarefreeOwl")!,
    UIImage(named: "GoodJobOwl")!,
    UIImage(named: "CouchOwl")!,
    UIImage(named: "NightOwl")!,
    UIImage(named: "LonelyOwl")!,
    UIImage(named: "ShyOwl")!,
    UIImage(named: "CryingOwl")!,
    UIImage(named: "GoodNightOwl")!,
    UIImage(named: "InLoveOwl")!,
  ]
  
  public static func resourceName(for productIdentifier: String) -> String? {
    return productIdentifier.components(separatedBy: ".").last
  }
  
  public static func setRandomProduct(with paidUp: Bool) {
    if paidUp {
      KeychainWrapper.standard.set(true, forKey: OwlProducts.randomProductID)
      store.purchasedProducts.insert(OwlProducts.randomProductID)
    } else {
      KeychainWrapper.standard.set(false, forKey: OwlProducts.randomProductID)
      store.purchasedProducts.remove(OwlProducts.randomProductID)
    }
  }
  
  public static func daysRemainingOnSubscription() -> Int {
    if let expiryDate = UserSettings.shared.expirationDate {
      return Calendar.current.dateComponents([.day], from: Date(), to: expiryDate).day!
    }
    return 0
  }
  
  public static func getExpiryDateString() -> String {
    let remaining = daysRemainingOnSubscription()
    if remaining > 0, let expiryDate = UserSettings.shared.expirationDate {
      let df = DateFormatter()
      df.dateFormat = "dd/MM/yyyy"
      let dateString = df.string(from: expiryDate)
      return "Subscribed! \nExpires: \(dateString) (\(remaining) Days)"
    }
    return "Not Subscribed"
  }
  
  public static func paidUp() -> Bool {
    var paidUp = false
    if OwlProducts.daysRemainingOnSubscription() > 0 {
      paidUp = true
    } else if UserSettings.shared.randomRemaining > 0 {
      paidUp = true
    }
    setRandomProduct(with: paidUp)
    return paidUp
  }
  
  private static func handleMonthlySubscription(months: Int) {
    UserSettings.shared.increaseRandomExpirationDate(by: months)
    setRandomProduct(with: true)
  }
  
  static let store = IAPHelper(productIDs:
                               OwlProducts.productIDsConsumables
                                .union(OwlProducts.productIdsNonConsumables)
                                .union(OwlProducts.productIDsNonRenewing))
  
  static func handlePurchase(purchaseIdentifier: String) {
    if productIDsConsumables.contains(purchaseIdentifier) {
      UserSettings.shared.increaseRandomRemaining(by: 5)
      setRandomProduct(with: true)
    } else if productIDsNonRenewing.contains(purchaseIdentifier), purchaseIdentifier.contains("3Month") {
      handleMonthlySubscription(months: 3)
    } else if productIDsNonRenewing.contains(purchaseIdentifier), purchaseIdentifier.contains("6month") {
      handleMonthlySubscription(months: 6)
    } else if productIdsNonConsumables.contains(purchaseIdentifier) {
      store.purchasedProducts.insert(purchaseIdentifier)
      KeychainWrapper.standard.set(true, forKey: purchaseIdentifier)
    }
  }
  
}
