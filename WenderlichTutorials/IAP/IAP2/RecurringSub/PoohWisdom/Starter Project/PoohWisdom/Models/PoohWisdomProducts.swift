import Foundation

public struct PoohWisdomProducts {
  public static let monthlySub = "com.beerbarrelpokerstudios.poohWisdomDonCl.monthlySub"
  public static let yearlySub =  "com.beerbarrelpokerstudios.poohWisdomDonCl.yearlySub"
  public static let store = IAPManager(productIDs: PoohWisdomProducts.productIDs)
  private static let productIDs: Set<ProductID> = [PoohWisdomProducts.monthlySub, PoohWisdomProducts.yearlySub]
}

public func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
  return productIdentifier.components(separatedBy: ".").last
}
