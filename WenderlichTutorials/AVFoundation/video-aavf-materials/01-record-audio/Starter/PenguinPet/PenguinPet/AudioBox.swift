//
//  AudioBox.swift
//  PenguinPet
//
//  Created by Don Clore on 3/16/22.
//

import Foundation
import AVFoundation

class AudioBox: NSObject, ObservableObject {
  @Published var status: AudioStatus = .stopped
  
  var audioRecorder: AVAudioRecorder?
  var audioPlayer: AVAudioPlayer?
  
  
  
  
  var urlForMemo: URL {
    let fileManager = FileManager.default
    let tempDir = fileManager.temporaryDirectory
    let filePath = "TempMemo.caf"
    return tempDir.appendingPathComponent(filePath)
  }
  
  func setupRecorder() {
    let recordSettings: [String: Any] = [
      AVFormatIDKey: Int(kAudioFormatLinearPCM),
      AVSampleRateKey: 44100.0,
      AVNumberOfChannelsKey: 1,
      AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue,
    ]
    
    do {
      audioRecorder = try AVAudioRecorder(url: urlForMemo, settings: recordSettings)
      audioRecorder?.delegate = self
    } catch {
      print("Error creating audiorecorder")
    }
  }
  
  func record() {
    audioRecorder?.record()
    status = .recording
  }
  
  func stopRecording() {
    audioRecorder?.stop()
    status = .stopped
  }
}

extension AudioBox : AVAudioRecorderDelegate {
  func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    status = .stopped
  }
}
