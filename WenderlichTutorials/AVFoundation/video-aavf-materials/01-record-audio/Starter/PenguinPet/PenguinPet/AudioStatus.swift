//
//  AudioStatus.swift
//  PenguinPet
//
//  Created by Don Clore on 3/16/22.
//

import Foundation

enum AudioStatus: Int, CustomStringConvertible {
  case stopped
  case playing
  case recording
  
  var audioName: String {
    let audioNames = ["Audio:Stopped", "Audio:Playing", "Audio:Recording"]
    return audioNames[rawValue]
  }
  
  var description: String {
    return audioName
  }
  
}
