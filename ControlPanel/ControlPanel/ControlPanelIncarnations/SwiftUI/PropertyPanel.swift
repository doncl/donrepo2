//
//  PropertyPanel.swift
//  ControlPanel
//
//  Created by Don Clore on 8/24/21.
//

import SwiftUI
import Combine

struct PropertyPanel: View {
	struct Constants {
		static let labelToField: CGFloat = 5
		static let width: CGFloat = 350 // per spec
		static let height: CGFloat = 350 // just, because
		static let pad: CGFloat = 28.0
		static let idTopPad: CGFloat = 4.0
		static let indent: CGFloat = 10.0 // should be indented 5 pixels relative to the label (or 10 from the left edge of the panel).
		static let edges: EdgeInsets =
			EdgeInsets(top: 10, // per spec "flow from the top should be 10 points from the top of the panel"
					   leading: 5,  // per spec "label should be 5 points from left edge of panel"
					   bottom: 8,
					   trailing: 8 + Constants.indent)
	}
	
	@ObservedObject var item: Item
	@State private var keyboardHeight: CGFloat = 0
		
    var body: some View {
		ScrollView {
			HStack(alignment: .top) {
				VStack(alignment: .leading) {
					VStack(alignment: .leading, spacing: Constants.labelToField) {
						Text("Item Name: ")
							.font(Font.system(.title))

						TextField("", text: $item.name)
							.font(Font.system(.title2))
							.offset(x: Constants.indent, y: 0)
						
					}.padding([.bottom], Constants.pad)
					.border(Color.white, width: 1)
					
					VStack(alignment: .leading) {
						Text("Item Identifier: ")
							.font(Font.system(.title))
						
						Text(item.id)
							.font(Font.system(.callout))
							.allowsTightening(true)
							.offset(x: Constants.indent, y: 0)
							.padding(.top, Constants.idTopPad)
					}
					.padding([.top], Constants.pad)
					
					Spacer()
				}
			}
			.padding(Constants.edges)
			.padding(.bottom, keyboardHeight)
			.frame(width: Constants.width, height: Constants.height)
			.foregroundColor(Color("PropertyPanelText"))
			.background(Color("PropertyPanelBackground"))
		}
    }
}

struct PropertyPanel_Previews: PreviewProvider {
    static var previews: some View {
		let item = Item(name: "Fake Item", id: "b39a059d603d4f2d9665100362abd5c7")
		return PropertyPanel(item: item)
			.previewLayout(.fixed(width: 896, height: 414))
    }
}
