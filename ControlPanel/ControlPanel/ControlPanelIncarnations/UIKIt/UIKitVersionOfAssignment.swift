//
//  TableWithPropertiesPanelVC.swift
//  ControlPanel
//
//  Created by Don Clore on 8/23/21.
//

import UIKit
import SwiftUI

class UIKitVersionOfAssignment: UIViewController {
	struct Constants {
		static let estimatedRowHeight: CGFloat = 50
		static let pad: CGFloat = 16.0
	}
	
	let picker = UIKitItemPicker(frame: .zero, style: .plain)
	var panel:  UIHostingController<PropertyPanel>? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

		let item = API.shared.allItems()[0]
		
		let newPanel = UIHostingController(rootView: PropertyPanel(item: item))
		guard let panelView = newPanel.view else {
			return
		}
		self.panel = newPanel
		
		addChild(newPanel)
		[picker, panelView].forEach {
			$0.translatesAutoresizingMaskIntoConstraints = false
			view.addSubview($0)
		}
		
		newPanel.didMove(toParent: self)
		
		NSLayoutConstraint.activate([
			picker.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Constants.pad),
			picker.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: Constants.pad),
			picker.trailingAnchor.constraint(equalTo: panelView.leadingAnchor, constant: -Constants.pad),
			picker.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -Constants.pad),
			
			panelView.topAnchor.constraint(equalTo: picker.topAnchor),
			panelView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -Constants.pad),
			panelView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -Constants.pad),
			panelView.widthAnchor.constraint(equalToConstant: PropertyPanel.Constants.width),
		])
		
		picker.pickerDelegate = self
	}
}

extension UIKitVersionOfAssignment: UIKitItemPickerDelegate {	
    func itemSelected(index: Int, item: Item) {
		guard let panel = panel else {
			return
		}
		panel.rootView = PropertyPanel(item: item)
	}
}
