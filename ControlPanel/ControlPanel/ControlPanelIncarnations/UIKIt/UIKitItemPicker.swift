//
//  UIKitItemPicker.swift
//  ControlPanel
//
//  Created by Don Clore on 8/24/21.
//

import UIKit
import SwiftUI

protocol UIKitItemPickerDelegate: AnyObject {
	func itemSelected(index: Int, item: Item)
}

class UIKitItemPicker: UITableView, ObservableObject {
	struct Constants {
		static let estimatedRowHeight: CGFloat = 50
	}
	
	@Published var selectedTableRow: Int = 0

	weak var pickerDelegate: UIKitItemPickerDelegate?

	override init(frame: CGRect, style: UITableView.Style) {
		super.init(frame: frame, style: style)
		
		rowHeight = UITableView.automaticDimension
		estimatedRowHeight = Constants.estimatedRowHeight
		register(ItemPickerCell.self, forCellReuseIdentifier: ItemPickerCell.id)
		dataSource = self
		delegate = self
		separatorStyle = UITableViewCell.SeparatorStyle.none
		allowsSelection = true
		let ip = IndexPath(item: 0, section: 0)
		selectRow(at: ip, animated: false, scrollPosition: .top)
		selectCell(atIndexPath: ip, selected: true)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}		
}

extension UIKitItemPicker: UITableViewDelegate {
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		print("\(#function) indexPath = \(indexPath)")
		selectCell(atIndexPath: indexPath, selected: true)
		guard let del = pickerDelegate else {
			return
		}
		let items = API.shared.allItems()
		guard items.count > indexPath.item else {
			return
		}
		let item = items[indexPath.item]
		del.itemSelected(index: indexPath.item, item: item)
	}
	
	func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
		print("\(#function) indexPath = \(indexPath)")
		selectCell(atIndexPath: indexPath, selected: false)
	}
	
	private func selectCell(atIndexPath indexPath: IndexPath, selected: Bool) {
		guard let cell = cellForRow(at: indexPath) as? ItemPickerCell else {
			return
		}
		cell.select(selected)
		if selected {
			selectedTableRow = indexPath.item   // Publish with Combine 
		}
	}
}

extension UIKitItemPicker: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return API.shared.allItems().count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let plainCell = tableView.dequeueReusableCell(withIdentifier: ItemPickerCell.id, for: indexPath)
		guard let pickerCell = plainCell as? ItemPickerCell else {
			return plainCell
		}
		pickerCell.text = "Item \(indexPath.item + 1)"
		
		return pickerCell
	}
}

struct ItemPicker: UIViewRepresentable {
	@Binding var currentItemIndex: Int 
	
	func makeCoordinator() -> Coordinator {
		return Coordinator(picker: self)
	}
		
	class Coordinator: NSObject, UIKitItemPickerDelegate {
		var picker: ItemPicker
		
		func itemSelected(index: Int, item: Item) {
			print("\(#function)")
			picker.currentItemIndex = index
		}
				
		init(picker: ItemPicker) {
			self.picker = picker
			super.init()
		}
	}
	
	func makeUIView(context: Context) -> UIKitItemPicker {
		let table = UIKitItemPicker()
		table.pickerDelegate = context.coordinator
		
		return table
	}
	
	func updateUIView(_ uiView: UIKitItemPicker, context: Context) {
		
	}
}
