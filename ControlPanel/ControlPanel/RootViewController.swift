//
//  RootViewController.swift
//  ControlPanel
//
//  Created by Don Clore on 8/23/21.
//

import UIKit
import SwiftUI

class RootViewController: UITabBarController {
	let swiftUIVersion = UIHostingController(rootView: SwiftUIVersionOfAssignment())
	let uiKitVersion = UIKitVersionOfAssignment()
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = UIColor.systemBackground
		viewControllers = [swiftUIVersion, uiKitVersion]
		
		swiftUIVersion.tabBarItem = UITabBarItem(title: "SwiftUI", image: nil, selectedImage: nil)
		uiKitVersion.tabBarItem = UITabBarItem(title: "UIKit", image: nil, selectedImage: nil)
	}
}

