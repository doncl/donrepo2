//
//  ApiProtocol.swift
//  ControlPanel
//
//  Created by Don Clore on 8/23/21.
//

import Foundation
import Combine

protocol ApiProtocol: AnyObject {
	func getName(id: String, callback: @escaping (_: String) -> Void)
	func setName(id: String, name: String, callback: @escaping () -> Void)
	func allItems() -> [Item]
	func itemDirtied(id: String)
}
