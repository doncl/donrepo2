//
//  AssetLoader.swift
//  VideoFunzies
//
//  Created by Don Clore on 10/30/21.
//

import AVFoundation
import UIKit

extension VideoCompositionManager {

enum AssetsLoaderError: Error {
	case didntgetallproperties(Int, Int)
}

class AssetsLoader {
	enum AssetType {
		case video
		case audio
	}

	// MARK: Singleton property thingy
	static let shared = AssetsLoader()
	
	// MARK: private properties
	private let group = DispatchGroup()
	private let q = DispatchQueue(label: "Get the silly asset properties queue")
	private let propertyKeys: [String] = ["duration", "tracks", "commonMetadata"]
	
	private var videos: [AVURLAsset] = []
	private var audioTracks: [AVURLAsset] = []
	
	private var assetPropertiesLoaded: Bool {
		assert(Thread.isMainThread)
		return self.videoFileNamesCount == videos.count && self.audioFileNamesCount == audioTracks.count
	}

	private var videoFileNamesCount: Int = 0
	private var audioFileNamesCount: Int = 0
	
	// MARK: private iniitalizer
	private init() {}
	
	// MARK: Methods
	func loadAssets(videoFilenames: [String], audioFilenames: [String],
					completion: @escaping (Result<(videos: [AVURLAsset], audios: [AVURLAsset]), Error>) -> ()) {
		
		videoFileNamesCount = videoFilenames.count
		audioFileNamesCount = audioFilenames.count
		let start = Date()

		loadAssets(names: videoFilenames, ext: "MOV", assetType: .video)
		loadAssets(names: audioFilenames, ext: "m4a", assetType: .audio)
		
		group.notify(queue: .main) { [weak self] in
			guard let self = self else { return }
			
			defer {
				// This class doesn't own the assets, it just acquires them, and hands off to the manager.
				self.videos = []
				self.audioTracks = []
			}
			
			guard self.assetPropertiesLoaded else {
				// load up the error with what we actually did get.
				completion(.failure(AssetsLoaderError.didntgetallproperties(self.videos.count, self.audioTracks.count)))
				return
			}
			
			let elapsed = Date().timeIntervalSince(start)
			print("videos.count = \(self.videos.count), audios = \(self.audioTracks.count), elapsed: \(elapsed * 1000) ms.")
			
			completion(.success((videos: self.videos, audios: self.audioTracks)))
		}
	}
	
	/// Method for asynchronously loading AVURLAsset properties, and using DispatchGroup, so we know when all the work is done.
	/// - Parameters:
	///   - names: filenames, sans extension
	///   - ext: file extension, in this case, either "MOV", or "m4a"
	///   - video: true if we're doing video, false if audio
	private func loadAssets(names: [String], ext: String, assetType: AssetType) {
		for name in names {
			if let fileInBundle = Bundle.main.url(forResource: name, withExtension: ext) {
				let asset = AVURLAsset(url: fileInBundle, options: [AVURLAssetPreferPreciseDurationAndTimingKey: true])
				print("\(#function) - entering Group")
				group.enter()
				asset.loadValuesAsynchronously(forKeys: self.propertyKeys) {
										
					// Ensure thread safety by putting all this on our dedicated thread.
					self.q.async {
						if self.assetIsLoaded(asset: asset) {
							switch assetType {
								case .video:
									if !self.videos.contains(asset) {
										self.videos.append(asset)
									}
								case .audio:
									if !self.audioTracks.contains(asset) {
										self.audioTracks.append(asset)
									}
							}
							self.q.asyncAfter(deadline: .now() + 1) { [weak self] in
								guard let self = self else { return }
								self.group.leave()
							}
						}
					}
				}
			}
		}
	}
	
	
	/// Helper func that just determines whether all the properties we care about have been loaded or not.
	/// - Parameter asset: Movie or Audio file.
	/// - Returns: true if all the keys we care about are loaded, false otherwise.
	private func assetIsLoaded(asset: AVURLAsset) -> Bool {
		var error: NSError?
				
		for key in propertyKeys {
			guard asset.statusOfValue(forKey: key, error: &error) == .loaded, error == nil else {
				return false
			}
		}
		return true
	}
}
}
