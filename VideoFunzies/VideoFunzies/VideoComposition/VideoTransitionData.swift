//
//  VideoTransitionInstruction.swift
//  VideoFunzies
//
//  Created by Don Clore on 10/30/21.
//

import AVFoundation

struct VideoTransitionData {
	let compositionInstruction: AVMutableVideoCompositionInstruction
	let fromLayerInstruction: VideoLayerInstructionWithTransform
	let toLayerInstruction: VideoLayerInstructionWithTransform
	var transition: VideoTransition?
}
