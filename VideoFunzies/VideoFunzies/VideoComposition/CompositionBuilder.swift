//
//  CompositionBuilder.swift
//  VideoFunzies
//
//  Created by Don Clore on 10/30/21.
//

import AVFoundation

extension VideoCompositionManager {
	
// MARK: Setting up.
	
	class CompositionBuilder {
		static let shared = CompositionBuilder()
		
		private init() {}
	}
}

// MARK: Stuff we do
extension VideoCompositionManager.CompositionBuilder {
	func prepare(composition: AVMutableComposition,
				 withVideoAssets videoAssets: [AVURLAsset],
				 andAudioAssets audioAssets: [AVURLAsset],
				 andSetToRenderAt renderSize: CGSize) -> AVMutableVideoComposition? {
		
		// This is true for this demo code, but certainly wouldn't have to be, or probably wouldn't be true in a real case.
		// It's just a simplifying assumption for a minor point.
		assert(videoAssets.count <= audioAssets.count)
		assert(videoAssets.count > 0)
		
		guard let videoTrack0 = composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid),
			  let audioTrack0 = composition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid),
			  let videoTrack1 = composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid),
			  let audioTrack1 = composition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid) else {
			 
			  print("Could not create composition tracks")
			  return nil
        }

		var currTime: CMTime = CMTime.zero
		let transitionDuration: CMTime = CMTime(value: 60, timescale: 30)
		
		var passThroughRanges: [CMTimeRange] = []
		var videoAssetTracks: [AVAssetTrack] = []
		var transitionRanges: [CMTimeRange] = []
		
		for i in 0..<videoAssets.count {
			let videoAsset = videoAssets[i]
			let audioAsset = audioAssets[i]
			
			let even = i % 2 == 0
			
			let videoTrack = even ? videoTrack0 : videoTrack1
			let audioTrack = even ? audioTrack0 : audioTrack1

			guard let tuple = insert(videoAsset: videoAsset, intoVideoTrack: videoTrack, andAudioAsset: audioAsset,
									intoAudioTrack: audioTrack, atTime: currTime) else {
				print("Something went wrong on insertion of videoAsset and audio Asset, bailing")
				return nil
			}
			let duration = tuple.duration
			let videoAssetTrack = tuple.videoAssetTrack
			videoAssetTracks.append(videoAssetTrack)
			
			var passThroughRange: CMTimeRange = CMTimeRange(start: currTime, duration: duration)
			if i > 0 {
				passThroughRange.start = CMTimeAdd(passThroughRange.start, transitionDuration)
				passThroughRange.duration = CMTimeSubtract(passThroughRange.duration, transitionDuration)
			}
			
			if i + 1 < videoAssets.count {
				passThroughRange.duration = CMTimeSubtract(passThroughRange.duration, transitionDuration)
			}
			
			passThroughRanges.append(passThroughRange)
			
			currTime = CMTimeAdd(currTime, duration)
			currTime = CMTimeSubtract(currTime, transitionDuration)
			
			if i + 1 < videoAssets.count {
				let transitionRange: CMTimeRange = CMTimeRange(start: currTime, duration: transitionDuration)
				transitionRanges.append(transitionRange)
			}
		}
        
        print("\(#function) - passthroughRanges: ")
        for (i, range) in passThroughRanges.enumerated() {
            printRange(range, index: i)
        }
        
        print("\(#function) - transitionRanges: ")
        for (i, range) in transitionRanges.enumerated() {
            printRange(range, index: i)
        }

		let realRenderSize: CGSize = CGSize(width: renderSize.width * 3, height: renderSize.height * 3)
		let instructions = buildInstructions(composition: composition,
											 passThroughRanges: passThroughRanges,
											 videoAssetTracks: videoAssetTracks,
											 transitionRanges: transitionRanges,
                                             renderSize: realRenderSize)

		let videoComposition = buildVideoComposition(fromComposition: composition, instructions: instructions, renderSize: realRenderSize)
		return videoComposition
	}
    
    private func printRange(_ range: CMTimeRange, index: Int) {
        let start = range.start.seconds
        let end = CMTimeAdd(range.start, range.duration).seconds
        let duration = range.duration.seconds

        print("\(index) - start = \(start) end = \(end) - duration = \(duration)")
    }
	
	private func insert(videoAsset: AVURLAsset, intoVideoTrack videoTrack: AVMutableCompositionTrack,
						andAudioAsset audioAsset: AVURLAsset, intoAudioTrack audioTrack: AVMutableCompositionTrack,
						atTime time: CMTime) -> (duration: CMTime, videoAssetTrack: AVAssetTrack)? {
		
		
		let videoDuration: CMTime = videoAsset.duration
		let timeRange: CMTimeRange = CMTimeRange(start: CMTime.zero, duration: videoDuration)
		
		guard let assetVideoTrack = videoAsset.tracks(withMediaType: AVMediaType.video).first else {
			print("No videoTracks found on video asset")
			return nil
		}

		print("\(#function) - videoAsset preferred Transform = \(videoAsset.preferredTransform)")
		print("\(#function) - assetVideoTrack preferred transform = \(assetVideoTrack.preferredTransform)")
		
		guard let assetAudioTrack = audioAsset.tracks(withMediaType: AVMediaType.audio).first else {
			print("No audioTracks found on audio asset")
			return nil
		}
		
		do {
			try videoTrack.insertTimeRange(timeRange, of: assetVideoTrack, at: time)
			try audioTrack.insertTimeRange(timeRange, of: assetAudioTrack, at: time)
		} catch let error {
			// Obviously, this is not OK for production code.
			print("Could not insert timerange for video into videotrack, error = \(error)")
			return nil
		}
		return (duration: videoDuration, videoAssetTrack: assetVideoTrack)
	}
	
	private func buildInstructions(composition: AVMutableComposition,
								   passThroughRanges: [CMTimeRange],
								   videoAssetTracks: [AVAssetTrack],
								   transitionRanges: [CMTimeRange],
								   renderSize: CGSize) -> [AVMutableVideoCompositionInstruction] {
		
		var instructions: [AVMutableVideoCompositionInstruction] = []
		
		let tracks = composition.tracks(withMediaType: AVMediaType.video)
		
		// I'm putting all the types in here, instead of letting Swift type inference do it, so I don't confuse myself, or
		// slow the compiler down.   Mostly so I don't confuse myself.
		for (i, passThroughRange) in passThroughRanges.enumerated() {
			let videoAssetTrack = videoAssetTracks[i]
			let trackIndex: Int = i % 2
			let currentTrack: AVMutableCompositionTrack = tracks[trackIndex]
			let instruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
			instruction.timeRange = passThroughRange
			
			let layerinstruction: VideoLayerInstructionWithTransform =
				VideoLayerInstructionWithTransform(assetTrack: currentTrack)
			
			let preferredXForm = videoAssetTrack.preferredTransform
			layerinstruction.xform = preferredXForm
			layerinstruction.setTransform(preferredXForm, at: CMTime.zero)
									
			instruction.layerInstructions = [layerinstruction]
			
			instructions.append(instruction)
			
			if i < transitionRanges.count {
				let foregroundTrack = tracks[trackIndex]
				let backgroundTrack = tracks[1 - trackIndex]
				
				let fromAssetTrack = videoAssetTracks[i]
				
				assert(i + 1 < videoAssetTracks.count, "off by one error")
				let toAssetTrack = videoAssetTracks[i + 1]
				
				let fromXForm = fromAssetTrack.preferredTransform
				let toXForm = toAssetTrack.preferredTransform
				
				let instruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
				
				let timeRange: CMTimeRange = transitionRanges[i]
				instruction.timeRange = timeRange
				
				let fromLayerInstruction: VideoLayerInstructionWithTransform =
					VideoLayerInstructionWithTransform(assetTrack: foregroundTrack)
								
				
				fromLayerInstruction.setTransform(fromXForm, at: timeRange.start)
				fromLayerInstruction.xform = fromXForm
				
				let toLayerInstruction: VideoLayerInstructionWithTransform =
					VideoLayerInstructionWithTransform(assetTrack: backgroundTrack)
				
				toLayerInstruction.setTransform(toXForm, at: timeRange.start)
				toLayerInstruction.xform = toXForm
				
				instruction.layerInstructions = [fromLayerInstruction, toLayerInstruction]
				instructions.append(instruction)
			}
		}
		return instructions
	}
	
	private func buildVideoComposition(fromComposition composition: AVMutableComposition,
									   instructions: [AVMutableVideoCompositionInstruction],
									   renderSize: CGSize) -> AVMutableVideoComposition {
		

		let videoComposition: AVMutableVideoComposition = AVMutableVideoComposition(propertiesOf: composition)
		videoComposition.instructions = instructions
		videoComposition.frameDuration = CMTime(value: 1, timescale: 30)
		videoComposition.renderScale = 1.0
		videoComposition.renderSize = renderSize
		
		let transitionData = getTransitionData(inVideoComposition: videoComposition)
		
		for transitionDatum in transitionData {
			let timeRange = transitionDatum.compositionInstruction.timeRange
			let fromLayer: VideoLayerInstructionWithTransform = transitionDatum.fromLayerInstruction
			let toLayer: VideoLayerInstructionWithTransform = transitionDatum.toLayerInstruction
			
			if let transition = transitionDatum.transition {
				let type = transition.type
				switch type {
					case .dissolve:
						implementDissolve(videoComposition: videoComposition, fromLayer: fromLayer,
										  toLayer: toLayer, timeRange: timeRange)
					case .push:
						implementPush(videoComposition: videoComposition, fromLayer: fromLayer,
									  toLayer: toLayer, timeRange: timeRange)
					case .wipe:
						implementWipe(videoComposition: videoComposition, fromLayer: fromLayer, timeRange: timeRange)
				}
			}
		}
		
		return videoComposition
	}
	
	private func getTransitionData(inVideoComposition videoComposition: AVMutableVideoComposition) -> [VideoTransitionData] {
		var ret: [VideoTransitionData] = []
		
		var layerInstructionIndex = 1
				
		let compositionInstructions = videoComposition.instructions
		for (i, instruction) in compositionInstructions.enumerated() {
			guard let vci = instruction as? AVMutableVideoCompositionInstruction else {
				continue
			}
			if vci.layerInstructions.count == 2 {
				guard let fromLayerInstruction = vci.layerInstructions[1 - layerInstructionIndex] as? VideoLayerInstructionWithTransform,
					  let toLayerInstruction = vci.layerInstructions[layerInstructionIndex] as? VideoLayerInstructionWithTransform else {
						  continue
            	  }
				
				// Transition Types
				let numCases = TransitionType.allCases.count
				let transitionTypeIndex = i % numCases
				let transitionType = TransitionType.allCases[transitionTypeIndex]
				let transition = VideoTransition(duration: CMTime(value:30, timescale: 30), type: transitionType)
				let transitionInstruction = VideoTransitionData(compositionInstruction: vci,
															fromLayerInstruction: fromLayerInstruction,
															toLayerInstruction: toLayerInstruction,
															transition: transition)
				
				ret.append(transitionInstruction)
				
				//layerInstructionIndex = layerInstructionIndex == 1 ? 0 : 1
			}
		}
		return ret
	}
}
