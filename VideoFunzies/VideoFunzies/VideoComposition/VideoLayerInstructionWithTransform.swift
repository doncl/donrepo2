//
//  VideoLayerInstructionWithTransform.swift
//  VideoFunzies
//
//  Created by Don Clore on 10/31/21.
//

import AVFoundation

class VideoLayerInstructionWithTransform: AVMutableVideoCompositionLayerInstruction {
	var xform: CGAffineTransform? = nil
}
