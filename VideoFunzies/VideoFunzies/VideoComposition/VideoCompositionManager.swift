//
//  VideoCompositionBuilder.swift
//  VideoFunzies
//
//  Created by Don Clore on 10/29/21.
//

import AVFoundation

protocol VideoCompositionManagerDelegate: AnyObject {
	func getRenderSize() -> CGSize
}

class VideoCompositionManager {
	static let assetsReady = Notification.Name("video-and-audio-assets-loaded")
	static let videoCompositionPlayableNotification = Notification.Name("video-composition-ready-to-play")
	
	enum AssetType {
		case video
		case audio
	}
	
	static let shared = VideoCompositionManager()
	
	weak var delegate: VideoCompositionManagerDelegate?
	
	var videos: [AVURLAsset] = []
	var audioTracks: [AVURLAsset] = []
	let composition: AVMutableComposition = AVMutableComposition()
	var videoComposition: AVMutableVideoComposition?
	
//	private let videoFileNames: [String] = ["deck", "scooter", "keyboards", "cabinet", "piano",]
//	private let audioFileNames: [String] = ["CrystalSilence", "Horace", "MyRomance", "WhereHaveIKnownYouBefore", "Georgia",]
    
    private let videoFileNames: [String] = ["deck", "scooter", "keyboards",]
    private let audioFileNames: [String] = ["CrystalSilence", "Horace", "MyRomance", ]

		
	private init() {
		NotificationCenter.default.addObserver(self, selector: #selector(VideoCompositionManager.assetsLoaded(_:)),
											   name: VideoCompositionManager.assetsReady, object: nil)
	}
	
	@objc func assetsLoaded(_ note: Notification) {
		print("\(#function)")
		prepareComposition()
	}
}

// MARK: Asset property acquisition.
extension VideoCompositionManager {
	func loadAssets() {
		
		AssetsLoader.shared.loadAssets(videoFilenames: videoFileNames, audioFilenames: audioFileNames) { [weak self] result in
			guard let self = self else { return }
			switch result {
				case .success((let videos, let audios)):
					assert(Thread.isMainThread)
					//self.videos.append(contentsOf: videos)
					self.hackySortToPreserveOrderingForAppleDeveloperSupport(loadedVideos: videos)
					self.audioTracks.append(contentsOf: audios)
					NotificationCenter.default.post(name: VideoCompositionManager.assetsReady, object: nil)
				case .failure(let error):
					switch error {
						case AssetsLoaderError.didntgetallproperties(let videoCount, let audioCount):
							print("Failed to load all assets = vidCount = \(videoCount), audioCount = \(audioCount)")
						default:
							fatalError("Should never happen")
					}
			}
		}
	}
	
	private func prepareComposition() {
		guard let delegate = delegate else {
			return
		}
		let renderSize = delegate.getRenderSize()
		videoComposition = CompositionBuilder.shared.prepare(composition: composition,
															 withVideoAssets: videos,
															 andAudioAssets: audioTracks,
															 andSetToRenderAt: renderSize)
		
		NotificationCenter.default.post(name: VideoCompositionManager.videoCompositionPlayableNotification, object: nil)
	}
	
	private func hackySortToPreserveOrderingForAppleDeveloperSupport(loadedVideos: [AVURLAsset]) {
		for name in videoFileNames {
			guard let matching = loadedVideos.first(where: { $0.url.absoluteString.contains(name)}) else {
				continue
			}
			self.videos.append(matching)
		}
	}
}
