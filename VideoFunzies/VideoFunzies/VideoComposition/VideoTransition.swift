//
//  VideoTransition.swift
//  VideoFunzies
//
//  Created by Don Clore on 10/30/21.
//

import AVFoundation

enum TransitionType: CaseIterable {
	case dissolve
	case wipe
	case push
}

// Not in use as yet, but pretty obvious what to do.
enum TransitionDirection {
	case leftToRight
	case rightToLeft
	case topToBottom
	case bottomToTop
}

struct VideoTransition {
	var timeRange: CMTimeRange = CMTimeRange.invalid
	let type: TransitionType
	var duration: CMTime
	
	init(duration: CMTime, type: TransitionType) {
		self.type = type
		self.duration = duration
	}
	
	init() {
		type = .dissolve
		duration = CMTime.zero
	}
}
