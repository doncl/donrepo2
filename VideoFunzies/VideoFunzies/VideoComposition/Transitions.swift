//
//  Transitions.swift
//  VideoFunzies
//
//  Created by Don Clore on 11/4/21.
//

import AVFoundation

extension VideoCompositionManager.CompositionBuilder {
	func implementDissolve(videoComposition: AVMutableVideoComposition,
						   fromLayer: AVMutableVideoCompositionLayerInstruction,
						   toLayer: AVMutableVideoCompositionLayerInstruction,
						   timeRange: CMTimeRange) {
		fromLayer.setOpacityRamp(fromStartOpacity: 1.0, toEndOpacity: 0, timeRange: timeRange)
		toLayer.setOpacityRamp(fromStartOpacity: 0.0, toEndOpacity: 1.0, timeRange: timeRange)
	}
	
	func implementWipe(videoComposition: AVMutableVideoComposition,
					   fromLayer: AVMutableVideoCompositionLayerInstruction,
					   timeRange: CMTimeRange) {
		
		let width = videoComposition.renderSize.width
		let height = videoComposition.renderSize.height
		
		let startRect = CGRect(x: 0, y: 0, width: width, height: height)
		let endRect = CGRect(x: 0, y: height, width: width, height: 0)
		
		fromLayer.setCropRectangleRamp(fromStartCropRectangle: startRect, toEndCropRectangle: endRect, timeRange: timeRange)
	}
	
	func implementPush(videoComposition: AVMutableVideoComposition,
					   fromLayer: AVMutableVideoCompositionLayerInstruction,
					   toLayer: AVMutableVideoCompositionLayerInstruction,
					   timeRange: CMTimeRange) {
		
		let videoWidth = videoComposition.renderSize.width
		let fromXForm = CGAffineTransform(translationX: -videoWidth, y: 0)
		let toXForm = CGAffineTransform(translationX: videoWidth, y: 0)
		
		
		// This is about making sure we preserve the preferred transform when doing the transition.
		let identitySubstituteXFormFrom: CGAffineTransform
		let identitySubstituteXFormTo: CGAffineTransform
		if let ourSpecialLayerInstructionType = fromLayer as? VideoLayerInstructionWithTransform,
			let xform = ourSpecialLayerInstructionType.xform {
			identitySubstituteXFormFrom = xform
		} else {
			identitySubstituteXFormFrom = .identity
		}
		
		if let ourSpecialLayerInstructionType = toLayer as? VideoLayerInstructionWithTransform,
		   let xform = ourSpecialLayerInstructionType.xform {
			identitySubstituteXFormTo = xform
		} else {
			identitySubstituteXFormTo = .identity
		}
		
		
		fromLayer.setTransformRamp(fromStart: identitySubstituteXFormFrom, toEnd: fromXForm, timeRange: timeRange)
		toLayer.setTransformRamp(fromStart: toXForm, toEnd: identitySubstituteXFormTo, timeRange: timeRange)
	}
	

}
