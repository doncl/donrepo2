//
//  ViewController.swift
//  VideoFunzies
//
//  Created by Don Clore on 10/29/21.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
	let playerLayer = AVPlayerLayer()
	let player = AVPlayer()
	let dbg = APLCompositionDebugView()
		
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .black
		
		setupVideoPlayingCapability()
		
		setupComposition()
		
		dbg.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(dbg)
		NSLayoutConstraint.activate([
			dbg.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
			dbg.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
			dbg.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
			dbg.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.3),
		])
	}
		
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		layoutPlayerLayer()
	}
}

extension ViewController {
	private func layoutPlayerLayer() {
		let size = getRenderSize()
		let x = view.bounds.width / 2 - size.width / 2
		playerLayer.frame = CGRect(x: x, y: 50, width: size.width, height: size.height)
	}
	
	func setupVideoPlayingCapability() {
		VideoCompositionManager.shared.delegate = self
		playerLayer.player = player
		playerLayer.videoGravity = AVLayerVideoGravity.resizeAspect
		playerLayer.contentsGravity = CALayerContentsGravity.resizeAspect
		view.layer.addSublayer(playerLayer)
		layoutPlayerLayer()
	}
	
	private func setupComposition() {
		NotificationCenter.default.addObserver(self, selector: #selector(ViewController.compositionReady(_:)),
											   name: VideoCompositionManager.videoCompositionPlayableNotification, object: nil)
		
		VideoCompositionManager.shared.loadAssets()
	}
	
	@objc func compositionReady(_ note: Notification) {
		print("\(#function)")
		let composition: AVMutableComposition = VideoCompositionManager.shared.composition
		let videoComposition: AVMutableVideoComposition? = VideoCompositionManager.shared.videoComposition
		let item = AVPlayerItem(asset: composition)
		item.videoComposition = videoComposition
		player.replaceCurrentItem(with: item)
		player.play()
		dbg.player = player
		dbg.synchronize(to: composition, videoComposition: videoComposition, audioMix: nil)
	}
}

extension ViewController: VideoCompositionManagerDelegate {
	func getRenderSize() -> CGSize {
		let height = view.layer.bounds.height * 0.7
		let width = height * 0.5625
		return CGSize(width: width, height: height)
	}
}

