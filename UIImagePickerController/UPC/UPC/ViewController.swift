//
//  ViewController.swift
//  UPC
//
//  Created by Don Clore on 3/12/22.
//

import UIKit
import MobileCoreServices
import AVFoundation

class ViewController: UIViewController {
  @IBOutlet var imageView: UIImageView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }

  @IBAction func buttonTouched(_ sender: UIButton) {
    let picker = UIImagePickerController()
    picker.sourceType = UIImagePickerController.SourceType.photoLibrary
    picker.allowsEditing = true
    
    picker.mediaTypes = [String(kUTTypeMovie)]
    picker.delegate = self
    present(picker, animated: true)
  }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    picker.dismiss(animated: true)
    
    guard let image = info[.editedImage] as? UIImage else {
      return
    }
    
    self.imageView.contentMode = .scaleAspectFill
    self.imageView.image = image
  }
}
