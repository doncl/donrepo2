//
//  CustomSpinnerObjC.m
//  SpinnerTest
//
//  Created by Don Clore on 7/6/21.
//

#import "CustomSpinnerObjC.h"

// Constants
// Default values
#define kDefaultNumberOfSpinnerMarkers 12
#define kDefaultSpread 35.0
#define kDefaultColor ([UIColor whiteColor])
#define kDefaultThickness 8.0
#define kDefaultLength 25.0
#define kDefaultSpeed 1.0

// HUD defaults
#define kDefaultHUDSide 160.0
#define kDefaultHUDColor ([UIColor colorWithWhite:0.0 alpha:0.5])

#define kMarkerAnimationKey @"MarkerAnimationKey"



@implementation CustomSpinnerObjC

- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self setupEverything];
  }
  return self;
}


- (void)setupEverything {
  CALayer * marker = [CALayer layer];
  [marker setBounds:CGRectMake(0, 0, kDefaultThickness, kDefaultLength)];
  [marker setCornerRadius:kDefaultThickness*0.5];
  [marker setBackgroundColor:[kDefaultColor CGColor]];
  [marker setPosition:CGPointMake(kDefaultHUDSide*0.5, kDefaultHUDSide*0.5+kDefaultSpread)];


  //Next, we create the replicator layer and center it in our view. I want the replicator
  // to look like the volume HUD on iOS so I customize its size and appearance.
  CAReplicatorLayer * spinnerReplicator = [CAReplicatorLayer layer];
  [spinnerReplicator setBounds:CGRectMake(0, 0, kDefaultHUDSide, kDefaultHUDSide)];
  [spinnerReplicator setCornerRadius:10.0];
  [spinnerReplicator setBackgroundColor:[kDefaultHUDColor CGColor]];
  [spinnerReplicator setPosition:CGPointMake(CGRectGetMidX([self frame]),
                                             CGRectGetMidY([self frame]))];

  CGFloat angle = (2.0*M_PI)/(kDefaultNumberOfSpinnerMarkers);
  CATransform3D instanceRotation = CATransform3DMakeRotation(angle, 0.0, 0.0, 1.0);
  [spinnerReplicator setInstanceCount:kDefaultNumberOfSpinnerMarkers];
  [spinnerReplicator setInstanceTransform:instanceRotation];


  //Finally, to create the animation we set the alpha of the spinner marker to 0 and set
  // the instance delay to T⁄N with T = animation time and N = number of markers.
  // Now we apply an opacity animation from one to zero
  // (yes it may seem backwards but its what I meant)

  [spinnerReplicator addSublayer:marker];
  [[self layer] addSublayer:spinnerReplicator];
      
  [marker setOpacity:0.0];
  CABasicAnimation * fade = [CABasicAnimation animationWithKeyPath:@"opacity"];
  [fade setFromValue:[NSNumber numberWithFloat:1.0]];
  [fade setToValue:[NSNumber numberWithFloat:0.0]];
  [fade setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear]];
  [fade setRepeatCount:HUGE_VALF];
  [fade setDuration:kDefaultSpeed];
  CGFloat markerAnimationDuration = kDefaultSpeed/kDefaultNumberOfSpinnerMarkers;
  [spinnerReplicator setInstanceDelay:markerAnimationDuration];
  [marker addAnimation:fade forKey:kMarkerAnimationKey];
}
@end
