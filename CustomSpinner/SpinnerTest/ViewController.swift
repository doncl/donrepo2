//
//  ViewController.swift
//  SpinnerTest
//
//  Created by Don Clore on 7/6/21.
//

import UIKit

class ViewController: UIViewController {

  let dim: CGFloat = 300.0
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let spinner: CustomSpinner = CustomSpinner(frame: CGRect(x: 0, y: 0, width: dim, height: dim), color: UIColor.systemBlue)
    spinner.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(spinner)
    NSLayoutConstraint.activate ([
      spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      spinner.widthAnchor.constraint(equalToConstant: dim),
      spinner.heightAnchor.constraint(equalToConstant: dim),
    ])

    spinner.start()
  }
}

