//
//  CustomSpinnerObjC.h
//  SpinnerTest
//
//  Created by Don Clore on 7/6/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomSpinnerObjC : UIView
- (instancetype)initWithFrame:(CGRect)frame;
@end

NS_ASSUME_NONNULL_END
