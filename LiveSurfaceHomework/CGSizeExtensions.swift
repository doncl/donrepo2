//
//  CGSizeExtensions.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit
extension CGSize {
  func scale(byFactor factor : CGFloat) -> CGSize {
    return CGSize(width: width * factor, height: height * factor)
  }
}
