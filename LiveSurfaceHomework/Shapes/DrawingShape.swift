//
//  DrawingShape.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

protocol DrawingShape {
  var name: String { get }
  var rect: CGRect { get set }
  var drawingPath: UIBezierPath! { get set}
  var boundingPath: UIBezierPath! { get set }
  mutating func getPath(for rect: CGRect) -> UIBezierPath
  func getPathWithNoSideEffect(for rect: CGRect) -> UIBezierPath
  func getBoundingPath() -> UIBezierPath
  mutating func generateBoundingPath(for rect: CGRect)
}

extension DrawingShape {
  func getBoundingPath() -> UIBezierPath {
    return boundingPath
  }
  
  mutating func getPath(for rect: CGRect) -> UIBezierPath {
    self.rect = rect
    generateBoundingPath(for: rect)
    return drawingPath
  }
  
  mutating func generateBoundingPath(for rect: CGRect) {
    self.drawingPath = getPathWithNoSideEffect(for: rect)
    self.boundingPath = UIBezierPath(rect: self.drawingPath.boundingBox)
  }
}

struct DrawingRectangle: DrawingShape {
  var drawingPath: UIBezierPath!
  var boundingPath: UIBezierPath!
  var name: String
  var rect: CGRect
  
  func getPathWithNoSideEffect(for rect: CGRect) -> UIBezierPath {
    UIBezierPath(rect: rect)
  }  
}

struct DrawingRoundedRectangle: DrawingShape {
  var name: String
  var rect: CGRect
  var drawingPath: UIBezierPath!
  var boundingPath: UIBezierPath!
  
  func getPathWithNoSideEffect(for rect: CGRect) -> UIBezierPath {
    UIBezierPath(roundedRect: rect, cornerRadius: rect.height * 0.05)
  }
}

struct DrawingOval: DrawingShape {
  var name: String
  var rect: CGRect
  var drawingPath: UIBezierPath!
  var boundingPath: UIBezierPath!

  func getPathWithNoSideEffect(for rect: CGRect) -> UIBezierPath {
    UIBezierPath(ovalIn: rect)
  }
}

struct DrawingStar: DrawingShape {
  var name: String
  var rect: CGRect
  var drawingPath: UIBezierPath!
  var boundingPath: UIBezierPath!
  
  func getPathWithNoSideEffect(for rect: CGRect) -> UIBezierPath {
    let path = UIBezierPath.starShape(numberOfInflections: 5, percentInflection: 8.0)
    path.fit(to: rect)
    return path
  }
}
