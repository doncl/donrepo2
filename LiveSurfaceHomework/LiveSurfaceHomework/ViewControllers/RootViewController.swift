//
//  ViewController.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
  let fauxDuration: TimeInterval = 2.0
  let chooserSize: CGSize = CGSize(width: 300, height: 180)
  let fauxLaunch: FauxLaunchScreen = FauxLaunchScreen()
  let canvas: CanvasViewController = CanvasViewController()
  let list: ShapeListView = ShapeListView()
  let initialMultiplier: CGFloat = 0.33
  
  var listRight: NSLayoutConstraint = NSLayoutConstraint()
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return .darkContent
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.white
    addChild(fauxLaunch)
    view.addSubview(fauxLaunch.view)
    fauxLaunch.didMove(toParent: self)
    fauxLaunch.view.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      fauxLaunch.view.topAnchor.constraint(equalTo: view.topAnchor),
      fauxLaunch.view.leftAnchor.constraint(equalTo: view.leftAnchor),
      fauxLaunch.view.rightAnchor.constraint(equalTo: view.rightAnchor),
      fauxLaunch.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])
    navigationItem.title = "LiveSurface Homework!".uppercased()
    
    [list, canvas].forEach {
      $0.view.translatesAutoresizingMaskIntoConstraints = false
      addChild($0)
      view.addSubview($0.view)
      $0.didMove(toParent: self)
    }
  
    listRight = list.view.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 200)
    NSLayoutConstraint.activate([
      listRight,
      list.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      list.view.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      list.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),

      canvas.view.leftAnchor.constraint(equalTo: list.view.rightAnchor),
      canvas.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      canvas.view.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      canvas.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])

    canvas.delegate = self
    list.delegate = self 
    view.bringSubviewToFront(fauxLaunch.view)
  }
  
  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    let width = view.bounds.width * initialMultiplier
    listRight.constant = width
  }
  
  override func viewDidAppear(_ animated: Bool) {
    if let nav = navigationController {
      nav.navigationBar.barTintColor = UIColor(named: "LaunchBackground")!
      let add = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.add, target: self,
                                action: #selector(RootViewController.addButtonPressed(_:)))
      
      add.tintColor = UIColor(named: "AddBtnColor")!
      
      navigationItem.rightBarButtonItem = add
    }
    super.viewDidAppear(animated)
    
    let xform = CGAffineTransform(scaleX: 0.1, y: 0.1).concatenating(CGAffineTransform(rotationAngle: CGFloat.pi * 3))
    UIView.animate(withDuration: fauxDuration, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
      guard let self = self else { return }
      self.fauxLaunch.view.alpha = 0
      self.fauxLaunch.view.transform = xform
    }, completion: { [weak self] _ in
      guard let self = self else { return }
      self.fauxLaunch.willMove(toParent: nil)
      self.fauxLaunch.view.removeFromSuperview()
      self.fauxLaunch.removeFromParent()
    })
  }
}

extension RootViewController {
  @objc func addButtonPressed(_ sender: UIBarButtonItem) {
    let vc = ShapeChooserVC()
    vc.modalPresentationStyle = .formSheet
    vc.preferredContentSize = chooserSize
    vc.delegate = self
    present(vc, animated: true)
  }
}

extension RootViewController: CanvasViewControllerDelegate {
  func addShapeToLeftPane(_ shape: DrawingShape) {
    var desiredRect = list.getDesiredBezierPathRect()
    let path = shape.getPathWithNoSideEffect(for: desiredRect)
    ModelStore.shared.add(path: path, withName: shape.name)
    list.refresh()
  }  
}

extension RootViewController: ShapeChooserVCDelegate {
  func shapeIDAvailable(_ shapeID: String) -> Bool {
    return canvas.shapeIDIsAvailable(shapeID)
  }
  
  func shapeTypeSelected(_ shapeType: ShapeType, withShapeID shapeID: String) {
    print("\(#function) - shapeType = \(shapeType.getNameAndImage().name)")
    let shape = shapeType.getDrawingShape(withName: shapeID)
    canvas.selectedShape = shape
  }
}

extension RootViewController: ShapeListViewDelegate {
  func setShape(atKey key: String, toVisible visible: Bool) {
    canvas.setShape(atKey: key, toVisible: visible)
  }
  
  
}
