//
//  FauxLaunchScreen.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

class FauxLaunchScreen: UIViewController {
  let lLabel: UILabel = UILabel()
  let homeworkLabel: UILabel = UILabel()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor(named: "LaunchBackground")
    
    [lLabel, homeworkLabel].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      view.addSubview($0)
      $0.textColor = UIColor.white
      $0.textAlignment = NSTextAlignment.center
    }
    
    lLabel.font = UIFont.systemFont(ofSize: 260.0, weight: UIFont.Weight.bold)
    lLabel.text = "L"
    
    homeworkLabel.font = UIFont(name: "Noteworthy-Light", size: 32.0)!
    homeworkLabel.text = "Homework"
        
    NSLayoutConstraint.activate([
      lLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
      lLabel.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
      
      homeworkLabel.centerXAnchor.constraint(equalTo: lLabel.centerXAnchor),
      homeworkLabel.topAnchor.constraint(equalTo: lLabel.bottomAnchor)
    ])

  }
}
