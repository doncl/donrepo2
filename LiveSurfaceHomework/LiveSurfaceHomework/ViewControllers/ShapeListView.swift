//
//  ShapeListView.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

protocol ShapeListViewDelegate: class {
  func setShape(atKey key: String, toVisible visible: Bool)
}

class ShapeListView: UIViewController {
  let table: UITableView = UITableView(frame: CGRect.zero, style: UITableView.Style.plain)
  let rightBorder: CALayer = CALayer()
  let rightBorderWidth: CGFloat = 1.0
  let rowHeight: CGFloat = 100.0
  static let bezierSize: CGSize = CGSize(width: 70, height: 70)
  
  weak var delegate: ShapeListViewDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.accessibilityIdentifier = "shapeList"
    view.backgroundColor = UIColor.lightGray
    table.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(table)
    table.rowHeight = rowHeight
    table.allowsSelection = false 
    rightBorder.backgroundColor = UIColor(named: "shapeListBorderColor")!.cgColor
    view.layer.addSublayer(rightBorder)
    
    NSLayoutConstraint.activate([
      table.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      table.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      table.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -rightBorderWidth),
      table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
    
    table.register(ShapeListCell.self, forCellReuseIdentifier: ShapeListCell.id)
    table.allowsSelection = true
    table.separatorStyle = .none
    table.dataSource = self
    table.reloadData()
  }


  override func viewWillLayoutSubviews() {
    super.viewWillLayoutSubviews()
    let x = view.bounds.origin.x + view.bounds.width - rightBorderWidth
    let y = view.bounds.origin.y
    let width = rightBorderWidth
    let height = view.bounds.height
    
    rightBorder.frame = CGRect(x: x, y: y, width: width, height: height)
  }
}

extension ShapeListView {
  func getDesiredBezierPathRect() -> CGRect {
    CGRect(x: 0, y: 0, width: ShapeListView.bezierSize.width, height: ShapeListView.bezierSize.height)
  }
  
  func refresh() {
    table.reloadData()
  }
}


extension ShapeListView: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    ModelStore.shared.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: ShapeListCell.id, for: indexPath) as? ShapeListCell else {
      fatalError("fail fast, fail early")
    }
    let model = ModelStore.shared[indexPath.item]
    cell.set(withModel: model, delegate: self, andIndexPath: indexPath)
    return cell
  }
}

extension ShapeListView: ShapeListCellDelegate {
  func shapeVisiblityToggled(isOn: Bool, forShapeAtIndexPath indexPath: IndexPath) {
    let model = ModelStore.shared[indexPath.item]
    let key = model.name
    if let delegate = delegate {
      delegate.setShape(atKey: key, toVisible: isOn)
    }
  }
  
}
