//
//  ShapeChooserVC.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

enum ShapeType: Int, CaseIterable {
  case rectangle = 0
  case roundedRectangle = 1
  case oval = 2
  case star = 3
  
  func getNameAndImage() -> (name: String, image: UIImage) {
    switch self {
      case .rectangle:
        return (name: "Rectangle", image: UIImage(named: "rectangle")!)
      case .roundedRectangle:
        return (name: "Rounded Rectangle", image: UIImage(named: "roundedRectangle")!)
      case .oval:
        return (name: "Oval", image: UIImage(named: "circle")!)
      case .star:
        return (name: "Star", image: UIImage(named: "star")!)
    }
  }
  
  func getDrawingShape(withName name: String) -> DrawingShape {
    let r: CGRect = CGRect.zero
    switch self {
      case .rectangle:
        return DrawingRectangle(name: name, rect: r)
      case .roundedRectangle:
        return DrawingRoundedRectangle(name: name, rect: r)
      case .oval:
        return DrawingOval(name: name, rect: r)
      case .star:
        return DrawingStar(name: name, rect: r)
    }
  }
}

protocol ShapeChooserVCDelegate: class {
  func shapeTypeSelected(_ shapeType: ShapeType, withShapeID shapeID: String)
  func shapeIDAvailable(_ shapeID: String) -> Bool
}

class ShapeChooserVC: UIViewController {
  let titleFont: UIFont = UIFont.preferredFont(forTextStyle: .headline)
  let vertPad: CGFloat = 8.0
    
  let label: UILabel = UILabel()
  let table: UITableView = UITableView(frame: CGRect.zero, style: .plain)
  
  weak var delegate: ShapeChooserVCDelegate?
    
  override func viewDidLoad() {
    super.viewDidLoad()
  
    label.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(label)
    NSLayoutConstraint.activate([
      label.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      label.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: vertPad),
    ])
    
    label.textColor = UIColor(named: "shapePickerTitleColor")!
    label.font = titleFont
    label.textAlignment = NSTextAlignment.center
    label.text = "CHOOSE A SHAPE"
    
    table.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(table)
    NSLayoutConstraint.activate([
      table.topAnchor.constraint(equalTo: label.bottomAnchor, constant: vertPad),
      table.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      table.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
    table.separatorStyle = .none
    table.register(ShapeChooserCell.self, forCellReuseIdentifier: ShapeChooserCell.id)
    table.rowHeight = UITableView.automaticDimension
    table.estimatedRowHeight = 44.0
    table.dataSource = self
    table.delegate = self 
    table.reloadData()
  }
}

extension ShapeChooserVC: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    ShapeType.allCases.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: ShapeChooserCell.id, for: indexPath) as? ShapeChooserCell,
      let shapeType = ShapeType(rawValue: indexPath.item) else {
      fatalError("cell table relationship not wired up correctly")
    }
    cell.set(shapeType: shapeType)
    return cell
  }
}

extension ShapeChooserVC: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if let shapeType = ShapeType(rawValue: indexPath.item) {
      getShapeIDFromUserAndDismiss(shapeType: shapeType)
    }
  }
  
  private func getShapeIDFromUserAndDismiss(shapeType: ShapeType) {
    guard let delegate = delegate else {
      return
    }
    let ac = UIAlertController(title: "Shape Name", message: "Please provide a unique Name for this Shape", preferredStyle: .alert)
    ac.addTextField(configurationHandler: { textField in
      textField.placeholder = "Shape Name"
      textField.clearButtonMode = UITextField.ViewMode.whileEditing
      textField.borderStyle = .roundedRect
      textField.textColor = UIColor.blue
    })
    
    let ok = UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
      guard let self = self else { return }
      guard let textField = ac.textFields?.first, let shapeID = textField.text, !shapeID.isEmpty else {
        return
      }
      if delegate.shapeIDAvailable(shapeID) {
        delegate.shapeTypeSelected(shapeType, withShapeID: shapeID)
        self.dismiss(animated: true)
        return
      } 
      self.showIDNotAvailableError()
    })
    ac.addAction(ok)
    present(ac, animated: true, completion: nil)
  }
  
  private func showIDNotAvailableError() {
    let acNotAvailable = UIAlertController(title: "Name in use",
                                           message: "That shape name is already in use",
                                           preferredStyle: .alert)
    
    let ok = UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
      guard let self = self else { return }
      self.dismiss(animated: true)
    })
    
    acNotAvailable.addAction(ok)
    self.present(acNotAvailable, animated: true)
  }
}
