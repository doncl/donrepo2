//
//  ShapeChooserCell.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

class ShapeChooserCell: UITableViewCell {
  static let id = "ShapeChooserCellID"
  private let shapeImage: UIImageView = UIImageView()
  private let name: UILabel = UILabel()
  let horzPad: CGFloat = 8
  let vertPad: CGFloat = 8
  let imageDim: CGFloat = 25.0
  let fontSize: CGFloat = 16.0
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    [shapeImage, name].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      contentView.addSubview($0)
    }
    
    shapeImage.contentMode = UIView.ContentMode.scaleAspectFit
    name.font = UIFont.systemFont(ofSize: fontSize, weight: UIFont.Weight.semibold)
    name.textAlignment = NSTextAlignment.left
    
    NSLayoutConstraint.activate(constraints: [
      shapeImage.topAnchor.constraint(equalTo: contentView.topAnchor),
      shapeImage.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: horzPad),
      shapeImage.widthAnchor.constraint(equalToConstant: imageDim),
      shapeImage.heightAnchor.constraint(equalToConstant: imageDim),
      
      name.centerYAnchor.constraint(equalTo: shapeImage.centerYAnchor),
      name.leadingAnchor.constraint(equalTo: shapeImage.trailingAnchor, constant: horzPad),
      name.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
      contentView.bottomAnchor.constraint(equalTo: shapeImage.bottomAnchor, constant: vertPad),
      
    ], andSetPriority: UILayoutPriority(rawValue: 999))
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func set(shapeType: ShapeType) {
    let tuple = shapeType.getNameAndImage()
    self.name.text = tuple.name
    shapeImage.image = tuple.image
  }
}
