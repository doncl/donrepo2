//
//  ShapeListCell.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

protocol ShapeListCellDelegate: class {
  func shapeVisiblityToggled(isOn: Bool, forShapeAtIndexPath indexPath: IndexPath)
}

class ShapeListCell: UITableViewCell {
  static let id = "ShapeListCellID"
  
  let horzPad: CGFloat = 8
  
  let name: UILabel = UILabel()
  let bezierImage: UIImageView = UIImageView()
  let toggle: UISwitch = UISwitch()
  let toggleLabel: UILabel = UILabel()
  let labelFont: UIFont = UIFont.systemFont(ofSize: 12.0, weight: UIFont.Weight.medium)
  
  weak var delegate: ShapeListCellDelegate?
  var indexPath: IndexPath?
    
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    selectionStyle = .none
    
    [name, bezierImage, toggleLabel, toggle].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      contentView.addSubview($0)
    }
    
    [name, toggleLabel].forEach {
      $0.lineBreakMode = NSLineBreakMode.byTruncatingTail
      $0.font = labelFont
    }
    
    name.textColor = UIColor(named: "shapeNameColor")!
    bezierImage.contentMode = UIView.ContentMode.scaleAspectFit
    
    toggleLabel.text = "Visible"
    toggle.isOn = true
    
    NSLayoutConstraint.activate(constraints: [
      bezierImage.widthAnchor.constraint(equalToConstant: ShapeListView.bezierSize.width),
      bezierImage.heightAnchor.constraint(equalToConstant: ShapeListView.bezierSize.height),
      bezierImage.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: horzPad),
      bezierImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
      
      name.leftAnchor.constraint(equalTo: bezierImage.rightAnchor, constant: horzPad),
      name.centerYAnchor.constraint(equalTo: bezierImage.centerYAnchor),
      name.rightAnchor.constraint(lessThanOrEqualTo: toggleLabel.leftAnchor, constant: -horzPad),
    
      toggle.centerYAnchor.constraint(equalTo: bezierImage.centerYAnchor),
      toggle.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -horzPad),
    
      toggleLabel.centerYAnchor.constraint(equalTo: name.centerYAnchor),
      toggleLabel.rightAnchor.constraint(equalTo: toggle.leftAnchor, constant: -horzPad),
      
    ], andSetPriority: UILayoutPriority(rawValue: 999))
    
    toggle.addTarget(self, action: #selector(ShapeListCell.switchToggled(_:)), for: UIControl.Event.valueChanged)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension ShapeListCell {
  func set(withModel model: Model, delegate: ShapeListCellDelegate, andIndexPath indexPath: IndexPath) {
    self.indexPath = indexPath
    self.delegate = delegate
    name.text = model.name
    let renderer = UIGraphicsImageRenderer(size: ShapeListView.bezierSize)
    let img = renderer.image { ctx in
      model.path.stroke()
    }
    bezierImage.image = img
  }
  
  @objc func switchToggled(_ sender: UISwitch) {
    guard let delegate = delegate, let indexPath = indexPath else {
      return
    }
    delegate.shapeVisiblityToggled(isOn: sender.isOn, forShapeAtIndexPath: indexPath)
  }
}
