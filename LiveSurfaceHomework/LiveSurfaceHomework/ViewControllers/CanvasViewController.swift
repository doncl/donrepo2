//
//  CanvasViewController.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

protocol CanvasViewControllerDelegate: class {
  func addShapeToLeftPane(_ shape: DrawingShape)
}

typealias EditTuple = (key: String, value: (drawingLayer: CAShapeLayer, boundingBoxLayer: CAShapeLayer))

enum EditAction {
  case move
  case scale
  case rotate
}

class CanvasViewController: UIViewController {
  static let boundingColor: UIColor = UIColor(white: 127 / 255, alpha: 0.6)
  
  var selectedShape: DrawingShape? {
    didSet {
      if let _ = selectedShape {
        view.isUserInteractionEnabled = true
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(CanvasViewController.panFired(_:)))
        view.addGestureRecognizer(panGesture)
        self.pan = panGesture
      } else {
        view.isUserInteractionEnabled = false
        if let pan = self.pan {
          view.removeGestureRecognizer(pan)
          self.pan = nil
        }
      }
    }
  }
  
  var editTuple: EditTuple?
  var editAction: EditAction?
  
  var pan: UIGestureRecognizer?
  
  var shapeLayers: [String: (drawingLayer: CAShapeLayer, boundingBoxLayer: CAShapeLayer)] = [:]
  var anchorPoint: CGPoint?
  
  weak var delegate: CanvasViewControllerDelegate?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.accessibilityIdentifier = "canvas"
    let interaction = UIContextMenuInteraction(delegate: self)
    view.addInteraction(interaction)
  }
}

// MARK: Actual drawing.
extension CanvasViewController {
  
  @objc func panFired(_ pan: UIPanGestureRecognizer) {
    if var editTuple = editTuple, let editAction = editAction {
      handlePanForEditTuple(pan: pan, editTuple: &editTuple, editAction: editAction)
    } else if var selectedShape = selectedShape {
      handlePanForInitiallySelectedShape(pan: pan, selectedShape: &selectedShape)
    }
  }
  
  private func handlePanForEditTuple(pan: UIPanGestureRecognizer, editTuple: inout EditTuple, editAction: EditAction) {
    switch pan.state {
      
      case .possible:
        break
      case .began:
        let point = pan.location(in: view)
        anchorPoint = point
      case .changed:
        guard let anchorPoint = anchorPoint else {
          return
        }
        let newPoint = pan.location(in: view)
        let dx = newPoint.x - anchorPoint.x
        let dy = newPoint.y - anchorPoint.y
        
        guard let drawingBezier = bezierFromCAShapeLayer(layer: editTuple.value.drawingLayer) else {
          return
        }
        guard let boundingBoxBezier = bezierFromCAShapeLayer(layer: editTuple.value.boundingBoxLayer) else {
          return
        }
        switch editAction {
          case .move:
            let offsetSize: CGSize = CGSize(width: dx, height: dy)
            drawingBezier.offset(by: offsetSize)
            boundingBoxBezier.offset(by: offsetSize)
          case .scale:
            let scaleX = dx / boundingBoxBezier.boundingBox.width
            let scaleY = dy / boundingBoxBezier.boundingBox.height
            drawingBezier.scale(sx: scaleX, sy: scaleY)
          case .rotate:
            let distance = dist(dx: dx, dy: dy)
            print("\(#function) - distance = \(distance)")
          break
      }

      case .ended, .cancelled, .failed:
        self.editAction = nil
        self.editTuple = nil
      @unknown default:
        self.editAction = nil
        self.editTuple = nil
    }
  }
  
  private func dist(dx: CGFloat, dy: CGFloat) -> CGFloat {
    return sqrt((dx * dx) + (dy * dy))
  }
  
  private func bezierFromCAShapeLayer(layer: CAShapeLayer) -> UIBezierPath? {
    guard let cgPath = layer.path else {
      return nil
    }
    return UIBezierPath(cgPath: cgPath)
  }
  
  private func handlePanForInitiallySelectedShape(pan: UIPanGestureRecognizer, selectedShape: inout DrawingShape) {
    switch pan.state {
      case .possible:
        break
      case .began:
        let point = pan.location(in: view)
        anchorPoint = point
      case .changed:
        if let anchorPoint = anchorPoint {
          let newPoint = pan.location(in: view)
          let width = newPoint.x - anchorPoint.x
          let height = newPoint.y - anchorPoint.y
          let rect = CGRect(x: anchorPoint.x, y: anchorPoint.y, width: width, height: height)
          let newPath = selectedShape.getPath(for: rect)
          let boundingPath = selectedShape.getBoundingPath()
          let shapeLayer = provisionDrawingShapeLayer(from: selectedShape)
          let boundingBoxLayer = provisionBoundingBoxShapeLayer(from: selectedShape)
          shapeLayer.path = newPath.cgPath
          boundingBoxLayer.path = boundingPath.cgPath
          shapeLayers[selectedShape.name] = (drawingLayer: shapeLayer, boundingBoxLayer: boundingBoxLayer)
      }
      case .ended:
        if let selectedShape = self.selectedShape, let delegate = delegate {
          delegate.addShapeToLeftPane(selectedShape)
        }
        anchorPoint = nil
      case .cancelled, .failed:
        anchorPoint = nil
        self.selectedShape = nil
      @unknown default:
        anchorPoint = nil
        self.selectedShape = nil
        self.editTuple = nil
    }
  }

  
  func shapeIDIsAvailable(_ shapeID: String) -> Bool {
    return shapeLayers[shapeID] == nil
  }
  
  func setShape(atKey key: String, toVisible visible: Bool) {
    if let tuple = shapeLayers[key] {
      tuple.drawingLayer.isHidden = !visible
      tuple.boundingBoxLayer.isHidden = !visible

    }
  }
  
  private func provisionDrawingShapeLayer(from selectedShape: DrawingShape) -> CAShapeLayer {
    if let tuple = shapeLayers[selectedShape.name] {
      return tuple.drawingLayer
    }
    
    let shapeLayer: CAShapeLayer = CAShapeLayer()
    shapeLayer.strokeColor = UIColor.blue.cgColor
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.lineWidth = 1.0
    view.layer.addSublayer(shapeLayer)
    return shapeLayer
  }
  
  private func provisionBoundingBoxShapeLayer(from selectedShape: DrawingShape) -> CAShapeLayer {
    if let tuple = shapeLayers[selectedShape.name] {
      return tuple.boundingBoxLayer
    }

    let shapeLayer: CAShapeLayer = CAShapeLayer()
    shapeLayer.strokeColor = CanvasViewController.boundingColor.cgColor
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.lineWidth = 1.0
    view.layer.addSublayer(shapeLayer)
    shapeLayer.isHidden = true // for now
    return shapeLayer
  }
}

extension CanvasViewController: UIContextMenuInteractionDelegate {
  func contextMenuInteraction(_ interaction: UIContextMenuInteraction,
                              configurationForMenuAtLocation location: CGPoint) -> UIContextMenuConfiguration? {
      
    guard let kvp = hitTest(location) else {
      return nil
    }
    editTuple = kvp
    let move = UIAction(title: "Move") { [unowned self] _ in
      kvp.value.boundingBoxLayer.isHidden = false
      self.editAction = EditAction.move
    }
      
    let scale = UIAction(title: "Scale") {[unowned self] _ in
      kvp.value.boundingBoxLayer.isHidden = false
      self.editAction = EditAction.scale
    }
    
    let rotate = UIAction(title: "Rotate") { [unowned self] _ in
      kvp.value.boundingBoxLayer.isHidden = false
      self.editAction = EditAction.rotate
    }
    
    return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { _ in
      return UIMenu(title: "Actions", children: [move, scale, rotate])
    }
  }
  
  private func hitTest(_ point: CGPoint) -> EditTuple? {
    let layers = Array(shapeLayers.values)
    guard let last = layers.last(where: { tuple in
      guard let cgPath = tuple.boundingBoxLayer.path else {
        return false
      }
      let bezier = UIBezierPath(cgPath: cgPath)
      return bezier.boundingBox.contains(point)
    }) else {
      return nil
    }

    // There is nothing efficient about this, but it's a hitTest
    for kvp in shapeLayers {
      if kvp.value.boundingBoxLayer == last.boundingBoxLayer && kvp.value.drawingLayer == last.drawingLayer {
        return (key: kvp.key, value: (drawingLayer: kvp.value.drawingLayer, boundingBoxLayer: kvp.value.boundingBoxLayer))
      }
    }
    return nil
  }
}
