//
//  Model.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation
import UIKit

struct Model {
  let path: UIBezierPath
  let name: String
}

struct ModelStore {
  private var store: [Model] = []
  
  var count: Int {
    return store.count
  }
  
  subscript(index: Int) -> Model {
    return store[index]
  }
  
  subscript(index: String) -> Model? {
    return store.first(where: {$0.name == index })
  }
  
  private init() {
    
  }
  
  static var shared: ModelStore = ModelStore()
  
  mutating func add(path: UIBezierPath, withName name: String) {
    let model = Model(path: path, name: name)
    store.append(model)
  }
}
