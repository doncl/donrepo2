//
//  NSLayoutConstraintExtensions.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
  class func activate(constraints: [NSLayoutConstraint], andSetPriority priority: UILayoutPriority) {
    constraints.forEach {
      $0.priority = priority
      $0.isActive = true
    }
  }
}
