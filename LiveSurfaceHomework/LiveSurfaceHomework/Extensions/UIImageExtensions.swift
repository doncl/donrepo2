//
//  UIImageExtensions.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

extension UIImage {
  static func from(color: UIColor, ofSize size: CGSize) -> UIImage {
    let renderer = UIGraphicsImageRenderer(size: size)
    let img = renderer.image(actions: { (ctx: UIGraphicsImageRendererContext) in
      color.setFill()
      ctx.fill(CGRect(origin: .zero, size: size))
    })
    return img
  }
}
