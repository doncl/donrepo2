//
//  CGRectExtensions.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

extension CGRect {
  var center : CGPoint {
    return CGPoint(x: midX, y: midY)
  }
  
  // What do we have to scale by to make it fit (with letterboxing or pillarboxing)?
  func aspectScaleFit(destSize: CGSize) -> CGFloat {
    let dimsScale = getDimensionsScale(destSize: destSize)
    
    return min(dimsScale.0, dimsScale.1)
  }
  fileprivate func getDimensionsScale(destSize : CGSize) -> (CGFloat, CGFloat) {
    let scaleW = size.width == 0 ? 0 : destSize.width / size.width
    let scaleH = size.height == 0 ? 0 : destSize.height / size.height

    return (scaleW, scaleH)
  }

  func fit(into destRect : CGRect) -> CGRect {
    let aspect = aspectScaleFit(destSize: destRect.size)
    let targetSize = size.scale(byFactor: aspect)
    return CGRect.rectAroundCenter(center: destRect.center, size: targetSize)
  }

  static func rectAroundCenter(center : CGPoint, size: CGSize) -> CGRect {
    let halfWidth = size.width / 2.0
    let halfHeight = size.height / 2.0
    
    return CGRect(x: center.x - halfWidth, y: center.y - halfHeight, width: size.width,
      height: size.height)
  }
}
