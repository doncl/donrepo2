//
//  UIViewControllerExtensions.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//


import UIKit
extension UIViewController {
  func isRegularSizeClass() -> Bool {
    return isRegularSizeClass(thisTraitCollection: traitCollection)
  }

  func isRegularSizeClass(thisTraitCollection: UITraitCollection) -> Bool {
    return thisTraitCollection.horizontalSizeClass == .regular && thisTraitCollection.verticalSizeClass == .regular
  }
}
