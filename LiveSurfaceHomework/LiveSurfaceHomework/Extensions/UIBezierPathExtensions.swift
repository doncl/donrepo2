//
//  UIBezierPathExtensions.swift
//  LiveSurfaceHomework
//
//  Created by Don Clore on 8/16/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import UIKit

extension UIBezierPath {
  var boundingBox : CGRect {
    return cgPath.boundingBoxOfPath
  }
  
  var boundingBoxCenter : CGPoint {
    return boundingBox.center
  }
  
  func fit(to destRect: CGRect) {
    let bounds = boundingBox
    let fitRect = bounds.fit(into: destRect)
    let scaleValue = bounds.aspectScaleFit(destSize: destRect.size)
    let newCenter = fitRect.center
    moveCenter(to: newCenter)
    scale(sx: scaleValue, sy: scaleValue)
  }
  
  func scale(sx : CGFloat, sy: CGFloat) {
    let t = CGAffineTransform(scaleX: sx, y: sy)
    applyCentered(transform: t)
  }

  // It's kind of counter-intuitive why you need to translate
  // the path to the center of the bounding box; there's a good video diagram
  // of it in Wenderlich's Core Video courses (either Beginner or Intermediate,
  // can't remember).   But basically, you need to move the  entire path over
  // such that its edge is exactly on the center of where it was, then apply
  // the desired transform, then move it back.
  func applyCentered(transform: CGAffineTransform) {
    let center = boundingBoxCenter
    var t : CGAffineTransform = .identity
    t = t.translatedBy(x: center.x, y: center.y)
    t = transform.concatenating(t)
    t = t.translatedBy(x: -center.x, y: -center.y)
    apply(t)
  }
  
  // In radians, of couse.
  func rotate(angleInRadians : CGFloat) {
    let t = CGAffineTransform(rotationAngle: angleInRadians)
    applyCentered(transform: t)
  }
    
  func moveCenter(to destPoint : CGPoint) {
    let bounds = boundingBox
    let p1 = bounds.origin
    let p2 = destPoint
    var vector = CGSize(width: p2.x - p1.x, height: p2.y - p1.y)
    vector.width -= bounds.size.width / 2.0
    vector.height -= bounds.size.height / 2.0
    offset(by: vector)
  }

  func offset(by offset: CGSize) {
    let t = CGAffineTransform(translationX: offset.width, y: offset.height)
    applyCentered(transform: t)
  }
  
  static func starShape(numberOfInflections: Int, percentInflection : CGFloat) -> UIBezierPath {
    guard numberOfInflections > 2 else {
      fatalError("not permitted")
    }
      
    let path = UIBezierPath()
    let destinationRect = CGRect(x: 0, y: 0, width: 1, height: 1)
    let center = destinationRect.center
    let r : CGFloat = 0.5
    let rr = r * (1.0 + percentInflection)
      
    var firstPoint = true
     
    // Cast the int to CGFloat just once.
    let fNumberOfInflections : CGFloat = CGFloat(numberOfInflections)
  
    for i in 0..<numberOfInflections {
      let theta = CGFloat(i) * CGFloat.pi * 2.0 / fNumberOfInflections
      let dTheta = CGFloat.pi * 2.0 / fNumberOfInflections
      
      if firstPoint {
        let xa = center.x + r * sin(theta)
        let ya = center.y + r * cos(theta)
        let pa = CGPoint(x: xa, y: ya)
        path.move(to: pa)
        firstPoint = false
      }
      
      let cp1x = center.x + rr * sin(theta + dTheta / 2)
      let cp1y = center.y + rr * cos(theta + dTheta / 2)
      let cp1 = CGPoint(x: cp1x, y: cp1y)
      
      let xb = center.x + r * sin(theta + dTheta)
      let yb = center.y + r * cos(theta + dTheta)
      let pb = CGPoint(x: xb, y: yb)
      
      path.addLine(to: cp1)
      path.addLine(to: pb)
    }
    
    path.close()
   
    return path
  }
}
