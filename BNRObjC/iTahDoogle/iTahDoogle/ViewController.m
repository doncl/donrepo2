//
//  ViewController.m
//  iTahDoogle
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "ViewController.h"

static NSString *cellID = @"CellId";

NSString *BBPDocPath(void);

@interface ViewController ()
@end

@implementation ViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
  NSArray *plist = [NSArray arrayWithContentsOfFile:BBPDocPath()];
  if (plist) {
    self.tasks = [plist mutableCopy];
  } else {
    self.tasks = [NSMutableArray new];
  }

  self.view.backgroundColor = [UIColor whiteColor];
  
  _taskField = [UITextField new];
  _taskTable = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
  _taskTable.separatorColor = [UIColor clearColor];
  _insertButton = [UIButton new];
  
  _taskField.translatesAutoresizingMaskIntoConstraints = NO;
  [self.view addSubview:_taskField];
  
  _insertButton.translatesAutoresizingMaskIntoConstraints = NO;
  [self.view addSubview:_insertButton];
  
  _taskTable.translatesAutoresizingMaskIntoConstraints = NO;
  [self.view addSubview:_taskTable];
  
  [NSLayoutConstraint activateConstraints:@[
    [_taskField.topAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.topAnchor constant: 8],
    [_taskField.leadingAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.leadingAnchor constant:8],
    [_taskField.trailingAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.trailingAnchor constant:-88],
    [_taskField.heightAnchor constraintEqualToConstant:31],
    
    [_insertButton.topAnchor constraintEqualToAnchor:_taskField.topAnchor],
    [_insertButton.leadingAnchor constraintEqualToAnchor:_taskField.trailingAnchor constant:8],
    [_insertButton.trailingAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.trailingAnchor constant:-8],
    [_insertButton.heightAnchor constraintEqualToAnchor:_taskField.heightAnchor],
    
    [_taskTable.topAnchor constraintEqualToAnchor:_taskField.bottomAnchor constant:8],
    [_taskTable.leadingAnchor constraintEqualToAnchor:_taskField.leadingAnchor],
    [_taskTable.trailingAnchor constraintEqualToAnchor:_insertButton.trailingAnchor],
    [_taskTable.bottomAnchor constraintEqualToAnchor:self.view.safeAreaLayoutGuide.bottomAnchor constant:-8],
  ]];
  
  _taskField.borderStyle = UITextBorderStyleRoundedRect;
  _taskField.placeholder = @"Type a task, tap Insert.";
  
  [_insertButton setTitle:@"Insert" forState:UIControlStateNormal];
  [_insertButton setTitleColor:[UIColor systemBlueColor] forState:UIControlStateNormal];
  [_insertButton setUserInteractionEnabled:YES];
  
  UIColor *borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.8];
  _insertButton.layer.borderColor = borderColor.CGColor;
  _insertButton.layer.borderWidth = 1.0;
  _insertButton.layer.cornerRadius = 15.0;
  
  _taskTable.layer.borderColor = borderColor.CGColor;
  _taskTable.layer.borderWidth = 1.0;
  
  [_insertButton addTarget:self
                    action:@selector(addTask:)
          forControlEvents:UIControlEventTouchUpInside];
  
  [self.taskTable registerClass:UITableViewCell.class forCellReuseIdentifier:cellID];
  self.taskTable.dataSource = self;
  [self.taskTable reloadData];

  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(enteredBackground:)
                                               name:UIApplicationWillResignActiveNotification
                                             object:nil];
}

- (void)dealloc
{
  [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)addTask:(id)sender {
  NSString *text = self.taskField.text;
  
  if (!text.length) {
    return;
  }
  
  NSLog(@"Task entered: %@", text);
  
  [self.tasks addObject:text];
  
  [self.taskTable reloadData];
  
  // clear out text field
  self.taskField.text = @"";
  [self.taskField resignFirstResponder];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.tasks.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID forIndexPath:indexPath];
  
  NSString *item = self.tasks[indexPath.item];
  cell.textLabel.text = item;
  
  return cell;
}

- (void)enteredBackground:(NSNotification *)note
{
  NSLog(@"Entered Background");
  [self.tasks writeToFile:BBPDocPath() atomically: YES];
}

@end

NSString *BBPDocPath() {
  NSArray *pathList = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  return [pathList[0] stringByAppendingPathComponent:@"data.td"];
}
