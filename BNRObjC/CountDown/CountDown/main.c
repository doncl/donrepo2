//
//  main.c
//  CountDown
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
  
  int n = 99;
  
  for (int i = n; i >= 0; i -= 3) {
    printf("%d\n", i);
    if (i % 5 == 0) {
      printf("Found one!\n");
    }
  }
  
  return 0;
}
