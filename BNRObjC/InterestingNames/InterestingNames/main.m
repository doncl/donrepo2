//
//  main.m
//  InterestingNames
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    // Read in a file as a huge string (ignoring the possibility of an error)
    NSString *nameString = [NSString stringWithContentsOfFile:@"/usr/share/dict/propernames"
                                                     encoding:NSUTF8StringEncoding
                                                        error:nil];
    
    // Break it into an array of string
    NSArray *names = [nameString componentsSeparatedByString:@"\n"];
    
    NSString *wordString = [NSString stringWithContentsOfFile:@"/usr/share/dict/words" encoding:NSUTF8StringEncoding error:nil];
    NSArray *words = [wordString componentsSeparatedByString:@"\n"];
  
    
    // Go through the array one string at a time
    for (NSString *n in names) {
      for (NSString *w in words) {
        NSRange r = [n rangeOfString:w options:NSCaseInsensitiveSearch];
        // Was it found?
        if (r.location != NSNotFound) {
          NSLog(@"Name = %@, Word = %@", n, w);
        }
      }
    }
    
  }
  return 0;
}
