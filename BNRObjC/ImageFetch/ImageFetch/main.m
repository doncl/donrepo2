//
//  main.m
//  ImageFetch
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>



int main(int argc, const char * argv[]) {
  @autoreleasepool {
    NSURL *url = [NSURL URLWithString:@"https://maven-user-video-thumbnails.s3.amazonaws.com/18Q8LnC4N0iF3z9zRWA0DQ/roamingmillennial/culture/ac3ad40b-6b40-42d0-9b1e-2106885decd1/ac3ad40b-6b40-42d0-9b1e-2106885decd1thumb300000001.png"];
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(0);
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^void(NSData *data, NSURLResponse *response, NSError *error) {
      if (!data) {
        NSLog(@"Fetch failed: %@", [error localizedDescription]);
        dispatch_semaphore_signal(semaphore);
        return;
      }
      NSLog(@"The file is %lu bytes", (unsigned long) [data length]);
      
      BOOL written = [data writeToFile:@"/tmp/google.png" options:NSDataWritingAtomic error: &error];
      if (!written) {
        NSLog(@"write failed: %@", error.localizedDescription);
        dispatch_semaphore_signal(semaphore);
        return;
      }
      NSLog(@"success!");
      
      NSData *readData = [NSData dataWithContentsOfFile:@"/tmp/google.png"];
      NSLog(@"The file read from the disk has %lu bytes", (unsigned long)[readData length]);
      dispatch_semaphore_signal(semaphore);
    }];
    
    [task resume];
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
  }
  

  return 0;
}
