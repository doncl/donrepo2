//
//  main.c
//  TwoFloats
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
  
  float f1 = 3.14;
  float f2 = 42.0;
  
  double d1 = f1 + f2;
  
  printf("sum is %lf\n", d1);
  
  return 0;
}
