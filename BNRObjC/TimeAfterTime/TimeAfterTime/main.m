//
//  main.m
//  TimeAfterTime
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    
    NSDate *now = [[NSDate alloc] init];
    NSLog(@"This NSDate object lives at %p", now);
    NSLog(@"The date is %@", now);
    
    double seconds = [now timeIntervalSince1970];
    NSLog(@"It has been %f seconds since the start of 1970", seconds);
    
    NSDate *later = [now dateByAddingTimeInterval:100000];
    NSLog(@"in 100000 seconds, it will be %@", later);
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSLog(@"My calendar is %@", [cal calendarIdentifier]);
    
    unsigned long day = [cal ordinalityOfUnit:NSCalendarUnitDay
                                       inUnit:NSCalendarUnitMonth
                                      forDate:now];
    
    NSLog(@"This is day %lu of the month", day);
    
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:1956];
    [comps setMonth:4];
    [comps setDay: 8];
    [comps setHour: 5];
    [comps setMinute:28];
    
    NSCalendar *g = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDate *dateOfBirth = [g dateFromComponents:comps];
    
    NSDate *rightNow = [NSDate new];
    NSTimeInterval aliveSeconds = [rightNow timeIntervalSinceDate:dateOfBirth];
    NSLog(@"I've been alive %@ seconds", @(aliveSeconds));
    
    return 0;
  }
  return 0;
}
