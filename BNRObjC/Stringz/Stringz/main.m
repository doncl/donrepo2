//
//  main.m
//  Stringz
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    NSMutableString *str = [NSMutableString new];
    for (int i = 0; i < 10; i++) {
      [str appendString:@"Aaron is cool!\n"];
    }
    
    NSError *error;
    
    BOOL success = [str writeToFile:@"/tmp/cool.txt" atomically:YES encoding:NSUTF8StringEncoding error:&error];
    if (success) {
      NSLog(@"done writing /tmp/cool/txt");
    } else {
      NSLog(@"writing /tmp/cool.txt failed %@", [error localizedDescription]);
    }
    
    NSString *resolveStr = [[NSString alloc] initWithContentsOfFile:@"/etc/resolv.conf" encoding:NSASCIIStringEncoding error:&error];
    
    if (!resolveStr) {
      NSLog(@"read filed: %@", error.localizedDescription);
    } else {
      NSLog(@"resolv.conf looks like this: %@", resolveStr);
    }

  }
  return 0;
}
