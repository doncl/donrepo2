//
//  main.m
//  ReadALine
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <readline/readline.h>
#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    NSLog(@"Who is cool? ");
    const char *name = readline(NULL);
    
    NSString *nsName = [NSString stringWithUTF8String:name];
    NSLog(@"NSName is %@", nsName);
  }
  return 0;
}
