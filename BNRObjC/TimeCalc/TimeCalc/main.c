//
//  main.c
//  TimeCalc
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>
#include <time.h>

int main(int argc, const char * argv[]) {
  // insert code here...
  long secondsSince1970 = time(NULL);
  long secondsSince1970FourMillionSecondsFromNow = secondsSince1970 + 4000000;
  
  struct tm now;
  localtime_r(&secondsSince1970FourMillionSecondsFromNow, &now);
  
  int year = 1970 + now.tm_year;
  int month = now.tm_mon + 1;
  int day = now.tm_mday + 1;
  
  printf("the date will be %d-%d-%d", month, day, year);
  
  
  return 0;
}
