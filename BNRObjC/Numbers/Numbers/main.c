//
//  main.c
//  Numbers
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


int main(int argc, const char * argv[]) {
  double y = 12345.6789;
  printf("y is %.2f\n", y);
  printf("y is %.2e\n", y);
  
  double s = sin(1);
  printf("Sin of 1 radian = %.3f\n", s);
  return 0;
}
