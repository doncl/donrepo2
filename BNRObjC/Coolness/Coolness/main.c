//
//  main.c
//  Coolness
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <readline/readline.h>
#include <stdio.h>

int main(int argc, const char * argv[]) {

  printf("Were should I start counting? ");
  const char *input = readline(NULL);
  int n = atoi(input);
  
  for (int i = n; i >= 0; i -= 3) {
    printf("%d\n", i);
    if (i % 5 == 0) {
      printf("Found one!\n");
    }
  }
  
  return 0;
}
