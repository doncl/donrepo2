//
//  main.m
//  Stocks
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BBPStockHolding.h"
#import "BBPForeignStockHolding.h"
#import "BBPPortfolio.h"

void print(NSArray<BBPStockHolding *> *holdings);

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    NSMutableArray<BBPStockHolding *> *holdings = [NSMutableArray<BBPStockHolding *> new];
    BBPStockHolding *stock1 = [BBPStockHolding new];
    stock1.purchaseSharePrice = 2.30;
    stock1.currentSharePrice = 4.50;
    stock1.numberOfShares = 40;
    stock1.symbol = @"ABC";
    
    BBPStockHolding *stock2 = [BBPStockHolding new];
    [stock2 setPurchaseSharePrice:12.19];
    [stock2 setCurrentSharePrice:10.56];
    [stock2 setNumberOfShares:90];
    stock2.symbol = @"BCD";
    
    BBPStockHolding *stock3 = [BBPStockHolding new];
    stock3.purchaseSharePrice = 45.10;
    stock3.currentSharePrice = 49.51;
    stock3.numberOfShares = 210;
    stock3.symbol = @"CDE";
    
    [holdings addObject:stock1];
    [holdings addObject:stock2];
    [holdings addObject:stock3];
    
    
    BBPForeignStockHolding *stock4 = [BBPForeignStockHolding new];
    stock4.purchaseSharePrice = 2.30;
    stock4.currentSharePrice = 4.50;
    stock4.numberOfShares = 40;
    stock4.conversionRate = 0.94;
    stock4.symbol = @"DEF";
    
    [holdings addObject:stock4];
    
    BBPPortfolio *port = [BBPPortfolio new];
    port.holdings = holdings;
    unsigned int portValue = port.portfolioValue;
    
    print(port.holdings);
    
    NSArray<BBPStockHolding *> *topPricedHoldings = [port getThreeMostValuableHoldings];
    NSLog(@"Three top stocks\n\n");
    print(topPricedHoldings);
    
    NSArray<BBPStockHolding *> *sortedHoldings = [port getSortedHoldings];
    print(sortedHoldings);

  }
  return 0;
}

void print(NSArray<BBPStockHolding *> *holdings)
{
  NSLog(@"");
  NSLog(@"");

  for (BBPStockHolding *stock in holdings) {
    NSLog(@"++++++++++++++++++++++++++++++++++++++++++");
    NSLog(@"Symbol = %@", stock.symbol);
    NSLog(@"Purchase Price = $%.2f", stock.purchaseSharePrice);
    NSLog(@"Current Price = $%.2f", stock.currentSharePrice);
    NSLog(@"Number of Shares = %d", stock.numberOfShares);
    NSLog(@"Cost in dollars = $%.2f", stock.costInDollars);
    NSLog(@"Value in dollars = $%.2f", stock.valueInDollars);
    NSLog(@"++++++++++++++++++++++++++++++++++++++++++");
  }
}
