//
//  BBPStockHolding.h
//  Stocks
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BBPStockHolding : NSObject
{
  float _purchaseSharePrice;
  float _currentSharePrice;
  int _numberOfShares;
}
@property (nonatomic, copy) NSString *symbol;
- (float)purchaseSharePrice;
- (void)setPurchaseSharePrice:(float)sharePrice;
- (float)currentSharePrice;
- (void)setCurrentSharePrice:(float)sharePrice;
- (int)numberOfShares;
- (void)setNumberOfShares:(int)numShares;

- (float)costInDollars;
- (float)valueInDollars;
@end

NS_ASSUME_NONNULL_END
