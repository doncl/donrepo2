//
//  BBPPortfolio.m
//  Stocks
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "BBPPortfolio.h"
#import "BBPStockHolding.h"

@interface BBPPortfolio ()
{
  NSMutableArray<BBPStockHolding *> *_holdings;
}
@end

@implementation BBPPortfolio

- (void)setHoldings:(NSArray<BBPStockHolding *> *)holdings
{
  _holdings = [holdings mutableCopy];
}

- (void)add:(BBPStockHolding *)holding
{
  if (!_holdings) {
    _holdings = [NSMutableArray<BBPStockHolding *> new];
  }
  [_holdings addObject:holding];
}

- (unsigned int) portfolioValue
{
  unsigned int ret = 0;
  for (BBPStockHolding *holding in _holdings) {
    ret += holding.valueInDollars;
  }
  return ret;
}

- (NSArray<BBPStockHolding *> *)getThreeMostValuableHoldings
{
  NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"valueInDollars" ascending:YES];
  [_holdings sortUsingDescriptors:@[sd]];
  NSUInteger loc = _holdings.count;
  NSUInteger len = _holdings.count >= 3 ? 3 : _holdings.count;
  loc -= len;
  NSRange endRange = NSMakeRange(loc, len);
  NSArray<BBPStockHolding *> *lastElements = [_holdings subarrayWithRange:endRange];
  return lastElements;
}

- (NSArray<BBPStockHolding *> *)getSortedHoldings
{
  NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"symbol" ascending:YES];
  [_holdings sortUsingDescriptors:@[sd]];
  return _holdings;
}
@end
