//
//  BBPForeignStockHolding.m
//  Stocks
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "BBPForeignStockHolding.h"

@implementation BBPForeignStockHolding
- (float) costInDollars
{
  return [super costInDollars] * self.conversionRate;
}

- (float) valueInDollars
{
  return [super valueInDollars] * self.conversionRate;
}
@end
