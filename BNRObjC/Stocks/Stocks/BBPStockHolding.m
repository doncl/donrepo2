//
//  BBPStockHolding.m
//  Stocks
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "BBPStockHolding.h"

@implementation BBPStockHolding
- (float)purchaseSharePrice
{
  return _purchaseSharePrice;
}

- (void)setPurchaseSharePrice:(float)sharePrice
{
  _purchaseSharePrice = sharePrice;
}

- (float)currentSharePrice
{
  return _currentSharePrice;
}

- (void)setCurrentSharePrice:(float)sharePrice
{
  _currentSharePrice = sharePrice;
}

- (int)numberOfShares
{
  return _numberOfShares;
}

- (void)setNumberOfShares:(int)numShares
{
  _numberOfShares = numShares;
}

- (float)costInDollars
{
  return [self purchaseSharePrice]  * [self numberOfShares];
}

- (float)valueInDollars
{
  return self.currentSharePrice * self.numberOfShares;
}
@end
