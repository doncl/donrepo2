//
//  BBPPortfolio.h
//  Stocks
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BBPStockHolding;

NS_ASSUME_NONNULL_BEGIN
@interface BBPPortfolio : NSObject

@property (nonatomic, copy) NSArray<BBPStockHolding *> *holdings;
@property (nonatomic, readonly) unsigned int portfolioValue;

- (void)add:(BBPStockHolding *)holding;
- (NSArray<BBPStockHolding *> *)getThreeMostValuableHoldings;
- (NSArray<BBPStockHolding *> *)getSortedHoldings;
@end

NS_ASSUME_NONNULL_END
