//
//  BBPForeignStockHolding.h
//  Stocks
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "BBPStockHolding.h"

NS_ASSUME_NONNULL_BEGIN

@interface BBPForeignStockHolding : BBPStockHolding
@property (nonatomic) float conversionRate;
@end

NS_ASSUME_NONNULL_END
