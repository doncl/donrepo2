//
//  main.m
//  MyComputerName
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    NSHost *me = [NSHost currentHost];
    NSString *name = [me localizedName];
    NSLog(@"My computer name is %@", name);
  }
  return 0;
}
