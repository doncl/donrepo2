//
//  main.c
//  Squarer
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>

int squarer(int n) {
  int square = n * n;
  printf("\"%d\" squared is \"%d\"\n", n, square);
  return square;
}

int main(int argc, const char * argv[]) {
  squarer(5);
  
  return 0;
}
