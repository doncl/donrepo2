//
//  main.m
//  TimesTwo
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>


int main(int argc, const char * argv[]) {
  @autoreleasepool {
    NSDate *currentTime = [NSDate new];
    NSLog(@"currentTime's value is %p", currentTime);
    
    sleep(2);
    
    NSDate *startTime = currentTime;
    
    currentTime = [NSDate new];
    NSLog(@"currentTime's value is now %p");
    NSLog(@"The address of the original object is %p", startTime);    
  }
  return 0;
}
