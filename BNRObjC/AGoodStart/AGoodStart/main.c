//
//  main.c
//  AGoodStart
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
  // Print the beginning of the novel
  printf("It was the best of times.\n");
  printf("It was the worst of times.\n");
  /* Is that actually any good?  Maybe it needs a rewrite. */
  return 0;
}
