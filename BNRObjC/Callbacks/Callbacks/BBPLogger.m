//
//  BBPLogger.m
//  Callbacks
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "BBPLogger.h"

@implementation BBPLogger
- (NSString *)lastTimeString
{
  static NSDateFormatter *dateFormatter = nil;
  if (!dateFormatter) {
    dateFormatter = [NSDateFormatter new];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
    NSLog(@"Created dateFormatter");
  }
  return [dateFormatter stringFromDate:self.lastTime];
}

- (void)updateLastTime:(NSTimer *)t
{
  NSDate *now = [NSDate new];
  self.lastTime = now;
  NSLog(@"Just set time to %@", self.lastTimeString);
}
@end
