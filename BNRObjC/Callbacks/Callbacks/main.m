//
//  main.m
//  Callbacks
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BBPLogger.h"

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    
    BBPLogger *logger = [BBPLogger new];
    
    NSURL *url = [NSURL URLWithString:@"https://maven-user-video-thumbnails.s3.amazonaws.com/18Q8LnC4N0iF3z9zRWA0DQ/roamingmillennial/culture/ac3ad40b-6b40-42d0-9b1e-2106885decd1/ac3ad40b-6b40-42d0-9b1e-2106885decd1thumb300000001.png"];
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
      if (error || !data || !response) {
        NSLog(@"Something went wrong.");
        return;
      }
      
      
    }];
    
    
    __unused NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:2.0
                                                               target:logger
                                                             selector:@selector(updateLastTime:)
                                                             userInfo:nil
                                                              repeats:YES];
    
   [task resume];
   
    [[NSRunLoop currentRunLoop] run];
  }
  return 0;
}
