//
//  main.c
//  BMICalc
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

typedef struct {
  float heightInMeters;
  int   weightInKilos;
} Person;

float bodyMassIndex(Person *p) {
  return p->weightInKilos / (p->heightInMeters * p->heightInMeters);
}

int main(int argc, const char * argv[]) {

  Person *mikey = (Person *)malloc(sizeof(Person));
  
  mikey->heightInMeters = 1.7;
  mikey->weightInKilos = 96;
  
  Person *aaron = (Person *)malloc(sizeof(Person));
  aaron->heightInMeters = 1.97;
  aaron->weightInKilos = 84;
  
  float bmi;
  bmi = bodyMassIndex(mikey);
  printf("mikey has a BMI of %.2f\n", bmi);
  
  bmi = bodyMassIndex(aaron);
  printf("aaron has a BMI of %.2f\n", bmi);
  
  free(mikey);
  free(aaron);
  
  mikey = NULL;
  aaron = NULL;
  
  return 0;
}
