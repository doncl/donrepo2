//
//  main.m
//  VowelMovement
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ArrayEnumerationBlock)(id, NSUInteger, BOOL *);

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    NSArray<NSString *> *originalStrings = @[@"Sauerkraut", @"Raygun", @"Big Nerd Ranch", @"Mississippi"];
    
    NSLog(@"original strings: %@", originalStrings);
    
    NSMutableArray<NSString *> *devowelizedStrings = [NSMutableArray<NSString *> new];
    
    // Create a list of characters to be removed from the string
    NSArray<NSString *> *vowels = @[@"a", @"e", @"i", @"o", @"u"];
    
    ArrayEnumerationBlock devowelizer;
    
  //    devowelizer = ^(id string, NSUInteger i, BOOL *stop) {
  //      NSRange yRange = [string rangeOfString:@"y" options:NSCaseInsensitiveSearch];
  //
  //      if (yRange.location != NSNotFound) {
  //        *stop = YES;
  //        return;
  //      }
  //      NSMutableString *newString = [NSMutableString stringWithString:string];
  //      for (NSString *s in vowels) {
  //        NSRange fullRange = NSMakeRange(0, newString.length);
  //        [newString replaceOccurrencesOfString:s withString:@"" options:NSCaseInsensitiveSearch range:fullRange];
  //      }
  //      [devowelizedStrings addObject:newString];
  //    };
  //
    [originalStrings enumerateObjectsUsingBlock:^(id string, NSUInteger i, BOOL *stop) {
      NSRange yRange = [string rangeOfString:@"y" options:NSCaseInsensitiveSearch];
      
      if (yRange.location != NSNotFound) {
        *stop = YES;
        return;
      }
      NSMutableString *newString = [NSMutableString stringWithString:string];
      for (NSString *s in vowels) {
        NSRange fullRange = NSMakeRange(0, newString.length);
        [newString replaceOccurrencesOfString:s withString:@"" options:NSCaseInsensitiveSearch range:fullRange];
      }
      [devowelizedStrings addObject:newString];
    }];
    
    NSLog(@"devowelized string: %@", devowelizedStrings);
    
  }
  return 0;
}
