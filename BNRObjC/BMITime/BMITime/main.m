//
//  main.m
//  BMITime
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BBPEmployee.h"
#import "BBPAsset.h"

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    
    // Create an array of BBPEmployee objects
    NSMutableArray<BBPEmployee *> *employees = [NSMutableArray<BBPEmployee *> new];
    NSMutableDictionary<NSString *, BBPEmployee *> *executives = [NSMutableDictionary<NSString *, BBPEmployee *> new];
    
    
    for (int i = 0; i < 10; i++) {
      BBPEmployee *mikey = [BBPEmployee new];
    

      mikey.weightInKilos = 90 + i;
      mikey.heightInMeters = 1.8 - i / 10.0;
      mikey.employeeID = i;
      
      NSDateFormatter *df = [NSDateFormatter new];
      df.dateFormat = @"MMM d, yyyy";
      mikey.hireDate = [df dateFromString:@"Aug 2, 2010"];
      
      [employees addObject:mikey];
      
      if (i == 0) {
        [executives setObject:mikey forKey:@"CEO"];
      }
      if (i == 1) {
        [executives setObject:mikey forKey:@"CTO"];
      }
    }
    
    NSMutableArray<BBPAsset *> *allAssets = [NSMutableArray<BBPAsset *> new];
    
    // Create 10 assets
    for (int i = 0; i < 10; i++) {
      BBPAsset *asset = [BBPAsset new];
      
      // Give it an interesting label
      NSString *currentLabel = [NSString stringWithFormat:@"Laptop %d", i];
      asset.label = currentLabel;
      asset.resaleValue = 350 + i * 17;
      
      // Get a random number between 0 and 9 inclusive
      NSUInteger randomIndex = random() % [employees count];
      
      // Find that employee
      BBPEmployee *randomEmployee = [employees objectAtIndex:randomIndex];
      
      [randomEmployee addAsset:asset];
      
      [allAssets addObject:asset];
    }
    
    NSSortDescriptor *voa = [NSSortDescriptor sortDescriptorWithKey:@"valueOfAssets" ascending:YES];
    NSSortDescriptor *eid = [NSSortDescriptor sortDescriptorWithKey:@"employeeID" ascending:YES];
    
    [employees sortUsingDescriptors:@[voa, eid]];
    
    NSLog(@"Employees: %@", employees);
    
    NSLog(@"Giving up ownership of one employee");
    
    [employees removeObjectAtIndex:5];
    
    NSLog(@"allAssets: %@", allAssets);
    
    // Print out the entire dictionary
    NSLog(@"executives: %@", executives);
    
    // Print out the CEO's information
    NSLog(@"CEO: %@", executives[@"CEO"]);
    executives = nil;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"holder.valueOfAssets > 70"];
    NSArray<BBPAsset *> *toBeReclaimed = [allAssets filteredArrayUsingPredicate:predicate];
    NSLog(@"toBeReclaimed: %@", toBeReclaimed);
    toBeReclaimed = nil;
    
    NSLog(@"Giving up ownership of arrays");
    
    allAssets = nil;
    employees = nil;
  }
  sleep(100);
  return 0;
}
