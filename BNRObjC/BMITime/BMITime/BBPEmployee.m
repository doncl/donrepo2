//
//  BBPEmployee.m
//  BMITime
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "BBPEmployee.h"
#import "BBPAsset.h"

@interface BBPEmployee ()
{
  NSMutableSet<BBPAsset *> *_assets;
}

@property (nonatomic) unsigned int officeAlarmCode;

@end



@implementation BBPEmployee

- (void)setAssets:(NSSet<BBPAsset *> *)assets
{
  _assets = [assets mutableCopy];
}

- (NSArray<BBPAsset *> *)assets
{
  return [_assets copy];
}

- (void)addAsset:(BBPAsset *)a
{
  if (!_assets) {
    _assets = [NSMutableSet<BBPAsset *> new];
  }
  [_assets addObject:a];
  a.holder = self;
}

- (unsigned int)valueOfAssets
{
  unsigned int sum = 0;
  for (BBPAsset *a in _assets) {
    sum += [a resaleValue];
  }
  return sum;
}

- (NSString *)description {
  return [NSString stringWithFormat:@"<Employee %u: $%u in assets>", self.employeeID, self.valueOfAssets];
}

- (void)dealloc
{
  NSLog(@"deallocating %@", self);
}

- (double)yearsOfEmployment
{
  if (self.hireDate) {
    NSDate *now = [NSDate new];
    NSTimeInterval secs = [now timeIntervalSinceDate:self.hireDate];
    return secs / 31557600.0;  // Seconds per year
  } else {
    return 0;
  }
}

- (float)bodyMassIndex
{
  float normalBMI = [super bodyMassIndex];
  return normalBMI * 0.9;
}

- (BBPAsset *)removeAsset:(BBPAsset *)a
{
  BOOL found = [_assets containsObject:a];
  if (!found) {
    return nil;
  }
  [_assets removeObject:a];
  return a;
}
@end
