//
//  BBPAsset.m
//  BMITime
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "BBPAsset.h"

@implementation BBPAsset

- (NSString *)description
{
  if (self.holder) {
    return [NSString stringWithFormat:@"<%@: $%d, assigned to %@>", self.label, self.resaleValue, self.holder];
  } else {
    return [NSString stringWithFormat:@"<%@: $%u>", self.label, self.resaleValue];
  }
}

- (void)dealloc
{
  NSLog(@"deallocating %@", self);
}
@end
