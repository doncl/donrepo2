//
//  BBPAsset.h
//  BMITime
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>
@class BBPEmployee;

NS_ASSUME_NONNULL_BEGIN

@interface BBPAsset : NSObject
@property (nonatomic, copy) NSString *label;
@property (nonatomic, weak) BBPEmployee *holder;
@property (nonatomic) unsigned int resaleValue;
@end

NS_ASSUME_NONNULL_END
