//
//  BBPEmployee.h
//  BMITime
//
//  Created by Don Clore on 7/12/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BBPPerson.h"
@class BBPAsset;

NS_ASSUME_NONNULL_BEGIN

@interface BBPEmployee : BBPPerson
@property (nonatomic) unsigned int employeeID;
@property (nonatomic) NSDate *hireDate;
@property (nonatomic, copy) NSSet<BBPAsset *> *assets;

- (double)yearsOfEmployment;
- (void)addAsset:(BBPAsset *)a;
- (BBPAsset *)removeAsset:(BBPAsset *)a;
- (unsigned int)valueOfAssets;

@end

NS_ASSUME_NONNULL_END
