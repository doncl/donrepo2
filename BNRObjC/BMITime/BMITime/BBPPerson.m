//
//  BBPPerson.m
//  BMITime
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "BBPPerson.h"

@implementation BBPPerson
- (float)bodyMassIndex
{
  float h = [self heightInMeters];
  return [self weightInKilos] / (h * h);
}
@end
