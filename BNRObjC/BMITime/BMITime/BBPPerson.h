//
//  BBPPerson.h
//  BMITime
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BBPPerson : NSObject

@property (nonatomic) float heightInMeters;
@property (nonatomic) int weightInKilos;

// BNRPerson has methods to read and set its instance variables
- (float)bodyMassIndex;
@end

NS_ASSUME_NONNULL_END
