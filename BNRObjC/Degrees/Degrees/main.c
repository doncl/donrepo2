//
//  main.c
//  Degrees
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

// Declare a global variable
static float lastTemperature;

float fahrenheightFromCelsius(float cel) {
  lastTemperature = cel;
  float fahr = cel * 1.8 + 32.0;
  printf("%f Celsius is %f Fahrenheit\n", cel, fahr);
  return fahr;
}

int main(int argc, const char * argv[]) {
  float freezeInC = 0;
  float freezeInF = fahrenheightFromCelsius(freezeInC);
  printf("Water freezes at %f degrees Fahrenheight.\n", freezeInF);
  printf("The last temperature converted was %f.\n", lastTemperature);
  return EXIT_SUCCESS;
}
