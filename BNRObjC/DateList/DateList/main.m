//
//  main.m
//  DateList
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    // Create three NSDate objects
    NSDate *now = [NSDate new];
    NSDate *tomorrow = [now dateByAddingTimeInterval:24.0 * 60.0 * 60.0];
    NSDate *yesterday = [now dateByAddingTimeInterval:-24.0 * 60.0 * 60.0];
    
    // Create an array containing all three
    NSMutableArray *dateList = [NSMutableArray new];
    
    [dateList addObject:now];
    [dateList addObject:tomorrow];
    
    [dateList insertObject:yesterday atIndex:0];

    
    for (NSDate *d in dateList) {
      NSLog(@"Here is a date: %@", d);
    }
    
    [dateList removeObjectAtIndex:0];
    NSLog(@"Now the first date is %@", dateList[0]);
    
  }
  return 0;
}
