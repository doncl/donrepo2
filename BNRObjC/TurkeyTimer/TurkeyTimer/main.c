//
//  main.c
//  TurkeyTimer
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>

void showCookTimeForTurkey(int pounds) {
  int necessaryMinutes = 15 + 15 * pounds;
  printf("Cook for %d minutes.\n", necessaryMinutes);
  if (necessaryMinutes > 120) {
    int halfway = necessaryMinutes / 2;
    printf("Rotate after %d of the %d minutes.\n", halfway, necessaryMinutes);
  }
}


int main(int argc, const char * argv[]) {
  // insert code here...
  int totalWeight = 10;
  int gibletsWeight = 1;
  int turkeyWeight = totalWeight - gibletsWeight;
  showCookTimeForTurkey(turkeyWeight);
  
  return 0;
}
