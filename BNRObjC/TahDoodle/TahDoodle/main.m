//
//  main.m
//  TahDoodle
//
//  Created by Don Clore on 7/13/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
  @autoreleasepool {
      // Setup code that might create autoreleased objects goes here.
  }
  return NSApplicationMain(argc, argv);
}
