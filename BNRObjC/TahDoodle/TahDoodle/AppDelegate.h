//
//  AppDelegate.h
//  TahDoodle
//
//  Created by Don Clore on 7/13/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

