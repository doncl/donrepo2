//
//  Document.h
//  TahDoodle
//
//  Created by Don Clore on 7/13/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface Document : NSDocument
@property (nonatomic) NSMutableArray *tasks;
@property (nonatomic) IBOutlet NSTableView *taskTable;

- (IBAction)addTask:(id)sender;
@end

