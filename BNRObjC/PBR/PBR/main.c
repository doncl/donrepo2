//
//  main.c
//  PBR
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>
#include <math.h>

void metersToFeetAndInches(double meters, unsigned int *ftPtr, double *inPtr) {
  // This function assumes meters is non-negative
  
  // Convert the number of meters into a floating-point number of feet.
  double rawFeet = meters * 3.281; // e.g. 2.4536
  double fractionalPart;
  double integerPart;
  
  // How many complete feet as an insigned int.
  fractionalPart = modf(rawFeet, &integerPart);
  
  if (ftPtr) {
    *ftPtr = integerPart;
  }
                                     
  // Calculate inches
  double inches = fractionalPart * 12.0;
  
  // Store the number of inches at the supplied address.
  if (inPtr) {
    printf("Storing %.2f to the address %p\n", inches, inPtr);
    *inPtr = inches;
  }
}

int main(int argc, const char * argv[]) {

  double meters = 3.0;
  unsigned int feet;
  
  double inches;
  
  metersToFeetAndInches(meters, &feet, &inches);
  printf("%.1f meters is equal to %d feet and %1f inches\n", meters, feet, inches);
  
  return 0;
}
