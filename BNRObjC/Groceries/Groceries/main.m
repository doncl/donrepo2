//
//  main.m
//  Groceries
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
  @autoreleasepool {
    NSMutableArray<NSString *> *groceries = [NSMutableArray<NSString *> new];
    [groceries addObject:@"Milk"];
    [groceries addObject:@"Dog Food"];
    [groceries addObject:@"Pop"];
    
    for (NSString *item in groceries) {
      NSLog(@"item = %@", item);
    }
  }
  return 0;
}
