//
//  main.c
//  Addresses
//
//  Created by Don Clore on 7/11/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[]) {
  int i = 17;
  int *addressOfI = &i;
  printf("i stores its value at %p\n", addressOfI);
  
  *addressOfI = 89;
  
  printf("Now it is %d\n", i);
  
  printf("An int is %zu bytes\n", sizeof(int));
  printf("A pointer is %zu bytes\n", sizeof addressOfI);
  
  printf("A float is %zu bytes\n", sizeof (float));
  
  return 0;
}
