//
//  ViewController.swift
//  CamScaffold
//
//  Created by Don Clore on 1/29/22.
//

import UIKit

class ViewController: UIViewController {
  var camera: McCuneCameraManager?
  var previewView = PreviewView()

  override func viewDidLoad() {
    super.viewDidLoad()
    
    [previewView,].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      view.addSubview($0)
    }
    
    NSLayoutConstraint.activate([
      previewView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      previewView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      previewView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      previewView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
        
    let c = McCuneCameraManager()
    camera = c
    
    let result = c.setupSession()
    switch result {
      case .success(let sess):
        previewView.setSession(sess)
        c.delegate = self
        c.startSession()
      case .failure(let error):
        fatalError(error.localizedDescription)
    }
  }
}

extension ViewController: McCuneCameraManagerDelegate {
  func deviceConfigurationFailed(error: Error) {
    print("\(#function) - error \(error)")
  }
  
  func mediaCaptureFailed(error: Error) {
    print("\(#function) - error \(error)")
  }
  
  func assetLibraryWriteFailed(error: Error) {
    print("\(#function) - error \(error)")
  }
  
  
}

