//
//  PreviewView.swift
//  CamScaffold
//
//  Created by Don Clore on 1/29/22.
//

import UIKit
import AVFoundation

class PreviewView: UIView {
  class override var layerClass: AnyClass {
    return AVCaptureVideoPreviewLayer.self
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setupView()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  func setupView() {
    guard let previewLayer = layer as? AVCaptureVideoPreviewLayer else {
      return
    }
    previewLayer.videoGravity = .resizeAspectFill
  }
  
  func setSession(_ session: AVCaptureSession) {
    guard let previewLayer = layer as? AVCaptureVideoPreviewLayer else {
      return
    }
    previewLayer.session = session
  }  
}
