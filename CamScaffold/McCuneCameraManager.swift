import Foundation

//
//  McCuneCameraManager.swift
//
//  Created by Don Clore on 1/29/22.
//

import Foundation
import AVFoundation
import UIKit

enum McCuneCameraManagerError: Error {
  case couldntgetvideodevice
  case couldntgetaudiodevice
  case neverhappen
  case couldntaddvideoinput
  case couldntaddaudioinput
  case couldntaddfileoutput
  case cantswitchcameras
  case cantfindinactivecamera
  case noactivevideoinput
  case unexpectedlynotrunning
  case unexpectednilmovieoutput
}

protocol McCuneCameraManagerDelegate: AnyObject {
  func deviceConfigurationFailed(error: Error)
  func mediaCaptureFailed(error: Error)
  func assetLibraryWriteFailed(error: Error)
}

class McCuneCameraManager: NSObject {
  var captureSession: AVCaptureSession?
  var activeVideoInput: AVCaptureDeviceInput?
  var movieOutput: AVCaptureMovieFileOutput?
  var outputURL: URL?

  private var videoCompletion: ((_ videoURL: URL?, _ error: Error?) -> Void)?

  weak var delegate: McCuneCameraManagerDelegate?
  
  private lazy var frontCameraDevice: AVCaptureDevice? = {
    let frontVideoDeviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInTrueDepthCamera, .builtInWideAngleCamera],
                                                                            mediaType: .video, position: .front)
    return frontVideoDeviceDiscoverySession.devices.first

  }()

  var uniqueURL: URL {
    let fm = FileManager.default
    let dirPath: URL = fm.temporaryDirectoryUnique()
    let file = dirPath.appendingPathComponent("kamera_movie.mp4")
    return file
  }

  var canSwitchCameras: Bool {
    return cameraCount > 1
  }

  var isRecording: Bool {
    guard let movieOutput = movieOutput else {
      return false
    }
    return movieOutput.isRecording
  }


  var activeCamera: AVCaptureDevice? {
    return activeVideoInput?.device
  }

  var inactiveCamera: AVCaptureDevice? {
    guard cameraCount > 1 else {
      return nil
    }
    guard let activeCamera = activeCamera else {
      return nil
    }

    switch activeCamera.position {
      case .back:
        let inactive = camera(withPosition: AVCaptureDevice.Position.front)
        return inactive
      case .front:
        let inactive = camera(withPosition: AVCaptureDevice.Position.back)
        return inactive

      default:
        return nil
    }
  }

  let videoCameraTypes: [AVCaptureDevice.DeviceType] = [
    .builtInDualCamera,
    .builtInDualWideCamera,
    .builtInTelephotoCamera,
    .builtInTripleCamera,
    .builtInTrueDepthCamera,
    .builtInUltraWideCamera,
    .builtInWideAngleCamera,
  ]

  lazy var deviceDiscoverySession: AVCaptureDevice.DiscoverySession = {
    let session = AVCaptureDevice.DiscoverySession(deviceTypes: videoCameraTypes,
                                                   mediaType: AVMediaType.video,
                                                   position: AVCaptureDevice.Position.unspecified)

    return session
  }()

  var cameraCount: Int {
    return deviceDiscoverySession.devices.count
  }

  lazy var globalQ: DispatchQueue = {
    let q = DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive)
    return q
  }()

  func setupSession() -> Result<AVCaptureSession, Error> {
    let sess = AVCaptureSession()
    captureSession = sess
    captureSession?.sessionPreset = AVCaptureSession.Preset.high

    do {

      try createVideoInput()
      try createAudioInput()
      try setupMovieOutput()

    } catch let error {
      print("\(#function) - could not get device input, error = \(error)")
      return .failure(error)
    }

    return .success(sess)
  }

  func startSession() {
    guard let captureSession = captureSession else {
      fatalError("Must never happen")
    }

    guard !captureSession.isRunning else {
      return
    }

    globalQ.async {
      captureSession.startRunning()
    }
  }

  func stopSession() {
    guard let captureSession = captureSession else {
      fatalError("Must never happen")
    }

    guard captureSession.isRunning else {
      return
    }

    globalQ.async {
      captureSession.stopRunning()
    }
  }
}


// MARK: Device configuration
extension McCuneCameraManager {
  func camera(withPosition position: AVCaptureDevice.Position) -> AVCaptureDevice? {
    let firstDeviceWithPOsition = deviceDiscoverySession.devices.first(where: { $0.position == position })
    return firstDeviceWithPOsition
  }

  func switchCameras() -> Result<Bool, Error> {
    guard let captureSession = captureSession else {
      fatalError("Must never happen")
    }

    guard let activeVideoInput = activeVideoInput else {
      return .failure(McCuneCameraManagerError.noactivevideoinput)
    }

    guard canSwitchCameras else {
      return .failure(McCuneCameraManagerError.cantswitchcameras)
    }

    guard let videoDevice = inactiveCamera else {
      return .failure(McCuneCameraManagerError.cantfindinactivecamera)
    }

    do {
      let videoInput = try AVCaptureDeviceInput(device: videoDevice)
      captureSession.beginConfiguration()
      captureSession.removeInput(activeVideoInput)

      if captureSession.canAddInput(videoInput) {
        captureSession.addInput(videoInput)
      } else {
        captureSession.addInput(activeVideoInput)
      }
      captureSession.commitConfiguration()

    } catch let error {
      return .failure(error)
    }

    return .success(true)
  }

}

// MARK: Video Capture Methods
extension McCuneCameraManager {
  func startRecording() {
    guard !isRecording, let movieOutput = movieOutput, let device = activeCamera, let delegate = delegate else {
      return
    }
    guard let videoConnection = movieOutput.connection(with: AVMediaType.video) else {
      fatalError("Can't create video connection")
      //return
    }

    if videoConnection.isVideoOrientationSupported {
      videoConnection.videoOrientation = currentVideoOrientation
    }

    if videoConnection.isVideoStabilizationSupported {
      videoConnection.preferredVideoStabilizationMode = AVCaptureVideoStabilizationMode.auto
    }

    if device.isSmoothAutoFocusEnabled {
      do {
        try device.lockForConfiguration()
        device.isSmoothAutoFocusEnabled = false
        device.unlockForConfiguration()
      } catch let error {
        delegate.deviceConfigurationFailed(error: error)
      }
    }

    outputURL = uniqueURL

    movieOutput.startRecording(to: uniqueURL, recordingDelegate: self)
  }

  func stopVideoRecording(_ completion: ((_ videoURL: URL?, _ error: Error?) -> Void)?) {
    guard isRecording else {
      if let completion = completion {
        completion(nil, McCuneCameraManagerError.unexpectedlynotrunning)
      }
      return
    }
    guard let movieOutput = movieOutput else {
      if let completion = completion {
        completion(nil, McCuneCameraManagerError.unexpectednilmovieoutput)
      }
      return
    }
    videoCompletion = completion
    movieOutput.stopRecording()
  }
}


// MARK: Helper methods
extension McCuneCameraManager {
  private func createVideoInput() throws {
    guard let captureSession = captureSession, let frontDeviceType = frontCameraDevice?.deviceType else {
      throw McCuneCameraManagerError.neverhappen
    }

    guard let videoDevice = AVCaptureDevice.default(frontDeviceType, for: AVMediaType.video, position: .front) else {
      print("\(#function) - could not get video device")
      throw McCuneCameraManagerError.couldntgetvideodevice
    }

    let videoInput = try AVCaptureDeviceInput(device: videoDevice)

    guard captureSession.canAddInput(videoInput) else {
      throw McCuneCameraManagerError.couldntaddvideoinput
    }
    captureSession.addInput(videoInput)
    activeVideoInput = videoInput
  }

  private func createAudioInput() throws {
    guard let captureSession = captureSession else {
      throw McCuneCameraManagerError.neverhappen
    }

    // Set up default microphone
    guard let audioDevice = AVCaptureDevice.default(for: AVMediaType.audio) else {
      print("\(#function) - could not get audio device")
      throw McCuneCameraManagerError.couldntgetaudiodevice
    }

    let audioInput = try AVCaptureDeviceInput(device: audioDevice)

    guard captureSession.canAddInput(audioInput) else {
      throw McCuneCameraManagerError.couldntaddaudioinput
    }

    captureSession.addInput(audioInput)
  }

  private func setupMovieOutput() throws {
    guard let captureSession = captureSession else {
      throw McCuneCameraManagerError.neverhappen
    }

    let output = AVCaptureMovieFileOutput()
    movieOutput = output
    guard captureSession.canAddOutput(output) else {
      throw McCuneCameraManagerError.couldntaddfileoutput
    }

    captureSession.addOutput(output)
  }
}

// MARK: Orientation

extension McCuneCameraManager {
  var currentVideoOrientation: AVCaptureVideoOrientation {
    let device = UIDevice.current
    let orientation: UIDeviceOrientation = device.orientation

    switch orientation {
      case .unknown:
        return AVCaptureVideoOrientation.portrait
      case .portrait:
        return AVCaptureVideoOrientation.portrait
      case .portraitUpsideDown:
        return AVCaptureVideoOrientation.portraitUpsideDown
      case .landscapeLeft:
        return AVCaptureVideoOrientation.landscapeLeft
      case .landscapeRight:
        return AVCaptureVideoOrientation.landscapeRight
      case .faceUp:
        return AVCaptureVideoOrientation.portrait
      case .faceDown:
        return AVCaptureVideoOrientation.portrait

      @unknown default:
        return AVCaptureVideoOrientation.portrait
    }
  }
}

// MARK: AVCaptureFileOutputRecordingDelegate
extension McCuneCameraManager: AVCaptureFileOutputRecordingDelegate {
  func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
    print("\(#function) - fileURL = \(outputFileURL.absoluteString)")
    if let error = error {
      print("\(#function) Unable to save video to the device\(error.localizedDescription)")
    } else {
      print("\(#function) - success - saved video to \(outputFileURL.absoluteString)")
      if let validCompletion = videoCompletion {
        validCompletion(outputFileURL, error)
        videoCompletion = nil
      }
    }
  }
}

extension FileManager {
  func temporaryDirectoryUnique() -> URL {
    let fm = FileManager.default
    var dir = URL(fileURLWithPath: fm.temporaryDirectory.path, isDirectory: true)
    let ticks = String(Int(Date().timeIntervalSinceReferenceDate))
    dir.appendPathComponent(ticks)
    return dir
  }
}
