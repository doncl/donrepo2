//
//  main.m
//  ObjCLingo
//
//  Created by Don Clore on 8/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
  @autoreleasepool {
    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
  }
}
