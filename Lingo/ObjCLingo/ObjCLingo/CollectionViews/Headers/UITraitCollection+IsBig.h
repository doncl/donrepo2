//
//  UITraitCollection+IsBig.h
//  ObjCLingo
//
//  Created by Don Clore on 8/5/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITraitCollection (IsBig)
- (BOOL)isBig;
@end
