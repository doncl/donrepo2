//
//  TargetCollectionHeader.m
//  ObjCLingo
//
//  Created by Don Clore on 8/5/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import "TargetCollectionHeader.h"
#import "UITraitCollection+IsBig.h"

typedef struct struct_headerfontSizes {
  CGFloat wordFontSize;
  CGFloat wordValFontSize;
  CGFloat langFontSize;
} HeaderFontSizes;

typedef struct struct_header_constants {
  CGFloat horzPad;
  CGFloat regularVertPad;
  CGFloat compactVertPad;
  
} TargetCollectionHeaderConstants;

@interface FontPair : NSObject
@property (nonatomic, strong, readwrite) UIFont *regular;
@property (nonatomic, strong, readwrite) UIFont *compact;
@end

@implementation FontPair
- (instancetype)initWithRegularFont:(UIFont *)regular andCompactFont:(UIFont *)compact {
  self = [super init];
  if (self) {
    self.regular = regular;
    self.compact = compact;
  }
  return self;
}
@end

@interface TargetCollectionHeader() {
  
@private HeaderFontSizes compactFontSizes;
@private HeaderFontSizes regularFontSizes;
@private TargetCollectionHeaderConstants constants;
}

@property (nonatomic, strong, readwrite) UILabel * _Nonnull wordLabel;
@property (nonatomic, strong, readwrite) UILabel * _Nonnull wordValLabel;
@property (nonatomic, strong, readwrite) UILabel * _Nonnull sourceLangLabel;
@property (nonatomic, strong, readwrite) UILabel * _Nonnull sourceLangValLabel;
@property (nonatomic, strong, readwrite) UILabel * _Nonnull targetLangLabel;
@property (nonatomic, strong, readwrite) UILabel * _Nonnull targetLangValLabel;
@property (nonatomic, strong, readwrite) NSMutableDictionary<NSNumber *, FontPair *> * _Nonnull labelFontMap;
@end

@implementation TargetCollectionHeader
- (instancetype)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self commonInit];
  }
  return self;
}

- (instancetype)init {
  self = [super init];
  if (self) {
    [self commonInit];
  }
  return self;
}

- (void)commonInit {
  self->compactFontSizes.wordFontSize = 16.0;
  self->compactFontSizes.wordValFontSize = 14.0;
  self->compactFontSizes.langFontSize = 12.0;
  
  self->regularFontSizes.wordValFontSize = 24.0;
  self->regularFontSizes.wordValFontSize = 20.0;
  self->regularFontSizes.langFontSize = 18.0;
  
  self->constants.horzPad = 5.0;
  self->constants.regularVertPad = 5.0;
  self->constants.compactVertPad = 10.0;
  
  self.wordLabel = [UILabel new];
  self.wordLabel.tag = 1;
  [self addSubview:self.wordLabel];
  [self setCategoryLabel:self.wordLabel text:@"Word: "];
  
  self.wordValLabel = [UILabel new];
  self.wordValLabel.tag = 2;
  [self addSubview:self.wordValLabel];
  [self setValueLabel:self.wordValLabel];
  
  self.sourceLangLabel = [UILabel new];
  self.sourceLangLabel.tag = 3;
  [self addSubview:self.sourceLangLabel];
  [self setCategoryLabel:self.sourceLangLabel text:@"Source: "];
  
  self.sourceLangValLabel = [UILabel new];
  self.sourceLangValLabel.tag = 4;
  [self addSubview: self.sourceLangValLabel];
  [self setValueLabel:self.sourceLangValLabel];
  
  self.targetLangLabel = [UILabel new];
  self.targetLangLabel.tag = 5;
  [self addSubview:self.targetLangLabel];
  [self setCategoryLabel:self.targetLangLabel text:@"Target: "];
  
  self.targetLangValLabel = [UILabel new];
  self.targetLangValLabel.tag = 6;
  [self addSubview:self.targetLangValLabel];
  [self setValueLabel:self.targetLangValLabel];
  
  self.backgroundColor = [UIColor whiteColor];
  
  [self setupLabelFontMap];
}

- (void)setupLabelFontMap {
  self.labelFontMap = [NSMutableDictionary<NSNumber *, FontPair *> new];
  
  NSNumber *wordLabelTag = [[NSNumber alloc] initWithLong:self.wordLabel.tag];
  
  self.labelFontMap[wordLabelTag] = [[FontPair alloc] initWithRegularFont:[UIFont boldSystemFontOfSize:self->regularFontSizes.wordFontSize] andCompactFont:[UIFont boldSystemFontOfSize:self->compactFontSizes.wordFontSize]];
  
  NSNumber *wordValLabelTag = [[NSNumber alloc] initWithLong: self.wordValLabel.tag];
  
  self.labelFontMap[wordValLabelTag] = [[FontPair alloc] initWithRegularFont:[UIFont systemFontOfSize:self->regularFontSizes.wordValFontSize] andCompactFont:[UIFont systemFontOfSize:self->compactFontSizes.wordValFontSize]];
  
  NSNumber *sourceLangLabelTag = [[NSNumber alloc] initWithLong: self.sourceLangLabel.tag];
  
  self.labelFontMap[sourceLangLabelTag] = [[FontPair alloc] initWithRegularFont:[UIFont systemFontOfSize:self->regularFontSizes.langFontSize] andCompactFont:[UIFont systemFontOfSize:self->compactFontSizes.langFontSize]];
  
  NSNumber *sourceLangValLabelTag = [[NSNumber alloc] initWithLong:self.sourceLangValLabel.tag];
  
  self.labelFontMap[sourceLangValLabelTag] = [[FontPair alloc] initWithRegularFont:[UIFont systemFontOfSize:self->regularFontSizes.langFontSize] andCompactFont:[UIFont systemFontOfSize:self->compactFontSizes.langFontSize]];
  
  NSNumber *targetLangLabelTag = [[NSNumber alloc] initWithLong: self.targetLangLabel.tag];
  
  self.labelFontMap[targetLangLabelTag] = [[FontPair alloc] initWithRegularFont:[UIFont systemFontOfSize:self->regularFontSizes.langFontSize] andCompactFont:[UIFont systemFontOfSize:self->compactFontSizes.langFontSize]];
  
  NSNumber *targetLangValLabelTag = [[NSNumber alloc] initWithLong:self.targetLangValLabel.tag];
  
  self.labelFontMap[targetLangValLabelTag] = [[FontPair alloc] initWithRegularFont:[UIFont systemFontOfSize:self->regularFontSizes.langFontSize] andCompactFont:[UIFont systemFontOfSize:self->compactFontSizes.langFontSize]];
}

- (void)setCategoryLabel:(UILabel *)label text:(NSString *)text {
  [self setFontForLabel:label];
  label.textColor = [UIColor blackColor];
  label.textAlignment = NSTextAlignmentLeft;
  label.text = text;
}

- (void)setValueLabel:(UILabel *)label {
  [self setFontForLabel:label];
  label.textColor = [UIColor blackColor];
  label.textAlignment = NSTextAlignmentLeft;
}

- (void)setFontForLabel:(UILabel *)label {
  NSNumber *tagKey = [[NSNumber alloc] initWithLong:label.tag];
  FontPair *fp = self.labelFontMap[tagKey];
  label.font = [self.traitCollection isBig] ? fp.regular : fp.compact;
}

- (void)setAllLabelFonts {
  for (NSNumber *key in self.labelFontMap) {
    FontPair *fp = self.labelFontMap[key];
    NSInteger tag = [key longValue];
    UILabel *labelForTag = [self getLabelForTag:tag];
    UIFont *fontToUse = [self.traitCollection isBig] ? fp.regular : fp.compact;
    labelForTag.font = fontToUse;
    [labelForTag setNeedsLayout];
    [labelForTag setNeedsDisplay];
  }
}

- (UILabel *)getLabelForTag:(NSInteger)tag {
  for (UIView *view in [self subviews]) {
    if (false == [view isKindOfClass:[UILabel class]]) {
      continue;
    }
    if (tag == view.tag) {
      return (UILabel *)view;
    }
  }
  NSAssert(false, @"Should never happen");
  return nil;
}

- (void)layoutSubviews {
  [super layoutSubviews];
  CGFloat startY = [self.traitCollection isBig] ? self->constants.regularVertPad : self->constants.compactVertPad;
  CGFloat horzPad = self->constants.horzPad;
  
  CGSize size = self.wordLabel.intrinsicContentSize;
  self.wordLabel.frame = CGRectMake(horzPad, startY, size.width, size.height);
  
  size = self.wordValLabel.intrinsicContentSize;
  self.wordValLabel.frame =
    CGRectMake(self.bounds.size.width - (size.width + horzPad), startY, size.width, size.height);
  
  size = self.sourceLangLabel.intrinsicContentSize;
  CGFloat maxY = CGRectGetMaxY(self.wordLabel.frame) + self->constants.regularVertPad;
  self.sourceLangLabel.frame = CGRectMake(horzPad, maxY, size.width, size.height);
  
  size = self.sourceLangValLabel.intrinsicContentSize;
  self.sourceLangValLabel.frame =
    CGRectMake(self.bounds.size.width - (size.width + horzPad), maxY, size.width, size.height);
  
  size = self.targetLangLabel.intrinsicContentSize;
  maxY = CGRectGetMaxY(self.sourceLangLabel.frame) + self->constants.regularVertPad;
  self.targetLangLabel.frame = CGRectMake(horzPad, maxY, size.width, size.height);
  
  size = self.targetLangValLabel.intrinsicContentSize;
  self.targetLangValLabel.frame =
    CGRectMake(self.bounds.size.width - (size.width + horzPad), maxY, size.width, size.height);
}

- (void)traitCollectionDidChange:(UITraitCollection *)previousTraitCollection {
  if (previousTraitCollection) {
    if ([self.traitCollection isBig] == [previousTraitCollection isBig]) {
      return;
    }
  }
  [self setAllLabelFonts];
}

@end





















