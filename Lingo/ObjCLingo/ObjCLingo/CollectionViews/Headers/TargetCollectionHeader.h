//
//  TargetCollectionHeader.h
//  ObjCLingo
//
//  Created by Don Clore on 8/5/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TargetCollectionHeader : UICollectionReusableView
@property (nonatomic, strong, readonly) UILabel *wordLabel;
@property (nonatomic, strong, readonly) UILabel *wordValLabel;
@property (nonatomic, strong, readonly) UILabel *sourceLangLabel;
@property (nonatomic, strong, readonly) UILabel *sourceLangValLabel;
@property (nonatomic, strong, readonly) UILabel *targetLangLabel;
@property (nonatomic, strong, readonly) UILabel *targetLangValLabel;
@end
