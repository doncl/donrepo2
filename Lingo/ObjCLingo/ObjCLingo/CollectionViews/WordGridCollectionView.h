//
//  WordGridCollectionView.h
//  ObjCLingo
//
//  Created by Don Clore on 8/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjCLingo-Swift.h"

@protocol WordGridCollectionViewDelegate <NSObject>
- (void)foundWinner:(NSString *_Nonnull)winner midPoint:(CGPoint  * _Nullable)midPoint foundAll:(BOOL)foundAll;
@end



@interface IB_DESIGNABLE WordGridCollectionView : UICollectionView<UICollectionViewDataSource>
@property (nonatomic, weak) id<WordGridCollectionViewDelegate> _Nullable wordGridDelegate;
@property (nonatomic, strong) WordGrid * _Nullable wordGrid;

- (void)clearAll;
- (void)updateCellSize;
- (void)transitioning;
@end
