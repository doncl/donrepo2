//
//  TargetWordCollectionView.h
//  ObjCLingo
//
//  Created by Don Clore on 8/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ObjCLingo-Swift.h"

@interface TargetWordCollectionView : UICollectionView<UICollectionViewDataSource>
@property (nonatomic, weak) WordGrid * _Nullable wordGrid;


- (instancetype _Nullable)initWithCoder:(NSCoder * _Nonnull)aDecoder;
- (void)updateCellSize;
- (void)weHaveAWinner:(NSString * _Nonnull)winner;
- (void)removeCheckmarkFromAllCells;
@end
