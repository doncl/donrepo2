//
//  TargetWordCell.h
//  ObjCLingo
//
//  Created by Don Clore on 8/5/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TargetWordCell : UICollectionViewCell
@property (nonatomic, readwrite, strong) UIImageView *checkMark;
@property (nonatomic, readwrite, strong) UILabel *label;
+ (UIFont *)preferredFont;

@end
