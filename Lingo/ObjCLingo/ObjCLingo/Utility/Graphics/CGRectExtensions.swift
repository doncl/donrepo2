//
//  CGRectExtensions.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 7/2/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

//
//  CGRectExtensions.swift
//  DrawingUtilities
//
//  Created by Don Clore on 6/30/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.
//
import UIKit

extension CGRect {
  //MARK: general Geometry
  var center : CGPoint {
    return CGPoint(x: midX, y: midY)
  }
  
  //MARK: Rectangle construction
  static func from(size: CGSize) -> CGRect {
    return CGRect(origin: .zero, size: size)
  }

  static func rectAroundCenter(center : CGPoint, size: CGSize) -> CGRect {
    let halfWidth = size.width / 2.0
    let halfHeight = size.height / 2.0

    return CGRect(x: center.x - halfWidth, y: center.y - halfHeight, width: size.width,
                  height: size.height)
  }

  //MARK: Cardinal points
  var topLeft : CGPoint {
    return CGPoint(x: minX, y: minY)
  }

  var topRight : CGPoint {
    return CGPoint(x: maxX, y: minY)
  }

  var bottomLeft : CGPoint {
    return CGPoint(x: minX, y: maxY)
  }

  var bottomRight : CGPoint {
    return CGPoint(x: maxX, y: maxY)  //MARK: Linear
  }

  var midTop : CGPoint {
    return CGPoint(x: midX, y: minY)
  }

  var midBottom : CGPoint {
    return CGPoint(x: midX, y: maxY)
  }

  var midLeft : CGPoint {
    return CGPoint(x: minX, y: midY)
  }

  var midRight : CGPoint {
    return CGPoint(x: maxX, y: midY)
  }
  
  func inset(byPercent percent : CGFloat) -> CGRect {
    let wInset = size.width * (percent / 2.0)
    let yInset = size.height * (percent / 2.0)
    return insetBy(dx: wInset, dy: yInset)
  }

  // What do we have to scale by to make it fit (with letterboxing or pillarboxing)?
  func aspectScaleFit(destSize: CGSize) -> CGFloat {
    let dimsScale = getDimensionsScale(destSize: destSize)

    return min(dimsScale.0, dimsScale.1)
  }

  fileprivate func getDimensionsScale(destSize : CGSize) -> (CGFloat, CGFloat) {
    let scaleW = size.width == 0 ? 0 : destSize.width / size.width
    let scaleH = size.height == 0 ? 0 : destSize.height / size.height

    return (scaleW, scaleH)
  }

  func fit(into destRect : CGRect) -> CGRect {
    let aspect = aspectScaleFit(destSize: destRect.size)
    let targetSize = size.scale(byFactor: aspect)
    return CGRect.rectAroundCenter(center: destRect.center, size: targetSize)
  }
}
