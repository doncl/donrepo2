//
//  UIBezierPathExtensions.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/1/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.
//       (Note to reviewer) I'm pretty sure I excised every single method that is not needed
//       for the silly little text effect I wanted.  The original translated file is many times
//       this size, and of course, the original ObjC it was translated from was much longer
//       yet, being a more verbose language.  

import UIKit
import QuartzCore

//MARK: Bounds
public extension UIBezierPath {
  var boundingBox : CGRect {
    return cgPath.boundingBoxOfPath
  }
  var boundingBoxCenter : CGPoint {
    return boundingBox.center
  }
}
//MARK: Transforms

public extension UIBezierPath {
  // It's kind of counter-intuitive why you need to translate
  // the path to the center of the bounding box; there's a good video diagram
  // of it in Wenderlich's Core Video courses (either Beginner or Intermediate,
  // can't remember).   But basically, you need to move the  entire path over
  // such that its edge is exactly on the center of where it was, then apply
  // the desired transform, then move it back.
  func applyCentered(transform: CGAffineTransform) {
    let center = boundingBoxCenter
    var t : CGAffineTransform = .identity
    t = t.translatedBy(x: center.x, y: center.y)
    t = transform.concatenating(t)
    t = t.translatedBy(x: -center.x, y: -center.y)
    apply(t)
  }
  
  // In radians, of couse.
  func scale(sx : CGFloat, sy: CGFloat) {
    let t = CGAffineTransform(scaleX: sx, y: sy)
    applyCentered(transform: t)
  }
  
  func offset(by offset: CGSize) {
    let t = CGAffineTransform(translationX: offset.width, y: offset.height)
    applyCentered(transform: t)
  }
  
  func moveCenter(to destPoint : CGPoint) {
    let bounds = boundingBox
    let p1 = bounds.origin
    let p2 = destPoint
    var vector = CGSize(width: p2.x - p1.x, height: p2.y - p1.y)
    vector.width -= bounds.size.width / 2.0
    vector.height -= bounds.size.height / 2.0
    offset(by: vector)
  }
  
  func mirrorHorizontally() {
    let t = CGAffineTransform(scaleX: -1, y: 1)
    applyCentered(transform: t)
  }
  
  func mirrorVertically() {
    let t = CGAffineTransform(scaleX: 1, y: -1)
    applyCentered(transform: t)
  }
}

//MARK: Fitting to rect
extension UIBezierPath {
  func fit(to destRect: CGRect) {
    let bounds = boundingBox
    let fitRect = bounds.fit(into: destRect)
    let scaleValue = bounds.aspectScaleFit(destSize: destRect.size)
    let newCenter = fitRect.center
    moveCenter(to: newCenter)
    scale(sx: scaleValue, sy: scaleValue)
  }
}

//MARK: Text
extension UIBezierPath {
  static func from(string: String, withFont font : UIFont) -> UIBezierPath? {
    if string.count == 0 {
      return nil
    }
    // init path
    let path = UIBezierPath()
    
    
    // Create font ref
    let fontRef = CTFontCreateWithName(font.fontName as CFString, font.pointSize,
                                       nil)
    
    // Create glyphs
    var glyphs : [CGGlyph] = Array(repeating: 0, count: string.count)
    
    let nsString : NSString = NSString(string:string)
    
    guard let cstr = nsString.utf8String else {
      return nil
    }
    var unichars : [UniChar] = Array(repeating: 0, count: nsString.length)
    for i in 0..<nsString.length {
      unichars[i] = UniChar(cstr[i])
    }
    
    if false == CTFontGetGlyphsForCharacters(fontRef, unichars, &glyphs,
                                             nsString.length) {
      
      return nil
    }
    
    // Draw each glyph into path
    for i in 0..<nsString.length {
      let glyph = glyphs[i]
      if let pathRef = CTFontCreatePathForGlyph(fontRef, glyph, nil) {
        let b = UIBezierPath(cgPath: pathRef)
        path.append(b)
      }
      let subString = nsString.substring(with: NSMakeRange(i, 1))
      let key = NSAttributedStringKey.font
      let size = subString.size(withAttributes: [key : font])
      path.offset(by: CGSize(width: -size.width, height: 0))
    }
    
    
    // Math
    path.mirrorVertically()
    return path
  }
  
  static func from(string: String, withFontFace : String) -> UIBezierPath? {
    var fontOpt = UIFont(name: withFontFace, size: 16.0)
    if fontOpt == nil {
      fontOpt = UIFont.systemFont(ofSize: 16.0)
    }
    guard let font = fontOpt else {
      return nil
    }
    return UIBezierPath.from(string: string, withFont: font)
  }
  
}
//MARK: Handy Utilities
extension UIBezierPath {
  func fill(withColor color: UIColor?) {
    guard let _ = UIGraphicsGetCurrentContext() else {
      return
    }
    
    pushDraw(block: {
      if let color = color {
        color.setFill()
      }
      self.fill()
    })
  }
  
  func fill(withColor color: UIColor?, withMode mode: CGBlendMode) {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    pushDraw(block: {
      context.setBlendMode(mode)
      self.fill(withColor: color)
    })
  }
}

//MARK: - Clippage
extension UIBezierPath {
  
  func clip(toStrokeWidth width: Int) {
    let pathRef = cgPath.copy(strokingWithWidth: CGFloat(width), lineCap: .butt, lineJoin: .miter, miterLimit: 0.4)
    let clipPath = UIBezierPath(cgPath: pathRef)
    clipPath.addClip()
  }
}

//MARK: Inverting path
extension UIBezierPath {
  func inverse(into rect : CGRect) -> UIBezierPath {
    let path = UIBezierPath()
    path.append(self)
    let rectPath = UIBezierPath(rect: rect)
    path.append(rectPath)
    path.usesEvenOddFillRule = true
    return path
  }
  
  var inverse : UIBezierPath {
    return inverse(into: CGRect.infinite)
  }
  
  var boundedInverse : UIBezierPath {
    return inverse(into: bounds)
  }
}

//MARK: Drawing an inner shadow
public extension UIBezierPath {
  func drawInnerShadow(color : UIColor?, size : CGSize, blur : CGFloat) {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    // Build shadow
    context.saveGState()
    context.setShadow(offset: size, blur: blur, color: color?.cgColor)
    
    // clip to the original path
    addClip()
    
    // Fill the inverted path
    inverse.fill(withColor: color)
    context.restoreGState()
  }
}

//MARK:  Text-y, shadow-y stuff
public extension UIBezierPath {
  @objc public static func drawStrokedShadowedText(string: String, fontFace : String,
                                      baseColor : UIColor, dest: CGRect) {
    
    // Create text path
    guard let t = UIBezierPath.from(string: string, withFontFace: fontFace) else {
      return
    }
    t.fit(to: dest)
    t.drawStrokedShadowedShape(baseColor: baseColor, dest: dest)
  }
  
  public func drawStrokedShadowedShape(baseColor : UIColor, dest : CGRect) {
    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }
    
    pushDraw(block: {
      let offset : CGSize = CGSize(width: 4, height: 4)
      
      context.setShadow(offset: offset, blur: 4)
      
      pushLayerDraw(block: {
        // Push letter gradient (to half brightness)
        pushDraw(block: {
          let scaledColor = baseColor.scaleBrightness(amount: 0.5)
          guard let innerGradient = Gradient.from(color1: baseColor,
                                                  to: scaledColor) else {
                                                    
                                                    return
          }
          self.addClip()
          innerGradient.drawTopToBottom(rect: self.bounds)
        })
        
        // Add the inner shadow with darker color
        pushDraw(block: {
          context.setBlendMode(.multiply)
          let innerColor : UIColor = baseColor.scaleBrightness(amount: 0.3)
          let innerOffset : CGSize = CGSize(width: 0, height: -2)
          self.drawInnerShadow(color: innerColor, size: innerOffset, blur: 2)
        })
        
        // Stroke with reversed gray gradient
        pushDraw(block: {
          self.clip(toStrokeWidth: 6)
          self.inverse.addClip()
          let c1: UIColor = UIColor(white: 0.0, alpha: 1.0)
          let c2 : UIColor = UIColor(white: 0.5, alpha: 1.0)
          guard let grayGradient = Gradient.from(color1: c1, to: c2) else {
            return
          }
          grayGradient.drawTopToBottom(rect: dest)
        })
      })
    })
  }
}

