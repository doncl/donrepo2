//
//  OverridableSizeClassVC.h
//  ObjCLingo
//
//  Created by Don Clore on 8/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WordGridCollectionView.h"

@interface OverridableSizeClassVC : UIViewController<WordGridCollectionViewDelegate,
WinnerPopupDelegate, UIViewControllerTransitioningDelegate>

- (void)transitioning;
@end
