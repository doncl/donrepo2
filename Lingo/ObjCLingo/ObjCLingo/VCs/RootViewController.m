//
//  ViewController.m
//  ObjCLingo
//
//  Created by Don Clore on 8/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

#import "RootViewController.h"
#import "ObjCLingo-Swift.h"
#import "OverridableSizeClassVC.h"

NSString * _Nonnull childVCId = @"OverridableSizeClassVCId";

@interface RootViewController ()
@property (weak, readwrite) OverridableSizeClassVC *childVC;
@property BOOL willTransitionToPortrait;
@end

@implementation RootViewController

- (void)viewDidLoad {
  [super viewDidLoad];

  UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
  OverridableSizeClassVC *overrideableSizeClassVC =
    [storyBoard instantiateViewControllerWithIdentifier:childVCId];
  
  // Establish family relationship
  [self addChildViewController:overrideableSizeClassVC];
  [self.view addSubview:overrideableSizeClassVC.view];
  [overrideableSizeClassVC didMoveToParentViewController:self];
  
  // Use Auto-layout to pin the thing to the parent rectangle.
  overrideableSizeClassVC.view.translatesAutoresizingMaskIntoConstraints = NO;
  
  UIView *overrideView = overrideableSizeClassVC.view;
  
  NSArray *constraints = @[
    [overrideView.topAnchor constraintEqualToAnchor: self.view.topAnchor],
    [overrideView.leadingAnchor constraintEqualToAnchor: self.view.leadingAnchor],
    [overrideView.trailingAnchor constraintEqualToAnchor: self.view.trailingAnchor],
    [overrideView.bottomAnchor constraintEqualToAnchor: self.view.bottomAnchor],
  ];
  
  [NSLayoutConstraint activateConstraints:constraints];
  self.childVC = overrideableSizeClassVC;
  
  [self setBackground];
}

- (void)setBackground {
  UIImage * backgroundTile = [UIImage imageNamed:@"BackgroundTile"];
  if (backgroundTile) {
    self.view.backgroundColor = [UIColor colorWithPatternImage:backgroundTile];
  } else {
    self.view.backgroundColor = [UIColor whiteColor];
  }
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  
  self.willTransitionToPortrait = self.view.frame.size.height > self.view.frame.size.width;
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
  
  if (self.childVC) {
    [self.childVC transitioning];
  }
  
  self.willTransitionToPortrait = size.height > size.width;
}

- (UITraitCollection *)overrideTraitCollectionForChildViewController:(UIViewController *)childViewController {
  if (!self.childVC) {
    return nil;
  }
  
  if (self.willTransitionToPortrait) {
    return [UITraitCollection traitCollectionWithTraitsFromCollections:@[
      [UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassCompact],
      [UITraitCollection traitCollectionWithVerticalSizeClass:UIUserInterfaceSizeClassRegular],
    ]];
  } else {
    return [UITraitCollection traitCollectionWithTraitsFromCollections:@[
      [UITraitCollection traitCollectionWithHorizontalSizeClass:UIUserInterfaceSizeClassUnspecified],
      [UITraitCollection traitCollectionWithVerticalSizeClass:UIUserInterfaceSizeClassUnspecified],
    ]];
  }
}

@end
