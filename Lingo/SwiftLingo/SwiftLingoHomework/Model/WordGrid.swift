//
//  WordGrid.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 6/29/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

/// It was more convenient for me to wrap the lines of text into an object that collects them
/// all, and can be treated as a Codable and Storable.  It's Equatable so we can detect when
/// the server has fresh information.
struct WordGridCollection : Codable, JSONCoding, Storable, Equatable {
  typealias codableType = WordGridCollection
  typealias StorableType = WordGridCollection
  
  var grids : [WordGrid] = []
    
  /// Clear out only the WordGrid file from the caches.
  ///
  /// - Parameter failure: Failure from Storable protocol extension.
  static func clearCache(failure: (String) ->() ) {
    WordGridCollection.remove(Model.cachedDataName, from: .caches, failure: failure)
  }
}

/// This needs reference semantics, so it's a class, not a struct.
class TargetWordInfo {
  var word : String
  var coords : [CellCoords]
  var found : Bool
  
  init(word: String, coords: [CellCoords]) {
    self.word = word
    self.coords = coords
    self.found = false
  }
}

/// This is an object representation of the lines of text in the challenges text file.
struct WordGrid : Codable, JSONCoding, Equatable {
  typealias codableType = WordGrid
  
  private enum CodingKeys : String, CodingKey {
    case sourceLanguage = "source_language"
    case word = "word"
    case characterGrid = "character_grid"
    case wordLocations = "word_locations"
    case targetLanguage = "target_language"
  }
  
  var sourceLanguage : String?
  var word : String?
  var characterGrid : [[String]]?
  var wordLocations : [String : String]?
  var targetLanguage : String?
  
  func getTargetWordInfos() -> [TargetWordInfo] {
    var ret : [TargetWordInfo] = []
    guard let wordLocations = wordLocations else {
      return ret
    }
    
    for kvp in wordLocations {
      let targetString = kvp.value
      let locationString = kvp.key
      var coords = [CellCoords]()
      let separator : Character = ","
      var pair : [Int] = []
      for numericSubString in locationString.split(separator: separator) {
        guard let number = Int(numericSubString) else {
          break
        }
        pair.append(number)
        if pair.count == 2 {
          let coord = CellCoords(col: pair[0], row: pair[1])
          coords.append(coord)
          pair.removeAll()
        }
      }
      let tgtWrdInfo = TargetWordInfo(word: targetString, coords: coords)
      ret.append(tgtWrdInfo)
    }
    return ret
  }
}
