//
//  Gradient.swift
//  DrawingUtilities
//
//  Created by Don Clore on 7/9/17.
//  Copyright © 2017 Don Clore. All rights reserved.
//
//  N.B. Shameless translation of Erica Sadun's image utility code from Obj-C.

import UIKit
import QuartzCore
import CoreGraphics

typealias InterpolationBlock = (CGFloat) -> (CGFloat)

class Gradient: NSObject {
  
  private static let keepDrawing : CGGradientDrawingOptions =
    [.drawsBeforeStartLocation, .drawsAfterEndLocation]

  private static let twoPi : CGFloat = 2.0 * CGFloat.pi

  private var _storedGradient : CGGradient

  private init(_ wrappedGradient: CGGradient) {
    _storedGradient = wrappedGradient
  }

  var gradient : CGGradient {
    return _storedGradient
  }

  class func with(colors: [UIColor], andlocations locations: [CGFloat]) -> Gradient? {

    guard colors.count > 0 && locations.count > 0 else {
      return nil
    }

    let space = CGColorSpaceCreateDeviceRGB()

    let colorArray = colors.map({$0.cgColor}) as CFArray

    // Create the actual gradient object
    guard let gradient = CGGradient(colorsSpace: space, colors: colorArray,
                                    locations: locations) else {

      return nil
    }

    let returnedGradient = Gradient(gradient)
    return returnedGradient
  }

  class func from(color1 : UIColor, to color2: UIColor) -> Gradient? {
    return Gradient.with(colors: [color1, color2], andlocations: [0.0, 1.0])
  }

  //
  // MARK: Linear
  //
  func draw(from p1 : CGPoint, to p2 : CGPoint,
            style mask: CGGradientDrawingOptions) {

    guard let context = UIGraphicsGetCurrentContext() else {
      return
    }

    context.drawLinearGradient(gradient, start: p1, end: p2, options: mask)
  }

  func drawTopToBottom(rect: CGRect) {
    let p1 = rect.midTop
    let p2 = rect.midBottom

    draw(from: p1, to: p2, style: Gradient.keepDrawing)
  }
}




























