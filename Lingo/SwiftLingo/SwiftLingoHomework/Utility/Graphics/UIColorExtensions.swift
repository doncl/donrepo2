//
//  UIColorExtensions.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 7/2/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
  
  func scaleBrightness(amount: CGFloat) -> UIColor {
    var h : CGFloat = 0
    var s : CGFloat = 0
    var v : CGFloat = 0
    var a : CGFloat = 0
  
    getHue(&h, saturation: &s, brightness: &v, alpha: &a)
    
    let v1 = (v * amount).clamp(min: 0, max: 1)
    return UIColor(hue: h, saturation: s, brightness: v1, alpha: a)
  }
  
  var contrast : UIColor {
    if let colorSpace = cgColor.colorSpace {
      let numComponents = colorSpace.numberOfComponents
      if 3 == numComponents {
        var r : CGFloat = 0
        var g : CGFloat = 0
        var b : CGFloat = 0
        var a : CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let luminance : CGFloat = r * 0.2126 + g * 0.7152 + b * 0.0722
        return luminance > 0.5 ? .black : .white
      }
    }
    
    
    var w : CGFloat = 0
    var a : CGFloat = 0
    getWhite(&w, alpha: &a)
    return w > 0.5 ? .black : .white
  }
}

// MARK: Clamping
extension CGFloat {
  func clamp(min minClampingValue: CGFloat, max maxClampingValue: CGFloat) -> CGFloat {
    let maxOfTwo = fmax(minClampingValue, self)
    let minOfThat = fmin(maxOfTwo, maxClampingValue)
    return minOfThat
  }
}
