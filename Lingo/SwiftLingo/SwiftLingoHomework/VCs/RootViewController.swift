//
//  RootViewController.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 6/30/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

/// A managing view controller, inserted at the root of the hierarchy solely to make iPad
/// layouts behave as desired.
class RootViewController: UIViewController {
  
  let childVCId = "OverridableSizeClassVCId"  // storyboard id.
  var willTransitionToPortrait : Bool = false
  weak var childVC : OverridableSizeClassVC?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Load the real main VC from storyboard.
    let storyBoard = UIStoryboard.init(name: "Main", bundle: nil)
    guard let overridableSizeClassVC =
      storyBoard.instantiateViewController(withIdentifier: childVCId) as? OverridableSizeClassVC else {
        fatalError("Can't instantiate VC")
    }
    
    // Establish family relationship.
    addChildViewController(overridableSizeClassVC)
    view.addSubview(overridableSizeClassVC.view)
    overridableSizeClassVC.didMove(toParentViewController: self)
    
    // Use Auto-Layout to pin the thing to the parent rectangle.
    overridableSizeClassVC.view.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      overridableSizeClassVC.view.topAnchor.constraint(equalTo: view.topAnchor),
      overridableSizeClassVC.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      overridableSizeClassVC.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      overridableSizeClassVC.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])
    
    // Cache it for later use.
    childVC = overridableSizeClassVC
    
    setBackground()
  }
  
  private func setBackground() {
    guard let backgroundTile = UIImage(named: "BackgroundTile") else {
      view.backgroundColor = .white
      return
    }
    view.backgroundColor = UIColor(patternImage: backgroundTile)
  }
  
  /// Store the height vs. width relationship whenever VC is about to appear.
  ///
  /// - Parameter animated: just passes to super.
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    willTransitionToPortrait = view.frame.height > view.frame.width
  }
  
  /// Detects transitioning to new size (i.e. rotation).  Stores the height vs. width
  /// relationship.  Calls a method in the child VC to get it to participate in the transition.
  ///
  /// - Parameters:
  ///   - size: new size of VC.
  ///   - coordinator: The coordinator guy, used to synchronize animations on transition.
  override func viewWillTransition(to size: CGSize,
                                   with coordinator: UIViewControllerTransitionCoordinator) {
    
    childVC?.transitioning()
    willTransitionToPortrait = size.height > size.width
  }
  
  /// Override this function to specify different trait collection behavior than what is built
  /// in to the system. In this case, we basically have two Collection view's, and we want
  /// exactly two layouts - when in portrait we want them vertically stacked, and in landscape,
  /// we want them side-by-side.  Possibly we could have achieved the same thing with a
  /// UIStackView, swapping the axis when orientation changes, but anyway....it's done here
  /// with size classes and traitcollections.  On iPad's of any sort, all orientations are
  /// considered equal by Apple - it's always regular horz and vert size classes.  The desired
  /// behavior here was to have iPad distinguish between portrait and landscape, and give a
  /// different behavior in each case.   So...when we're in portrait, we're overriding the
  /// child vc's traitcollection to be regular vertical and compact horizontal, we can nudge
  /// the device into using the layout we want.
  ///
  /// Honestly, this is a case where it'd be probably easier to just do all the layout in code,
  /// and ignore Interface Builder, but it was an interesting exercise.
  // swiftlint:disable all
  /// Thanks to:  https://stackoverflow.com/questions/26633172/sizing-class-for-ipad-portrait-and-landscape-modes/28268200#28268200
  /// The comments to this post suggest that Apple themselves use this technique for 'redefining
  /// the width size class in Safari'.
  // swiftlint:enable all
  ///
  /// - Parameter childViewController: The actual main VC of the app - this parent VC exists
  ///     only to perpetrate this scheme.
  /// - Returns: The desired traitCollection, i.e. {H: .compact, V: .regular} for portrait, and
  ///   {H: Any, V: Any} for landscape.
  override func overrideTraitCollection(
    forChildViewController childViewController: UIViewController) -> UITraitCollection? {
    
    guard childViewController == childVC else {
      return nil
    }
    
    if willTransitionToPortrait {
      return UITraitCollection(traitsFrom: [
        UITraitCollection(horizontalSizeClass: .compact),
        UITraitCollection(verticalSizeClass: .regular)
      ])
    } else {
      return UITraitCollection(traitsFrom: [
        UITraitCollection(horizontalSizeClass: .unspecified),
        UITraitCollection(verticalSizeClass: .unspecified),
      ])
    }
  }
  
}
