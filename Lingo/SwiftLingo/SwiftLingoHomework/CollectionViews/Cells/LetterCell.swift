//
//  LetterCell.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 7/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

/// CollectionViewCell with label and some CA-based custom border stuff.
class LetterCell: UICollectionViewCell {
  var label : UILabel = UILabel()
  
  // Grid cell borders - originally, I just did these with CALayer, and set the frame, but
  // they didn't quite look right where the lines joined up, it was subtle.   So, I think I
  // get a slightly better result by using shape layer and a bezierpath with butted ends and
  // miter line join.   
  var lightBorder : CAShapeLayer = CAShapeLayer()
  var darkBorder : CAShapeLayer = CAShapeLayer()
  
  var inProcessSelectionLayer : CAShapeLayer = CAShapeLayer()
  
  // Colors from asset catalog....
  let greyBackground : UIColor = UIColor(named: "LetterBackground")!
  let selectedColor : UIColor = UIColor(named: "Selected")!
  
  //Color Literals.  FWIW.
  let lightBorderColor = #colorLiteral(red: 0.9333333333, green: 0.9333333333, blue: 0.9333333333, alpha: 1).cgColor
  let darkBorderColor = #colorLiteral(red: 0.8156862745, green: 0.8156862745, blue: 0.8156862745, alpha: 1).cgColor

  override init(frame: CGRect) {
    super.init(frame: frame)
 
    backgroundColor = greyBackground
    
    contentView.addSubview(label)
    label.textAlignment = .center
    label.font = UIFont.preferredFont(forTextStyle: .headline)
    label.textColor = .black

    layer.addSublayer(darkBorder)
    layer.addSublayer(lightBorder)
    
    contentView.layer.insertSublayer(inProcessSelectionLayer, below: label.layer)
    inProcessSelectionLayer.isHidden = true
    inProcessSelectionLayer.fillColor = UIColor(named: "InProcessSelection")!.cgColor
  }
  
  /// A nib doesn't really offer much value to us.  This is just a label with a letter, and
  /// some CA to do the borders.
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  /// Core Animation layers have no concept of auto-layout, so we just position the frames
  /// during layoutSubviews.
  override func layoutSubviews() {
    super.layoutSubviews()
    label.frame = contentView.bounds  // in lieu of Auto-layout.
    
    let bottom = contentView.bounds.height
    let width = contentView.bounds.width

    var path = UIBezierPath()
    path.move(to: CGPoint(x: 0, y: 0))
    path.addLine(to: CGPoint(x: 0, y: bottom))
    path.addLine(to: CGPoint(x: width, y: bottom))
    
    setupPath(path: path, shapeLayer: darkBorder, color: darkBorderColor)
    
    path = UIBezierPath()
    path.move(to: CGPoint(x: 1.0, y: 0))
    path.addLine(to: CGPoint(x: 1.0, y: bottom - 1.0))
    path.addLine(to: CGPoint(x: width, y: bottom - 1.0))
    
    setupPath(path: path, shapeLayer: lightBorder, color: lightBorderColor)
  }
  
  private func setupPath(path: UIBezierPath, shapeLayer : CAShapeLayer, color: CGColor) {
    shapeLayer.path = path.cgPath
    shapeLayer.strokeColor = color
    shapeLayer.lineWidth = 1.0
    shapeLayer.lineJoin = kCALineJoinMiter
    shapeLayer.lineCap = kCALineCapButt
    shapeLayer.fillColor = UIColor.clear.cgColor
    shapeLayer.backgroundColor = color
  }
}

extension LetterCell {    
  func setSelected(blueCircle: Bool, amberBackground: Bool) {
    assert(blueCircle && !amberBackground || !blueCircle && amberBackground ||
      !blueCircle && !amberBackground)
    
    if false == blueCircle {
      inProcessSelectionLayer.isHidden = true
      label.textColor = .black
    } else {
      let path = UIBezierPath(ovalIn: contentView.bounds.inset(byPercent: 0.15))
      inProcessSelectionLayer.path = path.cgPath
      inProcessSelectionLayer.isHidden = false
      label.textColor = .white
    }
    
    if false == amberBackground {
      backgroundColor = greyBackground
      label.textColor = .black
    } else {
      // probably this could be built on top of the normal selection mechanism, but this seems
      // more straightforward at the moment.
      backgroundColor = selectedColor
      label.textColor = .white
    }
    
    setNeedsDisplay()
  }
}
