//
//  TargetWordCollectionView.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 7/1/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import UIKit

/// This is the rectangular area that has target words in it, and it has a header that shows
/// the original source word, and source and destination languages.
/// Dislays two columns if there are multiple words, and there is room for that.
/// It's responsible for putting the checkmark by the target word when one is found in the
/// word grid by the user.
@IBDesignable class TargetWordCollectionView: UICollectionView {
  let targetCellId = "targetCellId"
  let headerId = "targetCollectionHeaderId"
  let headerReferenceSize = CGSize(width: 0, height: 87)
  let sectionToItemsPad = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
  
  weak var flow : UICollectionViewFlowLayout?
  
  let insets = UIEdgeInsets(top: 5.0, left: 10.0, bottom: 5.0, right: 10.0)
  var targetWords : [String] = []
  
  var wordGrid : WordGrid? {
    didSet {
      guard let grid = self.wordGrid else {
        return
      }
      guard let wordLocationDictionary = grid.wordLocations else {
        return
      }
      
      targetWords = wordLocationDictionary.map {$0.1}
      
      updateCellSize()
      reloadSections(IndexSet(integer:0))
    }
  }
  
  // MARK: - Initializer
  
  // Load from IB.
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    backgroundColor = .purple
    dataSource = self
    
    guard let flowLayout = collectionViewLayout as? UICollectionViewFlowLayout else {
      fatalError("Assumptions broken - this class is supposed to use the flow layout")
    }
  
    flowLayout.sectionInset = sectionToItemsPad
    flowLayout.headerReferenceSize = headerReferenceSize
    
    flow = flowLayout
    
    self.contentInset = insets
    register(TargetWordCell.self, forCellWithReuseIdentifier: targetCellId)
    register(TargetCollectionHeader.self,
             forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
             withReuseIdentifier: headerId)
  }
  
  func updateCellSize() {
    guard  let flow = flow else {
      fatalError("Unexpected - this class uses flow layout")
    }
    guard targetWords.count > 0 else {
      return
    }
    
    var largestWidth : CGFloat = 0
    var largestHeight : CGFloat = 0
    for targetWord in targetWords {
      let ns = NSString(string: targetWord)
      let size = ns.size(withAttributes: [NSAttributedStringKey.font : TargetWordCell.preferredFont])
      largestWidth = max(size.width, largestWidth)
      largestHeight = max(size.height, largestHeight)
    }
    let width = self.frame.width - (insets.left + insets.right)
    let maxTwoColWidth = width / 2 - flow.minimumInteritemSpacing
    
    let itemSize : CGSize
    if largestWidth > maxTwoColWidth {
      itemSize = CGSize(width: largestWidth, height: largestHeight)
    } else {
      // Two Columns
      itemSize = CGSize(width: maxTwoColWidth, height: largestHeight)
    }
    flow.itemSize = itemSize
    flow.invalidateLayout()
  }
  
  func removeCheckmarkFromAllCells() {
    for cell in visibleCells {
      if let targetCell = cell as? TargetWordCell {
        targetCell.checkMark.isHidden = true
      }
    }
  }
}

// MARK: UICollectionViewDataSource
extension TargetWordCollectionView : UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int)
    -> Int {
      
    return targetWords.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    assert(targetWords.count > indexPath.item)
    
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: targetCellId,
                                                        for: indexPath) as? TargetWordCell else {
                                                          fatalError("cannot dequeue cell")
    }
    
    cell.label.text = targetWords[indexPath.item].uppercased()
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView,
                      viewForSupplementaryElementOfKind kind: String,
                      at indexPath: IndexPath) -> UICollectionReusableView {
    
    guard let headerView =
      collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: headerId,
                                                      for: indexPath) as? TargetCollectionHeader else {
                                                        fatalError("cannot dequeue header")
    }
    
    if let wordGrid = wordGrid {
      if let word = wordGrid.word {
        headerView.wordValLabel.text = word.uppercased()
      } 
      if let srcLang = wordGrid.sourceLanguage {
        headerView.sourceLangValLabel.text = srcLang.uppercased()
      }
      if let tgtLang = wordGrid.targetLanguage {
        headerView.targetLangValLabel.text = tgtLang.uppercased()
      }
    }
    headerView.setNeedsLayout()
    return headerView
  }
}
// MARK: Handle winning.
extension TargetWordCollectionView {
  func weHaveAWinner(winner: String) {
    guard let index = targetWords.index(of: winner) else {
      print("Couldn't find target word \(winner)")
      return
    }
    
    let indexPath = IndexPath(item: index, section: 0)
    selectItem(at: indexPath, animated: false, scrollPosition: .top)
    if let cell = cellForItem(at: indexPath) as? TargetWordCell {
      cell.checkMark.isHidden = false
      cell.setNeedsDisplay()
    }
  }
}

