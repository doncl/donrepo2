//
//  CellCoords.swift
//  SwiftLingoHomework
//
//  Created by Don Clore on 7/4/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import Foundation

/// Per the homework description: "Target words will only ever appear horizontally
/// (left to right), vertically (top to bottom) or diagonally (left to right, top to bottom).
///
/// I read this as: "Once selection has begun, the current candidate cell for selection must
/// be either the very first cell to be selected, OR, when compared to the previous cell, must
/// have either a larger row or column.  I read the words 'diagonally (left to right, top to
/// bottom) as SPECIFICALLY disallowing lines that:
///  a)  go left to right, in an upwards direction
///  b)  go right to left in any way, up or down.
///
///  The ONLY cases that are permitted are vertical vectors with a downward direction,
///  horizontal vectors with a left to right direction, and vectors with a negative slope, and
///  left to right direction.

/// CellCoords started out as something easier to read in code as opposed to using Int tuples,
/// but it grew into a tool for calculating the legal vectors that could be generated when
/// the user is dragging.
struct CellCoords : Equatable {
  var col : Int
  var row : Int
  
  
  /// Given a pair of CellCoords, return a vector for the grid to paint with blue circles,
  /// obeying the rules as described in the assignment, and implied by the way the website
  /// game behaves.
  ///
  /// - Parameters:
  ///   - src: origin of the user's swipe.
  ///   - dest: the cell the user is over right now.
  /// - Returns: The set of cells that should be painted with blue oval paths.
  static func getVector(src : CellCoords, dest: CellCoords) -> [CellCoords]? {
    if src == dest {
      return nil
    }
    
    // if it's on the left, return nil.
    if dest.col < src.col {
      return nil
    }
    
    // It's above.
    if dest.row < src.row {
      return nil
    }
    assert(dest.col >= src.col && dest.row >= src.row)
    var ret : [CellCoords] = [CellCoords]()
    
    // Only 3 geometric slopes to worry about.  If x1 == x2, then it's a vertical line downwards,
    // if x2 > x1 and y2 > y1 then it's diagonal pointing down, and if y1 == y2, then it's
    // horizontal from left to right.
    if dest.col == src.col {
      ret = makeVerticalDownwardsVector(src: src, dest: dest)
    } else if dest.col > src.col && dest.row > src.row {
      ret = makeDiagonalLeftToRightUpToDownVector(src: src, dest: dest)
    } else if dest.row == src.row {
      ret = makeHorizontalLeftToRightVector(src: src, dest: dest)
    }
    
    assert(false == ret.isEmpty)
    
    return ret
  }
  
  // MARK: The 3 Vector's implementations
  
  static private func makeHorizontalLeftToRightVector(src: CellCoords, dest: CellCoords)
    -> [CellCoords] {
      
      assert(src.row == dest.row)
      assert(src.col < dest.col)
      
      var ret : [CellCoords] = [CellCoords]()
      for i in src.col...dest.col {
        ret.append(CellCoords(col: i, row: src.row))
      }
      return ret
  }
  
  static private func makeDiagonalLeftToRightUpToDownVector(src: CellCoords, dest: CellCoords)
    -> [CellCoords] {
      
      assert(src.col < dest.col)
      assert(src.row < dest.row)
      
      var ret : [CellCoords] = [CellCoords]()
      var row = src.row
      for i in src.col...dest.col {
        ret.append(CellCoords(col: i, row: row))
        row += 1
      }
      return ret
  }
  
  static private func makeVerticalDownwardsVector(src: CellCoords, dest: CellCoords)
    -> [CellCoords] {
      
      var ret : [CellCoords] = []
      for i in src.row...dest.row {
        ret.append(CellCoords(col: src.col, row: i))
      }
      return ret
  }
}

extension CellCoords : CustomStringConvertible {
  var description : String {
    return "(\(col), \(row))"
  }
}
