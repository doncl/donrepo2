//
//  ModelRobustnessTests.swift
//  SwiftLingoHomeworkTests
//
//  Created by Don Clore on 6/30/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import XCTest
@testable import SwiftLingoHomework

/// PRECONDITION: N.B. - for these tests to succeed, the Test scheme must have an argument of
/// "testing" passed to the target.
///
/// The sole and entire purpose of this set of tests is just to demonstrate that the Model can
/// still deliver data when the network is not available.
class ModelRobustnessTests: XCTestCase {
  var receivedNotification : Bool = false

  /// Set up observer for data change notification, and installs Network mock.
  override func setUp() {
    receivedNotification = false
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(ModelRobustnessTests.dataChanged(_:)),
                                           name: Model.modelRefreshedFromNet, object: nil)
    super.setUp()
   
    let mock = NetMock()
    mock.forceFail = false
    Net.sharedInstance = NetMock()
  }
  
  
  /// Clears the observer and the received notification flag.
  override func tearDown() {
    NotificationCenter.default.removeObserver(self)
    receivedNotification = false
  }
  
  /// Initial test demonstrates that the mock works, and returns an empty set, and that this
  /// causes a notification to get fired, since the empty set is different than what's in the
  /// bundle.
  func testEmpty() {
    WordGridCollection.clearCache(failure: { err in
      XCTFail("Failed to clear cache error = \(err)")
      return
    })

    // In this case, the cache is cleared, so it will build data from the bundle, and then hit
    // the net to check for fresh data.   Since the mock is in play, and gives it an empty set,
    // it will be empty.
    let model = Model.sharedInstance
    model.secondPhaseInitializer()
    
    XCTAssertTrue(model.data.grids.isEmpty)
    
    // But it will have received data change notification.
    XCTAssertTrue(receivedNotification)
  }
  
  /// Sets up some test data from a test bundle, clears cache and proves that the Model returns
  /// the version the NetMock delivered, and the notification is received.
  func testThatChangedValueCausesNotification() {
    WordGridCollection.clearCache(failure: { err in
      XCTFail("Failed to clear cache error = \(err)")
      return
    })
    guard let testLines = getTestBundleLines() else {
      XCTFail("Failed setup - couldn't get test data from bundle.")
      return
    }
    
    guard let testGrids = Model.build(from: testLines) else {
      XCTFail("Failed setup - couldn't build testGrids from testLines")
      return
    }
    
    guard let netMock = Net.sharedInstance as? NetMock else {
      XCTFail("Something very wrong - Net shared instance is not a mock")
      return
    }
    
    netMock.mockGrids = testGrids
    let model = Model.sharedInstance
    model.secondPhaseInitializer()
    XCTAssertEqual(model.data.grids, testGrids)
    XCTAssertTrue(receivedNotification, "Failed to receive data change notification")
  }
  
  
  /// Demonstrates that data in the cache that's different than the bundle will be honored.
  func testThatCachedDataOverridesBundle() {
    WordGridCollection.clearCache(failure: { err in
      XCTFail("Failed to clear cache error = \(err)")
      return
    })
    guard let testLines = getTestBundleLines() else {
      XCTFail("Failed setup - couldn't get test data from bundle.")
      return
    }
    
    guard let testGrids = Model.build(from: testLines) else {
      XCTFail("Failed setup - couldn't build testGrids from testLines")
      return
    }
    
    guard let netMock = Net.sharedInstance as? NetMock else {
      XCTFail("Something very wrong - Net shared instance is not a mock")
      return
    }

    let model = Model.sharedInstance
    model.data.grids = testGrids
    model.data.store(to: .caches, as: Model.cachedDataName, success: {
      self.receivedNotification = false // sharedInstance set it to true, possibly.
      netMock.forceFail = true // Force it to use cache.
      model.secondPhaseInitializer()
      XCTAssertEqual(model.data.grids, testGrids)
      XCTAssertFalse(receivedNotification,
                     "Should not have triggered data change notif when falling back to cache")
    }, failure: { err in
      XCTFail("Failed to store to cache directory, error = \(err)")
    })
  }
  
  /// Tests that when nothing else is available, network is down, and system has cleared cache,
  /// that it still gets data from the bundle.
  func testThatFallbackToBundleWorks() {
    WordGridCollection.clearCache(failure: { err in
      XCTFail("Failed to clear cache error = \(err)")
      return
    })
    guard let mock = Net.sharedInstance as? NetMock else {
      XCTFail("Unexpected - net shared instance should be a mock!")
      return
    }
    mock.forceFail = true
    
    let model = Model.sharedInstance
    model.secondPhaseInitializer()
    
    guard let lines = Model.sharedInstance.getBundleLines(),
      let grids = Model.build(from: lines) else {
        XCTFail("Failed to build the baseline grids from bundle")
        return
    }
    
    XCTAssertEqual(grids, Model.sharedInstance.data.grids)
    XCTAssertFalse(receivedNotification,
                   "Should not have triggered data change notif when falling back to bundle")
  }
  
  @objc func dataChanged(_ note: Notification) {
    receivedNotification = true
  }
  
  /// Loads some data from the test bundle.
  ///
  /// - Returns: Test data lines of text.
  private func getTestBundleLines() -> [String]? {
    let resourceName = "modified_find_challenges"
    let bundle = Bundle(for: ModelRobustnessTests.self)
    
    guard let url = bundle.url(forResource: resourceName, withExtension: "txt") else {
      XCTFail("Could not find \(resourceName).txt")
      return nil
    }
    
    do {
      let content = try String(contentsOf: url)
      let lines = content.components(separatedBy: "\n")
      return lines.filter({$0.isEmpty == false})
    } catch let err {
      XCTFail("Failed to load \(resourceName) from bundle, err = \(err)")
      return nil
    }
  }
}

/// Simple mock of my Net class - allows me to simulate an empty return value, or a network
/// failure, or really any desired result.
private class NetMock : InterWebs {
  var mockGrids : [WordGrid] = []
  var forceFail : Bool = false
  
  func getNetworkData(uri: String, success: @escaping (String) -> (),
                      failure: @escaping (String) -> ()) {
    var ret : String = ""
    var lines : [String] = []
    
    if forceFail {
      failure("Forced failure")
      return
    }
    
    if false == mockGrids.isEmpty {
      for mockGrid in mockGrids {
        WordGrid.jsonEncode(mockGrid, success: { line in
          lines.append(line)
        }, failure: { err in
          XCTFail("Error serializing mockGrid = \(err)")
          return
        })
      }
    }
    
    ret = lines.joined(separator: "\n")
    success(ret)
  }
}
