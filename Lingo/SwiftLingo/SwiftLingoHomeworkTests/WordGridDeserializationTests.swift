//
//  WordGridDeserializationTests.swift
//  SwiftLingoHomeworkTests
//
//  Created by Don Clore on 6/29/18.
//  Copyright © 2018 Don Clore. All rights reserved.
//

import XCTest
@testable import SwiftLingoHomework

/// Tests we have the ability to deserialize data in the form it's provided from the server.
/// This is the flat file with lines of JSON.
class WordGridDeserializationTests: XCTestCase {
  var lines : [String] = []
  
  override func setUp() {
    super.setUp()

    guard let linesOfText = Model.sharedInstance.getBundleLines() else {
      XCTFail("Couldn't get lines of text from bundle")
      return
    }
    lines = linesOfText
  }
  
  
  /// Main test.  Deserialize each line separately.
  func testWordGridDeserialization() {
    for line in lines {
      WordGrid.jsonDecode(line, success: { wordGrid in
        // success
      }, failure: { err in
        XCTFail("Error decoding \(line) = \(err)")
      })
    }
  }
}
