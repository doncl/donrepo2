////+--------------------------------------------------------------------------
// 
//   StaticEntityListDecodingTests.swift
//   SWAPIDemoTests
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import XCTest
@testable import SWAPIDemo

class StaticEntityListDecodingTests: XCTestCase, ResourceTestable {
  func testLoadPeopleList() {
    let data = getResourceData(named: "StaticListOfPeople", withExtension: "json")
    
    StaticEntityList<Person>.jsonDecode(data, success: { list in
      XCTAssertEqual(list.results.count, 82)
    }, failure: { error in
      XCTFail(error)
    })
  }
  
  func testLoadFilmsList() {
    let data = getResourceData(named: "StaticListOfFilms", withExtension: "json")
    
    StaticEntityList<Film>.jsonDecode(data, success: { list in
      XCTAssertEqual(list.results.count, 6)
    }, failure: { error in
      XCTFail(error)
    })
  }
  
  func testLoadStarshipsList() {
    let data = getResourceData(named: "StaticListOfStarships", withExtension: "json")
    
    StaticEntityList<Starship>.jsonDecode(data, success: { list in
      XCTAssertEqual(list.results.count, 36)
    }, failure: { error in
      XCTFail(error)
    })
  }

  func testLoadVehiclesList() {
    let data = getResourceData(named: "StaticListOfVehicles", withExtension: "json")
    
    StaticEntityList<Vehicle>.jsonDecode(data, success: { list in
      XCTAssertEqual(list.results.count, 39)
    }, failure: { error in
      XCTFail(error)
    })
  }

  func testLoadSpeciesList() {
    let data = getResourceData(named: "StaticListOfSpecies", withExtension: "json")
    
    StaticEntityList<Species>.jsonDecode(data, success: { list in
      XCTAssertEqual(list.results.count, 37)
    }, failure: { error in
      XCTFail(error)
    })
  }

  func testLoadPlanetsList() {
    let data = getResourceData(named: "StaticListOfPlanets", withExtension: "json")
    
    StaticEntityList<Planet>.jsonDecode(data, success: { list in
      XCTAssertEqual(list.results.count, 60)
    }, failure: { error in
      XCTFail(error)
    })
  }

}
