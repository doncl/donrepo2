////+--------------------------------------------------------------------------
// 
//   EntityResponseDecodingTests.swift
//   SWAPIDemoTests
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import XCTest
@testable import SWAPIDemo

class EntityResponseDecodingTests: XCTestCase, ResourceTestable {
  func testPeopleEntityResponse() {
    let data = getResourceData(named: "PeopleEntityResponse", withExtension: "json")
    
    EntityResponse<Person>.jsonDecode(data, success: { response in
      XCTAssertEqual(response.count, 82)
      XCTAssertEqual(response.next, URL(string: "http://swapi.dev/api/people/?page=2")!)
      XCTAssertEqual(response.results.count, 10)
    }, failure: { error in
      XCTFail(error)
    })
  }
  
  func testFilmsEntityResponse() {
    let data = getResourceData(named: "FilmsEntityResponse", withExtension: "json")
    
    EntityResponse<Film>.jsonDecode(data, success: { response in
      XCTAssertEqual(response.count, 6)
      XCTAssertNil(response.next)
      XCTAssertEqual(response.results.count, 6)
    }, failure: { error in
      XCTFail(error)
    })
  }
  
  func testStarshipsEntityResponse() {
    let data = getResourceData(named: "StarshipsEntityResponse", withExtension: "json")
    
    EntityResponse<Starship>.jsonDecode(data, success: { response in
      XCTAssertEqual(response.count, 36)
      XCTAssertEqual(response.next, URL(string: "http://swapi.dev/api/starships/?page=2")!)
      XCTAssertEqual(response.results.count, 10)
    }, failure: { error in
      XCTFail(error)
    })
  }
  
  func testVehiclesEntityResponse() {
    let data = getResourceData(named: "VehiclesEntityResponse", withExtension: "json")
    
    EntityResponse<Vehicle>.jsonDecode(data, success: { response in
      XCTAssertEqual(response.count, 39)
      XCTAssertEqual(response.next, URL(string: "http://swapi.dev/api/vehicles/?page=2")!)
      XCTAssertEqual(response.results.count, 10)
    }, failure: { error in
      XCTFail(error)
    })
  }

  func testSpeciesEntityResponse() {
    let data = getResourceData(named: "SpeciesEntityResponse", withExtension: "json")
    
    EntityResponse<Species>.jsonDecode(data, success: { response in
      XCTAssertEqual(response.count, 37)
      XCTAssertEqual(response.next, URL(string: "http://swapi.dev/api/species/?page=2")!)
      XCTAssertEqual(response.results.count, 10)
    }, failure: { error in
      XCTFail(error)
    })
  }

  func testPlanetsEntityResponse() {
    let data = getResourceData(named: "PlanetsEntityResponse", withExtension: "json")
    
    EntityResponse<Planet>.jsonDecode(data, success: { response in
      XCTAssertEqual(response.count, 60)
      XCTAssertEqual(response.next, URL(string: "http://swapi.dev/api/planets/?page=2")!)
      XCTAssertEqual(response.results.count, 10)
    }, failure: { error in
      XCTFail(error)
    })
  }

}
