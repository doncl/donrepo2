////+--------------------------------------------------------------------------
// 
//   ModelEntityStorageManagerTests.swift
//   SWAPIDemoTests
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------


import XCTest
@testable import SWAPIDemo

class ModelEntityStorageManagerTests: XCTestCase {
  func testStorageManager() {
    let mgr = ModelEntityStorageManager.shared
    XCTAssertEqual(mgr.people.count, 82)
    XCTAssertEqual(mgr.films.count, 6)
    XCTAssertEqual(mgr.starships.count, 36)
    XCTAssertEqual(mgr.vehicles.count, 39)
    XCTAssertEqual(mgr.species.count, 37)
    XCTAssertEqual(mgr.planets.count, 60)
  }
}
