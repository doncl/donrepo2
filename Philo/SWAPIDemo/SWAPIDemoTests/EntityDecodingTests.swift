////+--------------------------------------------------------------------------
// 
//   EntityDecodingTests.swift
//   SWAPIDemoTests
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import XCTest
@testable import SWAPIDemo

class EntityDecodingTests: XCTestCase, ResourceTestable {
  func testDecodePerson() {
    let data = getResourceData(named: "LukeSkywalker", withExtension: "json")
    
    Person.jsonDecode(data, success: { person in
      XCTAssertEqual(person.name, "Luke Skywalker")
      XCTAssertEqual(person.height, "172")
      XCTAssertEqual(person.mass, "77")
      XCTAssertEqual(person.hairColor, "blond")
      XCTAssertEqual(person.skinColor, "fair")
      XCTAssertEqual(person.eyeColor, "blue")
      XCTAssertEqual(person.gender, "male")
      XCTAssertTrue(person.species.isEmpty)
      
      let id = person.getID()
      let secureURL = person.getSecureURL()
      let entityName = person.getAPIEntityName()
      
      XCTAssertEqual(entityName, "people")
      XCTAssertEqual(id, "1")
      XCTAssertEqual(secureURL.absoluteString, "https://swapi.dev/api/people/1/")
    }, failure: { error in
      XCTFail(error)
    })
  }
  
  func testDecodeFilm() {
    let data = getResourceData(named: "ANewHope", withExtension: "json")
    
    Film.jsonDecode(data, success: { film in
      XCTAssertEqual(film.title, "A New Hope")
      XCTAssertEqual(film.episodeID, 4)
      XCTAssertTrue(film.openingCrawl.contains("It is a period of civil war."))
      XCTAssertEqual(film.director, "George Lucas")
      XCTAssertEqual(film.producer, "Gary Kurtz, Rick McCallum")
      XCTAssertEqual(film.characters.count, 18)
      XCTAssertEqual(film.planets.count, 3)
      XCTAssertEqual(film.starships.count, 8)
      XCTAssertEqual(film.vehicles.count, 4)
      XCTAssertEqual(film.species.count, 5)
      
      let id = film.getID()
      let secureURL = film.getSecureURL()
      let entityName = film.getAPIEntityName()
      
      XCTAssertEqual(entityName, "films")
      XCTAssertEqual(id, "1")
      XCTAssertEqual(secureURL.absoluteString, "https://swapi.dev/api/films/1/")

    }, failure: { error in
      XCTFail(error)
    })
  }
  
  func testDecodeStarship() {
    let data = getResourceData(named: "CR90Corvette", withExtension: "json")
    
    Starship.jsonDecode(data, success: { starship in
      XCTAssertEqual(starship.name, "CR90 corvette")
      XCTAssertEqual(starship.model, "CR90 corvette")
      XCTAssertEqual(starship.manufacturer, "Corellian Engineering Corporation")
      XCTAssertEqual(starship.costInCredits, "3500000")
      XCTAssertEqual(starship.length, "150")
      XCTAssertEqual(starship.maxAtmospheringSpeed, "950")
      XCTAssertEqual(starship.crew, "30-165")
      XCTAssertEqual(starship.passengers, "600")
      XCTAssertEqual(starship.cargoCapacity, "3000000")
      XCTAssertEqual(starship.consumables, "1 year")
      XCTAssertEqual(starship.hyperdriveRating, "2.0")
      XCTAssertEqual(starship.mglt, "60")
      XCTAssertEqual(starship.starshipClass, "corvette")
      XCTAssertTrue(starship.pilots.isEmpty)
      XCTAssertEqual(starship.films.count, 3)
      
      let id = starship.getID()
      let secureURL = starship.getSecureURL()
      let entityName = starship.getAPIEntityName()
      
      XCTAssertEqual(entityName, "starships")
      XCTAssertEqual(id, "2")
      XCTAssertEqual(secureURL.absoluteString, "https://swapi.dev/api/starships/2/")

    }, failure: { error in
      XCTFail(error)
    })
  }
  
  func testDecodeVehicle() {
    let data = getResourceData(named: "X34Landspeeder", withExtension: "json")
    
    Vehicle.jsonDecode(data, success: { vehicle in
      XCTAssertEqual(vehicle.name, "X-34 landspeeder")
      XCTAssertEqual(vehicle.name, vehicle.model)
      XCTAssertEqual(vehicle.manufacturer, "SoroSuub Corporation")
      XCTAssertEqual(vehicle.costInCredits, "10550")
      XCTAssertEqual(vehicle.length, "3.4 ")  //sic
      XCTAssertEqual(vehicle.maxAtmospheringSpeed, "250")
      XCTAssertEqual(vehicle.crew, "1")
      XCTAssertEqual(vehicle.passengers, "1")
      XCTAssertEqual(vehicle.cargoCapacity, "5")
      XCTAssertEqual(vehicle.consumables, "unknown")
      XCTAssertEqual(vehicle.vehicleClass, "repulsorcraft")
      XCTAssertTrue(vehicle.pilots.isEmpty)
      XCTAssertEqual(vehicle.films.count, 1)
      
      let id = vehicle.getID()
      let secureURL = vehicle.getSecureURL()
      let entityName = vehicle.getAPIEntityName()
      
      XCTAssertEqual(entityName, "vehicles")
      XCTAssertEqual(id, "7")
      XCTAssertEqual(secureURL.absoluteString, "https://swapi.dev/api/vehicles/7/")

    }, failure: { error in
      XCTFail(error)
    })
  }
  
  func testDecodeSpecies() {
    let data = getResourceData(named: "Human", withExtension: "json")

    Species.jsonDecode(data, success: { species in
      XCTAssertEqual(species.name, "Human")
      XCTAssertEqual(species.classification, "mammal")
      XCTAssertEqual(species.averageHeight, "180")
      XCTAssertEqual(species.hairColors, "blonde, brown, black, red")
      XCTAssertEqual(species.eyeColors, "brown, blue, green, hazel, grey, amber")
      XCTAssertEqual(species.averageLifespan, "120")
      XCTAssertEqual(species.language, "Galactic Basic")
      XCTAssertEqual(species.people.count, 4)
      XCTAssertEqual(species.films.count, 6)
      
      let id = species.getID()
      let secureURL = species.getSecureURL()
      let entityName = species.getAPIEntityName()
      
      XCTAssertEqual(entityName, "species")
      XCTAssertEqual(id, "1")
      XCTAssertEqual(secureURL.absoluteString, "https://swapi.dev/api/species/1/")
    }, failure: { error in
      XCTFail(error)
    })
  }

  func testDecodePlanet() {
    let data = getResourceData(named: "Tatooine", withExtension: "json")

    Planet.jsonDecode(data, success: { planet in
      XCTAssertEqual(planet.name, "Tatooine")
      XCTAssertEqual(planet.rotationPeriod, "23")
      XCTAssertEqual(planet.orbitalPeriod, "304")
      XCTAssertEqual(planet.diameter, "10465")
      XCTAssertEqual(planet.climate, "arid")
      XCTAssertEqual(planet.gravity, "1 standard")
      XCTAssertEqual(planet.terrain, "desert")
      XCTAssertEqual(planet.surfaceWater, "1")
      XCTAssertEqual(planet.population, "200000")
      XCTAssertEqual(planet.residents.count, 10)
      XCTAssertEqual(planet.films.count, 5)
      
      let id = planet.getID()
      let secureURL = planet.getSecureURL()
      let entityName = planet.getAPIEntityName()
      
      XCTAssertEqual(entityName, "planets")
      XCTAssertEqual(id, "1")
      XCTAssertEqual(secureURL.absoluteString, "https://swapi.dev/api/planets/1/")
    }, failure: { error in
      XCTFail(error)
    })
  }
}
