////+--------------------------------------------------------------------------
// 
//   SWAPIDemoIntegrationTests.swift
//   SWAPIDemoIntegrationTests
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------
import XCTest
@testable import SWAPIDemo

// N.B. These are not unit tests - they are real hitting-the-wire kinds of things that are separate from the unit tests.
class SWAPIDemoIntegrationTests: XCTestCase {
  func testGetMovies() {
    let expect = XCTestExpectation(description: "Waiting for entities")
    ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "films") { (result: Result<[Film], Error>) in
      switch result {
        case .success(let films):
          XCTAssertEqual(films.count, 6)
          expect.fulfill()
        case .failure(let error):
          XCTFail(error.localizedDescription)
          expect.fulfill()
      }
    }
    wait(for: [expect], timeout: 60)
  }
  
  func testGetPeople() {
    let expect = XCTestExpectation(description: "Waiting for entities")
    ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "people") { (result: Result<[Person], Error>) in
      switch result {
        case .success(let people):
          XCTAssertEqual(people.count, 82)
          expect.fulfill()
        case .failure(let error):
          XCTFail(error.localizedDescription)
          expect.fulfill()
      }
    }
    wait(for: [expect], timeout: 120)
  }
  
  func testGetStarships() {
    let expect = XCTestExpectation(description: "Waiting for entities")
    ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "starships") { (result: Result<[Starship], Error>) in
      switch result {
        case .success(let starships):
          XCTAssertEqual(starships.count, 36)
          expect.fulfill()
        case .failure(let error):
          XCTFail(error.localizedDescription)
          expect.fulfill()
      }
    }
    wait(for: [expect], timeout: 120)
  }

  func testGetVehicles() {
    let expect = XCTestExpectation(description: "Waiting for entities")
    ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "vehicles") { (result: Result<[Vehicle], Error>) in
      switch result {
        case .success(let vehicles):
          XCTAssertEqual(vehicles.count, 39)
          expect.fulfill()
        case .failure(let error):
          XCTFail(error.localizedDescription)
          expect.fulfill()
      }
    }
    wait(for: [expect], timeout: 120)
  }
  
  func testGetSpecies() {
    let expect = XCTestExpectation(description: "Waiting for entities")
    ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "species") { (result: Result<[Species], Error>) in
      switch result {
        case .success(let species):
          XCTAssertEqual(species.count, 37)
          expect.fulfill()
        case .failure(let error):
          XCTFail(error.localizedDescription)
          expect.fulfill()
      }
    }
    wait(for: [expect], timeout: 120)
  }

  func testGetPlanets() {
    let expect = XCTestExpectation(description: "Waiting for entities")
    ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "planets") { (result: Result<[Planet], Error>) in
      switch result {
        case .success(let planets):
          XCTAssertEqual(planets.count, 60)
          expect.fulfill()
        case .failure(let error):
          XCTFail(error.localizedDescription)
          expect.fulfill()
      }
    }
    wait(for: [expect], timeout: 120)
  }
}
