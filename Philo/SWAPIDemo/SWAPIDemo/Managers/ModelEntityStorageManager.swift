////+--------------------------------------------------------------------------
// 
//   ModelEntityStorageManager.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import Foundation

struct StaticEntityList<T: ModelEntity & Codable & JSONCoding>: Codable, JSONCoding {
  typealias codableType = StaticEntityList
  
  let results: [T]
}

class ModelEntityStorageManager {
  let oneMinute: TimeInterval = 60.0
    
  var people: [Person] = []
  var films: [Film] = []
  var starships: [Starship] = []
  var vehicles: [Vehicle] = []
  var species: [Species] = []
  var planets: [Planet] = []
  
  var loaded: Bool = false
  
  static let shared: ModelEntityStorageManager = ModelEntityStorageManager()
  
  private init() {}
    
  func load() {
    guard !loaded else { return }
    defer {
      loaded = true 
    }
    people = getModelData(fromResourceFile: "StaticListOfPeople")
    films = getModelData(fromResourceFile: "StaticListOfFilms")
    starships = getModelData(fromResourceFile: "StaticListOfStarships")
    vehicles = getModelData(fromResourceFile: "StaticListOfVehicles")
    species = getModelData(fromResourceFile: "StaticListOfSpecies")
    planets = getModelData(fromResourceFile: "StaticListOfPlanets")
    lazilyAcquireLatestVersions()
  }
  
  func getDisplayName(forURL url: URL, ofType type: ModelEnum) -> String {
    let fallback = type.rawValue
    switch type {
      case .people:
        guard let match = people.first(where: { $0.url == url }) else {
          return fallback
        }
        return match.name 
      case .films:
        guard let match = films.first(where: { $0.url == url }) else {
          return fallback
        }
        return match.title
      case .starships:
        guard let match = starships.first(where: { $0.url == url }) else {
          return fallback
        }
        return match.name
      case .vehicles:
        guard let match = vehicles.first(where: { $0.url == url }) else {
          return fallback
        }
        return match.name
      case .species:
        guard let match = species.first(where: { $0.url == url }) else {
          return fallback
        }
        return match.name
      case .planets:
        guard let match = planets.first(where: { $0.url == url }) else {
          return fallback
        }
        return match.name
    }
  }
  
  func getDisplayName(forEntity entity: ModelEntity) -> String {
    let fallback = entity.getAPIEntityName()
    guard let modEnum = entity.getModelEnum() else {
      return fallback
    }
    let id = entity.getID()
    
    switch modEnum {
      case .people:
        guard let match = people.first(where: { $0.getID() == id }) else {
          return fallback
        }
        return match.name
      case .films:
        guard let match = films.first(where: { $0.getID() == id }) else {
          return fallback
        }
        return match.title
      case .starships:
        guard let match = starships.first(where: { $0.getID() == id }) else {
          return fallback
        }
        return match.name
      case .vehicles:
        guard let match = vehicles.first(where: { $0.getID() == id }) else {
          return fallback
        }
        return match.name
      case .species:
        guard let match = species.first(where: { $0.getID() == id }) else {
          return fallback
        }
        return match.name
      case .planets:
        guard let match = planets.first(where: { $0.getID() == id }) else {
          return fallback
        }
        return match.name
    }    
  }
    
  private func getModelData<T: ModelEntity & JSONCoding & Codable>(fromResourceFile resourceFile: String) -> [T] {
    let bundle = Bundle.main
    var results: [T] = []
    guard let url = bundle.url(forResource: resourceFile, withExtension: "json") else {
      return results
    }
  
    guard let data = try? Data(contentsOf: url) else {
      return results
    }
    
    StaticEntityList<T>.jsonDecode(data, success: { list in
      results = list.results
    }, failure: { error in
      fatalError("App is not provisioned properly")
    })
    
    return results
  }
}

extension ModelEntityStorageManager {
  // N.B. - the entire goal here is to do all this on a background thread, and do it very lazily, acquiring each block of
  // things a minute apart.   There are other ways to accomplish this, one can imagine using a background URLSession that
  // wakes up in the middle of the night or something, and lazily, and unobtrusively freshens the list of data.
  private func lazilyAcquireLatestVersions() {
    getPeople { [weak self] in
      guard let self = self else { return }
      self.getFilms { [weak self] in
        guard let self = self else { return }
        self.getStarships { [weak self] in
          guard let self = self else { return }
          self.getVehicles { [ weak self] in
            guard let self = self else { return }
            self.getSpecies { [weak self] in
              guard let self = self else { return }
              self.getPlanets { }
            }
          }
        }
      }
    }
  }

  // MARK: TODO - Paramterize all this so it's more DRY - I want to move on now.
  private func getPeople(completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + self.oneMinute) {
      ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "people") { [weak self] (result: Result<[Person], Error>) in
        guard let self = self else { return }
        self.manage(result: result, entities: &self.people)
        completion()
      }
    }
  }

  private func getFilms(completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + self.oneMinute) {
      ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "films") { [weak self] (result: Result<[Film], Error>) in
        guard let self = self else { return }
        self.manage(result: result, entities: &self.films)
        completion()
      }
    }
  }
  
  private func getStarships(completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + self.oneMinute) {
      ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "starships") { [weak self] (result: Result<[Starship], Error>) in
        guard let self = self else { return }
        self.manage(result: result, entities: &self.starships)
        completion()
      }
    }
  }

  private func getVehicles(completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + self.oneMinute) {
      ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "vehicles") { [weak self] (result: Result<[Vehicle], Error>) in
        guard let self = self else { return }
        self.manage(result: result, entities: &self.vehicles)
        completion()
      }
    }
  }
  
  private func getSpecies(completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + self.oneMinute) {
      ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "species") { [weak self] (result: Result<[Species], Error>) in
        guard let self = self else { return }
        self.manage(result: result, entities: &self.species)
        completion()
      }
    }
  }

  private func getPlanets(completion: @escaping () -> ()) {
    DispatchQueue.main.asyncAfter(deadline: .now() + self.oneMinute) {
      ModelEntityAcquisitionManager.shared.getAll(entityAPIName: "planets") { [weak self] (result: Result<[Planet], Error>) in
        guard let self = self else { return }
        self.manage(result: result, entities: &self.planets)
        completion()
      }
    }
  }
    
  // N.B. probably a final, more polished version of this would do a diff of the in-memory objects to the ones just acquired.
  // This is (I hope) sufficient to get the idea across, and to convince the reader that I could implement something like that
  // if we defined a need for it.
  // The main thing I wanted to have was the entire corpus of data available on startup, read from a resource, because the
  // data set just isn't that big, and changes rarely (more accurately, probably never).
  private func manage<T: ModelEntity>(result: Result<[T], Error>, entities: inout [T]) {
    precondition(Thread.isMainThread)
    
    switch result {
      case .success(let freshEntities):
        entities.removeAll()
        entities.append(contentsOf: freshEntities)
      case .failure(let error):
        print(error)
    }
  }
}
