////+--------------------------------------------------------------------------
// 
//   ModelEntityManager.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import Foundation

typealias ResponseEntity = Codable & JSONCoding & ModelEntity

enum EntityFetchError: Error {
  case networkfetchfailure(String)
  case jsondecodefailure(String)
}

struct ModelEntityAcquisitionManager {
  let pageSize: Int = 10
  private let q : DispatchQueue
  static let shared = ModelEntityAcquisitionManager()
  
  private init() {
    q = DispatchQueue(label: "ModelEntityAcquisitionManager Queue", qos: .background, attributes: [],
                      autoreleaseFrequency: .inherit, target: nil)
  }
  
  func getAll<T: ModelEntity & Codable & JSONCoding>(entityAPIName: String, result: @escaping (Result<[T], Error>) -> ()) {
    var totalAdditionalPages: Int = 0
    var results: [(pageNumber: Int, entities:[T])] = []
    let base = "https://swapi.dev/api/\(entityAPIName)/?"
    let opQ = OperationQueue()
    let completionOperation = BlockOperation {
      var completionResults: [T] = []
      let sortedResults = results.sorted(by: { lhs, rhs in
        return lhs.pageNumber < rhs.pageNumber
      })
      for resultItem in sortedResults {
        completionResults.append(contentsOf: resultItem.entities)
      }
      DispatchQueue.main.async {
        result(.success(completionResults))
      }
    }
    
    q.async {
      let firstOperation = GetModelDataOperation(base: base, page: 1, entityAPIName: entityAPIName, q: q) { page, string in
        guard let string = string else {
          result(.failure(EntityFetchError.networkfetchfailure("Nothing came back from network")))
          return
        }
        EntityResponse<T>.jsonDecode(string, success: { response in
          let docCount = response.count
          totalAdditionalPages = docCount / pageSize
          if docCount % pageSize == 0 {
            totalAdditionalPages = max(0, (totalAdditionalPages - 1))
          }
          let documents = response.results
          let tuple = (pageNumber: page, entities: documents)
          results.append(tuple)
          
          for i in 0..<totalAdditionalPages {
            let op = GetModelDataOperation(base: base, page: i + 2, entityAPIName: entityAPIName, q: q) { page, string in
              guard let string = string else {
                // operation did 'complete operation'
                return
              }
              EntityResponse<T>.jsonDecode(string, success: { response in
                let tuple = (pageNumber: page, entities: response.results)
                results.append(tuple)
              }, failure: { error in
                print("Error decoding returned data for \(base)\(i + 2) = \(error), continuing on")
              })
            }
            completionOperation.addDependency(op)
            opQ.addOperation(op)
          }
        }, failure: { error in
          result(.failure(EntityFetchError.jsondecodefailure(error)))
        })
      }
      
      completionOperation.addDependency(firstOperation)
      opQ.addOperation(firstOperation)
      opQ.addOperation(completionOperation)
    }
  }
}

@objc class GetModelDataOperation: ConcurrentOperation {
  let page: Int
  let entityAPIName: String
  let base: String
  weak var q: DispatchQueue?
  let completion: (Int, String?) -> ()
 
  init(base: String, page: Int, entityAPIName: String, q: DispatchQueue, completion: @escaping (Int, String?) -> ()) {
    self.page = page
    self.q = q
    self.entityAPIName = entityAPIName
    self.completion = completion
    self.base = base
    super.init()
  }
  
  override func main() {
    let pageURL = "\(base)page=\(page)"
    Net.sharedInstance.getNetworkData(uri: pageURL, success: { [weak self] stringData in
      guard let self = self, let q = self.q else {
        return
      }
      q.async {
        self.completion(self.page, stringData)
        self.completeOperation()
      }
    }, failure: { [weak self] error in
      guard let self = self, let q = self.q else {
        return
      }
      print("failure making call \(pageURL) = \(error)")
      q.async {
        self.completion(self.page, nil)
        self.completeOperation()
      }
    })
  }
}
