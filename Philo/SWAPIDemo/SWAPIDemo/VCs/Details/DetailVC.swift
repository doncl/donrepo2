////+--------------------------------------------------------------------------
// 
//   DetailVC.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------


import UIKit

protocol DetailVCDelegate: class {
  func getRootVC() -> RootVC
}

class DetailVC: UIViewController {
  struct Constants {
    static let topPad: CGFloat = 32.0
    static let vertPad: CGFloat = 8.0
    static let horzPad: CGFloat = 12.0
    static let leadingPad: CGFloat = 8.0
    static let imageRatio: CGFloat = 0.45
    static let aspectRatio: CGFloat = 1.3
  }
  
  // Yes, this could be a SwiftUI Form, or maybe PersonVC and FilmVC could be one, or subclasses, but this is what I chose to do.   I'm glad to discuss, if it matters.

  let heroImage: UIImageView = UIImageView()
  let nameLabel: UILabel = UILabel()
  var detailsFormParent: UIView = UIView()
  
  weak var delegate: DetailVCDelegate?
  
  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.systemBackground
    
    [nameLabel, heroImage, detailsFormParent].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      view.addSubview($0)
    }
    
    heroImage.contentMode = UIView.ContentMode.scaleAspectFill
    heroImage.clipsToBounds = true
            
    NSLayoutConstraint.activate([
      nameLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Constants.topPad),
      nameLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
      
      heroImage.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: Constants.topPad),
      heroImage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: Constants.leadingPad),
      heroImage.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor, multiplier: Constants.imageRatio),
      heroImage.heightAnchor.constraint(equalTo: heroImage.widthAnchor, multiplier: Constants.aspectRatio),
      
      detailsFormParent.topAnchor.constraint(equalTo: heroImage.topAnchor),
      detailsFormParent.leadingAnchor.constraint(equalTo: heroImage.trailingAnchor, constant: Constants.horzPad),
      detailsFormParent.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -Constants.horzPad),
      detailsFormParent.heightAnchor.constraint(equalTo: heroImage.heightAnchor),
    ])
  }
}

