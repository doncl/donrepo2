////+--------------------------------------------------------------------------
// 
//   FilmVC.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

class FilmVC: DetailVC, HyperPaneProtocol {
  static let vertPad: CGFloat = 24.0
  
  let film: Film
  let detailsForm: FilmDetailsForm
  let hyperPane: FilmHyperPane
  
  init(film: Film) {
    self.film = film
    detailsForm = FilmDetailsForm(film: film)
    hyperPane = FilmHyperPane(film: film)
    super.init()
    let id = film.getID()
    let filmImageName = "Film\(id)"
    if let filmImage = UIImage(named: filmImageName) {
      heroImage.image = filmImage
    }
    nameLabel.text = film.title
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    detailsForm.translatesAutoresizingMaskIntoConstraints = false
    detailsFormParent.addSubview(detailsForm)
    NSLayoutConstraint.activate([
      detailsForm.topAnchor.constraint(equalTo: detailsFormParent.topAnchor),
      detailsForm.leadingAnchor.constraint(equalTo: detailsFormParent.leadingAnchor),
      detailsForm.trailingAnchor.constraint(equalTo: detailsFormParent.trailingAnchor),
      detailsForm.bottomAnchor.constraint(equalTo: detailsFormParent.bottomAnchor),
    ])
    
    hyperPane.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(hyperPane)
    hyperPane.delegate = self
    
    NSLayoutConstraint.activate([
      hyperPane.topAnchor.constraint(equalTo: detailsForm.bottomAnchor, constant: FilmVC.vertPad),
      hyperPane.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      hyperPane.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      hyperPane.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])

  }
}

extension FilmVC {
  class FilmDetailsForm: UIView {
    let film: Film
    let episodeId, director, producer, created, released: TwoLabelStack
    
    let stack: UIStackView
    
    // TODO:  Make a special animated opening crawl pane.

    init(film: Film) {
      self.film = film
      let df = DateFormatter()
      df.dateStyle = .medium
      df.timeStyle = .none
      let createdDate = df.string(from: film.created)
      let releasedDate = df.string(from: film.releaseDate)
      self.episodeId = TwoLabelStack(name: "EpisodeID:", value: String(film.episodeID))
      self.director = TwoLabelStack(name: "Director:", value: film.director)
      self.producer = TwoLabelStack(name: "Producer:", value: film.producer)
      self.created = TwoLabelStack(name: "Created:", value: createdDate)
      self.released = TwoLabelStack(name: "Opened:", value: releasedDate)
      stack = UIStackView(arrangedSubviews: [self.episodeId, self.director,
                                             self.producer, self.created, self.released])
      super.init(frame: CGRect.zero)
      
      stack.axis = .vertical
      stack.distribution = .fillEqually
      addSubview(stack)
      stack.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
  }
}
