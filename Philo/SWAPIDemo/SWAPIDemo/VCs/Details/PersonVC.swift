////+--------------------------------------------------------------------------
// 
//   PersonVC.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

class PersonVC: DetailVC, HyperPaneProtocol {
  static let vertPad: CGFloat = 24.0
  let person: Person
  let detailsForm: PersonDetailsForm
  let hyperPane: PersonHyperPane
  
  init(person: Person) {
    self.person = person
    detailsForm = PersonDetailsForm(person: person)
    hyperPane = PersonHyperPane(person: person)
    super.init()
    let id = person.getID()
    let personImageName: String = "Person\(id)"
    if let personImage = UIImage(named: personImageName) {
      heroImage.image = personImage
    }
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  override func viewDidLoad() {
    super.viewDidLoad()
    detailsForm.translatesAutoresizingMaskIntoConstraints = false
    detailsFormParent.addSubview(detailsForm)
    NSLayoutConstraint.activate([
      detailsForm.topAnchor.constraint(equalTo: detailsFormParent.topAnchor),
      detailsForm.leadingAnchor.constraint(equalTo: detailsFormParent.leadingAnchor),
      detailsForm.trailingAnchor.constraint(equalTo: detailsFormParent.trailingAnchor),
      detailsForm.bottomAnchor.constraint(equalTo: detailsFormParent.bottomAnchor),
    ])
    
    hyperPane.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(hyperPane)
    hyperPane.delegate = self
    NSLayoutConstraint.activate([
      hyperPane.topAnchor.constraint(equalTo: detailsForm.bottomAnchor, constant: PersonVC.vertPad),
      hyperPane.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      hyperPane.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      hyperPane.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
  }
}

// MARK: PersonDetailsForm
extension PersonVC {
  class PersonDetailsForm: UIView {
    let person: Person
    let height: TwoLabelStack
    let mass: TwoLabelStack
    let hairColor: TwoLabelStack
    let eyeColor: TwoLabelStack
    let birthYear: TwoLabelStack
    let gender: TwoLabelStack
    
    let stack: UIStackView
    
    init(person: Person) {
      self.person = person
      self.height = TwoLabelStack(name: "Height:", value: person.height)
      self.mass = TwoLabelStack(name: "Mass:", value: person.mass)
      self.hairColor = TwoLabelStack(name: "HairColor:", value: person.hairColor)
      self.eyeColor = TwoLabelStack(name: "EyeColor:", value: person.eyeColor)
      self.birthYear = TwoLabelStack(name: "Birth Year:", value: person.birthYear)
      self.gender = TwoLabelStack(name: "Gender:", value: person.gender)
      stack = UIStackView(arrangedSubviews: [self.height, self.mass, self.hairColor, self.eyeColor, self.birthYear, self.gender])
      
      super.init(frame: CGRect.zero)
      
      stack.axis = .vertical
      stack.distribution = .fillEqually
      addSubview(stack)
      stack.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    required init?(coder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
  }
}
