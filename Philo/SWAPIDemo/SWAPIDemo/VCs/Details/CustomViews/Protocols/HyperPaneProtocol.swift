////+--------------------------------------------------------------------------
// 
//   HyperPaneProtocol.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

protocol HyperPaneProtocol: AnyObject {
  var delegate: DetailVCDelegate? { get }
  func hyperLinkTouched(forEntityType: ModelEnum, hyperLinkURL: URL)
}

extension HyperPaneProtocol where Self: UIViewController {
  func hyperLinkTouched(forEntityType: ModelEnum, hyperLinkURL: URL) {
    guard let rootVC = delegate?.getRootVC() else {
      return
    }
    
    switch forEntityType {
      case .people:
        guard let person = ModelEntityStorageManager.shared.people.first(where: { $0.url == hyperLinkURL }) else {
          break
        }
        rootVC.selected(person: person)
      case .films:
        guard let film = ModelEntityStorageManager.shared.films.first(where: { $0.url == hyperLinkURL }) else {
          break
        }
        rootVC.selected(film: film)
      case .starships, .vehicles, .species, .planets:
        // Not Yet Implemented
        break
    }
  }
}
