////+--------------------------------------------------------------------------
// 
//   FilmHyperPane.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------


import UIKit

class FilmHyperPane: UIView {
  static let topPad: CGFloat = 24.0
  static let vertPad: CGFloat = 8.0
  static let labelToPickerPad: CGFloat = 42.0
  static let horzPad: CGFloat = 24.0
  static let pickerHeight: CGFloat = 48.0
  static let pickerWidth: CGFloat = 300.0
  let film: Film

  weak var delegate: HyperPaneProtocol?
  
  let peoplePicker: HyperPickerControl
  
  init(film: Film) {
    self.film = film
    self.peoplePicker = HyperPickerControl(title: "Characters", entityEnum: .people, type: .people, urls: film.characters)
    super.init(frame: CGRect.zero)
    
    peoplePicker.translatesAutoresizingMaskIntoConstraints = false
    addSubview(peoplePicker)
    
    NSLayoutConstraint.activate([
      peoplePicker.topAnchor.constraint(equalTo: topAnchor, constant: FilmHyperPane.topPad),
      peoplePicker.centerXAnchor.constraint(equalTo: centerXAnchor),
      peoplePicker.widthAnchor.constraint(equalToConstant: FilmHyperPane.pickerWidth),
      peoplePicker.heightAnchor.constraint(equalToConstant: FilmHyperPane.pickerHeight),
    ])
    
    peoplePicker.delegate = self
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension FilmHyperPane: HyperPickerControlDelegate {
  func entityPicked(_ hyperpickerControl: HyperPickerControl, url: URL, entityEnum: ModelEnum) {
    guard let delegate = delegate else {
      return
    }
    delegate.hyperLinkTouched(forEntityType: entityEnum, hyperLinkURL: url)
  }
}
