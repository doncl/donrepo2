////+--------------------------------------------------------------------------
// 
//   TwoLabelStack.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

class TwoLabelStack: UIStackView {
  let name: UILabel = UILabel()
  let value: UILabel = UILabel()
  
  init(name: String, value: String) {
    super.init(frame: CGRect.zero)
    self.name.text = name
    self.value.text = value
    addArrangedSubview(self.name)
    addArrangedSubview(self.value)
    axis = .horizontal
    alignment = .center
    distribution = .fillProportionally
    self.name.textAlignment = .left  // TODO: figure out better way for right-to-left languages.
    self.value.textAlignment = .right
    self.name.font = UIFont.preferredFont(forTextStyle: .title3).bold()
    self.name.adjustsFontSizeToFitWidth = true
    self.value.font = UIFont.preferredFont(forTextStyle: .footnote).italic()
    self.value.adjustsFontSizeToFitWidth = true
    self.value.numberOfLines = 2
  }
  
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
