////+--------------------------------------------------------------------------
// 
//   HyperPickerControl.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------


import UIKit

protocol HyperPickerControlDelegate: AnyObject {
  func entityPicked(_ hyperpickerControl: HyperPickerControl, url: URL, entityEnum: ModelEnum)
}

enum PickerType: Int {
  case people = -1
  case starships = -2
  case vehicles = -3
  case species = -4
  case films = -5
}

class HyperPickerControl: UIView {
  static let horzPad: CGFloat = 8
  static let titleWidth: CGFloat = 100.0
  static let pickerWidth: CGFloat = 200.0
  let title: String
  let entityEnum: ModelEnum
  let pickerType: PickerType
  let urls: [URL]
  var displayNames: [String] = []
  let titleLabel: UILabel = UILabel()
  let picker: UIPickerView = UIPickerView()
  
  weak var delegate: HyperPickerControlDelegate?
  
  init(title: String, entityEnum: ModelEnum, type: PickerType, urls: [URL]) {
    self.title = title
    self.entityEnum = entityEnum
    self.pickerType = type
    self.urls = urls
    super.init(frame: CGRect.zero)
    displayNames = getTitles(for: entityEnum, entities: urls)
    
    [titleLabel, picker].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      addSubview($0)
    }
    
    titleLabel.text = title
    titleLabel.sizeToFit()
    
    NSLayoutConstraint.activate([
      titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
      titleLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
      titleLabel.widthAnchor.constraint(equalToConstant: HyperPickerControl.titleWidth),
      
      picker.centerYAnchor.constraint(equalTo: titleLabel.centerYAnchor),
      picker.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -HyperPickerControl.horzPad),
      picker.leadingAnchor.constraint(equalTo: titleLabel.trailingAnchor, constant: HyperPickerControl.horzPad),
      picker.widthAnchor.constraint(equalToConstant: HyperPickerControl.pickerWidth),
    ])
    
    picker.dataSource = self
    picker.delegate = self
    picker.clipsToBounds = true
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func getTitles(for enumType: ModelEnum, entities: [URL]) -> [String] {
    var titles: [String] = []
    for entity in entities {
      let displayName = ModelEntityStorageManager.shared.getDisplayName(forURL: entity, ofType: enumType)
      titles.append(displayName)
    }
    return titles
  }
}

extension HyperPickerControl: UIPickerViewDelegate {
  func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
    let label: UILabel
    if let oldLabel = view as? UILabel {
      label = oldLabel
    } else {
      label = UILabel()
    }
    
    label.font = UIFont.systemFont(ofSize: 14.0)
    label.textAlignment = .center
    let text = displayNames[row]
    label.text = text 
    return label
  }
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    guard let delegate = delegate else {
      return
    }

    let urlPicked = urls[row]
    delegate.entityPicked(self, url: urlPicked, entityEnum: entityEnum)
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return displayNames[row]
  }
}

extension HyperPickerControl: UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return displayNames.count
  }
}
