////+--------------------------------------------------------------------------
// 
//   PersonHyperPane.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

class PersonHyperPane: UIView {
  static let topPad: CGFloat = 24.0
  static let vertPad: CGFloat = 8.0
  static let labelToPickerPad: CGFloat = 42.0
  static let horzPad: CGFloat = 24.0
  static let pickerHeight: CGFloat = 48.0
  static let pickerWidth: CGFloat = 300.0
  let person: Person
  let homeworldLabel: HyperLabelControl
  let filmsPicker: HyperPickerControl
  
  weak var delegate: HyperPaneProtocol?
  
  init(person: Person) {
    self.person = person
    let homeWorldDisplayName = ModelEntityStorageManager.shared.getDisplayName(forURL: person.homeworld, ofType: .planets)
    self.homeworldLabel = HyperLabelControl(text: "Homeworld", displayName: homeWorldDisplayName, link: person.homeworld)
    filmsPicker = HyperPickerControl(title: "Films", entityEnum: .films, type: .films, urls: person.films)
    filmsPicker.tag = PickerType.films.rawValue
    
    super.init(frame: CGRect.zero)
    
    [homeworldLabel, filmsPicker].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      addSubview($0)
    }

    homeworldLabel.translatesAutoresizingMaskIntoConstraints = false
    addSubview(homeworldLabel)
    homeworldLabel.addTarget(self, action: #selector(PersonHyperPane.hyperlinkTouched(_:)), for: .touchUpInside)
    
    
    NSLayoutConstraint.activate([
      homeworldLabel.topAnchor.constraint(equalTo: topAnchor, constant: PersonHyperPane.topPad),
      homeworldLabel.leadingAnchor.constraint(equalTo: filmsPicker.leadingAnchor),
    ])
    
    filmsPicker.delegate = self
        
    NSLayoutConstraint.activate([
      filmsPicker.topAnchor.constraint(equalTo: homeworldLabel.bottomAnchor, constant: PersonHyperPane.labelToPickerPad),
      filmsPicker.centerXAnchor.constraint(equalTo: centerXAnchor),
      filmsPicker.widthAnchor.constraint(equalToConstant: PersonHyperPane.pickerWidth),
      filmsPicker.heightAnchor.constraint(equalToConstant: PersonHyperPane.pickerHeight),
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  @objc func hyperlinkTouched(_ sender: HyperLabelControl) {
    guard let delegate = delegate else {
      return
    }
    guard let url = sender.hyperLinkURL.secureVersion else {
      return
    }
    delegate.hyperLinkTouched(forEntityType: ModelEnum.planets, hyperLinkURL: url)
  }
}

extension PersonHyperPane: HyperPickerControlDelegate {
  func entityPicked(_ hyperpickerControl: HyperPickerControl, url: URL, entityEnum: ModelEnum) {
    guard let delegate = delegate else {
      return
    }
    delegate.hyperLinkTouched(forEntityType: entityEnum, hyperLinkURL: url)
  }
}
