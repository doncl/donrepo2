////+--------------------------------------------------------------------------
// 
//   HyperLabelControl.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

class HyperLabelControl: UIControl {
  static let font: UIFont = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.subheadline)
  static let stackSpacing: CGFloat = 8.0
  let nameLabel: UILabel = UILabel()
  let displayNameValueLabel: UILabel = UILabel()
  let stack = UIStackView()

  let nameText: String
  let displayName: String
  let hyperLinkURL: URL

  init(text: String, displayName: String, link: URL) {
    self.displayName = displayName
    nameText = text
    hyperLinkURL = link
    super.init(frame: CGRect.zero)
    
    nameLabel.text = nameText
    nameLabel.font = HyperLabelControl.font.bold()
    
    let attributes: [NSAttributedString.Key: Any] = [
      NSAttributedString.Key.link: hyperLinkURL,
      NSAttributedString.Key.font: HyperLabelControl.font,
    ]
    let attrString: NSAttributedString = NSAttributedString(string: displayName, attributes: attributes)
    displayNameValueLabel.attributedText = attrString

    addSubview(stack)
    stack.axis = .horizontal
    stack.spacing = HyperLabelControl.stackSpacing
    
    stack.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      stack.topAnchor.constraint(equalTo: topAnchor),
      stack.leadingAnchor.constraint(equalTo: leadingAnchor),
      stack.trailingAnchor.constraint(equalTo: trailingAnchor),
      stack.bottomAnchor.constraint(equalTo: bottomAnchor),
    ])
  
    nameLabel.clipsToBounds = true
    nameLabel.textAlignment = .left
    displayNameValueLabel.textAlignment = .right
    displayNameValueLabel.isUserInteractionEnabled = true
  
    [nameLabel, displayNameValueLabel].forEach {
      $0.clipsToBounds = true
      stack.addArrangedSubview($0)
      $0.sizeToFit()
    }
    let tap = UITapGestureRecognizer(target: self, action: #selector(HyperLabelControl.labelTouched(_:)))
    displayNameValueLabel.addGestureRecognizer(tap)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension HyperLabelControl {
  @objc func labelTouched(_ sender: UITapGestureRecognizer) {
    sendActions(for: UIControl.Event.touchUpInside)
  }
}
