////+--------------------------------------------------------------------------
//
//   RootVC.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

class RootVC: UISplitViewController {
  let searchNav: UINavigationController = UINavigationController()
  let searchVC: SearchVC = SearchVC()
  let detailsNav: UINavigationController = UINavigationController()

  override func viewDidLoad() {
    super.viewDidLoad()
    viewControllers = [searchNav, detailsNav]
    searchVC.delegate = self
    searchNav.show(searchVC, sender: nil)
    setupSideBySideParameters()
  }
}

extension RootVC: UISplitViewControllerDelegate {
  func splitViewController(_ splitViewController: UISplitViewController,
                           collapseSecondary secondaryViewController: UIViewController,
                           onto primaryViewController: UIViewController) -> Bool {

    guard let nav = secondaryViewController as? UINavigationController, let _ = nav.topViewController as? DetailVC else  {
      return true
    }

    return false
  }
  
  private func setupSideBySideParameters() {
    #if targetEnvironment(macCatalyst)
    preferredDisplayMode = UISplitViewController.DisplayMode.allVisible
    #else
    preferredDisplayMode = UISplitViewController.DisplayMode.oneBesideSecondary
    #endif
    delegate = self
    preferredPrimaryColumnWidthFraction = 0.5
    maximumPrimaryColumnWidth = 512
  }
}

extension RootVC: SearchVCDelegate {
  func selected(person: Person) {
    print("\(#function) - selected id = \(person.getID()), \(person.name)")
    let vc = PersonVC(person: person)
    vc.delegate = self
    showDetailViewController(vc, sender: self)
  }
  
  func selected(film: Film) {
    print("\(#function) - selected id = \(film.getID()), \(film.title)")
    let vc = FilmVC(film: film)
    vc.delegate = self
    showDetailViewController(vc, sender: self)
  }
}

extension RootVC: DetailVCDelegate {
  func getRootVC() -> RootVC {
    return self
  }
}
