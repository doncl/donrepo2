////+--------------------------------------------------------------------------
//
//   SearchVC.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

protocol SearchVCDelegate: class {
  func selected(person: Person)
  func selected(film: Film)
}

class SearchVC: UIViewController {
  // MARK: Private Enums
  private enum Section: Int {
    case person = 0
    case film = 1
  }
  
  private enum SearchType: String, CaseIterable {
    case all = "All"
    case people = "People"
    case films = "Films"
  }
  
  // MARK: Constants
  static let horzGutter: CGFloat = 12.0
  static let aspectRatio: CGFloat = 0.929
  static let lineSpacing: CGFloat = 20.0
  static let sectionInsets: UIEdgeInsets = UIEdgeInsets(top: 24, left: SearchVC.horzGutter,
                                                        bottom: 24, right: SearchVC.horzGutter)
  static let headerHeight: CGFloat = 60.0
  
  // MARK: Stored Properties
  let grid: UICollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout())
  let searchController = UISearchController(searchResultsController: nil)
  var people: [Person] = []
  var filteredPeople: [Person] = []
  var films: [Film] = []
  var filteredFilms: [Film] = []
  
  weak var delegate: SearchVCDelegate?
  
  // MARK: Calculated Properties
  var isSearchBarEmpty: Bool {
    return searchController.searchBar.text?.isEmpty ?? true
  }
  
  var isFiltering: Bool {
    let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
    return searchController.isActive && (!isSearchBarEmpty || searchBarScopeIsFiltering)
  }
  
  private var _itemSize: CGSize?
  var itemSize: CGSize {
    if let isiz = _itemSize {
      return isiz
    }
    var width = (view.bounds.width - (SearchVC.horzGutter * 3)) / 2
    if view.bounds.width > view.bounds.height {
      width -= (view.safeAreaInsets.left + view.safeAreaInsets.right)
    }
    let height = width * SearchVC.aspectRatio
    let calcedSize = CGSize(width: width, height: height)
    _itemSize = calcedSize
    return calcedSize
  }
  
  // MARK: For Bumps
  lazy private var haptic : UINotificationFeedbackGenerator = {
    let gen = UINotificationFeedbackGenerator()
    gen.prepare()
    return gen
  }()
      
  // MARK: METHODS
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Set up Models
    people = ModelEntityStorageManager.shared.people
    films = ModelEntityStorageManager.shared.films
    
    // Put the title on the navbar
    navigationItem.title = "Search"

    setupSearchController()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupGrid()
  }
  
  private func setupSearchController() {
    searchController.searchResultsUpdater = self
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "People or Films"
    navigationItem.searchController = searchController
    definesPresentationContext = true
    searchController.searchBar.scopeButtonTitles = SearchType.allCases.map { $0.rawValue }
    searchController.searchBar.delegate = self
  }
  
  private func setupGrid() {
    grid.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(grid)
    
    NSLayoutConstraint.activate([
      grid.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      grid.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      grid.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      grid.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
    
    // In a future life, when I grow up, all UICollectionView's will use compositional layout. But for now, on a limited time
    // budget, I went with what I could implement quickly, because I've done it a zillion times.  (I have done tutorials of
    // compositional layout, and can do it, but not today).
    if let flow = grid.collectionViewLayout as? UICollectionViewFlowLayout {
      flow.scrollDirection = .vertical
      flow.itemSize = itemSize
      flow.minimumLineSpacing = SearchVC.lineSpacing
      flow.estimatedItemSize = CGSize.zero
      flow.sectionInset = SearchVC.sectionInsets
      flow.headerReferenceSize = CGSize(width: view.bounds.width, height: SearchVC.headerHeight)
    }
    grid.allowsSelection = true
    
    grid.register(SearchResultCell.self, forCellWithReuseIdentifier: SearchResultCell.id)
    grid.register(GridHeader.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                  withReuseIdentifier: GridHeader.id)
    
    grid.dataSource = self
    grid.delegate = self
    grid.reloadData()
  }  
}

// MARK: UICollectionViewDelegateFlowLayout
extension SearchVC: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let delegate = delegate else {
      return
    }
    
    let resultSection = sectionType(forRawSectionIndex: indexPath.section)
    
    switch resultSection {
      case .person:
        let person = isFiltering ? filteredPeople[indexPath.item] : people[indexPath.item]
        delegate.selected(person: person)
      case .film:
        let film = isFiltering ? filteredFilms[indexPath.item] : films[indexPath.item]
        delegate.selected(film: film)
    }
  }
}

// MARK: UICollectionViewDataSource
extension SearchVC: UICollectionViewDataSource {
  private func sectionType(forRawSectionIndex rawSectionIndex: Int) -> Section {
    let defaultSectionType: Section = Section(rawValue: rawSectionIndex)!
    let scopeButtonIndex = searchController.searchBar.selectedScopeButtonIndex
    let currentlySelectedSearchTypeRawValue: String = searchController.searchBar.scopeButtonTitles![scopeButtonIndex]
    guard let searchType: SearchVC.SearchType = SearchVC.SearchType(rawValue: currentlySelectedSearchTypeRawValue) else {
      return defaultSectionType
    }

    switch searchType {
      case .all:
        return defaultSectionType
      case .people:
        return Section.person
      case .films:
        return Section.film
    }
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    let max = SearchType.allCases.count - 1
    let index = searchController.searchBar.selectedScopeButtonIndex
    if index == 0 {
      return max
    }
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    let resultSection = sectionType(forRawSectionIndex: section)
    switch resultSection {
      case .person:
        return isFiltering ? filteredPeople.count :  people.count
      case .film:
        return isFiltering ? filteredFilms.count : films.count
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let resultSection = sectionType(forRawSectionIndex: indexPath.section)
    
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SearchResultCell.id, for: indexPath)
            as? SearchResultCell else {
      
      fatalError("Incorrectly configured")
    }
    
    switch resultSection {
      case .person:
        let person = isFiltering ? filteredPeople[indexPath.item] : people[indexPath.item]
        cell.set(person: person)
      case .film:
        let film = isFiltering ? filteredFilms[indexPath.item] : films[indexPath.item]
        cell.set(film: film)
    }
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
    let section: Section = sectionType(forRawSectionIndex: indexPath.section)
    
    guard let header =
      collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                      withReuseIdentifier: GridHeader.id,
                                                      for: indexPath) as? GridHeader else {
      fatalError("Incorrectly configured")
    }
    switch section {
      case .person:
        header.set(text: "PEOPLE", andImage: UIImage(named: "people"))
      case .film:
        header.set(text: "FILMS", andImage: UIImage(named: "films"))
    }
    
    return header
  }
  
  private func refilter(_ searchBar: UISearchBar, scopeButtonIndex: Int) {
    let currentlySelectedSearchTypeRawValue: String = searchBar.scopeButtonTitles![scopeButtonIndex]
    let searchType: SearchType? = SearchType(rawValue: currentlySelectedSearchTypeRawValue)
    filterContentForSearchText(searchBar.text!, searchType: searchType)
  }
}

// MARK: UISearchBarDelegate
extension SearchVC: UISearchBarDelegate {
  func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
    refilter(searchBar, scopeButtonIndex: selectedScope)
  }
}

// MARK: UISearchResultsUpdating
extension SearchVC: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    refilter(searchBar, scopeButtonIndex: searchBar.selectedScopeButtonIndex)
  }
  
  private func filterContentForSearchText(_ searchText: String, searchType: SearchType? = nil) {
    defer {
      grid.reloadData()
    }
    
    // Is a scope button selected, even if it's 'All'?
    if let searchType = searchType {
      switch searchType {
        case .all:
          filterAll(bySearchText: searchText)
        case .people:
          filterPeople(bySearchText: searchText)
        case .films:
          filterFilms(bySearchText: searchText)
      }
      return
    }
    
    // No scope button selected
    if isSearchBarEmpty {
      return
    }
    filterAll(bySearchText: searchText)
  }
  
  private func filterAll(bySearchText searchText: String) {
    filterPeople(bySearchText: searchText)
    filterFilms(bySearchText: searchText)
  }
  
  private func filterFilms(bySearchText searchText: String) {
    filteredFilms = films.filter { film in
      if isSearchBarEmpty {
        return true
      }
      return film.title.lowercased().contains(searchText.lowercased())
    }
  }
  
  private func filterPeople(bySearchText searchText: String) {
    filteredPeople = people.filter( { person in
      if isSearchBarEmpty {
        return true
      }
      return person.name.lowercased().contains(searchText.lowercased())
    })
  }
}

