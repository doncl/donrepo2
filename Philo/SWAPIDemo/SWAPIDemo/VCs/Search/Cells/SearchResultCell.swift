////+--------------------------------------------------------------------------
// 
//   PersonCell.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------


import UIKit

class SearchResultCell: UICollectionViewCell {
  class GradientView: UIView {
    static override var layerClass: AnyClass {
      return CAGradientLayer.self
    }
  }
  
  static let id: String = "SearchResultCellID"
  
  static let captionFontSize: CGFloat = 18.0
  static let colorImageLabelFontSize: CGFloat = 30.0
  static let leadingPad: CGFloat = 8.0
  static let vertPad: CGFloat = 8.0
  static let selectionBorderWidth: CGFloat = 3.0
  
  static let captionFont: UIFont = UIFont.systemFont(ofSize: SearchResultCell.captionFontSize,
                                                     weight: UIFont.Weight.bold)
   
  let imageView: UIImageView = UIImageView()
  let whiteSelectionBorder: UIView = UIView()
  let gradient: GradientView = GradientView()
  
  private let captionLabel: UILabel = UILabel()
   
  override var isSelected: Bool {
    get {
      return super.isSelected
    }

    set (newValue) {
      super.isSelected = newValue
      showSelectionBorder(newValue)
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    doAllTheLayoutStuff()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension SearchResultCell {
  private func doAllTheLayoutStuff() {
    imageView.contentMode = UIView.ContentMode.scaleAspectFill
    imageView.clipsToBounds = true
    imageView.layer.cornerRadius = 3.0
    contentView.addSubview(imageView)
    
    [imageView, captionLabel, whiteSelectionBorder, gradient].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false 
    }
    
    NSLayoutConstraint.activate([
      imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
      imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
      imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
      imageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
    ])
    
    contentView.addSubview(captionLabel)
    captionLabel.font = SearchResultCell.captionFont
    captionLabel.textColor = UIColor.white
    captionLabel.textAlignment = NSTextAlignment.left
        
    captionLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
    captionLabel.numberOfLines = 0
    
    NSLayoutConstraint.activate([
      captionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: SearchResultCell.leadingPad),
      captionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -SearchResultCell.leadingPad),
      captionLabel.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -SearchResultCell.vertPad)
    ])
    
    // Make selection border
    whiteSelectionBorder.layer.borderColor = UIColor.white.cgColor
    
    imageView.addSubview(whiteSelectionBorder)
    whiteSelectionBorder.clipsToBounds = true
    whiteSelectionBorder.isHidden = true
    whiteSelectionBorder.layer.borderWidth = SearchResultCell.selectionBorderWidth
    whiteSelectionBorder.backgroundColor = UIColor.clear
        
    NSLayoutConstraint.activate([
      whiteSelectionBorder.topAnchor.constraint(equalTo: imageView.topAnchor),
      whiteSelectionBorder.leftAnchor.constraint(equalTo: imageView.leftAnchor),
      whiteSelectionBorder.rightAnchor.constraint(equalTo: imageView.rightAnchor),
      whiteSelectionBorder.bottomAnchor.constraint(equalTo: imageView.bottomAnchor),
    ])
    
    if let gradientLayer = gradient.layer as? CAGradientLayer {
      gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.withAlphaComponent(0.4).cgColor]
    }

    imageView.addSubview(gradient)
    
    NSLayoutConstraint.activate([
      gradient.topAnchor.constraint(equalTo: imageView.topAnchor),
      gradient.leftAnchor.constraint(equalTo: imageView.leftAnchor),
      gradient.rightAnchor.constraint(equalTo: imageView.rightAnchor),
      gradient.bottomAnchor.constraint(equalTo: imageView.bottomAnchor),
    ])
    
    gradient.clipsToBounds = true
  }
}

extension SearchResultCell {
  func set(person: Person) {
    let id = person.getID()
    let imageName: String = "Person\(id)"
    if let image = UIImage(named: imageName) {
      imageView.image = image
    }
    captionLabel.text = person.name
  }
  
  func set(film: Film) {
    let id = film.getID()
    let imageName: String = "Film\(id)"
    if let image = UIImage(named: imageName) {
      imageView.image = image
    }
    captionLabel.text = film.title
  }
  
  func showSelectionBorder(_ show: Bool) {
    [whiteSelectionBorder].forEach {
      $0.isHidden = !show
      $0.setNeedsDisplay()
    }
    setNeedsDisplay()
  }
}
