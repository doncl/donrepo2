////+--------------------------------------------------------------------------
// 
//   GridHeader.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

class GridHeader: UICollectionReusableView {
  static let id: String = "GridHeaderID"
  
  static let xOffset:CGFloat = 50.0
  static let font: UIFont = UIFont.boldSystemFont(ofSize: 32.0)
  static let imageDim: CGFloat = 50.0
  private let label: UILabel = UILabel()
  private let image: UIImageView = UIImageView()

  
  override init(frame: CGRect) {
    super.init(frame: frame)
    accessibilityLabel = "Grid Header"
    backgroundColor = .lightGray
    
    [label, image].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      addSubview($0)
    }
    
    NSLayoutConstraint.activate([
      label.centerXAnchor.constraint(equalTo: centerXAnchor, constant: -GridHeader.xOffset),
      label.centerYAnchor.constraint(equalTo: centerYAnchor),
      
      image.centerXAnchor.constraint(equalTo: centerXAnchor, constant: GridHeader.xOffset),
      image.centerYAnchor.constraint(equalTo: label.centerYAnchor),
      image.heightAnchor.constraint(equalToConstant: GridHeader.imageDim),
      image.widthAnchor.constraint(equalToConstant: GridHeader.imageDim),
    ])
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension GridHeader {
  func set(text: String, andImage image: UIImage?) {
    label.text = text
    label.textAlignment = .left
    label.textColor = UIColor.black
    label.font = GridHeader.font
    self.image.image = image 
  }
}
