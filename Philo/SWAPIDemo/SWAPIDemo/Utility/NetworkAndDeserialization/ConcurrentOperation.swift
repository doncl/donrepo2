////+--------------------------------------------------------------------------
// 
//   ConcurrentOperation.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------


import Foundation

@objc class ConcurrentOperation: Operation {
  
  // MARK: - Overrides
  
  override var isAsynchronous: Bool {
    return true
  }
  
  override func start() {
    guard !isCancelled else {
      _finished = true
      return
    }
    
    _executing = true
    
    main()
  }
  
  private var _executing: Bool = false {
    willSet {
      willChangeValue(forKey: "isExecuting")
    }
    didSet {
      didChangeValue(forKey: "isExecuting")
    }
  }
  override var isExecuting: Bool {
    return _executing
  }
  
  private var _finished: Bool = false {
    willSet {
      willChangeValue(forKey: "isFinished")
    }
    didSet {
      didChangeValue(forKey: "isFinished")
    }
  }
  override var isFinished: Bool {
    return _finished
  }
  
  // MARK: - Completion  
  func completeOperation() {
    _executing = false
    _finished = true
  }
}
