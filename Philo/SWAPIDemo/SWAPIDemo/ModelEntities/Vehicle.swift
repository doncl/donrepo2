////+--------------------------------------------------------------------------
//
//   Vehicle.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import Foundation

struct Vehicle: Codable, JSONCoding, ModelEntity {
  typealias codableType = Vehicle
  typealias storableType = Vehicle 
  
  let name, model, manufacturer, costInCredits: String
  let length, maxAtmospheringSpeed, crew, passengers: String
  let cargoCapacity, consumables, vehicleClass: String
  let pilots: [URL]
  let films: [URL]
  let created, edited: Date
  let url: URL

  enum CodingKeys: String, CodingKey {
    case name, model, manufacturer
    case costInCredits = "cost_in_credits"
    case length
    case maxAtmospheringSpeed = "max_atmosphering_speed"
    case crew, passengers
    case cargoCapacity = "cargo_capacity"
    case consumables
    case vehicleClass = "vehicle_class"
    case pilots, films, created, edited, url
  }
}
