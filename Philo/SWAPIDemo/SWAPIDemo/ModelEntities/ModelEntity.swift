////+--------------------------------------------------------------------------
// 
//   ModelEntity.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import Foundation

enum ModelEnum: String {
  case people = "people"
  case films = "films"
  case starships = "starships"
  case vehicles = "vehicles"
  case species = "species"
  case planets = "planets"
}

protocol ModelEntity {
  var url: URL { get }
  func getSecureURL() -> URL
  func getID() -> String
  func getAPIEntityName() -> String
  func getModelEnum() -> ModelEnum?
}

extension ModelEntity {
  func getID() -> String {
    return url.lastPathComponent
  }
  
  func getAPIEntityName() -> String {
    guard url.pathComponents.count > 1 else {
      fatalError("Something very wrong with your assumptions about SWAPI URLs")
    }
    let lengh = url.pathComponents.count
    return url.pathComponents[lengh - 2]
  }
  
  func getModelEnum() -> ModelEnum? {
    let entityAPIName = getAPIEntityName()
    guard let enm = ModelEnum.init(rawValue: entityAPIName) else {
      return nil
    }
    return enm
  }
  
  // TODO:  Build this stuff into the JSONCoding stuff itself.  
  func getSecureURL() -> URL {
    var components = URLComponents(url: url, resolvingAgainstBaseURL: false)!
    components.scheme = "https"
    return components.url!
  }
}
