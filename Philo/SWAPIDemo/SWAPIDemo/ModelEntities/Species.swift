////+--------------------------------------------------------------------------
//
//   Species.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import Foundation

struct Species: Codable, JSONCoding, ModelEntity {
  typealias codableType = Species
  typealias storableType = Species
  
  let name, classification, designation, averageHeight: String
  let hairColors, eyeColors, averageLifespan: String
  let homeworld: URL?
  let language: String
  let people, films: [String]
  let created, edited: Date
  let url: URL

  enum CodingKeys: String, CodingKey {
    case name, classification, designation
    case averageHeight = "average_height"
    case hairColors = "hair_colors"
    case eyeColors = "eye_colors"
    case averageLifespan = "average_lifespan"
    case homeworld, language, people, films, created, edited, url
  }  
}
