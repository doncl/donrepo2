////+--------------------------------------------------------------------------
//
//   Planet.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import Foundation

struct Planet: Codable, JSONCoding, ModelEntity {
  typealias codableType = Planet
  typealias storableType = Planet
  
  let name, rotationPeriod, orbitalPeriod, diameter: String
  let climate, gravity, terrain, surfaceWater: String
  let population: String
  let residents, films: [URL]
  let created, edited: String
  let url: URL

  enum CodingKeys: String, CodingKey {
    case name
    case rotationPeriod = "rotation_period"
    case orbitalPeriod = "orbital_period"
    case diameter, climate, gravity, terrain
    case surfaceWater = "surface_water"
    case population, residents, films, created, edited, url
  }  
}
