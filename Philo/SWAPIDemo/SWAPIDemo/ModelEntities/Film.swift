////+--------------------------------------------------------------------------
//
//   Film.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import Foundation

struct Film: Codable, JSONCoding, ModelEntity {
  typealias storableType = Film
  typealias codableType = Film
  
  let title: String
  let episodeID: Int
  let openingCrawl, director, producer: String
  let characters, planets, starships, vehicles: [URL]
  let species: [URL]
  let created, edited, releaseDate: Date
  let url: URL

  enum CodingKeys: String, CodingKey {
    case title
    case episodeID = "episode_id"
    case openingCrawl = "opening_crawl"
    case director, producer
    case releaseDate = "release_date"
    case characters, planets, starships, vehicles, species, created, edited, url
  }
}
