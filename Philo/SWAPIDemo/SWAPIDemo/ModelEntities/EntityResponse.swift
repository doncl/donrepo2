////+--------------------------------------------------------------------------
// 
//   EntityResponse.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------


import Foundation

struct EntityResponse<T: ModelEntity & Codable>: Codable, JSONCoding {
  typealias codableType = EntityResponse
  
  let count: Int
  let next: URL?
  let results: [T]
}
