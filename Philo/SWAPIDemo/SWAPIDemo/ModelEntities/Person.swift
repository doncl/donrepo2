////+--------------------------------------------------------------------------
//
//   Person.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/26/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import Foundation

// N.B. as a Timesaver, I pasted the JSON for Luke Skywalker into QuickType, to generate a Swift entity - but deleted most of
// the excess boilerplate it created, in favor of using my own JSONCoding protocol.
struct Person: Codable, JSONCoding, ModelEntity {
  typealias codableType = Person
  typealias storableType = Person
  
  let name, height, mass, hairColor: String
  let skinColor, eyeColor, birthYear, gender: String
  let homeworld: URL
  
  let films: [URL]
  let species: [URL]
  let vehicles, starships: [URL]
  let created, edited: Date
  let url: URL

  enum CodingKeys: String, CodingKey {
    case name, height, mass
    case hairColor = "hair_color"
    case skinColor = "skin_color"
    case eyeColor = "eye_color"
    case birthYear = "birth_year"
    case gender, homeworld, films, species, vehicles, starships, created, edited, url
  }
}
