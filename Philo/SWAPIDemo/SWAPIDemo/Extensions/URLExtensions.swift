////+--------------------------------------------------------------------------
// 
//   URLExtensions.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------


import UIKit

extension URL {
  var secureVersion: URL? {
    guard var comps = URLComponents(url: self, resolvingAgainstBaseURL: false) else {
      return nil
    }
    comps.scheme = "https"
    return comps.url
  }
  
}
