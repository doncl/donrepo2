////+--------------------------------------------------------------------------
// 
//   UIImageExtensions.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

extension UIImage {
  static func from(color: UIColor, ofSize size: CGSize) -> UIImage {
    let renderer = UIGraphicsImageRenderer(size: size)
    let img = renderer.image(actions: { (ctx: UIGraphicsImageRendererContext) in
      color.setFill()
      ctx.fill(CGRect(origin: .zero, size: size))
    })
    return img
  }
  
  func scaled(toSize size: CGSize, withRoundedCorners rounded: Bool = false) -> UIImage? {
    UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
    defer {
      UIGraphicsEndImageContext()
    }
    let r = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    if rounded {
      UIBezierPath(roundedRect: r, cornerRadius: size.width).addClip()
    }
    draw(in: r)
    return UIGraphicsGetImageFromCurrentImageContext()
  }
}
