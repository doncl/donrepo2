////+--------------------------------------------------------------------------
// 
//   UIColorExtensions.swift
//   SWAPIDemo
//   Created by: Anonymous Candidate on 9/27/20
//   Copyright (c) 2020 Beer Barrel Studios, Inc.
//
//-----------------------------------------------------------------------------

import UIKit

enum ColorType{
  case light
  case dark
}

extension UIColor {
  func getColorType() -> ColorType {
  
    var aRed : CGFloat = 0
    var aGreen : CGFloat = 0
    var aBlue : CGFloat = 0
  
    if getRed(&aRed, green: &aGreen, blue: &aBlue, alpha: nil) == true {
    
      let iRed = Int(aRed * 255.0)
      let iGreen = Int(aGreen * 255.0)
      let iBlue = Int(aBlue * 255.0)
      
      let value = ((iRed * 299) + (iGreen * 587) + (iBlue * 114))/1000
      
      if(value > 200){
        return .light
      } else{
        return .dark
      }      
    }
    
    return .dark
  }
}

