//
//  NoIssueView.swift
//  DonClorePortfolio
//
//  Created by Don Clore on 4/12/23.
//

import SwiftUI

struct NoIssueView: View {
  @EnvironmentObject var dataController: DataController
  
  var body: some View {
    Text("No Issue Selected")
      .font(.title)
      .foregroundStyle(.secondary)
    
    Button("New Issue") {
      dataController.newIssue()
    }
  }  
}

struct NoIssueView_Previews: PreviewProvider {
    static var previews: some View {
        NoIssueView()
    }
}
