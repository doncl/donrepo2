//
//  DonClorePortfolioApp.swift
//  DonClorePortfolio
//
//  Created by Don Clore on 5/22/21.
//

import SwiftUI

@main
struct DonClorePortfolioApp: App {
  @StateObject var dataController = DataController()
  @Environment(\.scenePhase) var scenePhase
  
  var body: some Scene {
    WindowGroup {
      NavigationSplitView {
        SidebarView()
      } content: {
        ContentView()
      } detail: {
        DetailView()
      }
      .environment(\.managedObjectContext, dataController.container.viewContext)
      .environmentObject(dataController)
      .onChange(of: scenePhase) { phase in
        if phase != .active {
          dataController.save()
        }
      }
    }
  }
}
