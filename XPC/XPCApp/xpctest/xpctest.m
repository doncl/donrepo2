//
//  xpctest.m
//  xpctest
//
//  Created by Don Clore on 5/21/21.
//

#import "xpctest.h"

@implementation xpctest

// This implements the example protocol. Replace the body of this class with the implementation of this service's protocol.
- (void)upperCaseString:(NSString *)aString withReply:(void (^)(NSString *))reply {
    NSString *response = [aString uppercaseString];
    reply(response);
}

@end
