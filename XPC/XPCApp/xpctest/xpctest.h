//
//  xpctest.h
//  xpctest
//
//  Created by Don Clore on 5/21/21.
//

#import <Foundation/Foundation.h>
#import "xpctestProtocol.h"

// This object implements the protocol which we have defined. It provides the actual behavior for the service. It is 'exported' by the service to make it available to the process hosting the service over an NSXPCConnection.
@interface xpctest : NSObject <xpctestProtocol>
@end
