//
//  AppDelegate.h
//  XPCApp
//
//  Created by Don Clore on 5/21/21.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

