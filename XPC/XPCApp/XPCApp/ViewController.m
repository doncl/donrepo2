//
//  ViewController.m
//  XPCApp
//
//  Created by Don Clore on 5/21/21.
//

#import "ViewController.h"
#import "xpctestProtocol.h"

@implementation ViewController {
  NSXPCConnection *connection;
  id<xpctestProtocol> service;
}

- (void)viewDidLoad {
  [super viewDidLoad];

  connection = [[NSXPCConnection alloc]initWithServiceName:@"com.beerbarrelpokerstudios.xpctest"];
  connection.remoteObjectInterface = [NSXPCInterface interfaceWithProtocol:@protocol(xpctestProtocol)];
  connection.interruptionHandler = ^{
    NSLog(@"interruption");
  };
  
  connection.invalidationHandler = ^{
    NSLog(@"invalidation");
  };
  
  [connection resume];
  
  service = [connection remoteObjectProxyWithErrorHandler:^(NSError * _Nonnull error) {
    NSLog(@"%@", error);
  }];
  
  [service upperCaseString:@"heyyyy" withReply:^(NSString *result) {
    NSLog(@"%@", result);
  }];
}


- (void)setRepresentedObject:(id)representedObject {
  [super setRepresentedObject:representedObject];

  // Update the view, if already loaded.
}


@end
