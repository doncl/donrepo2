//
//  AlignmentGuidesToolSwiftUIApp.swift
//  AlignmentGuidesToolSwiftUI
//
//  Created by Don Clore on 2/18/21.
//

import SwiftUI

@main
struct AlignmentGuidesToolSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
