//
//  ContactViewModel.swift
//  HatchHomework
//
//  Created by Don Clore on 11/27/20.
//

import Foundation
import CoreLocation

struct ContactViewModel {
  let name: String?
  let phoneNumber: String?
  let address: String?
  let location: CLLocation?
  var distanceFromMe: String = "Unknown distance from me."
}
