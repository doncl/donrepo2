//
//  ContactMediator.swift
//  HatchHomework
//
//  Created by Don Clore on 11/27/20.
//

import Foundation
import Contacts
import CoreLocation

enum ContactMediatorError: Error {
  case nolocationfound(Error?)
  case noaddress
}

final class ContactMediator: NSObject {
  struct ContactWithLocation {
    let contact: CNContact
    let location: CLLocation?
    var failedGettingLocation: Bool = false
    
    func getViewModel() -> ContactViewModel {
      let firstname = contact.givenName
      let lastname = contact.familyName
      var name: String = ""
      if !firstname.isEmpty {
        name = firstname
        if !lastname.isEmpty {
          name += " \(lastname)"
        }
      } else {
        name = lastname
      }
      
      let phoneNumber = contact.phoneNumbers.first?.value.stringValue
      var address: String?
      if let cnaddress = contact.postalAddresses.first?.value {
        address = CNPostalAddressFormatter.string(from: cnaddress, style: CNPostalAddressFormatterStyle.mailingAddress)
      }
      
      var viewModel = ContactViewModel(name: name, phoneNumber: phoneNumber, address: address, location: location)
      
      if let myLocation = ContactMediator.shared.myLocation, let contactLocaton = location, !failedGettingLocation {
        let distance = contactLocaton.distance(from: myLocation) / 1000
        let nf = NumberFormatter()
        let nsDistance = NSNumber(floatLiteral: distance)
        nf.numberStyle = NumberFormatter.Style.decimal
        if let distanceString = nf.string(from: nsDistance) {
          viewModel.distanceFromMe = "\(distanceString) km. from me"
        }
      }
      
      return viewModel
    }
  }
  static let shared: ContactMediator = ContactMediator()
  
  private var contacts: [Int: [ContactWithLocation]] = [:]
  var containerNames: [Int: String] = [:]
  let fetchGroup: DispatchGroup = DispatchGroup()
  var myLocation: CLLocation? 
  
  var contactsGroupsCount: Int {
    return containerNames.count
  }
  
  var totalContactsCount: Int {
    var count = 0
    for kvp in contacts {
      count += kvp.value.count
    }
    return count
  }
          
  override private init() {
  }
  
  func count(forContainerIndex index: Int) -> Int {
    guard let contactArray = contacts[index] else {
      return 0
    }
    
    return contactArray.count
  }
  
  func getViewModel(forIndexPath indexPath: IndexPath) -> ContactViewModel? {
    guard let contactArray = contacts[indexPath.section] else {
      return nil
    }
    guard contactArray.count > indexPath.item else {
      return nil
    }
    let contactWithLocation = contactArray[indexPath.item]
    return contactWithLocation.getViewModel()
  }  
}

extension ContactMediator {
  func kickOffFetching() {
    let locationManager = CLLocationManager()
    locationManager.delegate = self
    locationManager.requestWhenInUseAuthorization()
    locationManager.requestLocation()
    NotificationCenter.default.post(name: NotificationKeys.fetchingContacts, object: nil, userInfo: nil)
    beginContactFetchingProcess()
  }
  
  func beginContactFetchingProcess() {
    DispatchQueue.global(qos: .userInitiated).async { [unowned self] in
      let store = CNContactStore()
      let containers: [CNContainer] = fetchContainers(fromStore: store)
        
      for (index, container) in containers.enumerated() {
        fetchContactsFor(container: container, usingStore: store, atIndex: index)
      }
      
      fetchGroup.wait()
      fetchGroup.notify(queue: .main) { [unowned self] in
        print("contacts count = \(self.contacts.count)")
        NotificationCenter.default.post(name: NotificationKeys.doneFetchingContacts, object: nil)
      }
    }
  }

  private func fetchContactsFor(container: CNContainer, usingStore store: CNContactStore, atIndex containerIndex: Int) {
    let keysToFetch = [
      CNContactGivenNameKey,
      CNContactFamilyNameKey,
      CNContactEmailAddressesKey,
      CNContactPhoneNumbersKey,
      CNContactImageDataAvailableKey,  // not now
      CNContactThumbnailImageDataKey,  // ditto
      CNContactPostalAddressesKey,
    ] as [CNKeyDescriptor]
    let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
    fetchGroup.enter()
    
    do {
      let containerResults = try store.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch)
      var contactsDict: [Int: ContactWithLocation] = [:]
      var foundAddress = false
      for (contactIndex, contact) in containerResults.enumerated() {
        guard let address = contact.postalAddresses.first?.value else {
          continue
        }
        foundAddress = true
        get(locationForAddress: address, forGroup: containerIndex, andIndex: contactIndex, completion: { result in
          let contactWithLocation: ContactWithLocation
          switch result {
           case .success(let location):
             contactWithLocation = ContactWithLocation(contact: contact, location: location)
           case .failure(_):
             contactWithLocation = ContactWithLocation(contact: contact, location: nil, failedGettingLocation: true)
          }
          contactsDict[contactIndex] = contactWithLocation
          
          if contactIndex == containerResults.count - 1 {
            DispatchQueue.main.async { [unowned self] in
              let contactsForSection: [ContactWithLocation] = Array(contactsDict.values)
              self.contacts[containerIndex] = contactsForSection
              self.containerNames[containerIndex] = container.name
              let userInfo: [AnyHashable: Any] = [
                "ContainerName": container.name,
                "ContainerIndex": containerIndex,
              ]
              NotificationCenter.default.post(name: NotificationKeys.contactFetchSuccess, object: nil, userInfo: userInfo)
              fetchGroup.leave()
            }
          }
        })
      }
      if !foundAddress {
        fetchGroup.leave()
      }
      
    } catch let error {
      print("\(#function) - snap! - error getting contacts for container \(container.name) = \(error.localizedDescription)")
      let userInfo: [AnyHashable: Any] = [
        "ContainerName": container.name,
        "ContainerIndex": index,
        "ErrorLocalizedDescription": error.localizedDescription,
      ]
      
      NotificationCenter.default.post(name: NotificationKeys.contactFetchFailure, object: nil, userInfo: userInfo)
      // hmm, keep going if we can.  This is kind of a product question.
      fetchGroup.leave()
    }
  }
   
  
  private func fetchContainers(fromStore store: CNContactStore) -> [CNContainer] {
    do {
      return try store.containers(matching: nil)
    } catch let error {
      print("\(#function) Owie - error getting containers = \(error.localizedDescription)")
      
      let userInfo: [AnyHashable: Any] = [
        "ErrorLocalizedDescription": error.localizedDescription,
      ]
      
      NotificationCenter.default.post(name: NotificationKeys.contactContainerFetchFailure, object: nil, userInfo: userInfo)
      return []
    }
  }
}

// MARK: Location
extension ContactMediator {
  func get(locationForAddress address: CNPostalAddress, forGroup group:Int, andIndex index: Int,
           completion: @escaping (Result<CLLocation, Error>) -> ()) {
    
    let geoCoder = CLGeocoder()
    
    geoCoder.geocodePostalAddress(address, completionHandler: { (placemarks, error) in
      if let error = error {
        completion(.failure(ContactMediatorError.nolocationfound(error)))
        return
      }
      guard let placemarks = placemarks, let location = placemarks.first?.location else {
        print("\(#function) - couldn't find location for address \(address) at group \(group), and index \(index)")
        completion(.failure(ContactMediatorError.nolocationfound(nil)))
        return
      }
      
      completion(.success(location))
    })
  }
}

extension ContactMediator: CLLocationManagerDelegate {
  func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
    let status = manager.authorizationStatus
    print("\(#function) - status = \(status)")
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    defer {
      manager.stopUpdatingLocation()
    }
    guard let location = locations.first else {
      return
    }
    ContactMediator.shared.myLocation = location
    NotificationCenter.default.post(name: NotificationKeys.acquiredLocation, object: nil)
  }
  
  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    print("\(#function) - failure \(error.localizedDescription)")
    DispatchQueue.main.async { [unowned self] in
      NotificationCenter.default.post(name: NotificationKeys.failedToGetLocation, object: nil)
    }
  }
}
