//
//  ContactCell.swift
//  HatchHomework
//
//  Created by Don Clore on 11/27/20.
//

import UIKit

//  This is super-minimal, I've worked on this enough.   Not going to do label - value pairs, just jam the
// values in labels.

protocol ContactCellDelegate: class {
  func sendMessageButtonTapped(forIndexPath indexPath: IndexPath)
}
class ContactCell: UITableViewCell {
  static let id: String = "ContactCellID"
  
  struct Constants {
    static let horzPad: CGFloat = 12.0
    static let vertPad: CGFloat = 12.0
    static let buttonSize: CGSize = CGSize(width: 70, height: 30)
  }
  
  weak var delegate: ContactCellDelegate?
  
  var indexPath: IndexPath?
  let nameLabel: UILabel = UILabel()
  let phoneLabel: UILabel = UILabel()
  let addressLabel: UILabel = UILabel()
  let distanceFromMe: UILabel = UILabel()
  let sendButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
  let stack: UIStackView = UIStackView()
  
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    
    stack.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(stack)
    stack.axis = .vertical
    stack.spacing = 3
    stack.distribution = .equalSpacing
    stack.alignment = .leading
    
    NSLayoutConstraint.activate([
      stack.topAnchor.constraint(equalTo: contentView.topAnchor, constant: Constants.vertPad),
      stack.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: Constants.horzPad),
      stack.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -Constants.horzPad),
      stack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -Constants.vertPad),
    ])
    
    [nameLabel, phoneLabel, addressLabel, distanceFromMe, sendButton].forEach {
      stack.addArrangedSubview($0)
    }
    sendButton.tintColor = .systemBlue
    sendButton.setTitleColor(.systemBlue, for: UIControl.State.normal)
    sendButton.setTitle("Send Message", for: UIControl.State.normal)
    sendButton.addTarget(self, action: #selector(ContactCell.sendButtonTapped(_:)), for: UIControl.Event.touchUpInside)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func configure(withViewModel viewModel: ContactViewModel, andIndexPath indexPath: IndexPath) {
    nameLabel.text = viewModel.name
    phoneLabel.text = viewModel.phoneNumber
    addressLabel.text = viewModel.address
    distanceFromMe.text = viewModel.distanceFromMe
    self.indexPath = indexPath
    contentView.setNeedsLayout()
  }
  
  @objc func sendButtonTapped(_ sender: UIButton) {
    guard let indexPath = indexPath, let delegate = delegate else {
      return
    }
    delegate.sendMessageButtonTapped(forIndexPath: indexPath)
  }
}
