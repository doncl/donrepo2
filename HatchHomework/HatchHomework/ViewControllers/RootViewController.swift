//
//  ViewController.swift
//  HatchHomework
//
//  Created by Don Clore on 11/27/20.
//

import UIKit
import MessageUI

class RootViewController: UIViewController {
  struct Constants {
    static let rowHeight: CGFloat = 160.0
  }
  let table: UITableView = UITableView(frame: CGRect.zero, style: UITableView.Style.grouped)
  let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))

  init() {
    super.init(nibName: nil, bundle: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(RootViewController.contactsAreFetching(_:)),
                                           name: NotificationKeys.fetchingContacts, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(RootViewController.contactContainerFailed(_:)),
                                           name: NotificationKeys.contactContainerFetchFailure, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(RootViewController.contactFetchForOneContainerSuccess(_:)),
                                           name: NotificationKeys.contactFetchSuccess, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(RootViewController.contactFetchForOneContainerFailed(_:)),
                                           name: NotificationKeys.contactFetchFailure, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(RootViewController.contactFetchingComplete(_:)),
                                           name: NotificationKeys.doneFetchingContacts, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(RootViewController.failedToGetLocation(_:)),
                                           name: NotificationKeys.failedToGetLocation, object: nil)
    
    NotificationCenter.default.addObserver(self, selector: #selector(RootViewController.acquiredLocation(_:)),
                                           name: NotificationKeys.acquiredLocation, object: nil)
    
    ContactMediator.shared.kickOffFetching()
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // N.B. if we cared about dark mode, we use colors in asset catalogs, and create different appearances
    // That's about all I want to say about that, just it seems outside the scope of what we're doing here.
    view.backgroundColor = .white
    view.addSubview(table)
    table.translatesAutoresizingMaskIntoConstraints = false
    title = "CONTACTS!!"
    navigationItem.title = title
    spinner.isHidden = true
    spinner.style = .large
        
    // Similary, the question of leading vs. left, and trailing vs. right anchors is one of localization and right-to-left
    // languages.   I *think* this only applies to textual UIControls, like label, textfield, textview, but I honestly have
    // never done a product that localized to Arabic or other right-to-left language. So I'm punting on this.
    NSLayoutConstraint.activate([
      table.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      table.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      table.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
    
    table.register(ContactCell.self, forCellReuseIdentifier: ContactCell.id)
    table.dataSource = self
    table.delegate = self
    table.rowHeight = Constants.rowHeight  // fixed height, self-sizing cells seem unnecssary.
  }
}

// MARK: Contact fetch notification handlers
extension RootViewController {
  @objc func contactsAreFetching(_ note: Notification) {
    print("\(#function)")
    DispatchQueue.main.async { [weak self] in
      guard let self = self else { return }
      self.spinner.center = self.view.center
      self.spinner.isHidden = false
      self.spinner.startAnimating()
      self.table.addSubview(self.spinner)
      self.table.bringSubviewToFront(self.spinner)
    }
  }
  
  @objc func contactContainerFailed(_ note: Notification) {
    assert(Thread.isMainThread)
    print("\(#function)")
    spinner.stopAnimating()
    spinner.isHidden = true
    showError(errMsg: "Couldn't fetch contacts containers, sorry, mwah, mwah")
  }
  
  @objc func contactFetchForOneContainerSuccess(_ note: Notification) {
    assert(Thread.isMainThread)
    print("\(#function)")
    let groupCount = ContactMediator.shared.contactsGroupsCount
    let totalCount = ContactMediator.shared.totalContactsCount
    print("contact group count = \(groupCount), total = \(totalCount)")
    
    table.reloadData()
  }
  
  @objc func contactFetchForOneContainerFailed(_ note: Notification) {
    print("\(#function)")
    DispatchQueue.main.async { [weak self] in
      guard let _ = self else { return }
      // Do something useful.
    }
  }
  
  @objc func contactFetchingComplete(_ note: Notification) {
    assert(Thread.isMainThread)
    print("\(#function)")
    shutdownSpinner()
  }
  
  @objc func failedToGetLocation(_ note: Notification) {
    assert(Thread.isMainThread)
    print("\(#function)")
    shutdownSpinner()
    showError(errMsg: "Failed utterly to get location.")
  }
  
  @objc func acquiredLocation(_ note: Notification) {
    assert(Thread.isMainThread)
    
    // Brute force - this could be done surgically.
    let numSections = table.numberOfSections
    var sections: [Int] = []
    for i in 0..<numSections {
      sections.append(i)
    }
    let indexSet: IndexSet = IndexSet(sections)
    table.reloadSections(indexSet, with: .automatic)
  }
  
  private func shutdownSpinner() {
    // for better or worse
    spinner.stopAnimating()
    spinner.removeFromSuperview()
    spinner.isHidden = true
  }
}

// MARK: UITableViewDelegate
extension RootViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    print("\(#function)")
  }
}

// MARK: UITableViewDataSource
extension RootViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return ContactMediator.shared.contactsGroupsCount
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return ContactMediator.shared.count(forContainerIndex: section)
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: ContactCell.id, for: indexPath) as? ContactCell else {
      fatalError("Table improperly provisioned")
    }
    
    if let viewModel = ContactMediator.shared.getViewModel(forIndexPath: indexPath) {
      cell.configure(withViewModel: viewModel, andIndexPath: indexPath)
      cell.delegate = self
    }
    return cell
  }
}

// MARK: Error messages
extension RootViewController {
  func showError(errMsg: String) {
    let ac = UIAlertController(title: "Error", message: errMsg, preferredStyle: .alert)
    let ok = UIAlertAction(title: "OK", style: .default)
    
    ac.addAction(ok)
    present(ac, animated: true)
  }
}

// MARK: ContactCellDelegate
extension RootViewController: ContactCellDelegate {
  func sendMessageButtonTapped(forIndexPath indexPath: IndexPath) {
    guard MFMessageComposeViewController.canSendText() else {
      showError(errMsg: "This device cannot send SMS messages.")
      return
    }
   
    if let viewModel = ContactMediator.shared.getViewModel(forIndexPath: indexPath),
       let phoneNumber = viewModel.phoneNumber {
      let composeVC = MFMessageComposeViewController()
      
      composeVC.recipients = [phoneNumber]
      composeVC.body = "Is this what you guys had in mind?"
      
      present(composeVC, animated: true)
    }
  }
}
  

