//
//  SceneDelegate.swift
//  HatchHomework
//
//  Created by Don Clore on 11/27/20.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

  var window: UIWindow?


  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    guard let windowScene = (scene as? UIWindowScene) else {
      return
    }
    let frame = windowScene.coordinateSpace.bounds
    window =  UIWindow(frame: frame)
    window?.windowScene = windowScene

    // As regards IB vs. layout-in-code, I'm mostly Switzerland on this issue.  I'm probably a little quicker and better at
    // layout in code, but I'm perfectly capable of using IB and doing size classes and trait variations in the storyboard.
    // But Maven's app has been completely IB-free - my coding partner, Aarti, threatened to quit if we used IB any more :).
    // She was well and truly disenchanted about it, and wanted to try SnapKit.
    // Personally, if I'm going to do it in code, I prefer just to use layout anchors - SnapKit seems like an unnecessary
    // dependency to take on, but...my principles are flexible.
    //
    // As Groucho Marx said, "These are my principles, and if you don't like 'em.....I have others".
    //
    // My only really serious development principles are:
    //
    // ** Be pessimistic about everything - if it can fail, sooner or later it will.  Belt AND Suspenders, 'elegance' is not a
    //    virtue I care about much.
    
    // ** Write code so simple a twelve year old could understand it (I fail this test a lot, but still....)
    
    // ** It's more important for code to be easily debuggable than easily readable. They're not the same thing.
    
    // ** It's more important to make the code behave properly for our users, than in earning ones peers' approval
    //    in a code review. Again, not quite the same thing. BUT, getting along with my peers, and compromising with them
    //    as needed, is almost equally important.
    
    let vc: UIViewController = RootViewController()
    let nav: UINavigationController = UINavigationController(rootViewController: vc)
    nav.navigationBar.prefersLargeTitles = true
    
    window!.rootViewController = nav
    window!.makeKeyAndVisible()
  }
}

