//
//  NotificationKeys.swift
//  HatchHomework
//
//  Created by Don Clore on 11/27/20.
//

import Foundation

// There are many ways to skin this cat, this is only one.  The NotificationCenter stuff seems out of favor in iOS development
// these days, but....I'm going to use it.

struct NotificationKeys {
  static let fetchingContacts = Notification.Name("fetching_contacts")
  static let contactFetchSuccess = Notification.Name("contact_fetch_succeeded")
  static let contactFetchFailure = Notification.Name("contact_fetch_failure")
  static let contactContainerFetchFailure = Notification.Name("contact_container_fetch_failures")
  static let doneFetchingContacts = Notification.Name("done_fetching_contacts")
  static let failedToGetLocation = Notification.Name("failed_to_get_location")
  static let acquiredLocation = Notification.Name("acquired_location")
}
