import Combine
import Foundation

class Board: ObservableObject {
  private var die = Die()
  private var greenDie = Die()
  @Published private(set) var dieView = DieView()
  @Published private(set) var greenDieView = DieView.green()
  @Published private(set) var isRunning = false
  @Published private(set) var tallies = Array(repeating: 0, count: 6)
  @Published private(set) var compareView = CompareView(values: (0, 0))
}

extension Board {
  func start() {
    connect()
    die.start()
    isRunning = true
  }
  
  func stop() {
    die.stop()
    isRunning = false
  }
}

extension Board {
  private func connect() {
    die = Die()
    greenDie = Die()
    dieViewPipeline()
    greenDieViewPipeline()
    compareViewPipeline()
  }
  
  private func dieViewPipeline() {
    die.sharedRoll
      .map(DieView.init)
      .handleEvents(receiveOutput: { [weak self] _ in
        self?.greenDie.next()
      })
      .assign(to: &$dieView)
  }
  
  private func greenDieViewPipeline() {
    greenDie.sharedRoll
      .print("Green Die View")
      .map {
        DieView.green($0)
      }
      .assign(to: &$greenDieView)
  }
  
  private func compareViewPipeline() {
    Publishers.Zip(die.sharedRoll, greenDie.sharedRoll)
      .map {
        CompareView(values: ($0, $1))
      }
      .assign(to: &$compareView)
  }
            
  
  private func tallyPipeline() {
    die.sharedRoll
      .scan(Array(repeating: 0, count: 6)) { (total, element) in
        var total = total
        total[element - 1] += 1
        return total
      }
      .assign(to: &$tallies)
  }
  
  private func demoPipeline() -> AnyCancellable {
    die.sharedRoll
      .map(\.spellOut)
//      .print("Demo ==>")
      .scan(0) { (total, element) in
        total + element.count
      }
      .sink { int in
  //      print("Sum of Letters:", int)
      }
  }
}
