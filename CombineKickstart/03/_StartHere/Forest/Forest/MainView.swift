//
//  MainView.swift
//  Forest
//
//  Created by Don Clore on 5/30/21.
//

import SwiftUI

struct MainView: View {
  @StateObject private var link = Link()
}

extension MainView {
  var body: some View {
    VStack(spacing: 80) {
      Text(link.contents)
      Button("Next",
             action: {
              link.next()
             })
    }
  }
}

//struct MainView_Previews: PreviewProvider {
//  static var previews: some View {
//    MainView()
//  }
//}
