//
//  Link.swift
//  Forest
//
//  Created by Don Clore on 5/30/21.
//

import Foundation
import FarFarAway
import Combine

class Link {
  private let state = State()
  @Published private(set) var contents: String? = "..."
  
  init() {
    contentsSubscription()
  }
}

extension Link {
  func contentsSubscription() {
    state.$model
      .dropFirst()
      .map(\.value)
      .assignDescription(asOptionalTo: &$contents)
  }
  func next() {
    state.next()
  }
}
