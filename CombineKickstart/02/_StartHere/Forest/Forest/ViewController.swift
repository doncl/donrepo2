import UIKit
import Combine

class ViewController: UIViewController {
  @IBOutlet private weak var label: UILabel!
  private var cancellables: Set<AnyCancellable> = Set<AnyCancellable>()
  private let link: Link = Link()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    labelSubscription().store(in: &cancellables)
  }
  
  private func labelSubscription() -> AnyCancellable {
    link.$contents
      .assign(to: \.text, on: label)
  }
  
  @IBAction private func next(_ sender: UIButton) {
    link.next()
  }
}
