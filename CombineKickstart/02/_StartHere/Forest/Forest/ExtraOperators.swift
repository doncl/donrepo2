//
//  ExtraOperators.swift
//  Forest
//
//  Created by Don Clore on 5/30/21.
//

import Foundation
import Combine

extension Publisher where Output: CustomStringConvertible, Failure == Never {
  func description() -> Publishers.Map<Self, String> {
    map(\.description)
  }
  
  func descriptionAsOptional() -> Publishers.Map<Self, String?> {
    map {.some($0.description)}
  }
  
  func assignDescription(to published: inout Published<String>.Publisher) {
    description().assign(to: &published)
  }
  
  func assignDescription(asOptionalTo published: inout Published<String?>.Publisher) {
    descriptionAsOptional().assign(to: &published)
  }
}
