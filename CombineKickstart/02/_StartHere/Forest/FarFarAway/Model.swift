//
//  Model.swift
//  FarFarAway
//
//  Created by Don Clore on 5/30/21.
//

import Foundation
import Combine

public class Model {
  public let value: Int
  
  public init() {
    value = 0
  }
  private init(value: Int) {
    self.value = value
  }
}

extension Model {
  
  var next: Model {
    Model(value: Int.random(in: 1...100))
  }
}
