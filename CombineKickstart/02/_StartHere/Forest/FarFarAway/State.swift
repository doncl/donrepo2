//
//  State.swift
//  FarFarAway
//
//  Created by Don Clore on 5/30/21.
//

import Foundation
import Combine

public class State {
  @Published public private(set) var model: Model = Model()
  
  public init() {}
}

extension State {
  public func next() {
    model = model.next
  }
}
