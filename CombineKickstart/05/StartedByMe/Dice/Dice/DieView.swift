//
//  DieView.swift
//  Dice
//
//  Created by Don Clore on 5/30/21.
//

import SwiftUI

struct DieView: View {
  private (set) var dots: Int = 0
}

extension DieView {
  var body: some View {
    dieFace
      .font(.largeTitle)
  }
}

extension DieView {
  private var dieFace: Image {
    guard (1...6).contains(dots) else {
      return Image(systemName: "square")
    }
    return Image(systemName: "die.face.\(dots)")
  }
}

