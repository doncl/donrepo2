//
//  DiceApp.swift
//  Dice
//
//  Created by Don Clore on 5/30/21.
//

import SwiftUI

@main
struct DiceApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
