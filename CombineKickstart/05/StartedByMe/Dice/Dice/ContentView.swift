//
//  ContentView.swift
//  Dice
//
//  Created by Don Clore on 5/30/21.
//

import SwiftUI

struct ContentView: View {
  @StateObject private var board = Board()
}

extension ContentView {
  var body: some View {
    VStack (spacing: 40) {
      board.dieView
      HStack(spacing: 50) {
        Button("Start",
               action: board.start)
          .disabled(board.isRunning)
        
        Button("Stop",
               action: board.stop)
          .disabled(!board.isRunning)
      }
    }
  }
}
