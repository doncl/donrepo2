import Combine

public class State {
  @Published private var model = Model()
  
  lazy public private(set) var valuePublisher: AnyPublisher<Int, Never>
    = $model
    .dropFirst()
    .map { model in
      model.value
    }
    .flatMap { value in
      Just(value).tryMap(self.normalizedValue)
        .catch { error in
          Empty()
        }
    }
    .eraseToAnyPublisher()
    
  public init() {}
  
  private func normalizedValue(_ value: Int) throws -> Int {
    if value.isMultiple(of: 5) {
      throw DivisibleByFive()
    } else {
      return value % 3
    }
  }
}

extension State {
  public func next() {
    model = model.next
  }
}

extension State {
  func safeTransform(_ int: Int) -> AnyPublisher<Int, Never> {
    Just(int)
      .tryMap { value in
        try self.normalizedValue(value)
      }
      .catch { error in
        Empty()
      }
      .eraseToAnyPublisher()
  }
}
