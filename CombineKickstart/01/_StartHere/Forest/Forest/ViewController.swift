import UIKit
import Combine

class ViewController: UIViewController {
  @IBOutlet private weak var label: UILabel!
  private var cancellable: AnyCancellable?
  
  @Published private var value: Int = 0
  
  @IBAction private func next(_ sender: UIButton) {
    value = Int.random(in: 1...100)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    cancellable = $value
      .dropFirst()
      .map(\.description)
      .sink { [weak self] s in
        self?.label.text = s
      }
  }
}

