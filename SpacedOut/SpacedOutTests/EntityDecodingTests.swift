//
//  EntityDecodingTests.swift
//  SpacedOutTests
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import XCTest
@testable import SpacedOut

class EntityDecodingTests: XCTestCase, ResourceTestable {
  func testVideoQueryResponseEntityDecoding() {
    let data = getResourceData(named: "ApolloVideoQueryResponse", withExtension: "json")
    
    VideoSearchResponse.jsonDecode(data, success: { response in
      let nasaIds = response.getNasaIDsAndTitles()
      XCTAssertEqual(nasaIds.count, 100)
    }, failure: { error in
      XCTFail(error)

    })
  }
  
  func testVideoAssetEntityDecoding() {
    let data = getResourceData(named: "EgressTrainingVideoAsset", withExtension: "json")
    
    VideoAsset.jsonDecode(data, success: { asset in
      XCTAssertEqual(asset.collection.items.count, 127)
    }, failure: { error in
      XCTFail(error)
    })
  }
  
  
  /// This one was because I needed to do a custom decode of the VideoAsset to urlencode the url strings.
  /// It may have been a mistake to use the URL type instead of a string, but....at least by the time we get the
  /// VideoAssetModel, we know it had a valid URL in it.
  func testDecodingJourneysOfApollo() {
    let data = getResourceData(named: "TheJourneysOfApollo", withExtension: "json")
    
    VideoAsset.jsonDecode(data, success: { asset in
      XCTAssertEqual(263, asset.collection.items.count)
    }, failure: { error in
      XCTFail(error)
    })
  }
}
