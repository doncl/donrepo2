//
//  VideoAssetModelTests.swift
//  SpacedOutTests
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import XCTest
@testable import SpacedOut

class VideoAssetModelTests: XCTestCase, ResourceTestable {
  
  // Friends don't let friends force unwrap optionals.   But I do it here.
  var videoAsset: VideoAsset!
  
  override func setUp() {
    let data = getResourceData(named: "EgressTrainingVideoAsset", withExtension: "json")
    
    VideoAsset.jsonDecode(data, success: { asset in
      videoAsset = asset
    }, failure: { error in
      XCTFail(error)
    })
  }

  func testVideoAssetModelCreation() {
    XCTAssertNotNil(videoAsset)
    
    let videoAssetAndTitle = VideoAssetAndTitle(asset: videoAsset, title: "\(#function)", nasaID: "fakeID")
    
    guard let _ = VideoAssetModel.from(videoAssetAndTitle: videoAssetAndTitle) else {
      XCTFail("Couldn't make an asset model")
      return 
    }
  }
  
}
