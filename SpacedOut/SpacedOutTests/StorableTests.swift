//
//  StorableTests.swift
//  SpacedOutTests
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

import XCTest
@testable import SpacedOut

struct TestStorable : Codable, Storable, Equatable {
  typealias StorableType = TestStorable
  
  var i : Int
  var s : String
  var f : Float
}

/// Simple test class to verify we can store to documents and caches directories with
/// Storable protocol extension.
class StorableTests: XCTestCase {
    
  func testCanStoreToDocumentsDir() {
    testCanStoreHelper(.documents)
  }
  
  func testCanStoreToCachesDir() {
     testCanStoreHelper(.caches)
  }
  
  private func testCanStoreHelper(_ dir : StorableDirectory) {
    let t : TestStorable = TestStorable(i: 42, s: "42", f: 42.0)
    
    t.store(to: dir, as: "foo.json", success: {
    }, failure: { err in
      XCTFail(err)
    })
    
    guard let t2 = TestStorable.retrieve("foo.json", from: dir) else {
      XCTFail("Failed to find test file")
      return
    }
    
    XCTAssertEqual(t, t2)
    
    TestStorable.remove("foo.json", from: dir, failure: { err in
      XCTFail(err)
      return
    })
  }
    
}
