//
//  ApiClientTests.swift
//  SpacedOutTests
//
//  Created by Don Clore on 2/20/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import XCTest
@testable import SpacedOut

/// Unit tests - testing ApiClient with mocked stuff.
class ApiClientTests: XCTestCase, ResourceTestable {
  func testForSuccessfulQuery() {
    let expect = XCTestExpectation(description: "waiting for mock network")

    MockNet.sharedInstance.fauxStatusCode = 200
    APIClient.injectMock(MockNet.sharedInstance)

    APIClient.sharedInstance.searchForVideos(query: "fake query", success: { results in
      expect.fulfill()
    }, error: { _ in
      XCTFail("unexpected failure")
      expect.fulfill()
      return
    })
    
    wait(for: [expect], timeout: 10)
  }
  
  func testForFailedQuery() {
    let expect = XCTestExpectation(description: "waiting for mock network")

    MockNet.sharedInstance.fauxStatusCode = 503
    APIClient.injectMock(MockNet.sharedInstance)

    APIClient.sharedInstance.searchForVideos(query: "fake query", success: { results in
      XCTFail("Unexpected success")
      expect.fulfill()
    }, error: { errorString in
      XCTAssertEqual("Request failed, code = 503", errorString)
      expect.fulfill()
      return
    })
        
    wait(for: [expect], timeout: 10)
  }
    
  func testForSuccessfulAssetFetch() {
    let expect = XCTestExpectation(description: "waiting for mock network")

    MockNet.sharedInstance.fauxStatusCode = 200
    APIClient.injectMock(MockNet.sharedInstance)

    APIClient.sharedInstance.getVideoAsset(from: "fakeAssetId", success: { asset in
      expect.fulfill()
    }, error: { error in
      XCTFail("unexpected failure")
      expect.fulfill()
      return
    })
    wait(for: [expect], timeout: 10)
  }
  
  func testForFailedAssetFetch() {
    let expect = XCTestExpectation(description: "waiting for mock network")

    MockNet.sharedInstance.fauxStatusCode = 503
    APIClient.injectMock(MockNet.sharedInstance)

    APIClient.sharedInstance.getVideoAsset(from: "fakeAssetId", success: { asset in
      XCTFail("Unexpected success")
      expect.fulfill()
    }, error: { error in
      XCTAssertEqual("Request failed, code = 503", error)
      expect.fulfill()
      return
    })
    wait(for: [expect], timeout: 10)
  }
}

class MockNet: InterWebs, ResourceTestable {
  var fauxQueryStringResults: String!
  var fauxAssetStringResults: String!

  enum RequestType: String, CaseIterable {
    case query = "q="
    case asset = "asset"
    
    static func parseRequest(uri: String) -> RequestType? {
      let vals = RequestType.allCases
      for val in vals {
        if uri.contains(val.rawValue) {
          return val
        }
      }
      return nil
    }
  }
  var fauxStatusCode: Int = 503  // you can change this to get slightly different responses.
  
  static var sharedInstance = MockNet()
  
  private init() {
    let querydata = getResourceData(named: "ApolloVideoQueryResponse", withExtension: "json")
    let assetData = getResourceData(named: "EgressTrainingVideoAsset", withExtension: "json")
        
    guard let fauxQueryReturnResults = String(data: querydata, encoding: .utf8) else {
      XCTFail("Couldn't encode faux query result")
      return
    }
    fauxQueryStringResults = fauxQueryReturnResults
    
    guard let fauxAssetReturnResults = String(data: assetData, encoding: .utf8) else {
      XCTFail("Couldn't encode faux asset result")
      return
    }
    
    fauxAssetStringResults = fauxAssetReturnResults
  }
  
  func getNetworkData(uri: String, success: @escaping (String) -> (), failure: @escaping (String) -> ()) {
    guard let requestType = RequestType.parseRequest(uri: uri) else {
      XCTFail("No idea what kind of request \(uri) is")
      return
    }
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.050, execute: {
      if (200..<300).contains(self.fauxStatusCode) {
        // fake success
        switch requestType {
        case .query:
          success(self.fauxQueryStringResults)
        case .asset:
          success(self.fauxAssetStringResults)
        }
      } else {
        failure("Request failed, code = \(self.fauxStatusCode)")
      }
    })
  }
}
