//
//  SpacedOutIntegrationTests.swift
//  SpacedOutIntegrationTests
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import XCTest
@testable import SpacedOut

// N.B. Presumably these would be run at a different stage of the CI pipeline than the Unit tests.  These actually hit the
// network.  The goal was to exercise my ApiClient without having to do it from UI code.

class SpacedOutIntegrationTests: XCTestCase {
  
  //MARK: APIClient tests
  func testSimpleVideoQuery() {
    let expect = XCTestExpectation(description: "Waiting for the API")
    
    APIClient.sharedInstance.searchForVideos(query: "apollo", success: { nasaIDs in
      print("Alert the media!  It worked")
      XCTAssertEqual(nasaIDs.count, 100)
      expect.fulfill()
    }, error: { errorString in
      XCTFail(errorString)
      expect.fulfill()
    })
    
    wait(for: [expect], timeout: 10)
  }
  
  func testGettingVideoAssetAndMakingModel() {
    let expect = XCTestExpectation(description: "Waiting for the API")
    let nasaID: NasaID = "T803048_EGRESS-TRAINING-IN-THE-GULF-OF-MEXICO-WITH-APOLLO-8-BACKUP-CREW"
    APIClient.sharedInstance.getVideoAsset(from: nasaID,
      success: { asset in
        let videoAssetAndTitle = VideoAssetAndTitle(asset: asset, title: "\(#function)", nasaID: nasaID)
        guard let _ = VideoAssetModel.from(videoAssetAndTitle: videoAssetAndTitle) else {
          XCTFail("Couldn't create video model")
          expect.fulfill()
          return
        }
        expect.fulfill()
    }, error: { error in
      XCTFail(error)
      expect.fulfill()
    })
    wait(for: [expect], timeout: 10)
  }
  
  // MARK: VideoManager tests
  func testVideoManagerSearchAndGet() {
    let expect = XCTestExpectation(description: "Waiting for the API")
    
    NASAVideoAPIManager.sharedInstance.searchForVideos(query: "apollo", success: { idsAndTitles in
      guard let first = idsAndTitles.first else {
        XCTFail("no ids returned")
        expect.fulfill()
        return
      }
      NASAVideoAPIManager.sharedInstance.getVideoAsset(from: (nasaID: first.nasaID, title: first.title), success: { videoAssetAndTitle in
        guard let _ = VideoAssetModel.from(videoAssetAndTitle: videoAssetAndTitle) else {
          XCTFail("Couldn't create video model")
          expect.fulfill()
          return
        }
        expect.fulfill()
      }, failure: { error in
        XCTFail(error)
        expect.fulfill()
        return
      })
    }, failure: { error in
      XCTFail(error)
      expect.fulfill()
    })
    
    wait(for: [expect], timeout: 30)
  }
  
  func testVideoManagerFetchBatch() {
    let expect = XCTestExpectation(description: "Waiting for the API")
    
    NASAVideoAPIManager.sharedInstance.searchForVideosAndNotifyWhenFetched(query: "apollo", count: 3, success: { videoAssetModels in
      XCTAssertEqual(3, videoAssetModels.count)
      expect.fulfill()
    }, failure: { errorString in
      XCTFail(errorString)
      expect.fulfill()
    })
    
    wait(for: [expect], timeout: 900)
  }
}
