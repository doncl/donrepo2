//
//  SpaceyTabBarController.swift
//  SpacedOutTV
//
//  Created by Don Clore on 2/29/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class SpaceyTabBarController: UITabBarController {
  let fauxLaunch: FauxLaunchScreen = FauxLaunchScreen()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    var vcs: [UIViewController] = []
    
    if let screen = storyboard?.instantiateViewController(identifier: "Screen") as? MainTVScreen {
        screen.title = "Main"
        vcs.append(screen)
    }
      
    let search = createSearch(storyboard: storyboard)
    vcs.append(search)
      
    viewControllers = vcs
    
    doTheFauxLaunchScreen()
  }
  
  func doTheFauxLaunchScreen() {
    addChild(fauxLaunch)
    view.addSubview(fauxLaunch.view)
    fauxLaunch.didMove(toParent: self)
    fauxLaunch.view.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate([
      fauxLaunch.view.topAnchor.constraint(equalTo: view.topAnchor),
      fauxLaunch.view.leftAnchor.constraint(equalTo: view.leftAnchor),
      fauxLaunch.view.rightAnchor.constraint(equalTo: view.rightAnchor),
      fauxLaunch.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])

    
    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
      let xform = CGAffineTransform(scaleX: 0.1, y: 0.1).concatenating(CGAffineTransform(rotationAngle: CGFloat.pi * 3))
      UIView.animate(withDuration: 0.400, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
        guard let self = self else { return }
        self.fauxLaunch.view.alpha = 0
        self.fauxLaunch.imageView.alpha = 0
        self.fauxLaunch.view.transform = xform
      }, completion: { [weak self] _ in
        guard let self = self else { return }
        self.fauxLaunch.willMove(toParent: nil)
        self.fauxLaunch.view.removeFromSuperview()
        self.fauxLaunch.removeFromParent()
      })
    })
  }
}

extension SpaceyTabBarController {
  func createSearch(storyboard: UIStoryboard?) -> UIViewController {
    guard let screen = storyboard?.instantiateViewController(identifier: "Screen") as? MainTVScreen else {
      fatalError("Unable to instantiate a screen.")
    }
    let searchController = UISearchController(searchResultsController: screen)
    searchController.searchResultsUpdater = screen
    
    let searchContainer = UISearchContainerViewController(searchController: searchController)
    searchContainer.title = "Search"
    return searchContainer
  }
}
