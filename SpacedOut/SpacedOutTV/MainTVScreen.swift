//
//  MainTVScreen.swift
//  SpacedOutTV
//
//  Created by Don Clore on 2/27/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class MainTVScreen: UIViewController, VCSharedCode {
  @IBOutlet var collectionView: UICollectionView!
  @IBOutlet var spinner: UIActivityIndicatorView!
  let queryCountResults: Int = 20
  
  var videos: [VideoAssetModel] = []

  override func viewDidLoad() {
    super.viewDidLoad()
    spinner.isHidden = true
    getThemVideos(query: "")
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupCollectionView()
  }
}

extension MainTVScreen {
  func setupCollectionView() {
    collectionView.register(NASAVideoAssetCell.self, forCellWithReuseIdentifier: NASAVideoAssetCell.id)
    if let flow = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      flow.scrollDirection = .vertical
    }
    collectionView.contentInset = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
  }
}

extension MainTVScreen {
  private func getThemVideos(query: String) {
    spinner.isHidden = false
    spinner.startAnimating()
    NASAVideoAPIManager.sharedInstance.searchForVideosAndNotifyWhenFetched(query: query, count: queryCountResults, success: { [weak self] videos in
      guard let self = self else { return }
      self.videos = videos
      self.collectionView.reloadData()
      self.spinner.stopAnimating()
      }, failure: { [weak self] error in
        guard let self = self else { return }
        self.spinner.stopAnimating()
        self.simpleError(message: error)
    })
  }
}

// MARK: UICollectionViewDataSource
extension MainTVScreen: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return videos.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell =
      collectionView.dequeueReusableCell(withReuseIdentifier: NASAVideoAssetCell.id, for: indexPath) as? NASAVideoAssetCell else {
      fatalError("This never happens")
    }
    let video = videos[indexPath.item]
    cell.delegate = self
    cell.indexPath = indexPath
    cell.assetModel = video
    return cell
  }
}

// MARK: UICollectionViewDelegateFlowLayout
extension MainTVScreen: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      return getItemSize()
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    let url = videos[indexPath.item].canonicalURL
    playVideo(url: url, indexPath: indexPath)
  }
}

extension MainTVScreen: NASAVideoAssetCellDelegate {
  func playVideo(url: URL, indexPath: IndexPath) {
    playVideo(url: url, fromCellAt: indexPath)
  }
  
  func downloadVideo(video: VideoAssetModel, indexPath: IndexPath) {
     // do nothing.
  }
}

extension MainTVScreen: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    guard let query = searchController.searchBar.text else {
      return
    }
    getThemVideos(query: query)
  }
}
