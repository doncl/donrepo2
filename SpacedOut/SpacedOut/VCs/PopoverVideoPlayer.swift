//
//  PopoverVideoPlayer.swift
//  SpacedOut
//
//  Created by Don Clore on 2/27/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit
import AVKit

class PopoverVideoPlayer: UIViewController {
  #if targetEnvironment(macCatalyst)
  let topStripHeight: CGFloat = 42.0
  let closeButtonDim: CGFloat = 36.0
  let closeButtonTopPad: CGFloat = 3.0
  #else
  let topStripHeight: CGFloat = 0.0
  #endif
  
  let playerVC: AVPlayerViewController
  let player: AVPlayer
  
  lazy var activity: UIActivityIndicatorView = {
    let a = UIActivityIndicatorView(style: .large)
    a.translatesAutoresizingMaskIntoConstraints = false
    return a
  }()

  lazy var close: UIButton = {
    let b = UIButton(type: UIButton.ButtonType.custom)
    b.translatesAutoresizingMaskIntoConstraints = false
    let image = UIImage(systemName: "xmark.circle")?.withRenderingMode(.alwaysTemplate)
    b.setImage(image, for: .normal)
    b.tintColor = .white
    b.addTarget(self, action: #selector(PopoverVideoPlayer.closeButtonTapped(_:)), for: .touchUpInside)
    return b
  }()
  
  init(videoURL: URL) {
    self.player = AVPlayer(url: videoURL)
    playerVC = AVPlayerViewController()
    playerVC.player = self.player
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    playerVC.view.translatesAutoresizingMaskIntoConstraints = false
    addChild(playerVC)
    view.addSubview(playerVC.view)
    playerVC.didMove(toParent: self)
    
    view.addSubview(activity)
    
    #if targetEnvironment(macCatalyst)
    view.addSubview(close)
    #endif
    
    view.backgroundColor = .black
    
    NSLayoutConstraint.activate([
      playerVC.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: topStripHeight),
      playerVC.view.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      playerVC.view.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      playerVC.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
      
      activity.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      activity.centerXAnchor.constraint(equalTo: view.centerXAnchor),
    ])
    
    #if targetEnvironment(macCatalyst)
    NSLayoutConstraint.activate([
      close.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: closeButtonTopPad),
      close.heightAnchor.constraint(equalToConstant: closeButtonDim),
      close.widthAnchor.constraint(equalToConstant: closeButtonDim),
      close.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: closeButtonTopPad),
    ])
    #endif

    activity.startAnimating()
    // Add a KVO observer so we can tell when to shut down the activity animation.
    player.currentItem?.addObserver(self, forKeyPath: "status", options: [], context: nil)

    player.play()    
  }
  
  @objc func closeButtonTapped(_ sender: UIButton) {
    dismiss(animated: true)
  }
}

extension PopoverVideoPlayer {
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    guard let item = object as? AVPlayerItem else {
      return
    }
    activity.stopAnimating()
    item.removeObserver(self, forKeyPath: "status")
    playerVC.view.backgroundColor = .clear
  }
}

extension PopoverVideoPlayer {
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    if traitCollection.userInterfaceIdiom == .pad {
      dismiss(animated: true)
    }
  }
  
  override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
    super.willTransition(to: newCollection, with: coordinator)
    if traitCollection.userInterfaceIdiom == .pad {
      dismiss(animated: true)
    }
  }
}
