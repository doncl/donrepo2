//
//  MainSearchScreen.swift
//  SpacedOut
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class MainSearchScreen: UIViewController, VCSharedCode {
  @IBOutlet var searchBar: UISearchBar!
  @IBOutlet var collectionView: UICollectionView!
  @IBOutlet var spinner: UIActivityIndicatorView!
  
  let fauxLaunch: FauxLaunchScreen = FauxLaunchScreen()
  var videos: [VideoAssetModel] = []
  
  var reenableSearchBar: BlockOperation?
  var reenableSearchBarAddedToQ: Bool = false
  
  let operationQueue = OperationQueue()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    spinner.isHidden = true
    searchBar.autocorrectionType = .no
    searchBar.autocapitalizationType = .none
    doTheFauxLaunchScreen()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    setupCollectionView()
  }
}

extension MainSearchScreen {
  func setupCollectionView() {
    guard traitCollection.horizontalSizeClass != .unspecified else {
      return
    }
    
    collectionView.register(NASAVideoAssetCell.self, forCellWithReuseIdentifier: NASAVideoAssetCell.id)
    
    assert(traitCollection.horizontalSizeClass == .regular || traitCollection.horizontalSizeClass == .compact)
    
    if let flow = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      flow.scrollDirection = .vertical
    }
  }
}

// MARK: Faux Launch Screen handling
extension MainSearchScreen {
  func doTheFauxLaunchScreen() {
    addChild(fauxLaunch)
    view.addSubview(fauxLaunch.view)
    fauxLaunch.didMove(toParent: self)
    fauxLaunch.view.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      fauxLaunch.view.topAnchor.constraint(equalTo: view.topAnchor),
      fauxLaunch.view.leftAnchor.constraint(equalTo: view.leftAnchor),
      fauxLaunch.view.rightAnchor.constraint(equalTo: view.rightAnchor),
      fauxLaunch.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])

    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
      let xform = CGAffineTransform(scaleX: 0.1, y: 0.1).concatenating(CGAffineTransform(rotationAngle: CGFloat.pi * 3))
      UIView.animate(withDuration: 0.400, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
        guard let self = self else { return }
        self.fauxLaunch.view.alpha = 0
        self.fauxLaunch.imageView.alpha = 0
        self.fauxLaunch.view.transform = xform
      }, completion: { [weak self] _ in
        guard let self = self else { return }
        self.fauxLaunch.willMove(toParent: nil)
        self.fauxLaunch.view.removeFromSuperview()
        self.fauxLaunch.removeFromParent()
      })
    })
  }
}

// MARK: UICollectionViewDataSource
extension MainSearchScreen: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return videos.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell =
      collectionView.dequeueReusableCell(withReuseIdentifier: NASAVideoAssetCell.id, for: indexPath) as? NASAVideoAssetCell else {
      fatalError("This never happens")
    }
    let video = videos[indexPath.item]
    cell.delegate = self
    cell.indexPath = indexPath
    cell.assetModel = video
    return cell
  }
}

// MARK: UICollectionViewDelegateFlowLayout
extension MainSearchScreen: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return getItemSize()
  }
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    searchBar.resignFirstResponder()
  }
}

// MARK: UISearchBarDelegate
extension MainSearchScreen: UISearchBarDelegate {
  func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool {
    return true
  }
  
  func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    guard let query = searchBar.text else {
      return
    }
    spinner.isHidden = false
    spinner.startAnimating()
    searchBar.searchTextField.isEnabled = false // Don't allow rediting query until returns
    
    NASAVideoAPIManager.sharedInstance.searchForVideosAndNotifyWhenFetched(query: query, count: 10, success: { [weak self] models in
      guard let self = self else { return }
      self.spinner.stopAnimating()
      self.videos = models
      self.collectionView.reloadData()
      self.searchBar.searchTextField.isEnabled = true
    }, failure: { [weak self] error in
      guard let self = self else { return }
      self.spinner.stopAnimating()
      self.simpleError(message: error)
      self.searchBar.searchTextField.isEnabled = true
    })
  }
}

// MARK:  Fun with rotation!
extension MainSearchScreen {
  
  // This one gets called on whenever it rotates.
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)

    if let collectionView = collectionView {
      collectionView.collectionViewLayout.invalidateLayout()
    }
  }
}

extension MainSearchScreen: NASAVideoAssetCellDelegate {
  func playVideo(url: URL, indexPath: IndexPath) {
    playVideo(url: url, fromCellAt: indexPath)
  }
  
  func downloadVideo(video: VideoAssetModel, indexPath: IndexPath) {
    searchBar.searchTextField.isEnabled = false
    
    if reenableSearchBar == nil {
      reenableSearchBar = BlockOperation{
        DispatchQueue.main.async { [weak self] in
          guard let self = self else {
             return
           }
           self.searchBar.searchTextField.isEnabled = true
           self.reenableSearchBar = nil
           self.reenableSearchBarAddedToQ = false
        }
      }
    }
   
    
    print("\(#function)")    
    let downloadOperation = VideoDownloadOperation(videoAssetModel: video, success: {[weak self] savedFile, asset in
      guard let self = self else { return }
      PersistenceManager.sharedInstance.saveDownloadedVideo(fileURL: savedFile, asset: asset)
      guard let cell = self.collectionView.cellForItem(at: indexPath) as? NASAVideoAssetCell else {
        return
      }
      cell.downloadButton.isHidden = true
      cell.hideProgress()
      cell.setNeedsDisplay()
    }, error: { [weak self] error in
      guard let self = self else { return }
      guard let cell = self.collectionView.cellForItem(at: indexPath) as? NASAVideoAssetCell else {
        return
      }
      self.simpleError(message: error)
      cell.downloadButton.setImage(#imageLiteral(resourceName: "download"), for: .normal)  // show that still needs download
      cell.hideProgress()
      cell.setNeedsDisplay()
    }, progress: { [weak self] percent in
      guard let self = self else { return }
      guard let cell = self.collectionView.cellForItem(at: indexPath) as? NASAVideoAssetCell else {
        return
      }
      cell.showProgess(percent: percent)
      cell.setNeedsDisplay()
    })
    
    operationQueue.addOperation(downloadOperation)
    if let reenableSearchBar = reenableSearchBar {
      reenableSearchBar.addDependency(downloadOperation)
      if !reenableSearchBarAddedToQ {
        operationQueue.addOperation(reenableSearchBar)
        reenableSearchBarAddedToQ = true
      }
    }

  }  
}

// MARK: DragDrop to reorder items on screen

// MARK: DragDrop to reorder items on screen
extension MainSearchScreen: UICollectionViewDragDelegate {
  func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
    return [getDragItem(at: indexPath)]
  }
}

extension MainSearchScreen: UICollectionViewDropDelegate {
  func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession,
                      withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
    
    return getDropProposal()
  }

  func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
    performDrop(with: coordinator)
  }
}
