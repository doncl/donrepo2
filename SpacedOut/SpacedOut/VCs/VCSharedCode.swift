//
//  VCSharedCode.swift
//  SpacedOut
//
//  Created by Don Clore on 2/19/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit
import AVKit

struct VCSharedConstants {
  static let horzPad: CGFloat = 8.0
  static let regularRowItems: CGFloat = 3
  static let compactRowItems: CGFloat = 1
  static let cellAspectRatio: CGFloat = 1.333
}

protocol VCSharedCode: class {

  var videos: [VideoAssetModel] { get set }
  var collectionView: UICollectionView! { get }
  
  func getItemSize() -> CGSize
  func simpleError(message: String)
  func playVideo(url: URL, fromCellAt indexPath: IndexPath)
  
  #if !os(tvOS)
  func getDragItem(at indexPath: IndexPath) -> UIDragItem
  func getDropProposal() -> UICollectionViewDropProposal
  func performDrop(with coordinator: UICollectionViewDropCoordinator)
  #endif

}

extension VCSharedCode where Self: UIViewController {
  func simpleError(message: String) {
    let ac = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
    let ok = UIAlertAction(title: "Press to resume", style: .default, handler: { _ in
      self.dismiss(animated: true, completion: nil)
    })
    ac.addAction(ok)
    present(ac, animated: true, completion: nil)    
  }
  
  func getItemSize() -> CGSize {
    let width: CGFloat
    let height: CGFloat
    
    // Regular horizontal size class gets cells sized such that flowLayout puts 3 across, compact gets 1.
    if traitCollection.horizontalSizeClass == .regular {
      width = collectionView.bounds.width / VCSharedConstants.regularRowItems - (VCSharedConstants.horzPad * (VCSharedConstants.regularRowItems + 1))
    } else {
      width = collectionView.bounds.width / VCSharedConstants.compactRowItems - (VCSharedConstants.horzPad * (VCSharedConstants.compactRowItems + 1))
    }

    height = VCSharedConstants.cellAspectRatio * width
    let size = CGSize(width: width, height: height)
    return size
  }
  
  #if !os(tvOS)
  func getDragItem(at indexPath: IndexPath) -> UIDragItem {
    let item = self.videos[indexPath.item]
    let itemProvider = NSItemProvider(object: item.title as NSString)
    let dragItem = UIDragItem(itemProvider: itemProvider)
    dragItem.localObject = item
    return dragItem
  }
  
  func getDropProposal() -> UICollectionViewDropProposal {
    if collectionView.hasActiveDrag {
      return UICollectionViewDropProposal(operation: UIDropOperation.move,
                                          intent: UICollectionViewDropProposal.Intent.insertAtDestinationIndexPath)
    } else {
      return UICollectionViewDropProposal(operation: .forbidden)
    }
  }
  
  func performDrop(with coordinator: UICollectionViewDropCoordinator) {
    if coordinator.proposal.operation == .move {
      let destIndexPath: IndexPath
      if let dest = coordinator.destinationIndexPath {
        destIndexPath = dest
      } else {
        let section = collectionView.numberOfSections - 1
        let row = collectionView.numberOfItems(inSection: section) - 1
        guard row >= 0, section >= 0 else {
          return
        }
        destIndexPath = IndexPath(item: row, section: section)
      }
      reorderItems(coordinator: coordinator, destinationIndexPath: destIndexPath, collectionView: collectionView)
    }
  }
  
  private func reorderItems(coordinator: UICollectionViewDropCoordinator, destinationIndexPath: IndexPath, collectionView: UICollectionView) {
    guard let item = coordinator.items.first,
      let sourceIndexPath = item.sourceIndexPath,
      let titleDragged = item.dragItem.localObject as? VideoAssetModel else {
      return
    }
    
    videos.remove(at: sourceIndexPath.item)
    videos.insert(titleDragged, at: destinationIndexPath.item)
    
    collectionView.performBatchUpdates({
      collectionView.deleteItems(at: [sourceIndexPath])
      collectionView.insertItems(at: [destinationIndexPath])
    }, completion: nil)
    
    coordinator.drop(item.dragItem, toItemAt: destinationIndexPath)
    
    collectionView.scrollToItem(at: destinationIndexPath, at: .centeredVertically, animated: true)
  }
  #endif
  
  func playVideo(url: URL, fromCellAt indexPath: IndexPath) {
    print("\(#function)")
    
    let vc = PopoverVideoPlayer(videoURL: url)
    #if !os(tvOS)
    vc.modalPresentationStyle = .formSheet
    #endif
    let bounds = view.bounds
    let width = bounds.width * 0.83
    let height = width * 0.5625 // 16:9
    
    vc.preferredContentSize = CGSize(width: width, height: height)
    
    self.present(vc, animated: true, completion: nil)
  }
}
