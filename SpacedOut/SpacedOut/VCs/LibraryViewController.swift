//
//  LibraryViewController.swift
//  SpacedOut
//
//  Created by Don Clore on 2/19/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class LibraryViewController: UIViewController, VCSharedCode {
  @IBOutlet var collectionView: UICollectionView!
  
  var videos: [VideoAssetModel] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    NotificationCenter.default.addObserver(self, selector: #selector(LibraryViewController.managedObjectContextChanged(_:)),
                                           name: Notification.Name.NSManagedObjectContextDidSave, object: nil)
    
    collectionView.register(NASAVideoAssetCell.self, forCellWithReuseIdentifier: NASAVideoAssetCell.id)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    videos = PersistenceManager.sharedInstance.fetchAllVideos()
    collectionView.reloadData()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }
  
  @objc func managedObjectContextChanged(_ note: Notification) {
    DispatchQueue.main.async {
      self.videos = PersistenceManager.sharedInstance.fetchAllVideos()
      self.collectionView.reloadData()
    }
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
}

extension LibraryViewController: UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return getItemSize()
  }
}

extension LibraryViewController: UICollectionViewDataSource {
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return videos.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NASAVideoAssetCell.id, for: indexPath) as? NASAVideoAssetCell else {
      fatalError("never happens")
    }

    let video = videos[indexPath.item]
    cell.assetModel = video
    cell.indexPath = indexPath
    cell.delegate = self
    cell.hideProgress()
    cell.downloadButton.isHidden = true
    return cell
  }
}

extension LibraryViewController: NASAVideoAssetCellDelegate {
  func playVideo(url: URL, indexPath: IndexPath) {
    playVideo(url: url, fromCellAt: indexPath)
  }
  
  func downloadVideo(video: VideoAssetModel, indexPath: IndexPath) {
    // not implemented.
  }
}


// MARK: Rotation!
extension LibraryViewController {
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    if let collectionView = collectionView {
      collectionView.collectionViewLayout.invalidateLayout()
    }
  }
}

// MARK: DragDrop to reorder items on screen
extension LibraryViewController: UICollectionViewDragDelegate {
  func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
    return [getDragItem(at: indexPath)]
  }
}

extension LibraryViewController: UICollectionViewDropDelegate {
  func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession,
                      withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
    
    return getDropProposal()
  }

  func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
    performDrop(with: coordinator)
  }
}
