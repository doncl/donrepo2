//
//  NASAVideoAssetCell.swift
//  SpacedOut
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit
import Kingfisher
import AVKit
import AVFoundation

protocol NASAVideoAssetCellDelegate: class {
  func downloadVideo(video: VideoAssetModel, indexPath: IndexPath)
  func playVideo(url: URL, indexPath: IndexPath)
}

// I thought I'd do this one all in code, as a contrast to the VC's I did in IB.  I am probably more
// conversant with doing things in code, but have no qualms about IB - I'm Switzerland in that debate,
// and I did IB quite a bit at Fubo.
class NASAVideoAssetCell: UICollectionViewCell {
  
  // MARK: Constants
  static let id = "NASAVideoAssetCell_Reuse_ID"
  let playButtonDim: CGFloat = 60.0
  static let captionHeight: CGFloat = 28.0
  static let downloadButtonMargin: CGFloat = 16.0
  static let downloadButtonDim: CGFloat = 30.0
  
  // MARK: Properties
  weak var delegate: NASAVideoAssetCellDelegate?
  var indexPath: IndexPath = IndexPath(item: 0, section: 0)
  
  lazy var downloadButton: UIButton = {
    let btn = UIButton(type: UIButton.ButtonType.custom)
    btn.setImage(#imageLiteral(resourceName: "download"), for: UIControl.State.normal)
    btn.translatesAutoresizingMaskIntoConstraints = false
    btn.isUserInteractionEnabled = true
    btn.addTarget(self, action: #selector(NASAVideoAssetCell.downloadButtonTapped(_:)), for: .touchUpInside)
    btn.tintColor = .white
    return btn
  }()
  
  lazy var imageView: UIImageView = {
    let iv = UIImageView()
    iv.translatesAutoresizingMaskIntoConstraints = false
    iv.contentMode = UIView.ContentMode.scaleAspectFill
    iv.clipsToBounds = true
    iv.image = UIImage(named: "placeHolder")
    iv.isUserInteractionEnabled = true
    return iv
  }()
  
  let caption: UILabel = {
    let l = UILabel()
    l.font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle.body)
    l.translatesAutoresizingMaskIntoConstraints = false
    return l
  }()
  
  lazy var playButtonImage: UIImageView = {
     let iv = UIImageView()
     iv.contentMode = UIView.ContentMode.scaleAspectFit
     iv.translatesAutoresizingMaskIntoConstraints = false
     iv.accessibilityIdentifier = "playbutton"
     iv.image = UIImage(named: "playCircle")
     iv.backgroundColor = UIColor.black.withAlphaComponent(0.5)
     iv.layer.cornerRadius = playButtonDim / 2
     iv.isUserInteractionEnabled = true
     let tap = UITapGestureRecognizer(target: self, action: #selector(NASAVideoAssetCell.playButtonTapped(_:)))
     iv.addGestureRecognizer(tap)
     return iv
   }()
  
  lazy var activity: UIActivityIndicatorView = {
    let a = UIActivityIndicatorView(style: .large)
    a.translatesAutoresizingMaskIntoConstraints = false
    return a
  }()
  
  lazy var progressLabel: UILabel = {
    let l = UILabel()
    l.translatesAutoresizingMaskIntoConstraints = false
    l.font = UIFont.preferredFont(forTextStyle: .headline)
    l.text = "0%"
    l.layer.cornerRadius = playButtonDim / 2
    l.backgroundColor = UIColor.white
    l.textAlignment = .center
    l.clipsToBounds = true 
    l.isHidden = true
    return l
  }()
  
  var assetModel: VideoAssetModel = VideoAssetModel(nasaID: "",
                                                    canonicalURL: NetworkConstants.videoPlaceHolderURL ,
                                                    mobileURL: NetworkConstants.videoPlaceHolderURL,
                                                    imageURL: NetworkConstants.imagePlaceHolderURL,
                                                    title: "") {
    didSet {
      imageView.kf.setImage(with: assetModel.imageURL, completionHandler: { [weak self] result in
        guard let self = self else { return }
        switch result {
        case .success(let outcome):
          self.imageView.image = outcome.image
          self.downloadButton.tintColor = outcome.image.isDark ? UIColor.white : UIColor.black
        case .failure(let error):
          self.imageView.image = UIImage(named: "placeHolder")
          print(error.localizedDescription)
        }
      })
      caption.text = assetModel.title
      #if os(tvOS)
      downloadButton.isHidden = true
      #else
      let exists = PersistenceManager.sharedInstance.videoExists(id: assetModel.nasaID)
      downloadButton.isHidden = exists
      #endif
      
      setNeedsDisplay()
    }
  }
   
  // MARK: Initializers
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    layoutAllTheThings()
    
    #if os(tvOS)
    imageView.adjustsImageWhenAncestorFocused = true
    clipsToBounds = false 
    #endif
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: Methods
  override func prepareForReuse() {
    imageView.image = UIImage(named: "placeHolder")
    caption.text = ""
    progressLabel.isHidden = true
  }
}

// MARK: Layout stuff.
extension NASAVideoAssetCell {
  private func layoutAllTheThings() {
    contentView.addSubview(imageView)
    contentView.addSubview(caption)
    imageView.addSubview(playButtonImage)
    imageView.addSubview(downloadButton)
    contentView.addSubview(activity)
    playButtonImage.addSubview(progressLabel)
    
    NSLayoutConstraint.activate(constraints: [
      
      // The main image.
      imageView.topAnchor.constraint(equalTo: contentView.topAnchor),
      imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
      imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
      imageView.bottomAnchor.constraint(equalTo: caption.topAnchor),
      
      // The caption.
      caption.leftAnchor.constraint(equalTo: contentView.leftAnchor),
      caption.rightAnchor.constraint(equalTo: contentView.rightAnchor),
      caption.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      caption.heightAnchor.constraint(equalToConstant: NASAVideoAssetCell.captionHeight),
      
      // The play button.
      playButtonImage.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
      playButtonImage.centerYAnchor.constraint(equalTo: imageView.centerYAnchor),
      playButtonImage.widthAnchor.constraint(equalToConstant: playButtonDim),
      playButtonImage.heightAnchor.constraint(equalTo: playButtonImage.widthAnchor),
      
      // Download Button
      downloadButton.bottomAnchor.constraint(equalTo: imageView.bottomAnchor, constant: -NASAVideoAssetCell.downloadButtonMargin),
      downloadButton.rightAnchor.constraint(equalTo: imageView.rightAnchor, constant: -NASAVideoAssetCell.downloadButtonMargin),
      downloadButton.widthAnchor.constraint(equalToConstant: NASAVideoAssetCell.downloadButtonDim),
      downloadButton.heightAnchor.constraint(equalToConstant: NASAVideoAssetCell.downloadButtonDim),
      
      // Spinner
      activity.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
      activity.centerYAnchor.constraint(equalTo: imageView.centerYAnchor),
      
      // progressLabel
      progressLabel.topAnchor.constraint(equalTo: playButtonImage.topAnchor),
      progressLabel.leftAnchor.constraint(equalTo: playButtonImage.leftAnchor),
      progressLabel.rightAnchor.constraint(equalTo: playButtonImage.rightAnchor),
      progressLabel.bottomAnchor.constraint(equalTo: playButtonImage.bottomAnchor),
    ], andSetPriority: UILayoutPriority(rawValue: 999))
  }
}

// MARK: Behaviors
extension NASAVideoAssetCell {
  @objc func downloadButtonTapped(_ sender: UIButton) {
    downloadButton.isHidden = true
    showProgess(percent: 0)
    delegate?.downloadVideo(video: assetModel, indexPath: indexPath)
  }
  
  @objc func playButtonTapped(_ sender: UITapGestureRecognizer) {
    guard let delegate = delegate else {
      return 
    }
    
    let videoURLToPlay: URL
    if let localFileURL = assetModel.fileURL, FileManager.default.fileExists(atPath: localFileURL.path) {
      videoURLToPlay = localFileURL
    } else {
      videoURLToPlay = traitCollection.horizontalSizeClass == .regular ? assetModel.canonicalURL : assetModel.mobileURL
    }
    
    delegate.playVideo(url: videoURLToPlay, indexPath: indexPath)
  }
}

// MARK: Download UI
extension NASAVideoAssetCell {
  func showProgess(percent: Double) {
    let formatter = NumberFormatter()
    formatter.numberStyle = NumberFormatter.Style.percent
    let nsnum = NSNumber(value: percent)
    let string = formatter.string(from: nsnum)
    progressLabel.text = string
    progressLabel.isHidden = false
  }
  
  func hideProgress() {
    progressLabel.isHidden = true 
  }
}

#if os(tvOS)
extension NASAVideoAssetCell {
//  override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
//    if self.isFocused {
//
//    }
//  }
}
#endif
