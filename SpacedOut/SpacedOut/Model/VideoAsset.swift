//
//  Asset.swift
//  SpacedOut
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

struct VideoAsset: Codable, JSONCoding {
  typealias CodableType = VideoAsset
    
  struct VideoAssetCollection: Codable, JSONCoding {
    typealias CodableType = VideoAssetCollection
    
    struct VideoItem: Codable, JSONCoding {
      typealias CodableType = VideoItem
      
      enum CodingKeys: String, CodingKey {
        case href = "href"
      }
      
      let href: URL?
      
      init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let urlString = try container.decode(String?.self, forKey: .href) {
          var urlStringSecure: String = urlString.replacingOccurrences(of: " ", with: "%20")
          if urlString.hasPrefix("http://") {
            urlStringSecure = urlStringSecure.replacingOccurrences(of: "http://", with: "https://")
          }
          if let url = URL(string: urlStringSecure) {
            href = url
          } else {
            href = nil
          }
        } else {
          href = nil
        }
      }
    }
    
    let items: [VideoItem]
  }
  
  let collection: VideoAssetCollection
}

// I was tempted to call this a 'view model', but I know I'm tempting fate if it doesn't fit exactly into the canonical
// box that ViewModel's are supposed to be.  Because...some people care about that kind of purity.  I don't...so much.
// Truth is, I have no principles.   Or rather, like Groucho Marx....these are my
// principles, and if you don't like 'em....I have others.

// Looking at the NASA Asset responses, there's a fair amount of wiggle room for how you might choose which video to take, and
// which image to take.  But for the purposes of this spike/assignment, just going to do something simple.   I'm going to
// attempt to take an image from the middle, in the hopes of getting something that looks better than the cover image.
struct VideoAssetModel {
  let nasaID: NasaID
  let canonicalURL: URL
  let mobileURL: URL
  let imageURL: URL
  let title: String
  var fileURL: URL?

  static func from(videoAssetAndTitle: VideoAssetAndTitle) -> VideoAssetModel? {
    let asset = videoAssetAndTitle.asset
    let title = videoAssetAndTitle.title
    let id = videoAssetAndTitle.nasaID
    let allURLs = asset.collection.items.compactMap { $0.href }
    let mp4s = allURLs.filter{$0.pathComponents.last?.hasSuffix("mp4") == true}
    let jpgs = allURLs.filter{$0.pathComponents.last?.hasSuffix("jpg") == true}
    let pngs = allURLs.filter{$0.pathComponents.last?.hasSuffix("png") == true}
    
    guard mp4s.count > 0 else {
      return nil
    }
    
    guard jpgs.count > 0 || pngs.count > 0 else {
      return nil
    }
    
    let urls = getVideoURLs(from: mp4s)
    let canonicalURL = urls.canonicalURL
    let mobileURL = urls.mobileURL
    let imageURL = getImageURL(from: pngs, or: jpgs)
    
    return VideoAssetModel(nasaID: id, canonicalURL: canonicalURL, mobileURL: mobileURL, imageURL: imageURL, title: title)
  }
  
  
  /// Get a pair of URLs to use - just heuristics from having eyeballed a few sample assets.
  /// - Parameter mp4s: all the mp4s in the asset. 
  static private func getVideoURLs(from mp4s: [URL]) -> (canonicalURL: URL, mobileURL: URL) {
    assert(mp4s.count > 0)
    
    var ret: (canonicalURL: URL, mobileURL: URL)
    if let medium = mp4s.first(where: { $0.absoluteString.contains("medium.mp4") }) {
      ret.canonicalURL = medium
    } else if let orig = mp4s.first(where: { $0.absoluteString.contains("orig.mp4") }) {
      ret.canonicalURL = orig
    } else if let large = mp4s.first(where: { $0.absoluteString.contains("large.mp4") }) {
      ret.canonicalURL = large // let's hope not.
    } else if let preview = mp4s.first(where: { $0.absoluteString.contains("preview.mp4" )}) {
      ret.canonicalURL = preview
    } else {
      ret.canonicalURL = mp4s[0]
    }
    
    if let mobile = mp4s.first(where: {$0.absoluteString.contains("mobile.mp4")}) {
      ret.mobileURL = mobile
    } else {
      ret.mobileURL = ret.canonicalURL
    }
    return ret
  }
  
  /// Just making it up as I go; get something plausible for a thumbnail image.
  /// - Parameters:
  ///   - pngs: Array of PNGS - might be empty.
  ///   - jpgs: Array of JPGS - might be empty.  - but one of them has something.
  static private func getImageURL(from pngs: [URL], or jpgs: [URL]) -> URL {
    assert(pngs.count > 0 || jpgs.count > 0)
    // favor the jpgs or pngs?  Probably pngs.
        
    var imagesToUse: [URL] = pngs.filter { $0.absoluteString.contains("large") }
    if imagesToUse.count == 0 {
      imagesToUse = jpgs.filter { $0.absoluteString.contains("large") }
    }
    if imagesToUse.count == 0 {
      imagesToUse = pngs.count > 0 ? pngs : jpgs
    }
    
    assert(imagesToUse.count > 0)
    
    let mid = imagesToUse.count / 2
    let midImageURL = imagesToUse[mid]
    return midImageURL
  }
}

