//
//  VideoSearchResponse.swift
//  SpacedOut
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

struct VideoSearchResponse: Codable, JSONCoding {
  typealias CodableType = VideoSearchResponse
  
  struct ResponseCollection: Codable, JSONCoding {
    typealias CodableType = ResponseCollection
    
    struct Item: Codable, JSONCoding {
      typealias CodableType = Item
      
      struct ItemData: Codable, JSONCoding {
        typealias CodableType = ItemData
        
        enum CodingKeys: String, CodingKey {
          case nasaID = "nasa_id"
          case title
        }
        let nasaID: NasaID? // it's useless without this, but do we trust their API?  Nope.
        let title: String?
      }
      let data: [ItemData]
    }
    
    let items: [Item]
  }
  
  let collection: ResponseCollection
}

extension VideoSearchResponse {
  func getNasaIDsAndTitles() -> [(nasaID: NasaID, title: String)] {
    // OK, folks, I was tempted to put this all in one functional line of code to show that I could, but I realized....I
    // hate code like that (tricky code)  So I broke it up into pedestrian sections that can be followed with the eye, or
    // more importantly, in lldb.  Like I frequently say, my goal is to produce code a 12 year old could understand, although
    // I confess I fail that test a lot.

    // Collect up the item data arrays
    let itemDataArrays: [[ItemData]] = collection.items.compactMap { $0.data}
    
    // squish them together.
    let itemDatas: [ItemData] = Array(itemDataArrays.joined())
    
    // Grovel out the nasaIDs from them.
    let idsAndTitles: [(nasaID: NasaID, title: String)] = itemDatas.compactMap{ itemData in
      guard let id = itemData.nasaID, let title = itemData.title else {
        return nil
      }
      return (nasaID: id, title: title)
    }
    return idsAndTitles
  }
}
