//
//  PersistenceManager.swift
//  SpacedOut
//
//  Created by Don Clore on 2/19/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation
import CoreData

struct PersistenceManager {
  
  static let entityName: String = "DownloadedVideo"
  static var sharedInstance = PersistenceManager()
  
  private init() {}
  
  // MARK: - Core Data stack
   lazy var persistentContainer: NSPersistentContainer = {
     let container = NSPersistentContainer(name: "SpacedOut")
     container.loadPersistentStores(completionHandler: { (storeDescription, error) in
       if let error = error as NSError? {
         print("\(#function) - error = \(error.localizedDescription)")
       }
     })
     return container
   }()
  
  lazy var context: NSManagedObjectContext = {
    return self.persistentContainer.viewContext
  }()


   // MARK: - Core Data Saving support
   mutating func saveContext () {
        
     let context = persistentContainer.viewContext
     if context.hasChanges {
       do {
         try context.save()
       } catch {
         let nserror = error as NSError
         print("\(#function) - error = \(nserror.localizedDescription)")
         print(nserror.userInfo)
       }
     }
   }
  
  mutating func saveDownloadedVideo(fileURL: URL, asset: VideoAssetModel) {
    assert(Thread.isMainThread)
    
    let video = NSEntityDescription.insertNewObject(forEntityName: PersistenceManager.entityName, into: context) as! DownloadedVideo
    video.canonicalURL = asset.canonicalURL
    video.mobileURL = asset.mobileURL
    video.imageURL = asset.imageURL
    video.localFileURL = fileURL
    video.nasaID = asset.nasaID
    video.title = asset.title
  
    saveContext()
  }
  
  mutating func fetchAllVideos() -> [VideoAssetModel] {
    let request = NSFetchRequest<DownloadedVideo>(entityName: PersistenceManager.entityName)
    
    do {
      let result: [DownloadedVideo] = try self.context.fetch(request)
      return result.compactMap{ VideoAssetModel.from(downloadedVideo: $0)}
    } catch let error {
      print(error.localizedDescription)
      return []
    }
  }
  
  mutating func fetchVideo(id: NasaID) -> DownloadedVideo? {
    let predicate = NSPredicate(format: "nasaID == %@", id)
    let fetchRequest = NSFetchRequest<DownloadedVideo>(entityName: PersistenceManager.entityName)
    fetchRequest.predicate = predicate
    
    do {
      let videos = try context.fetch(fetchRequest)
      return videos.first
    } catch let error {
      print(error.localizedDescription)
      return nil
    }
  }
  
  mutating func videoExists(id: NasaID) -> Bool {
    return fetchVideo(id: id) == nil ? false : true 
  }
}

fileprivate extension VideoAssetModel {
  static func from(downloadedVideo persist: DownloadedVideo) -> VideoAssetModel? {
    guard let nasaID = persist.nasaID, let canonicalURL = persist.canonicalURL, let mobileURL = persist.mobileURL,
      let imageURL = persist.imageURL, let title = persist.title else {
        return nil
    }
    return VideoAssetModel(nasaID: nasaID, canonicalURL: canonicalURL, mobileURL: mobileURL,
                           imageURL: imageURL, title: title, fileURL: persist.localFileURL)
  }
}
