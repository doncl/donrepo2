//
//  DownloadedVideo+CoreDataProperties.swift
//  SpacedOut
//
//  Created by Don Clore on 2/19/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//
//

import Foundation
import CoreData


extension DownloadedVideo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<DownloadedVideo> {
        return NSFetchRequest<DownloadedVideo>(entityName: "DownloadedVideo")
    }

    @NSManaged public var nasaID: String?
    @NSManaged public var canonicalURL: URL?
    @NSManaged public var localFileURL: URL?
    @NSManaged public var mobileURL: URL?
    @NSManaged public var imageURL: URL?
    @NSManaged public var title: String?

}
