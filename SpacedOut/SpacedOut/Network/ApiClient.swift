//
//  ApiClient.swift
//  SpacedOut
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

typealias NasaID = String
typealias ItemData = VideoSearchResponse.ResponseCollection.Item.ItemData

struct APIClient {
  static var sharedInstance = APIClient()
  
  // Injectable.
  var net: InterWebs = Net.sharedInstance
  
  private init() {}
  
  static func injectMock(_ mock: InterWebs) {
    Net.sharedInstance = mock
  }
  
  
  func searchForVideos(query: String, success: @escaping ([(nasaID: NasaID, title: String)]) -> (), error: @escaping (String) -> ()) {
    guard let escapedQuery = query.urlEncode() else {
      error("Unable to urlEncode query \(query)")
      return
    }
    
    // It seems NASA returns data in pages of 100, but by default, just returns the first page.
    let endpoint = "\(NetworkConstants.apiRoot)search?q=\(escapedQuery)&media_type=video&page=1"
    net.getNetworkData(uri: endpoint, success: { string in
      guard let data = string.data(using: .utf8) else {
        error("Failed to encode response as data")
        return
      }
      
      VideoSearchResponse.jsonDecode(data, success: { (response: VideoSearchResponse) in
        let nasaIdsAndTitles = response.getNasaIDsAndTitles()
        success(nasaIdsAndTitles)
        
      }, failure: { (errorString: String) in
        error(errorString)
      })
    }, failure: { errorString in
      error(errorString)
    })
  }
  
  func getVideoAsset(from nasaID: NasaID, success: @escaping (VideoAsset) -> (), error: @escaping (String) -> ()) {
    guard let escapedID = nasaID.urlEncode() else {
      error("Unable to urlEncode nasaID \(nasaID)")
      return
    }
    
    let endpoint = "\(NetworkConstants.apiRoot)asset/\(escapedID)"
    
    net.getNetworkData(uri: endpoint, success: { string in
      guard let data = string.data(using: .utf8) else {
        error("Failed to encode response as data")
        return
      }
      VideoAsset.jsonDecode(data, success: { (videoAsset: VideoAsset) in
        success(videoAsset)        
      }, failure: { errorString in
        error(errorString)
      })
    }, failure: { errorString in
      error(errorString)
    })
  }
}

extension String {
  func urlEncode() -> String? {
    return addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
  }
}
