//
//  NetworkConstants.swift
//  SpacedOut
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

struct NetworkConstants {
  // Evidently I don't need this, or it doesn't even work with the API.   Fun!  Leaving in, just in case.
  static let apiKey: String = "zULwagXOLYKcwKcDb408FAfF4hTcqB7YvNhdF2Ny"
  
  static let apiRoot: String = "https://images-api.nasa.gov/"
  
  static let imagePlaceHolderURL: URL =
  URL(string: "https://images-assets.nasa.gov/video/T803048_EGRESS-TRAINING-IN-THE-GULF-OF-MEXICO-WITH-APOLLO-8-BACKUP-CREW/T803048_EGRESS-TRAINING-IN-THE-GULF-OF-MEXICO-WITH-APOLLO-8-BACKUP-CREW~mobile_thumb_00010.png")!

  
  static let videoPlaceHolderURL: URL =
  URL(string: "https://images-assets.nasa.gov/video/T803048_EGRESS-TRAINING-IN-THE-GULF-OF-MEXICO-WITH-APOLLO-8-BACKUP-CREW/T803048_EGRESS-TRAINING-IN-THE-GULF-OF-MEXICO-WITH-APOLLO-8-BACKUP-CREW~small.mp4")!
}
