//
//  VideoDownloadOperation.swift
//  SpacedOut
//
//  Created by Don Clore on 2/19/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

typealias ErrorFunc = (String) -> ()
typealias SuccessFunc = (URL, VideoAssetModel) -> ()
typealias ProgressFunc = (Double) -> ()


@objc class VideoDownloadOperation: ConcurrentOperation {
  var downloadTask: URLSessionDownloadTask?
  let videoAssetModel: VideoAssetModel
  let errorFunc: ErrorFunc
  let successFunc: SuccessFunc
  let progressFunc: ProgressFunc
  
  private lazy var urlSession: URLSession = {
    let config = URLSessionConfiguration.background(withIdentifier: videoAssetModel.nasaID)
    config.isDiscretionary = false
    config.sessionSendsLaunchEvents = true
    return URLSession(configuration: config, delegate: self, delegateQueue: nil)
  }()
  
  init(videoAssetModel: VideoAssetModel, success: @escaping SuccessFunc, error: @escaping ErrorFunc, progress: @escaping ProgressFunc ) {
    self.videoAssetModel = videoAssetModel
    self.successFunc = success
    self.progressFunc = progress
    self.errorFunc = error
    super.init()
  }

  override func main() {
    print("\(#function)")
    downloadFunc()
  }
}

extension VideoDownloadOperation {
  private func downloadFunc() {
    let url = videoAssetModel.canonicalURL    
    let task = urlSession.downloadTask(with: url)
    print("\(#function) - downloading \(url)")
    downloadTask = task
    task.resume()
  }
}

extension VideoDownloadOperation: URLSessionDelegate {
  func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
    print("\(#function)")
    DispatchQueue.main.async {
      guard let appDelegate = UIApplication.shared.delegate as? AppDelegate,
        let backgroundCompletionHandler = appDelegate.backgroundCompletionHandler else {
          return
      }
      backgroundCompletionHandler()
    }
  }
  
  func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
    print("\(#function)")
    if let error = error {
      let errorMsg = error.localizedDescription
      print("\(#function) - error \(errorMsg)")
      DispatchQueue.main.async { [weak self] in
        guard let self = self else { return }
        self.errorFunc(errorMsg)
      }
    }
    completeOperation()
  }
}

extension VideoDownloadOperation: URLSessionDownloadDelegate {
  func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
    print("\(#function)")
    if let error = error {
      DispatchQueue.main.async {
        self.errorFunc(error.localizedDescription)
      }
    }
  }
  
  func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
    print("\(#function)")
    let docDir = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    let fm = FileManager.default
    let name = UUID().uuidString + ".mp4" // Since all the Nasa videos seem to be .mp4.  A more robust solution would look at the canonicalURL.
    let fullDestPath = "file://\(docDir)/\(name)"
  
    guard let dest = URL(string: fullDestPath) else {
      errorFunc("Could not create file URL from path \(fullDestPath)")
      return
    }
    
    do {
      try fm.copyItem(at: location, to: dest)
      DispatchQueue.main.async {
        self.successFunc(dest, self.videoAssetModel)
        self.completeOperation()
      }
    } catch let error {
      errorFunc("Failure copying \(location) to \(dest), error = \(error.localizedDescription)")
      self.completeOperation()
      return
    }
  }
  
  func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
    print("\(#function) - bytesWritten \(bytesWritten), totalBytesWritten \(totalBytesWritten), totalBytesExpected \(totalBytesExpectedToWrite)")
    DispatchQueue.main.async { [weak self] in
      guard let self = self else { return }
      guard totalBytesExpectedToWrite > 0 else {
        return
      }
      let percent: Double = (Double(totalBytesWritten) / Double(totalBytesExpectedToWrite))
      print("\(#function) - percent = \(percent)")
      if percent > 0 {
        self.progressFunc(percent)
      }
    }
  }
}
