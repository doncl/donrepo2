//
//  Net.swift
//  SpacedOut
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

/// Protocol to make it easier to mock this class for unit testing.
protocol InterWebs {
  func getNetworkData(uri : String, success: @escaping (String) -> (),
                      failure : @escaping (String) -> ())
}

/// Simple network GET tool.
class Net : InterWebs {
  static let requestTimeOut: TimeInterval = 30
  
  
  
  // To make it easy to mock, this sharedInstance is visible and mutable.
  static var sharedInstance : InterWebs = Net()
  
  private init() {
  }
      
  /// The one and only utility entrypoint to this struct.  A simple GET mechanism, using
  /// URLSession
  ///
  /// - Parameters:
  ///   - uri: The network resource to GET, in string form.
  ///   - success: Success callback with String data.
  ///   - failure: A description of the error.
  func getNetworkData(uri : String, success: @escaping (String) -> (),
    failure : @escaping (String) -> ()) {
    
    let session = URLSession(configuration: .default)
    let headers : [String : String] = [
      "Accept" : "application/json",
      ]
    
    guard let url : URL = URL(string: uri) else {
      return
    }
    let request = NSMutableURLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                      timeoutInterval: Net.requestTimeOut)
    
    request.httpMethod = "GET"
    for header in headers {
      request.addValue(header.1, forHTTPHeaderField: header.0)
    }
    
    let task = session.dataTask(with: request as URLRequest) { (data, response, error) -> () in
      
      if let error = error {
        failure(error.localizedDescription)
        return
      }
      if let httpResp = response as? HTTPURLResponse {
        if httpResp.statusCode != 200 {
          failure("Request failed, code = \(httpResp.statusCode)")
          return
        }
      }
      guard let data = data else {
        failure("Request returned nil data")
        return
      }
      guard let stringData = String(data: data, encoding: .utf8) else {
        failure("Unable to convert network returned data to String")
        return
      }
      success(stringData)
    }
    task.resume()
  }
}
