//
//  NASAVideoAPIManager.swift
//  SpacedOut
//
//  Created by Don Clore on 2/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

/// Just a little SRP distance from the ApiClient, whose job is just to manage talking to endpoints.
/// If we had lots of different entity types, we could have lots of different manager types; there's nothing special about it,
/// it's just a very relaxed organizing principle (notice, I didn't use the word 'Pattern' :)). It's a spot for injection and
/// all that kind of jazz.  I'm not going to clutter this up with declaring an injectable protocol, the Russian doll nesting
/// indirection has to stop somewhere, but hypothetically it could be done here.
///

struct NASAVideoAPIManager {
  static let sharedInstance = NASAVideoAPIManager()
  static let maxRequests: Int = 30
  
  private let q: DispatchQueue = DispatchQueue(label: "VideoManagerQueue")
  private let dispatchGroup = DispatchGroup()
      
  private init() {}
  
  func searchForVideos(query: String, success: @escaping ([(nasaID: NasaID, title: String)]) -> (), failure: @escaping (String) -> ()) {
    APIClient.sharedInstance.searchForVideos(query: query, success: { ids in
      success(ids)
    }, error: { error in
      failure(error)
    })
  }
  
  func getVideoAsset(from idAndTitle: (nasaID: NasaID, title: String), success: @escaping (VideoAssetAndTitle) -> (),
    failure: @escaping (String) -> ()) {
    
    APIClient.sharedInstance.getVideoAsset(from: idAndTitle.nasaID, success: { videoAsset in
      let assetAndTitle = VideoAssetAndTitle(asset: videoAsset, title: idAndTitle.title, nasaID: idAndTitle.nasaID)
      success(assetAndTitle)
    }, error: { error in
      failure(error)
    })
  }
  
  func searchForVideosAndNotifyWhenFetched(query: String, count: Int, success: @escaping ([VideoAssetModel]) -> (),
                                           failure: @escaping (String) -> ()) {
    var ret: [VideoAssetModel] = []
    let maxCount = min(count, NASAVideoAPIManager.maxRequests)  // Don't go crazy.
    
    searchForVideos(query: query, success: { idsAndTitles in
      let realCount = min(maxCount, idsAndTitles.count)
      for i in 0..<realCount {
        let idAndTitle = idsAndTitles[i]
        self.dispatchGroup.enter()
        self.getVideoAsset(from: idAndTitle, success: { videoAssetAndTitle in
          guard let videoAssetModel = VideoAssetModel.from(videoAssetAndTitle: videoAssetAndTitle) else {
            self.dispatchGroup.leave()
            return
          }
          self.q.sync {
            ret.append(videoAssetModel)
            self.dispatchGroup.leave()
          }
        }, failure: { errorString in
          self.dispatchGroup.leave()
          let id = idAndTitle.nasaID
          failure("Error fetching video \(i) \(id) = \(errorString)")
        })
      }
      self.dispatchGroup.notify(queue: .main) {
        success(ret)
      }
    }, failure: { error in
      DispatchQueue.main.async {
        failure(error)
      }
    })
  }
}

struct VideoAssetAndTitle {
  let asset: VideoAsset
  let title: String
  let nasaID: NasaID
}
