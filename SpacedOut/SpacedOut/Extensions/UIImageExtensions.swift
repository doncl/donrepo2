//
//  UIImageExtensions.swift
//  SpacedOut
//
//  Created by Don Clore on 2/19/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

extension UIImage {
  var isDark: Bool {
    return self.cgImage?.isDark ?? false
  }
}
