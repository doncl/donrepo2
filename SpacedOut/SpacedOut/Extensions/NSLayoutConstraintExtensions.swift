//
//  NSLayoutConstraintExtensions.swift
//  SpacedOut
//
//  Created by Don Clore on 2/19/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

extension NSLayoutConstraint {
  class func activate(constraints: [NSLayoutConstraint], andSetPriority priority: UILayoutPriority) {
    constraints.forEach {
      $0.priority = priority
      $0.isActive = true
    }
  }
}
