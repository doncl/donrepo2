//
//  EmojiMemoryGame.swift
//  Memorize
//
//  Created by Don Clore on 1/24/21.
//

import SwiftUI

class EmojiMemoryGame: ObservableObject {
  @Published private var model: MemoryGame<String> = EmojiMemoryGame.createMemoryGame()
  
  static private func createMemoryGame() -> MemoryGame<String> {
    let emojis: [String] = ["👻", "🎃", "🕷"]
    let numPairs = Int.random(in: 4..<12)
    return MemoryGame<String>(numberOfPairsOfCards: numPairs) { pairIndex in
      let index = pairIndex % emojis.count
      return emojis[index]
    }
  }
  
  // MARK: Access to the model
  var cards: Array<MemoryGame<String>.Card> {
    model.cards
  }
  
  // MARK: - Intent(s)
  func choose(card: MemoryGame<String>.Card) {
    model.choose(card: card)
  }
}
