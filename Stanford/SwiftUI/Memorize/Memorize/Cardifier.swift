//
//  Cardifier.swift
//  Memorize
//
//  Created by Don Clore on 1/30/21.
//

import SwiftUI

struct Cardify: ViewModifier {
  struct Constants {
    static let cornerRadius: CGFloat = 10.0
    static let lineWidth: CGFloat = 3.0
    static let fillColor: Color = Color.white
  }

  var isFaceUp: Bool

  func body(content: Content) -> some View {
    ZStack {
      if isFaceUp {
        RoundedRectangle(cornerRadius: Constants.cornerRadius).fill(Constants.fillColor)
        RoundedRectangle(cornerRadius: Constants.cornerRadius).stroke(lineWidth: Constants.lineWidth)
        content
      } else {
        RoundedRectangle(cornerRadius: Constants.cornerRadius).fill()
      }
    }
  }
}

extension View {
  func cardify(isFaceUp: Bool) -> some View {
    self.modifier(Cardify(isFaceUp: isFaceUp))
  }
}
