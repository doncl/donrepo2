//
//  MemoryGame.swift
//  Memorize
//
//  Created by Don Clore on 1/24/21.
//

import Foundation

struct MemoryGame<CardContent> where CardContent: Equatable {
  struct Card: Identifiable {
    var isFaceUp: Bool = false
    var isMatched: Bool = false
    var content: CardContent
    let id: Int
  }

  private(set) var cards: Array<Card> = []
  
  private var indexOfTheOneAndOnlyFaceUpCard: Int? {
    get {
      cards.indices.filter({ cards[$0].isFaceUp }).only
    }
    
    set {
      for index in cards.indices {
        cards[index].isFaceUp = index == newValue
      }
    }
  }
  
  init(numberOfPairsOfCards: Int, cardContentFactory: (Int) -> CardContent) {
    var unshuffledCards: [Card] = []
    for pairIndex in 0..<numberOfPairsOfCards {
      let content: CardContent = cardContentFactory(pairIndex)
      unshuffledCards.append(Card(content: content, id: pairIndex * 2))
      unshuffledCards.append(Card(content: content, id: pairIndex * 2 + 1))
    }
    cards = unshuffledCards.shuffled()
  }
 
  mutating func choose(card: Card) {
    if let chosenIndex: Int = cards.firstIndex(matching: card),
       !cards[chosenIndex].isFaceUp,
       !cards[chosenIndex].isMatched {
      
      if let potentialMatchIndex = indexOfTheOneAndOnlyFaceUpCard {
        if cards[chosenIndex].content == cards[potentialMatchIndex].content {
          cards[chosenIndex].isMatched = true
          cards[potentialMatchIndex].isMatched = true
        }
        self.cards[chosenIndex].isFaceUp = true
      } else {
        indexOfTheOneAndOnlyFaceUpCard = chosenIndex
      }
    }
  }
}
