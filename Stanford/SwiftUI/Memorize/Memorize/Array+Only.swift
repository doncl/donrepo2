//
//  Array+Only.swift
//  Memorize
//
//  Created by Don Clore on 1/26/21.
//

import Foundation

extension Array {
  var only: Element? {
    count == 1 ? first : nil 
  }
}
