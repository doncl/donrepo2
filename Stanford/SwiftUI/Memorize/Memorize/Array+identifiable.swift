//
//  Array+identifiable.swift
//  Memorize
//
//  Created by Don Clore on 1/26/21.
//

import Foundation

extension Array where Element: Identifiable {
  func firstIndex(matching: Element) -> Int? {
    return firstIndex(where: { $0.id == matching.id })
  }
}
