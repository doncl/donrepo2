//
//  SearchVC1.swift
//  SearchSpikeDemo
//
//  Created by Don Clore on 7/18/21.
//

import UIKit

class SearchVC1: UIViewController {
  let t = UITableView(frame: .zero, style: .plain)
  let titles = Books.main.allTitles()
  var filteredTitles: [String] = []
  let searchController = UISearchController(searchResultsController: nil)
  
  var video: VideoView?
  
  
  var isSearchBarEmpty: Bool {
    return searchController.searchBar.text?.isEmpty ?? true
  }

  var isFiltering: Bool {
    return searchController.isActive && !isSearchBarEmpty
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white
    navigationItem.title = "UISearchController with NavBar"
    
    searchController.searchResultsUpdater = self
    searchController.obscuresBackgroundDuringPresentation = false
    searchController.searchBar.placeholder = "Search Book Titles"
    definesPresentationContext = true
    extendedLayoutIncludesOpaqueBars = true
    searchController.hidesNavigationBarDuringPresentation = true
    
    searchController.searchBar.delegate = self
    
    let notificationCenter = NotificationCenter.default
    notificationCenter.addObserver(forName: UIResponder.keyboardWillChangeFrameNotification,
                                   object: nil, queue: .main) { (notification) in
                                    self.handleKeyboard(notification: notification) }
    notificationCenter.addObserver(forName: UIResponder.keyboardWillHideNotification,
                                   object: nil, queue: .main) { (notification) in
                                    self.handleKeyboard(notification: notification) }
    
    t.register(UITableViewCell.self, forCellReuseIdentifier: "id")
    
    t.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(t)
    searchController.searchBar.translatesAutoresizingMaskIntoConstraints = false
    navigationItem.searchController = searchController

    NSLayoutConstraint.activate([
      t.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      t.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      t.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
      t.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      
    ])
    
    t.dataSource = self
    t.delegate = self    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    video?.pause()
    video?.fadeOut()
  }
  
  func handleKeyboard(notification: Notification) {
    guard notification.name == UIResponder.keyboardWillChangeFrameNotification else {
      self.t.contentInset = .zero
      view.layoutIfNeeded()
      return
    }
    
    guard let info = notification.userInfo, let keyboardFrame = info[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else {
      return
    }
    
    let keyboardHeight = keyboardFrame.cgRectValue.size.height
    UIView.animate(withDuration: 0.1, animations: {
      self.t.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: keyboardHeight)
      self.view.layoutIfNeeded()
    })
  }
}

extension SearchVC1: UITableViewDelegate {
  
}

extension SearchVC1: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if isFiltering {
      return filteredTitles.count
    }
    
    return titles.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "id", for: indexPath)
    let title: String
    if isFiltering {
      title = filteredTitles[indexPath.item]
    } else {
      title = titles[indexPath.item]
    }
    cell.textLabel?.text = title 
    return cell
  }
}

extension SearchVC1: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchBar = searchController.searchBar
    guard let searchText = searchBar.text else {
      return
    }
    filteredTitles = Books.main.getSearchResults(fromString: searchText)
    t.reloadData()
  }
}

extension SearchVC1: UISearchBarDelegate {
  
}
