//
//  VideoView.swift
//  SearchSpikeDemo
//
//  Created by Don Clore on 7/18/21.
//

import UIKit
import UIKit
import QuartzCore
import AVFoundation

class VideoView: UIView {
  var layerAdded : Bool = false
  var shadowOffsetWidth : CGFloat = 2.5
  var shadowOffsetHeight : CGFloat = 2.5
  var shadowRadius : CGFloat = 2.5
  var shadowOpacity: Float = 0.5
  var videoURL : URL?
  var viewToShowIn : UIView?
  var observerAdded : Bool = false
  
  var isPlaying : Bool = false
  
  let playerLayer : AVPlayerLayer = AVPlayerLayer()
  var player : AVPlayer? {
    return playerLayer.player
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    initPhase2()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  fileprivate func initPhase2() {
    backgroundColor = #colorLiteral(red: 0.7759256959, green: 0.7759256959, blue: 0.7759256959, alpha: 0.6991087148)
    layer.shadowColor = UIColor.black.cgColor
    layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight)
    layer.shadowRadius = shadowRadius
    layer.shadowOpacity = shadowOpacity
    
  }
  
  fileprivate func fadeIn(completion: (() -> ())? = nil) {
    transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
    alpha = 0.0
    UIView.animate(withDuration: 0.35, animations: {
      self.alpha = 1.0
      self.transform = CGAffineTransform.identity
    }, completion: { _ in
      if let completion = completion {
        completion()
      }
    })
  }
  
  func fadeOut() {
    UIView.animate(withDuration: 0.35, animations: {
      self.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
      self.alpha = 0.0
    }, completion: {(finished) in
      if finished {
        self.removeFromSuperview()
      }
    })
  }
  
  // MARK: - Instance methods
  func showInView(_ view: UIView, completion: @escaping () -> ()) {
    view.addSubview(self)
    fadeIn(completion: completion)
  }
}

//MARK: AVFoundation stuff
extension VideoView {
  func load(video url : URL) {
    videoURL = url
    playerLayer.frame = bounds
    let asset = AVAsset(url: url)
    let playerItem = AVPlayerItem(asset: asset)
    
    if layerAdded == false {
      layer.addSublayer(playerLayer)
      NotificationCenter.default.addObserver(self,
        selector: #selector(VideoView.playerReachedEnd(_:)),
        name: NSNotification.Name(rawValue: "AVPlayerItemDidPlayToEndTimeNotification"),
        object: playerItem)
 
      layerAdded = true
    }
    
    // Register as an observer of the player item's status property
    playerItem.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status),
        options: [.old, .new], context: nil)
    
    observerAdded = true
    let player = AVPlayer(playerItem: playerItem)
    player.actionAtItemEnd = .none
    playerLayer.player = player
  }
  
  func play() {
    player?.play()
  }
  
  func pause() {
    killVideo()
  }
  
  @objc func playerReachedEnd(_ note: Notification) {
    killVideo()
  }
  
  fileprivate func killVideo() {
    if let player = player, let item = player.currentItem {
      if observerAdded {
        item.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
        observerAdded = false
      }

      player.pause()
    }
    
    fadeOut()
  }
}

//MARK: KVO
extension VideoView {
  override func observeValue(forKeyPath keyPath: String?, of object: Any?,
    change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
    guard let change = change else {
      return
    }
    
    if keyPath == #keyPath(AVPlayerItem.status) {
      let status: AVPlayerItem.Status
      
      // Get the status change from the change dictionary
      if let statusNumber = change[.newKey] as? NSNumber {
        status = AVPlayerItem.Status(rawValue: statusNumber.intValue)!
      } else {
        status = .unknown
      }
      
      // Switch over the status
      if status == .readyToPlay {
        if let viewToShowIn = viewToShowIn, let player = player {
          showInView(viewToShowIn, completion: {
            player.play()
          })
        }
      }
    }
  }
}
