//
//  SearchVC2.swift
//  SearchSpikeDemo
//
//  Created by Don Clore on 7/18/21.
//

import UIKit

class SearchVC2: UIViewController {
  var searchController: UISearchController?
  
  override var prefersStatusBarHidden: Bool  { true }
    
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .white
    navigationItem.title = "UISearchController with SearchResultsController"
    
    let resultVC = SearchResultsVC()
    let sc = UISearchController(searchResultsController: resultVC)
    searchController = sc
    sc.definesPresentationContext = true
    sc.searchResultsUpdater = resultVC
    
    let sb = sc.searchBar
    sb.placeholder = "Search Book Titles"
    view.addSubview(sb)
  }
}
