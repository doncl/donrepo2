//
//  SearchVC3.swift
//  SearchSpikeDemo
//
//  Created by Don Clore on 7/18/21.
//

import UIKit

class SearchVC3: UIViewController {
  
  let searchBar: UISearchBar = UISearchBar()
  let resultsController = SearchResultsVC()

  override func viewDidLoad() {
    super.viewDidLoad()
    [searchBar,].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      view.addSubview($0)
    }
    
    NSLayoutConstraint.activate([
      searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      searchBar.leadingAnchor.constraint(equalTo: view.leadingAnchor),
      searchBar.trailingAnchor.constraint(equalTo: view.trailingAnchor),
      searchBar.heightAnchor.constraint(equalToConstant: 56),
    ])

    addChild(resultsController)
    view.addSubview(resultsController.view)
    resultsController.didMove(toParent: self)
    
    searchBar.delegate = self 
    
    let rv = resultsController.view!
    rv.translatesAutoresizingMaskIntoConstraints = false 
    NSLayoutConstraint.activate([
      rv.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
      rv.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
      rv.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
      rv.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
  }
  
  func clearTextAndResignKeyboard() {
    searchBar.text = ""
    resultsController.doSearch(text: nil)
    searchBar.resignFirstResponder()
  }
}

extension SearchVC3: UISearchBarDelegate {
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
    resultsController.clearSearchResultsAndReloadTable()
    resultsController.doSearch(text: searchBar.text)
    searchBar.showsCancelButton = true
  }
    
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    clearTextAndResignKeyboard()
  }
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    resultsController.doSearch(text: searchText)
  }
}

