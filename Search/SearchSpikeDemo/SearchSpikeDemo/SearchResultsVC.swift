//
//  SearchResultsVC.swift
//  SearchSpikeDemo
//
//  Created by Don Clore on 7/18/21.
//

import UIKit

class SearchResultsVC: UIViewController {
  let table = UITableView(frame: .zero, style: .plain)
  
  var titles: [String] = []

  override func viewDidLoad() {
    super.viewDidLoad()

    table.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(table)

    table.dataSource = self
    table.delegate = self
    table.accessibilityIdentifier = "ResultTable"
    
    NSLayoutConstraint.activate([
      table.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      table.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      table.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
      table.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
    ])
    
    table.register(UITableViewCell.self, forCellReuseIdentifier: "id")
    titles = Books.main.allTitles()
  }
  
  func clearSearchResultsAndReloadTable() {
    titles.removeAll()
    table.reloadData()
  }
  
  func doSearch(text: String?) {
    defer {
      table.reloadData()
    }
    guard let text = text else {
      titles = Books.main.allTitles()
      return
    }
    titles = Books.main.getSearchResults(fromString: text)
  }
}

extension SearchResultsVC: UITableViewDelegate {
  
}

extension SearchResultsVC: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return titles.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "id", for: indexPath)
    let title: String = titles[indexPath.item]
    cell.textLabel?.text = title
    return cell
  }
}

extension SearchResultsVC: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    let searchText = searchController.searchBar.text!
    if searchText.isEmpty {
      titles = Books.main.allTitles()
    } else {
      titles = Books.main.getSearchResults(fromString: searchText)
    }

    DispatchQueue.main.async { [weak self] in
      guard let self = self else { return }
      self.table.reloadData()
    }
  }
}
