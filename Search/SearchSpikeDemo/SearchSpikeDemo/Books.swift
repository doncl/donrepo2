//
//  Books.swift
//  SearchSpikeDemo
//
//  Created by Don Clore on 7/18/21.
//

import Foundation

struct Books {
  static let main = Books()
  
  private let trie = Trie<String>()
  private var titles: [String] = []
  
  private init() {
    let bookList = getResourceData(named: "booklist", withExtension: "txt")
    titles = bookList.split(separator: "\n").map({ String($0)})
    for title in titles {
      trie.insert(title)
    }
  }
  
  private func getResourceData(named name: String, withExtension ext: String) -> String {
    let bundle = Bundle.main
    guard let url = bundle.url(forResource: name, withExtension: ext) else {
      fatalError("Cannot proceed")
    }
  
    guard let s = try? String(contentsOf: url) else {
      fatalError("Cannot proceed")
    }
    return s
  }
  
  func allTitles() -> [String] {
    return titles
  }
  
  func getSearchResults(fromString searchString: String) -> [String] {
    return trie.collections(startingWith: searchString)
  }
}
