//
//  ViewController.swift
//  Project3
//
//  Created by Don Clore on 2/29/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet var textField: UITextField!
  @IBOutlet var textFieldTip: UILabel!
  @IBOutlet var nextButton: UIButton!
  
  var focusGuide = UIFocusGuide()
  
  override var preferredFocusEnvironments: [UIFocusEnvironment] {
    return [textField]
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.addLayoutGuide(focusGuide)
    NSLayoutConstraint.activate([
      focusGuide.topAnchor.constraint(equalTo: textField.bottomAnchor),
      focusGuide.widthAnchor.constraint(equalTo: view.widthAnchor),
      focusGuide.heightAnchor.constraint(equalToConstant: 1),
    ])
    focusGuide.preferredFocusEnvironments = [nextButton]
    restoresFocusAfterTransition = false
  }

  override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
    super.didUpdateFocus(in: context, with: coordinator)
    
    if context.nextFocusedView == textField {
      focusGuide.preferredFocusEnvironments = [nextButton]
      coordinator.addCoordinatedAnimations({
        self.textFieldTip.alpha = 1
      })
    } else if context.nextFocusedView == nextButton {
      focusGuide.preferredFocusEnvironments = [textField]
      coordinator.addCoordinatedAnimations({
        self.textFieldTip.alpha = 0
      })
    }
    
    if context.nextFocusedView == textField {
      coordinator.addCoordinatedAnimations({
        self.textFieldTip.alpha = 1
      })
    } else if context.nextFocusedView == nextButton {
      coordinator.addCoordinatedAnimations({
        self.textFieldTip.alpha = 0
      })
    }

  }
  
  @IBAction func showAlert(_ sender: UIButton) {
    let ac = UIAlertController(title: "Hello", message: nil, preferredStyle: .alert)
    ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    present(ac, animated: true)
  }
  
}

// MARK: Behaviors
extension ViewController {
  
}
