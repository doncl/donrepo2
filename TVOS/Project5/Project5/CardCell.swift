//
//  CardCell.swift
//  Project5
//
//  Created by Don Clore on 2/29/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class CardCell: UICollectionViewCell {
  @IBOutlet var card: UIImageView!
  @IBOutlet var contents: UIImageView!  
  @IBOutlet var textLabel: UILabel!
  
  var word = "?"
  
}


extension CardCell {
  func flip(to image: String, hideContents: Bool) {
    UIView.transition(with: self, duration: 0.5, options: [.transitionFlipFromLeft], animations: {
      self.card.image = UIImage(named: image)
      self.contents.isHidden = hideContents
      self.textLabel.isHidden = hideContents
    }, completion: nil)
  }
}
