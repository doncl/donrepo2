//
//  ViewController.swift
//  Project5
//
//  Created by Don Clore on 2/29/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

  @IBOutlet var language: UISegmentedControl!
  @IBOutlet var words: UISegmentedControl!
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
      guard let vc = segue.destination as? GameViewController else { return }
      vc.targetLanguage = language.titleForSegment(at: language.selectedSegmentIndex)!.lowercased()
      vc.wordType = words.titleForSegment(at: words.selectedSegmentIndex)!.lowercased()
  }
}

