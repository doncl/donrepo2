//
//  City.swift
//  Project7
//
//  Created by Don Clore on 3/1/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation
import CoreLocation

struct City {
  var name: String
  var country: String
  var coordinates: CLLocationCoordinate2D
  
  var formattedName: String {
    return "\(name) (\(country))"
  }
}

extension City: Comparable {
  static func <(lhs: City, rhs: City) -> Bool {
    return lhs.name < rhs.name
  }
  
  static func ==(lhs: City, rhs: City) -> Bool {
    return lhs.name == rhs.name
  }
}

extension City {
  func matches(_ text: String) -> Bool {
    return name.localizedCaseInsensitiveContains(text) ||
      country.localizedCaseInsensitiveContains(text)
  }
}
