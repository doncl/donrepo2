//
//  NewsCell.swift
//  Project4
//
//  Created by Don Clore on 2/29/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class NewsCell: UICollectionViewCell {
  @IBOutlet var textLabel: UILabel!  
  @IBOutlet var unfocusedConstraint: NSLayoutConstraint!
  @IBOutlet var imageView: RemoteImageView!
  
  var focusedConstraint: NSLayoutConstraint!
  
  override func awakeFromNib() {
    focusedConstraint = textLabel.topAnchor.constraint(equalTo: imageView.focusedFrameGuide.bottomAnchor, constant: 15)
  }
  
  override func updateConstraints() {
    super.updateConstraints()
    
    focusedConstraint.isActive = isFocused
    unfocusedConstraint.isActive = !isFocused
  }
  
  override func didUpdateFocus(in context: UIFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator) {
    super.didUpdateFocus(in: context, with: coordinator)
    setNeedsUpdateConstraints()
    
    coordinator.addCoordinatedAnimations({
      self.layoutIfNeeded()
    }, completion: nil)
  }
}
