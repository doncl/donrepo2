//
//  ViewController.swift
//  Project4
//
//  Created by Don Clore on 2/29/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController {
  let apiKey: String = "4dff43fb-4932-4fea-b11a-c01fea14e12c"
  var articles: [JSON] = [JSON]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    guard let title = title else { return }
    guard let url = URL(string: "https://content.guardianapis.com/\(title.lowercased())?api-key=\(apiKey)&show-fields=thumbnail,headline,standfirst,body") else {
      return
    }
    DispatchQueue.global(qos: .userInteractive).async {
      self.fetch(url)
    }
  }

  
}

extension ViewController {
  override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return articles.count
  }
  
  override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? NewsCell else {
      fatalError("Couldn't dequeue a cell")
    }
    let newsItem = articles[indexPath.row]
    let title = newsItem["fields"]["headline"].stringValue
    let thumbnail = newsItem["fields"]["thumbnail"].stringValue
    if let imageURL = URL(string: thumbnail) {
      cell.imageView.load(imageURL)
    }
    
    cell.textLabel.text = title
    
    return cell 
  }
  
  override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let reader = storyboard?.instantiateViewController(identifier: "Reader") as? ReaderViewController else {
      return
    }
    reader.article = articles[indexPath.row]
    present(reader, animated: true)
  }
}

// MARK: Hitting the net.
extension ViewController {
  func fetch(_ url: URL) {
    if let data = try? Data(contentsOf: url) {
      articles = JSON(data)["response"]["results"].arrayValue
      
      DispatchQueue.main.async {
        self.collectionView?.reloadData()
      }
    }
  }
}

extension ViewController: UISearchResultsUpdating {
  func updateSearchResults(for searchController: UISearchController) {
    guard let text = searchController.searchBar.text else {
      return
    }
    if text.isEmpty {
      articles = [JSON]()
      collectionView?.reloadData()
    } else {
      guard let url = URL(string: "https://content.guardianapis.com/search?api-key=\(apiKey)&q=\(text)&show-fields=thumbnail,headline,standfirst,body") else {
        return
      }
      DispatchQueue.global(qos: .userInteractive).async {
        self.fetch(url)
      }
    }
  }
}
