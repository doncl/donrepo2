//
//  ContentProvider.swift
//  TopShelf
//
//  Created by Don Clore on 3/1/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import TVServices

class ContentProvider: TVTopShelfContentProvider {

  override func loadTopShelfContent(completionHandler: @escaping (TVTopShelfContent?) -> Void) {
      // Fetch content and call completionHandler
      completionHandler(nil);
  }

}

