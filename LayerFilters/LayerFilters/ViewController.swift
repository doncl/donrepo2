//
//  ViewController.swift
//  LayerFilters
//
//  Created by Don Clore on 8/22/21.
//

import UIKit

class ViewController: UIViewController {
	struct Constants {
		static let initialFontHeight: CGFloat = 100.0
	}

	let menuBtn = UIButton(type: .custom)
	let textLayer = CATextLayer()
	var textImage: CGImage?
	let imgView: UIImageView = UIImageView()
	var oldRC: CGRect = .zero

	override func viewDidLoad() {
		super.viewDidLoad()
		
		view.backgroundColor = .white
		
		setTextLayerString()
		textLayer.shouldRasterize = true
		textLayer.rasterizationScale = UIScreen.main.scale
		textLayer.backgroundColor = UIColor.clear.cgColor
		textLayer.alignmentMode = CATextLayerAlignmentMode.center
		
		menuBtn.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(menuBtn)
		
		NSLayoutConstraint.activate([
			menuBtn.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
			menuBtn.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
			menuBtn.widthAnchor.constraint(equalToConstant: 150),
			menuBtn.heightAnchor.constraint(equalToConstant: 60),
		])
		
		menuBtn.setTitleColor(.black, for: .normal)
		menuBtn.setTitle("FILTERS", for: .normal)
		
		let interaction = UIContextMenuInteraction(delegate: self)
		menuBtn.addInteraction(interaction)
	}


	private func makeImageFromLayer() {
		setTextLayerString()
		let size = textLayer.bounds.size
		let render = UIGraphicsImageRenderer(size: size)
		let uiimage = render.image { [weak self] context in
			guard let self = self else { return }
			return self.textLayer.render(in: context.cgContext)
		}
		textImage = uiimage.cgImage
		self.imgView.image = uiimage
	}
	
	private func setTextLayerString() {
		let para = NSMutableParagraphStyle()
		para.alignment = .center
		
		let fixedFont = UIFont.monospacedSystemFont(ofSize: Constants.initialFontHeight, weight: UIFont.Weight.heavy)
		let attrStr = NSAttributedString(string: "TYPEWRITER EFFECT", attributes: [
			NSAttributedString.Key.paragraphStyle: para,
			NSAttributedString.Key.font: fixedFont,
			NSAttributedString.Key.foregroundColor: UIColor.darkGray,
		])
		textLayer.string = attrStr
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		
		guard let ns = textLayer.string as? NSAttributedString else {
			return
		}
		let size = ns.size()
		
		let rc = CGRect(x: view.bounds.midX - size.width / 2, y: view.bounds.midY - size.height / 2,
						width: size.width, height: size.height)
		
		textLayer.frame = rc
		
		if rc != oldRC {
			makeImageFromLayer()
			if self.imgView.superview == nil {
				self.view.addSubview(self.imgView)
			}
			self.imgView.frame = rc
			oldRC = rc
		}
	}
		
	
	func sepiaFilter(_ input: CIImage, intensity: Double) -> CIImage? {
		let sepiaFilter = CIFilter(name:"CISepiaTone")
		sepiaFilter?.setValue(input, forKey: kCIInputImageKey)
		sepiaFilter?.setValue(intensity, forKey: kCIInputIntensityKey)
		return sepiaFilter?.outputImage
	}
	
	func bokehFilter(_ input: CIImage) -> CIImage? {
		let bokehFilter = CIFilter(name: "CIBokehBlur")
		bokehFilter?.setValue(input, forKey: kCIInputImageKey)
		bokehFilter?.setValue(20, forKey: kCIInputRadiusKey)
		bokehFilter?.setValue(10, forKey: "inputRingAmount")
		bokehFilter?.setValue(2, forKey: "inputRingSize")
		bokehFilter?.setValue(0, forKey: "inputSoftness")
		return bokehFilter?.outputImage
	}
}

extension ViewController: UIContextMenuInteractionDelegate {
	public func contextMenuInteraction(_ interaction: UIContextMenuInteraction, configurationForMenuAtLocation location: CGPoint)
		-> UIContextMenuConfiguration? {
	
		return UIContextMenuConfiguration(identifier: nil, previewProvider: nil) { [weak self] _ in
			guard let self = self else { return nil }
			return self.makeFilterMenu()
		}
	}
	
	func makeFilterMenu() -> UIMenu {
		let none = UIAction(title: "No filters", image: nil, identifier: UIAction.Identifier("0"), handler: noFilters)
		let sepia = UIAction(title: "Sepia", image: nil, identifier: UIAction.Identifier("1"), handler: sepiaAction)
		let bokeh = UIAction(title: "Bokeh", image: nil, identifier: UIAction.Identifier("2"), handler: bokehAction)
		
		return UIMenu(title: "Filter", image: nil, options: .displayInline, children: [none, sepia, bokeh])
	}
	
	func renderFromCIImage(ciImg: CIImage, size: CGSize) -> UIImage? {
		let rc = CGRect(origin: .zero, size: size)

		UIGraphicsBeginImageContextWithOptions(rc.size, false, UIScreen.main.scale)
		defer {
			UIGraphicsEndImageContext()
		}
		
		UIImage(ciImage: ciImg).draw(in: rc)
		
		let im = UIGraphicsGetImageFromCurrentImageContext()
		
		return im
	}
	
	func noFilters(from action: UIAction) {
		makeImageFromLayer()
	}
		
	func sepiaAction(from action: UIAction) {
		makeImageFromLayer()
		if let cgImage = self.textImage {
			let ciImage = CIImage(cgImage: cgImage)
			if let filteredCIImage = self.sepiaFilter(ciImage, intensity: 0.9) {
				useCIImage(filteredCIImage)
			}
		}
	}
	
	func bokehAction(from action: UIAction) {
		makeImageFromLayer()
		if let cgImage = self.textImage {
			let ciImage = CIImage(cgImage: cgImage)
			if let filteredCIImage = self.bokehFilter(ciImage) {
				useCIImage(filteredCIImage)
			}
		}
	}
	
	private func useCIImage(_ filteredCIImage: CIImage) {
		makeImageFromLayer()
		if let newUIImage = renderFromCIImage(ciImg: filteredCIImage, size: imgView.bounds.size) {
			self.textImage = newUIImage.cgImage
			self.imgView.image = newUIImage
			self.imgView.setNeedsDisplay()
		}
	}
}
