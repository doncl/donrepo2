//
//  ViewController.swift
//  CropMe
//
//  Created by Don Clore on 6/3/22.
//

import UIKit
import AVKit
import AVFoundation
import CoreImage.CIFilterBuiltins
import CoreImage

class ViewController: UIViewController {
  let uncroppedVideoURL: URL = Bundle.main.url(forResource: "video0", withExtension: "mp4")!
  private let tracksKey: String = "tracks"
  private let durationKey: String = "duration"
  
  let outputURL: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("result").appendingPathExtension("mp4")

  override func viewDidLoad() {
    super.viewDidLoad()
    
    assert(FileManager.default.fileExists(atPath: uncroppedVideoURL.path))

  }
  
  @IBAction func cropMeButtonTouched(_ sender: UIButton) {
    let asset: AVURLAsset = AVURLAsset(url: uncroppedVideoURL)
    
    deleteOldFile()
    
//    translateVideo(asset: asset)
    
//    cropToSquare(asset: asset)
    
    filterMe(asset: asset)
    
    
  }
  
  private func playVideo(asset: AVAsset) {
    let playerItem: AVPlayerItem = AVPlayerItem(asset: asset)
    
    playVideo(playerItem: playerItem)
  }
  
  private func playVideo(playerItem: AVPlayerItem) {
    let player: AVPlayer = AVPlayer(playerItem: playerItem)
    let playerLayer: AVPlayerLayer = AVPlayerLayer(player: player)
    
    view.layer.addSublayer(playerLayer)
    playerLayer.frame = view.layer.bounds
    
    player.play()

  }

}

extension ViewController {
  private func filterMe(asset: AVAsset) {
    let playerItem: AVPlayerItem = AVPlayerItem(asset: asset)
    let videoComposition = AVMutableVideoComposition(asset: playerItem.asset) { [weak self] request in
      guard let self = self else { return }
      
      let cropRect: CGRect = CGRect(x: 0, y: 0, width: 720, height: 1280)
      
      let sourceImage = request.sourceImage
//      let outputImage = sourceImage.cropped(to: cropRect)
      
//      let cropFilter = CIFilter(name: "CICrop")!
//      cropFilter.setValue(sourceImage, forKey: kCIInputImageKey)
//      cropFilter.setValue(CIVector(cgRect: cropRect), forKey: "inputRectangle")
//
//      guard let outputImage = cropFilter.outputImage else {
//        fatalError()
//      }
      
      let outputImage = sourceImage
      request.finish(with: outputImage, context: nil)
    }
    
    videoComposition.renderSize = CGSize(width: 720, height: 1280)
    videoComposition.renderScale = 1.0
    videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
    
    playerItem.videoComposition = videoComposition
    
    playVideo(playerItem: playerItem)
    
    

//    guard let export: AVAssetExportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetPassthrough) else {
//      return
//    }
//
//
//
//    export.videoComposition = videoComposition
//    export.outputURL = outputURL
//
//    export.outputFileType = AVFileType.mp4
//
//    export.shouldOptimizeForNetworkUse = true
//
//
//    export.exportAsynchronously{
//      DispatchQueue.main.async { [weak self] in
//        guard let self = self else { return }
//
//        if export.status == AVAssetExportSession.Status.completed {
//          let asset: AVAsset = AVURLAsset(url: self.outputURL)
//          self.playVideo(asset: asset)
//        } else {
//          if let error = export.error {
//            print("ERROR \(error)")
//          } else {
//            print("Unkown error")
//          }
//        }
//      }
//    }
  }
  
  private func cropToSquare(asset: AVAsset) {
    let composition: AVMutableComposition = AVMutableComposition()
    composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
    
    
    let sideLength: CGFloat = 720
    
    guard let assetVideoTrack = asset.tracks(withMediaType: .video).first else {
      return
    }
    
    

    let originalSize = assetVideoTrack.naturalSize
    
    let scale: CGFloat
    
    if originalSize.width < originalSize.height {
      scale = sideLength / originalSize.width
    } else {
      scale = sideLength / originalSize.height
    }
    
    let scaledSize: CGSize = CGSize(width: originalSize.width * scale, height: originalSize.height * scale)
    
    let topLeft: CGPoint = CGPoint(x: sideLength * 0.5 - scaledSize.width * 0.5, y: sideLength * 0.5 - scaledSize.width * 0.5)
    
    let layerInstruction: AVMutableVideoCompositionLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: assetVideoTrack)
    
    var orientationTransform: CGAffineTransform = assetVideoTrack.preferredTransform
    
    if orientationTransform.tx == originalSize.width || orientationTransform.tx == originalSize.height {
      orientationTransform.tx = sideLength
    }
    
    if orientationTransform.ty == originalSize.width || orientationTransform.ty == originalSize.height {
      orientationTransform.ty = sideLength
    }
    let scaleXForm: CGAffineTransform = CGAffineTransform(scaleX: scale, y: scale)
    let translation: CGAffineTransform = CGAffineTransform(translationX: topLeft.x, y: topLeft.y)
    let scaleTranslation: CGAffineTransform = scaleXForm.concatenating(translation)
    let transform: CGAffineTransform = scaleTranslation.concatenating(orientationTransform)
    
    layerInstruction.setTransform(transform, at: CMTime.zero)
    
    let instruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
    instruction.layerInstructions = [layerInstruction]
    instruction.timeRange = assetVideoTrack.timeRange
            
    let videoComposition: AVMutableVideoComposition = AVMutableVideoComposition()
    videoComposition.renderSize = CGSize(width: sideLength, height: sideLength)
    videoComposition.renderScale = 1.0
    videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
    
    videoComposition.instructions = [instruction]
    
    guard let export: AVAssetExportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPreset1280x720) else {
      fatalError()
    }
    export.videoComposition = videoComposition
    export.outputURL = outputURL
    export.outputFileType = AVFileType.mov
    export.shouldOptimizeForNetworkUse = true
    
    export.exportAsynchronously { [weak self] in
      guard let self = self else { return }

      if export.status == AVAssetExportSession.Status.completed {
        let asset: AVAsset = AVURLAsset(url: self.outputURL)
        self.playVideo(asset: asset)
      } else {
        if let error = export.error {
          print("ERROR \(error)")
        } else {
          print("Unkown error")
        }
      }
    }
    
  }
  
  private func deleteOldFile() {
    do {
      if FileManager.default.fileExists(atPath: outputURL.path) {
        try FileManager.default.removeItem(at: outputURL)
      }
    } catch {
      print("\(#function) - error \(error)")
    }
  }
  
  private func translateVideo(asset: AVAsset) {
    guard let assetVideoTrack = asset.tracks(withMediaType: .video).first else {
      return
    }
    
    guard let assetAudioTrack = asset.tracks(withMediaType: .audio).first else {
      return
    }
    
    let originalSize: CGSize = assetVideoTrack.naturalSize
    
    print("\(#function) - originalSize = \(originalSize)")
    
    let composition: AVMutableComposition = AVMutableComposition()
    composition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
    composition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
    
    let compositionVideoTrack = composition.tracks(withMediaType: .video).first!
    let compositionAudioTrack = composition.tracks(withMediaType: .audio).first!
    
    
    let timeRange: CMTimeRange = CMTimeRange(start: CMTime.zero, duration: asset.duration)
    
    do {
      try compositionVideoTrack.insertTimeRange(timeRange, of: assetVideoTrack, at: CMTime.zero)
      try compositionAudioTrack.insertTimeRange(timeRange, of: assetAudioTrack, at: CMTime.zero)
    } catch {
      print("\(#function) - error \(error)")
      return
    }
        
    let layerInstruction: AVMutableVideoCompositionLayerInstruction = AVMutableVideoCompositionLayerInstruction(assetTrack: compositionVideoTrack)
    
    layerInstruction.setTransform(assetVideoTrack.preferredTransform, at: CMTime.zero)
    
    let x: CGFloat = 0
    let y: CGFloat = 960
    let width: CGFloat = 720
    let height: CGFloat = 1280
    
    let cropRectangle: CGRect = CGRect(x: x, y: y, width: width, height: height)
    
    layerInstruction.setCropRectangle(cropRectangle, at: CMTime.zero)
    let instruction: AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
    
    instruction.layerInstructions = [layerInstruction]
    instruction.timeRange = assetVideoTrack.timeRange
      
    let videoComposition = AVMutableVideoComposition(propertiesOf: composition)
    videoComposition.renderSize = CGSize(width: 720, height: 1280)
    videoComposition.renderScale = 1.0
    videoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
    videoComposition.instructions = [instruction]
    
    guard let export: AVAssetExportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetPassthrough) else {
      return
    }
    
    export.videoComposition = videoComposition
    export.outputURL = outputURL
    
    export.outputFileType = AVFileType.mp4
    
    export.shouldOptimizeForNetworkUse = true
    
    
    let valid = videoComposition.isValid(for: asset, timeRange: assetVideoTrack.timeRange, validationDelegate: self)
    assert(valid)
    
    export.exportAsynchronously{
      DispatchQueue.main.async { [weak self] in
        guard let self = self else { return }
        
        if export.status == AVAssetExportSession.Status.completed {
          let asset: AVAsset = AVURLAsset(url: self.outputURL)
          self.playVideo(asset: asset)
        } else {
          if let error = export.error {
            print("ERROR \(error)")
          } else {
            print("Unkown error")
          }
        }
      }
    }
  
  }
}

extension ViewController: AVVideoCompositionValidationHandling {

  func videoComposition(_ videoComposition: AVVideoComposition, shouldContinueValidatingAfterFindingInvalidValueForKey key: String) -> Bool {
    return true
  }

    
  func videoComposition(_ videoComposition: AVVideoComposition, shouldContinueValidatingAfterFindingEmptyTimeRange timeRange: CMTimeRange) -> Bool {
    return true
  }

    
  func videoComposition(_ videoComposition: AVVideoComposition,
                        shouldContinueValidatingAfterFindingInvalidTimeRangeIn videoCompositionInstruction: AVVideoCompositionInstructionProtocol) -> Bool {
    return true
  }

    
  func videoComposition(_ videoComposition: AVVideoComposition,
                        shouldContinueValidatingAfterFindingInvalidTrackIDIn videoCompositionInstruction: AVVideoCompositionInstructionProtocol,
                        layerInstruction: AVVideoCompositionLayerInstruction, asset: AVAsset) -> Bool {
        
    return true
  }

}

