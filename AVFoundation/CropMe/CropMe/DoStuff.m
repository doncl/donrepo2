//
//  DoStuff.m
//  CropMe
//
//  Created by Don Clore on 6/3/22.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIkit/UIKit.h>

void doStuff(void);

void doStuff(void) {
  NSURL *outputFileURL = [[[[NSFileManager defaultManager] URLsForDirectory: NSDocumentDirectory inDomains:NSUserDomainMask][0] URLByAppendingPathComponent:@"output"] URLByAppendingPathExtension: @"mp4"];
  
  // output file
  NSString* docFolder = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
  NSString* outputPath = [docFolder stringByAppendingPathComponent:@"output2.mov"];
  if ([[NSFileManager defaultManager] fileExistsAtPath:outputPath])
      [[NSFileManager defaultManager] removeItemAtPath:outputPath error:nil];

  // input file
  AVAsset* asset = [AVAsset assetWithURL:outputFileURL];

  AVMutableComposition *composition = [AVMutableComposition composition];
  [composition  addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];

  // input clip
  AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];

  CGSize videoSize = CGSizeMake(720, 1280);


  AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
  videoComposition.renderSize = videoSize;
  videoComposition.frameDuration = CMTimeMake(1, 30);

  AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
  instruction.timeRange = CMTimeRangeMake(kCMTimeZero, CMTimeMakeWithSeconds(60, 30) );

  // rotate and position video
  AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];



  [transformer setTransform:videoTrack.preferredTransform atTime:kCMTimeZero];
  instruction.layerInstructions = [NSArray arrayWithObject: transformer];
  videoComposition.instructions = [NSArray arrayWithObject: instruction];

  // export
  AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality] ;
  exporter.videoComposition = videoComposition;
  exporter.outputURL=[NSURL fileURLWithPath:outputPath];
  exporter.outputFileType=AVFileTypeQuickTimeMovie;

  [exporter exportAsynchronouslyWithCompletionHandler:^(void){
      NSLog(@"Exporting done!");
  }];
}
