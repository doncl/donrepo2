//
//  MovieWriter.swift
//  CameraFun
//
//  Created by Don Clore on 4/21/22.
//

import UIKit
import AVFoundation
import Combine

protocol MovieWriterDelegate: AnyObject {
  func getWindowOrientation() -> UIInterfaceOrientation
  func didWriteMovie(atURL url: URL)
}

class MovieWriter: NSObject {
  let bitePosition: Int
  let viddlID: String
  let videoSettings: [String: Any]
  let audioSettings: [String: Any]
  let dispatchQueue: DispatchQueue
  
  var assetWriter: AVAssetWriter?
  var assetWriterVideoInput: AVAssetWriterInput?
  var assetWriterAudioInput: AVAssetWriterInput?
  var assetWriterInputPixelBufferAdaptor: AVAssetWriterInputPixelBufferAdaptor?
  var ciContext: CIContext = CIContext()
  let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceRGB()
  var activeFilter: CIFilter? = nil
  var firstSample: Bool = false
  var isWriting: Bool = false
  
  weak var delegate: MovieWriterDelegate?
  
  var outputURL: URL {
    //return VideoFilesManager.userAudioLocalFileURL(forIndex: bitePosition, viddlID: viddlID)
    let docDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let url = docDir.appendingPathComponent("movieWriterOutput.mp4")
    return url
  }

  init(bitePosition: Int, viddlID: String, videoSettings: [String: Any], audioSettings: [String: Any], dispatchQueue: DispatchQueue) {
    self.bitePosition = bitePosition
    self.viddlID = viddlID
    self.videoSettings = videoSettings
    self.audioSettings = audioSettings
    self.dispatchQueue = dispatchQueue
    
    super.init()
  }
  
  // TODO: Not in use yet.
  @objc func filterChanged(_ note: Notification) {
    if let filter = note.object as? CIFilter {
      activeFilter = filter
    }
  }
  
  func startWriting()  {
    dispatchQueue.async {
      
      let fileType: AVFileType = AVFileType.mp4
                  
      do {
        if FileManager.default.fileExists(atPath: self.outputURL.path) {
          try FileManager.default.removeItem(at: self.outputURL)
        }
        let assetWriter = try AVAssetWriter(outputURL: self.outputURL, fileType: fileType)
        self.assetWriter = assetWriter
                
        let videoInputWriter: AVAssetWriterInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: self.videoSettings)
        
        self.assetWriterVideoInput = videoInputWriter
        videoInputWriter.expectsMediaDataInRealTime = true
                
        let attributes: [String: Any] = [
          String(kCVPixelBufferPixelFormatTypeKey): kCVPixelFormatType_32BGRA,
          String(kCVPixelBufferWidthKey): self.videoSettings[AVVideoWidthKey] as Any,
          String(kCVPixelBufferHeightKey): self.videoSettings[AVVideoHeightKey] as Any,
        ]
        
        self.assetWriterInputPixelBufferAdaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: videoInputWriter, sourcePixelBufferAttributes: attributes)
        
        if assetWriter.canAdd(videoInputWriter) {
          assetWriter.add(videoInputWriter)
        } else {
          AppLogger.cameraLog.log("\(#function) - unable to add videoInputWriter input", isPrivate: false, at: .error)
          return
        }
        
        let audioInput: AVAssetWriterInput = AVAssetWriterInput(mediaType: AVMediaType.audio, outputSettings: self.audioSettings)
        self.assetWriterAudioInput = audioInput
        
        if assetWriter.canAdd(audioInput) {          assetWriter.add(audioInput)
        } else {
          AppLogger.cameraLog.log("\(#function) - unable to add audioInputWriter input", isPrivate: false, at: .error)
          return
        }
        
        self.isWriting = true
        self.firstSample = true
        
      } catch {
        AppLogger.cameraLog.log("\(#function) - error = \(error)", isPrivate: false, at: .error)
      }
    }
  }
  
  func process(sampleBuffer: CMSampleBuffer) {
    guard isWriting else {
      return
    }
    
    guard let assetWriterAudioInput = assetWriterAudioInput,
          let assetWriterVideoInput = assetWriterVideoInput,
          let assetWriter = assetWriter,
          let assetWriterInputPixelBufferAdaptor = assetWriterInputPixelBufferAdaptor else {
      
      return
    }
    
    guard let formatDesc: CMFormatDescription = CMSampleBufferGetFormatDescription(sampleBuffer) else {
      return
    }
    let mediaType: CMMediaType = CMFormatDescriptionGetMediaType(formatDesc)
    
    if mediaType == kCMMediaType_Video {
      let timeStamp: CMTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer)
      
      if firstSample {
        if assetWriter.startWriting() {
          assetWriter.startSession(atSourceTime: timeStamp)
        } else {
          AppLogger.cameraLog.log("\(#function) - failed to start writing", isPrivate: false, at: .error)
        }
        firstSample = false
      }
      
      var outputRenderBuffer: CVPixelBuffer? = nil
      
      guard let pixelBufferPool: CVPixelBufferPool = assetWriterInputPixelBufferAdaptor.pixelBufferPool else {
        AppLogger.cameraLog.log("\(#function) - failed to get pixelBufferPool", isPrivate: false, at: .error)
        return
      }
      
      let ret: CVReturn = CVPixelBufferPoolCreatePixelBuffer(nil, pixelBufferPool, &outputRenderBuffer)
      
      guard let outputRenderBuffer = outputRenderBuffer, ret == kCVReturnSuccess else {
        AppLogger.cameraLog.log("\(#function) - failed to create outputRenderBuffer", isPrivate: false, at: .error)
        return
      }
      
      guard let imageBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
        AppLogger.cameraLog.log("\(#function) - failed to get imageBuffer from sampleBuffer", isPrivate: false, at: .error)
        return
      }
      
      let filteredImage: CIImage
      
      let sourceImage: CIImage = CIImage(cvPixelBuffer: imageBuffer)
      
      if let filter = activeFilter {
        filter.setValue(sourceImage, forKey: kCIInputImageKey)
        if let img = filter.outputImage {
          filteredImage = img
        } else {
          filteredImage = sourceImage
        }
      } else {
        filteredImage = sourceImage
      }
            
      ciContext.render(filteredImage, to: outputRenderBuffer, bounds: filteredImage.extent, colorSpace: colorSpace)
      
      if assetWriterVideoInput.isReadyForMoreMediaData {
        if !assetWriterInputPixelBufferAdaptor.append(outputRenderBuffer, withPresentationTime: timeStamp) {
          AppLogger.cameraLog.log("\(#function) - unable to append video buffer", isPrivate: false, at: .error)
        }
      }
    } else if !firstSample, mediaType == kCMMediaType_Audio {
      if assetWriterAudioInput.isReadyForMoreMediaData {
        if !assetWriterAudioInput.append(sampleBuffer) {
          AppLogger.cameraLog.log("\(#function) - unable to append audio buffer", isPrivate: false, at: .error)
        }
      }
    }
  }
  
  func stopWriting() {
    isWriting = false
    
    dispatchQueue.async { [weak self] in
      guard let self = self else { return }
      guard let assetWriter = self.assetWriter else {
        return
      }
      
      assetWriter.finishWriting() {
        let status: AVAssetWriter.Status = assetWriter.status
        if status == AVAssetWriter.Status.completed {
          DispatchQueue.main.async { [weak self] in
            guard let self = self, let delegate = self.delegate else {
              return
            }
          
            let fileURL = self.outputURL
            delegate.didWriteMovie(atURL: fileURL)
          }
        } else {
          if let error = assetWriter.error {
            AppLogger.cameraLog.log("\(#function) - failed to write movie, error = \(error)", isPrivate: false, at: .error)
          } else {
            AppLogger.cameraLog.log("\(#function) - failed to write movie, assetWriter status = \(status.rawValue)", isPrivate: false, at: .error)
          }
        }
      }
    }
  }
}
