//
//  CICameraCapture.swift
//
//  Created by Don Clore on 3/11/22.
//

import CoreImage
import AVFoundation
import UIKit

protocol CICameraCaptureDelegate: AnyObject {
  func couldntGetCamera()
  func cantAddBackInput()
  func cantAddFrontInput()
  func cantAddAudioInput()
  func cantAddVideoOutput()
  func cantAddAudioOutput()
  func cantGetRecommendedVideoSettings()
  func cantGetRecommendedAudioSettings()
  func getWindowOrientation() -> UIInterfaceOrientation
  func getTraitCollection() -> UITraitCollection
}

class CICameraCapture: NSObject {
  struct Constants {
    static let shortEdge: Int = 720
    static let longEdge: Int = 1280
  }
  
  typealias Callback = (CIImage?) -> ()

  var movieWriter: MovieWriter?
  var cameraPosition: AVCaptureDevice.Position
  var backInput: AVCaptureInput?
  var frontInput: AVCaptureInput?
  let callback: Callback
  let session = AVCaptureSession()
  var videoDataOutput: AVCaptureVideoDataOutput = AVCaptureVideoDataOutput()
  var audioDataOutput: AVCaptureAudioDataOutput = AVCaptureAudioDataOutput()
  var recording: Bool = false
  
  private let sampleBufferQueue = DispatchQueue(label: "sample-buffer-queue", qos: .userInitiated)

  weak var delegate: CICameraCaptureDelegate?
  
  var bitePosition: Int = 0
  var viddlID: String = "abcdef"

  init(cameraPosition: AVCaptureDevice.Position, callback: @escaping Callback) {
    self.cameraPosition = cameraPosition
    self.callback = callback

    super.init()
  }

  func start() {
    setSession(orientation: .portrait)
    session.startRunning()
  }

  func stop() {
    session.stopRunning()
  }

  func flipCamera() {
    guard let backInput = backInput, let frontInput = frontInput else {
      return
    }

    session.beginConfiguration()

    if cameraPosition == AVCaptureDevice.Position.back {
      session.removeInput(backInput)
      session.addInput(frontInput)
      cameraPosition = AVCaptureDevice.Position.front
    } else {
      session.removeInput(frontInput)
      session.addInput(backInput)
      cameraPosition = AVCaptureDevice.Position.back
    }
    
    setSession(orientation: .portrait)
    
    session.commitConfiguration()
    
  }
  
  private func setSession(orientation: AVCaptureVideoOrientation) {
    assert(Thread.isMainThread)
    
    guard let delegate = delegate else {
      return
    }

    var initialVideoOrientation: AVCaptureVideoOrientation = AVCaptureVideoOrientation.portrait
    let windowOrientation: UIInterfaceOrientation = delegate.getWindowOrientation()
    if windowOrientation != .unknown {
      if let videoOrientation = AVCaptureVideoOrientation(interfaceOrientation: windowOrientation) {
        initialVideoOrientation = videoOrientation
      }
    }
    
    let connections = session.connections
    for connection in connections {
      if connection.isVideoOrientationSupported {
        connection.videoOrientation = initialVideoOrientation
      }
      
      if cameraPosition == AVCaptureDevice.Position.front {
        if connection.isVideoMirroringSupported {
          connection.isVideoMirrored = true
        }
      }
    }
  }
}

extension CICameraCapture {
  func prepareSession() {
    session.sessionPreset = .hd1280x720
    
    guard setupAudio(session: session) else {
      AppLogger.cameraLog.log("\(#function) - no audio device", at: .error)
      delegate?.cantAddAudioInput()
      return
    }

    guard let bInput = getInput(forPosition: .back), let fInput = getInput(forPosition: .front) else {
      return
    }

    guard session.canAddInput(bInput) else {
      delegate?.cantAddBackInput()
      return
    }

    guard session.canAddInput(fInput) else {
      delegate?.cantAddFrontInput()
      return
    }

    backInput = bInput
    frontInput = fInput
    
    switch cameraPosition {
      case .back:
        session.addInput(bInput)
      case .front, .unspecified:
        fallthrough
      @unknown default:
        session.addInput(fInput)
    }
    

    videoDataOutput = AVCaptureVideoDataOutput()
    let videoSettings: [String: Any] = [
      String(kCVPixelBufferPixelFormatTypeKey): NSNumber(value: kCVPixelFormatType_32ARGB),
    ]
    
    videoDataOutput.videoSettings = videoSettings
    videoDataOutput.alwaysDiscardsLateVideoFrames = false

    videoDataOutput.setSampleBufferDelegate(self, queue: sampleBufferQueue)
    
    if session.canAddOutput(videoDataOutput) {
      session.addOutput(videoDataOutput)
    } else {
      AppLogger.cameraLog.log("\(#function) cannot add video output", at: .error)
      delegate?.cantAddVideoOutput()
      return
    }

    audioDataOutput = AVCaptureAudioDataOutput()
    audioDataOutput.setSampleBufferDelegate(self, queue: sampleBufferQueue)

    if session.canAddOutput(audioDataOutput) {
      session.addOutput(audioDataOutput)
    } else {
      AppLogger.cameraLog.log("\(#function) cannot add audio output", at: .error)
      delegate?.cantAddAudioOutput()
    }
    
    setupMovieWriter()
  }
  
  private func setupMovieWriter() {
    let fileType: AVFileType = AVFileType.mp4
    guard var videoSettings: [String: Any] = videoDataOutput.recommendedVideoSettingsForAssetWriter(writingTo: fileType) else {
      AppLogger.cameraLog.log("\(#function) cannot add recommended video settings for fileType \(fileType)", at: .error)
      delegate?.cantGetRecommendedVideoSettings()
      return
    }
    
    guard let audioSettings: [String: Any] = audioDataOutput.recommendedAudioSettingsForAssetWriter(writingTo: fileType) else {
      AppLogger.cameraLog.log("\(#function) cannot add recommended audio settings for fileType \(fileType)", at: .error)
      delegate?.cantGetRecommendedAudioSettings()
      return
    }
    
    videoSettings[AVVideoWidthKey] = NSNumber(integerLiteral: Constants.shortEdge)
    videoSettings[AVVideoHeightKey] = NSNumber(integerLiteral: Constants.longEdge)
    
    if let delegate = delegate {
      
      // N.B. it's pretty unclear exactly what we want to do for iPad, and this code will have to be adjusted accordingly.
      let traitCollection = delegate.getTraitCollection()
      if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular {
        videoSettings[AVVideoWidthKey] = NSNumber(integerLiteral: Constants.longEdge)
        videoSettings[AVVideoHeightKey] = NSNumber(integerLiteral: Constants.shortEdge)
      }
    }
    
    movieWriter = MovieWriter(bitePosition: bitePosition,
                              viddlID: viddlID,
                              videoSettings: videoSettings,
                              audioSettings: audioSettings,
                              dispatchQueue: sampleBufferQueue)
    
    movieWriter?.delegate = self
  }

  private func getInput(forPosition position: AVCaptureDevice.Position) -> AVCaptureInput? {
    let cameraDiscovery = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera, .builtInWideAngleCamera], mediaType: .video, position: position)
    guard let camera = cameraDiscovery.devices.first, let input = try? AVCaptureDeviceInput(device: camera) else {
      delegate?.couldntGetCamera()
      return nil
    }
    return input
  }

  private func nonThrowingDeviceInput(device: AVCaptureDevice) -> AVCaptureDeviceInput? {
    do {
      let input = try AVCaptureDeviceInput(device: device)
      return input
    } catch {
      AppLogger.cameraLog.log("\(#function) cannot add input", isPrivate: false, at: .error)
      return nil
    }
  }
  
  @discardableResult
  private func setupAudio(session: AVCaptureSession) -> Bool {
    guard let audioDevice: AVCaptureDevice = AVCaptureDevice.default(for: AVMediaType.audio) else {
      return false
    }
    
    guard let audioInput = nonThrowingDeviceInput(device: audioDevice) else {
      return false
    }
    
    if session.canAddInput(audioInput) {
      session.addInput(audioInput)
    } else {
      AppLogger.cameraLog.log("\(#function) cannot add audio Input", isPrivate: false, at: .error)
      return false
    }
    return true
  }
}

// MARK: Recording Controls
extension CICameraCapture {
  func startRecording() {
    guard let movieWriter = movieWriter else {
      return
    }
    movieWriter.startWriting()
    recording = true
  }
  
  func stopRecording() {
    guard let movieWriter = movieWriter else {
      return
    }
    movieWriter.stopWriting()
    recording = false
  }
}


// MARK: AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate
extension CICameraCapture: AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate {
  func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
    if let movieWriter = movieWriter {
      movieWriter.process(sampleBuffer: sampleBuffer)
    }
    
    guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
      return
    }

    DispatchQueue.main.async {
      let image = CIImage(cvImageBuffer: imageBuffer)
      let rotationAngle: CGFloat = 0
      let xform = CGAffineTransform(rotationAngle: rotationAngle)
      self.callback(image.transformed(by: xform))
    }
  }
}

extension CICameraCapture: MovieWriterDelegate {
  func getWindowOrientation() -> UIInterfaceOrientation {
    guard let delegate = delegate else {
      return UIInterfaceOrientation.portrait
    }
    return delegate.getWindowOrientation()
  }
  
  func didWriteMovie(atURL url: URL) {
    AppLogger.cameraLog.log("\(#function) - url = \(url.absoluteString)")
  }
}


// MARK: AVCaptureVideoOrientation <- UIDeviceOrientation AND AVCaptureVideoOrientation <- UIDeviceOrientation
extension AVCaptureVideoOrientation {
  init?(deviceOrientation: UIDeviceOrientation) {
    switch deviceOrientation {
      case .portrait: self = .portrait
      case .portraitUpsideDown: self = .portraitUpsideDown
      case .landscapeLeft: self = .landscapeRight
      case .landscapeRight: self = .landscapeLeft
      default: return nil
    }
  }

  init?(interfaceOrientation: UIInterfaceOrientation) {
    switch interfaceOrientation {
      case .portrait: self = .portrait
      case .portraitUpsideDown: self = .portraitUpsideDown
      case .landscapeLeft: self = .landscapeLeft
      case .landscapeRight: self = .landscapeRight
      default: return nil
    }
  }
}
