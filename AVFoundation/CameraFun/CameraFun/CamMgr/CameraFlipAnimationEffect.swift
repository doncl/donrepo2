//
//  CameraFlipAnimationEffect.swift
//  CameraFun
//
//  Created by Don Clore on 4/21/22.
//

import Foundation
import UIKit 

protocol CameraFlipAnimationEffect: AnyObject {
  var view: UIView! { get }
  var cameraFlipAnimating: Bool { get set }
  var videoView: MetalRenderView? { get set }

  func doFlipAnimation(flipComplete: @escaping () -> ())
}

extension CameraFlipAnimationEffect {
  func doFlipAnimation(flipComplete: @escaping () -> ()) {
    guard let videoView = videoView else {
      return
    }

    guard !cameraFlipAnimating else {
      return
    }

    let blurEffect: UIBlurEffect = UIBlurEffect(style: .prominent)
    let tempView: UIVisualEffectView = UIVisualEffectView(effect: blurEffect)
    tempView.frame = videoView.bounds

    videoView.addSubview(tempView)
    videoView.bringSubviewToFront(tempView)

    guard let cameraTransitionView = videoView.snapshotView(afterScreenUpdates: true) else {
      return
    }

    view.insertSubview(cameraTransitionView, aboveSubview: videoView)

    cameraFlipAnimating = true

    videoView.alpha = 0

    DispatchQueue.main.async {
      UIView.transition(with: cameraTransitionView,
                        duration: 0.5,
                        options: UIView.AnimationOptions.transitionFlipFromLeft,
                        animations: {
                          tempView.alpha = 0
                        },
                        completion: { _ in
                          videoView.alpha = 1
                          UIView.animate(withDuration: 0.5, animations: {
                            cameraTransitionView.alpha = 0
                          }, completion: { _ in
                            self.cameraFlipAnimating = false
                            cameraTransitionView.removeFromSuperview()
                            tempView.removeFromSuperview()
                            flipComplete()
                          })
                        })
    }
  }
}
