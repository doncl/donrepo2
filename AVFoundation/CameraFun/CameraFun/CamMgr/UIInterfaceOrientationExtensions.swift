//
//  UIInterfaceOrientationExtensions.swift
//  CameraFun
//
//  Created by Don Clore on 4/21/22.
//

import UIKit

extension UIInterfaceOrientation {
  func getAffineRotationTransform() -> CGAffineTransform {
    let radians = getRotationInRadians()
    let xform = CGAffineTransform(rotationAngle: radians)
    return xform
  }

  func getRotationInRadians() -> CGFloat {
    let degrees = getRotationInDegrees()
    let radians = degrees * CGFloat.pi / -180
    return radians
  }

  func getRotationInDegrees() -> CGFloat {
    switch self {
      case .unknown:
        return 0
      case .portrait:
        return 0
      case .portraitUpsideDown:
        return 180
      case .landscapeLeft:
        return 90
      case .landscapeRight:
        return -90
      @unknown default:
        return 0
    }
  }
}
