//
//  MetalRenderView.swift
//  CameraFun
//
//  Created by Don Clore on 4/21/22.
//

import MetalKit
import CoreImage

protocol MetalRenderViewErrorReportingDelegate: AnyObject {
  func renderTaskFailed(withError error: Error)
}

class MetalRenderView: MTKView {
  weak var errorDelegate: MetalRenderViewErrorReportingDelegate?
  
  override init(frame frameRect: CGRect, device: MTLDevice?) {
    super.init(frame: frameRect, device: device)
    
    if super.device == nil {
      fatalError("\(#function) No support for Metal. Cannot continue.")
    }
    
    framebufferOnly = false
  }
  
  @available(*, unavailable)
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private lazy var commandQueue: MTLCommandQueue? = { [weak self] in
    guard let self = self else { return nil }
    guard let device = self.device else {
      return nil
    }
    return device.makeCommandQueue()
  }()
  
  lazy var ciContext: CIContext = { [weak self] in
    guard let self = self, let device = self.device else {
      fatalError("\(#function) - self is gone away, or device is nil.")
    }
    return CIContext(mtlDevice: device)
  }()
  
  var image: CIImage? {
    didSet {
      renderImage()
    }
  }
    
  private func renderImage() {
    guard let image = image,
          let errorDelegate = errorDelegate,
          let commandQueue = commandQueue,
          let commandBuffer = commandQueue.makeCommandBuffer(),
          let currentDrawable = currentDrawable else {
            
      return
    }
  
    let destination = CIRenderDestination(width: Int(drawableSize.width),
                                          height: Int(drawableSize.height),
                                          pixelFormat: .rgba8Unorm,
                                          commandBuffer: commandBuffer) { () -> MTLTexture in
      return currentDrawable.texture
    }
    
    do {
      try ciContext.startTask(toRender: image.transformToOrigin(withSize: drawableSize), to: destination)
    } catch let error {
      errorDelegate.renderTaskFailed(withError: error)
    }
    
    commandBuffer.present(currentDrawable)
    commandBuffer.commit()
    draw()
  }
}
