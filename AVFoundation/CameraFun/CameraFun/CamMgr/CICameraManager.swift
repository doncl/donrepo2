//
//  CICameraManager.swift
//  CameraFun
//
//  Created by Don Clore on 4/21/22.
//

import Foundation
import UIKit
import AVFoundation
import Combine

class CICameraManager: NSObject {
  var captureSession: AVCaptureSession?
  var activeVideoInput: AVCaptureDeviceInput?
  var videoDataOutput: AVCaptureVideoDataOutput = AVCaptureVideoDataOutput()
  var audioDataOutput: AVCaptureAudioDataOutput = AVCaptureAudioDataOutput()
  
  var outputURL: URL?
  
  let q: DispatchQueue = DispatchQueue(label: "cameramanager2-processing-queue")
  
  var canSwitchCameras: Bool {
    return cameraCount > 1
  }
  
  var activeCamera: AVCaptureDevice? {
    return activeVideoInput?.device
  }
  
  var inactiveCamera: AVCaptureDevice? {
    guard cameraCount > 1 else {
      return nil
    }
    guard let activeCamera = activeCamera else {
      return nil
    }

    if activeCamera.position == AVCaptureDevice.Position.back {
      return camera(withPosition: AVCaptureDevice.Position.front)
    } else {
      return camera(withPosition: AVCaptureDevice.Position.back)
    }
  }
  
  var cameraCount: Int {
    var count: Int = 0
    var session: AVCaptureDevice.DiscoverySession = frontCameraDiscoverySession()
    count += session.devices.count
    session = rearCameraDiscoverySession()
    count += session.devices.count
    return count
  }
  

  func setupSession() -> Bool {
    let sess = AVCaptureSession()
    captureSession = sess
    
    sess.sessionPreset = AVCaptureSession.Preset.hd1280x720

    var ret = setupSessionInputs(session: sess)
    if !ret {
      return ret
    }
    ret = setupSessionOutputs()
    return ret
  }
  
  private func setupSessionInputs(session: AVCaptureSession) -> Bool  {
    
    guard let videoDevice: AVCaptureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
      return false
    }
    
    guard let videoInput = nonThrowingDeviceInput(device: videoDevice) else {
      return false
    }
            
    if session.canAddInput(videoInput) {
      session.addInput(videoInput)
    } else {
      AppLogger.cameraLog.log("\(#function) cannot add video Input", isPrivate: false, at: .error)
      return false
    }
    
    guard let audioDevice: AVCaptureDevice = AVCaptureDevice.default(for: AVMediaType.audio) else {
      return false
    }
    
    guard let audioInput = nonThrowingDeviceInput(device: audioDevice) else {
      return false
    }
    
    if session.canAddInput(audioInput) {
      session.addInput(audioInput)
    } else {
      AppLogger.cameraLog.log("\(#function) cannot add audio Input", isPrivate: false, at: .error)
      return false
    }
  
    return true
  }
  
  private func nonThrowingDeviceInput(device: AVCaptureDevice) -> AVCaptureDeviceInput? {
    do {
      let input = try AVCaptureDeviceInput(device: device)
      return input
    } catch {
      AppLogger.cameraLog.log("\(#function) cannot add input", isPrivate: false, at: .error)
      return nil
    }
  }
  
  func setupSessionOutputs() -> Bool {
    guard let captureSession = captureSession else {
      return false
    }
    
    videoDataOutput = AVCaptureVideoDataOutput()
    
    let videoSettings: [String: Any] = [
      String(kCVPixelBufferPixelFormatTypeKey): NSNumber(value: kCVPixelFormatType_32ARGB),
    ]
    
    videoDataOutput.videoSettings = videoSettings
    videoDataOutput.alwaysDiscardsLateVideoFrames = false
    
    videoDataOutput.setSampleBufferDelegate(self, queue: q)
    
    if captureSession.canAddOutput(videoDataOutput) {
      captureSession.addOutput(videoDataOutput)
    } else {
      return false
    }
    
    audioDataOutput = AVCaptureAudioDataOutput()
    audioDataOutput.setSampleBufferDelegate(self, queue: q)
    
    if captureSession.canAddOutput(audioDataOutput) {
      captureSession.addOutput(audioDataOutput)
    } else {
      return false
    }
    
    let fileType: AVFileType = AVFileType.mp4
    guard let videoSettings: [String: Any] = videoDataOutput.recommendedVideoSettingsForAssetWriter(writingTo: fileType) else {
      return false
    }
    
    guard let audioSettings: [String: Any] = audioDataOutput.recommendedAudioSettingsForAssetWriter(writingTo: fileType) else {
      return false
    }
    
    _ = videoSettings
    _ = audioSettings
    
    // TODO:  Write the MovieWriter
    
    return false
  }
  
  func startSession() {
    
    q.async {
      guard let captureSession = self.captureSession else {
        return
      }
      if !captureSession.isRunning {
        captureSession.startRunning()
      }
    }
  }
  
  func stopSession() {
    q.async {
      guard let captureSession = self.captureSession else {
        return
      }

      if captureSession.isRunning {
        captureSession.stopRunning()
      }
    }
  }
}

// MARK: Device Configuration
extension CICameraManager {
  func camera(withPosition position: AVCaptureDevice.Position) -> AVCaptureDevice? {
    
    let session: AVCaptureDevice.DiscoverySession
    
    switch position {
      case .back:
        session = rearCameraDiscoverySession()
                
      case .front, .unspecified:
        session = frontCameraDiscoverySession()
        
      @unknown default:
        session = frontCameraDiscoverySession()
    }
    
    let devices = session.devices
    return devices.first
  }
  
  private func frontCameraDiscoverySession() -> AVCaptureDevice.DiscoverySession {
    let session: AVCaptureDevice.DiscoverySession =
      AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInTrueDepthCamera, .builtInWideAngleCamera],
                                       mediaType: AVMediaType.video,
                                       position: AVCaptureDevice.Position.front)

    return session
  }
  
  private func rearCameraDiscoverySession() -> AVCaptureDevice.DiscoverySession {
    let session: AVCaptureDevice.DiscoverySession =
      AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera, .builtInDualWideCamera, .builtInWideAngleCamera],
                                       mediaType: AVMediaType.video,
                                       position: AVCaptureDevice.Position.back)
    
    return session
  }
  
  func switchCameras() {
    guard canSwitchCameras, let captureSession = captureSession else {
      return
    }
    
    guard let videoDevice = inactiveCamera else {
      return
    }
    
    do {
      let videoInput = try AVCaptureDeviceInput(device: videoDevice)
      configure(session: captureSession, withVideoInput: videoInput)
    } catch {
      AppLogger.cameraLog.log("\(#function) failed error = \(error)", isPrivate: false, at: .error)
      return
    }
  }
  
  private func configure(session: AVCaptureSession, withVideoInput videoInput: AVCaptureDeviceInput) {
    session.beginConfiguration()
    defer {
      session.commitConfiguration()
    }
    
    if let activeVideoInput = activeVideoInput {
      session.removeInput(activeVideoInput)
    }

    if session.canAddInput(videoInput) {
      session.addInput(videoInput)
    } else {
      if let activeVideoInput = activeVideoInput {
        session.addInput(activeVideoInput)
      }
    }
  }
}

extension CICameraManager: AVCaptureVideoDataOutputSampleBufferDelegate, AVCaptureAudioDataOutputSampleBufferDelegate {
  func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
    
  }
}
