//
//  AppLogger.swift
//  CameraFun
//
//  Created by Don Clore on 4/21/22.
//

import Foundation
import os
import UIKit


enum AppLoggerError: Error, CustomStringConvertible {
  case generalinstabugerror(String)
  
  var description: String {
    switch self {
      case .generalinstabugerror(let stringmsg):
        return stringmsg
    }
  }
}

final class AppLogger {
  // this are the category to be put on Xcode Environment variables
  enum Category: String {
    case Default = "default_log"
    case Network = "network_log"
    case Composition = "composition_log"
    case Pager = "pager_log"
    case CameraManager = "camera_manager_log"
    case Themes = "themes_log"
    // can add more
  }
    
  /*
   Level    Description
     
   notice
   Writes a message to the log using the default log type.
     
   debug
   Writes a debug message to the log.
   Information that is diagnostically helpful to people more than just developers (IT, sysadmins, etc.).
     
   trace
   Writes a trace message to the log.
   Only when I would be "tracing" the code and trying to find one part of a function specifically.
     
   info
   Writes an informative message to the log.
   Generally useful information to log (service start/stop, configuration assumptions, etc). Info I want to always have available but usually don't care about under normal circumstances. This is my out-of-the-box config level.
     
   error
   Writes information about an error to the log.
   Any error which is fatal to the operation, but not the service or application (can't open a required file, missing data, etc.). These errors will force user (administrator, or direct user) intervention. These are usually reserved (in my apps) for incorrect connection strings, missing services, etc.
     
   warning
   Writes information about a warning to the log.
   Anything that can potentially cause application oddities, but for which I am automatically recovering. (Such as switching from a primary to backup server, retrying an operation, missing secondary data, etc.)
     
   fault
   Writes a message to the log about a bug that occurs when your app executes.
     
   critical
   Writes a message to the log about a critical event in your app’s execution.
     
   */
    
  enum Level {
    case notice
    case debug
    case track
    case info
    case error
    case warning
    case fault
    case critical
  }

  // MARK: - Properties
  /// Default values used by the `AppLogger`.
  struct Defaults {
    public static let subsystem = Bundle.main.bundleIdentifier ?? "com.viddl.app"
    public static let category = Category.Default.rawValue
    public static let isPrivate = false
  }

  // MARK: - Private Properties
  private let logger: Logger
  private let category: String
  // you can edit this list instead of using environmental variables
  // debug and info log has lowest priority, explicitly enable in console.app
  private let supportedLogs: [Category] = [.Default, .Network, .Composition, .Pager, .CameraManager, .Themes]
  // Can init also, it's a singleton plus class like FileManager.default and you can also init FileManager
  init(subsystem: String = Defaults.subsystem, category: String = Defaults.category) {
    self.category = category
    logger = Logger(subsystem: subsystem, category: category)
  }
    
}
extension AppLogger {
  // for default
  static let defaultLog = AppLogger()
    
  // for network
  static let networkLog = AppLogger(subsystem: Defaults.subsystem, category: Category.Network.rawValue)

  // for composition builder
  static let compositionLog = AppLogger(subsystem: Defaults.subsystem, category: Category.Composition.rawValue)
  
  static let pagerLog = AppLogger(subsystem: Defaults.subsystem, category: Category.Pager.rawValue)
  
  static let cameraLog = AppLogger(subsystem: Defaults.subsystem, category: Category.CameraManager.rawValue)
  
  static let themesLog = AppLogger(subsystem: Defaults.subsystem, category: Category.Themes.rawValue)
}

/*
 if you thinking why this code look too verbose, because anything starting with OSType cannot be passed around.
 see - https://stackoverflow.com/questions/62675874/xcode-12-and-oslog-os-log-wrapping-oslogmessage-causes-compile-error-argumen
 */

// MARK: - Interface
extension AppLogger {
    
  /// Log a String interpolation at the level.
  /// - Parameters:
  ///   - information: The `String` to be logged.
  ///   - isPrivate: Sets the `OSLogPrivacy` to be used by the function. `true` means a `.private` `OSLogPrivacy`;
  ///   - level: Log level `Level` see description above
  func log(_ information: String, isPrivate: Bool = Defaults.isPrivate, at level: Level = .debug) {

    guard let logCategory = Category(rawValue: category), self.supportedLogs.contains(logCategory) else {
      return
    }
        
    switch level {
      case .debug:
        debug(information,isPrivate: isPrivate)
      case .info:
        info(information,isPrivate: isPrivate)
      case .warning:
        break // not implemented
      case .error:
        error(information, isPrivate: isPrivate)
      case .notice:
        break // not implemented
      case .track:
        trace(information, isPrivate: isPrivate)
      case .fault:
        break // not implemented
      case .critical:
        break // not implemented
    }
  }
    
  private func trace(_ information: String, isPrivate: Bool = Defaults.isPrivate) {
    if isPrivate {
      logger.trace("\(information, privacy: .private)")
    } else {
      logger.trace("\(information, privacy: .public)")
    }
  }
    
  private func debug(_ information: String, isPrivate: Bool = Defaults.isPrivate) {
    
    if isPrivate {
      logger.debug("\(information, privacy: .private)")
    } else {
      logger.debug("\(information, privacy: .public)")
    }
  }
    
  private func info(_ information: String, isPrivate: Bool = Defaults.isPrivate) {
    if isPrivate {
      logger.info("\(information, privacy: .private)")
    } else {
      logger.info("\(information, privacy: .public)")
    }
  }
    
  private func error(_ information: String, isPrivate: Bool = Defaults.isPrivate) {
    if isPrivate {
      logger.error("\(information, privacy: .private)")
    } else {
      logger.error("\(information, privacy: .public)")
    }
  }
    
  // can make more,
}
