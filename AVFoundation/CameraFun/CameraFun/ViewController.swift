//
//  ViewController.swift
//  CameraFun
//
//  Created by Don Clore on 4/21/22.
//

import UIKit

class ViewController: UIViewController, CameraFlipAnimationEffect {
  struct Constants {
    static let closeButtonDim: CGFloat = 42
    static let closeButtonLeft: CGFloat = 16
    static let flipButtonRight: CGFloat = 16
    static let closeButtonTop: CGFloat = 16
    static let snapPicButtonDim: CGFloat = 90
    static let edgeButtonWidth: CGFloat = 60
    static let edgeButtonHeight: CGFloat = 60
    static let snapButtonToBottom: CGFloat = 24
    static let maxImageCount: Int = 3
    static let topGradientHeight: Int = 60
  }

  var bottomGradient: CAGradientLayer = CAGradientLayer()
  var topGradient: CAGradientLayer = CAGradientLayer()
  var cameraManager: CICameraManager?
  var cameraCapture: CICameraCapture?
  var videoView: MetalRenderView?
  var movieWriter: MovieWriter?
  let recordButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
  let flipButton: UIButton = UIButton(type: .custom)
  let closeButton: UIButton = UIButton(type: UIButton.ButtonType.custom)
  var madeGradients: Bool = false
  var cameraFlipAnimating: Bool = false
  var recording: Bool = false
  
  private lazy var haptic : UINotificationFeedbackGenerator = {
    let gen = UINotificationFeedbackGenerator()
    gen.prepare()
    return gen
  }()


  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.backgroundColor = UIColor.black

    videoView = MetalRenderView(frame: view.bounds, device: MTLCreateSystemDefaultDevice())
    guard let videoView = videoView else {
      return
    }

    videoView.errorDelegate = self

    [videoView,].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      view.addSubview($0)
    }

    videoView.accessibilityIdentifier = "VideoView"
    
    recordButton.setImage(UIImage(named: "InsertMediaRecord")!, for: UIControl.State.normal)
    
    let mediumFont = UIFont.systemFont(ofSize: 20)
    let configuration = UIImage.SymbolConfiguration(font: mediumFont)
    let closeImage = UIImage(systemName: "xmark", withConfiguration: configuration)
    closeButton.setImage(closeImage, for: UIControl.State.normal)
    closeButton.tintColor = UIColor.white

    let flipImage = UIImage(systemName: "arrow.triangle.2.circlepath.camera.fill", withConfiguration: configuration)
    flipButton.setImage(flipImage, for: UIControl.State.normal)
    flipButton.tintColor = UIColor.white
    
    [flipButton, recordButton, closeButton].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      view.addSubview($0)
      view.bringSubviewToFront($0)
    }
    
    NSLayoutConstraint.activate([
      videoView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      videoView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      videoView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      videoView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
      
      closeButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: Constants.closeButtonTop),
      closeButton.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: Constants.closeButtonLeft),
      closeButton.widthAnchor.constraint(equalToConstant: Constants.closeButtonDim),
      closeButton.heightAnchor.constraint(equalToConstant: Constants.closeButtonDim),

      flipButton.topAnchor.constraint(equalTo: closeButton.topAnchor),
      flipButton.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -Constants.flipButtonRight),
      flipButton.widthAnchor.constraint(equalToConstant: Constants.closeButtonDim),
      flipButton.heightAnchor.constraint(equalToConstant: Constants.closeButtonDim),
      
      recordButton.centerXAnchor.constraint(equalTo: videoView.centerXAnchor),
      recordButton.bottomAnchor.constraint(equalTo: videoView.bottomAnchor, constant: -Constants.snapButtonToBottom),
      recordButton.widthAnchor.constraint(equalToConstant: Constants.snapPicButtonDim),
      recordButton.heightAnchor.constraint(equalToConstant: Constants.snapPicButtonDim),
    ])

    videoView.backgroundColor = UIColor.clear
    flipButton.addTarget(self, action: #selector(ViewController.flipCameraTouched(_:)), for: UIControl.Event.touchUpInside)
    recordButton.addTarget(self, action: #selector(ViewController.recordButtonTouched(_:)), for: UIControl.Event.touchUpInside)
  }
  
 
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    setupCapture()
  }
  
  func makeGradient() {
    guard let videoView = videoView else {
      return
    }

    topGradient.colors = [
      UIColor(red: 0, green: 0, blue: 0, alpha: 0.65).cgColor,
      UIColor.clear.cgColor
    ]

    topGradient.locations = [0, 0.3]
    videoView.layer.addSublayer(topGradient)
    let rect: CGRect = CGRect(x: 0, y: 0, width: videoView.bounds.width, height: videoView.bounds.height * 0.25)
    topGradient.frame = rect

    bottomGradient.colors = [
      UIColor(red: 0, green: 0, blue: 0, alpha: 0).cgColor,
      UIColor(red: 0, green: 0, blue: 0, alpha: 0.33).cgColor
    ]
    bottomGradient.locations = [0.81, 1]
    bottomGradient.startPoint = CGPoint(x: 0.5, y: 0.5)
    bottomGradient.endPoint = CGPoint(x: 0.91, y: 0.5)
    bottomGradient.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 0, b: 1, c: -1, d: 0, tx: 1, ty: 0))
    bottomGradient.position = videoView.center
    bottomGradient.zPosition = 999
    bottomGradient.frame = videoView.bounds
    videoView.layer.addSublayer(bottomGradient)
  }
  
  private func setupCapture() {
    cameraCapture = CICameraCapture(cameraPosition: .front) { [weak self] image in
      guard let self = self else { return }
      guard let image = image, let videoView = self.videoView else {
        return
      }

      videoView.image = image
    }

    cameraCapture?.delegate = self
    cameraCapture?.prepareSession()
    cameraCapture?.start()
  }

  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()

    guard !madeGradients else {
      return
    }
    defer {
      madeGradients = true
    }

    makeGradient()
  }
  
  @objc func flipCameraTouched(_ sender: UIButton) {
    flipCamera()
  }

  func flipCamera() {
    guard let cameraCapture = cameraCapture else {
      return
    }

    flipButton.isUserInteractionEnabled = false
    defer {
      flipButton.isUserInteractionEnabled = true
    }

    haptic.notificationOccurred(.success)
    doFlipAnimation{ }
    cameraCapture.flipCamera()
  }
  
  @objc func recordButtonTouched(_ sender: UIButton) {
    guard let cameraCapture = cameraCapture else {
      return
    }

    recording = !recording
    
    print("\(#function) - recording = \(recording)")
    switch recording {
      case true:
        cameraCapture.startRecording()
      case false:
        cameraCapture.stopRecording()
    }
  }
}

extension ViewController: MetalRenderViewErrorReportingDelegate {
  func renderTaskFailed(withError error: Error) {
    AppLogger.cameraLog.log("\(#function) error \(error)", at: .error)
  }
}

extension ViewController: CICameraCaptureDelegate {
  func getTraitCollection() -> UITraitCollection {
    return traitCollection
  }
  
  func couldntGetCamera() {
    AppLogger.cameraLog.log("\(#function)", at: .error)
    fatalError()
  }
  
  func cantAddBackInput() {
    AppLogger.cameraLog.log("\(#function)", at: .error)
    fatalError()
  }
  
  func cantAddFrontInput() {
    AppLogger.cameraLog.log("\(#function)", at: .error)
    fatalError()
  }
  
  func cantAddAudioInput() {
    AppLogger.cameraLog.log("\(#function)", at: .error)
    fatalError()
  }
  
  func cantAddVideoOutput() {
    AppLogger.cameraLog.log("\(#function)", at: .error)
    fatalError()
  }
  
  func cantAddAudioOutput() {
    AppLogger.cameraLog.log("\(#function)", at: .error)
    fatalError()
  }
  
  func cantGetRecommendedVideoSettings() {
    AppLogger.cameraLog.log("\(#function)", at: .error)
    fatalError()
  }
  
  func cantGetRecommendedAudioSettings() {
    AppLogger.cameraLog.log("\(#function)", at: .error)
    fatalError()
  }
  
  func getWindowOrientation() -> UIInterfaceOrientation {
    assert(Thread.isMainThread)
    guard let interfaceOrientation = view.window?.windowScene?.interfaceOrientation else {
      return UIInterfaceOrientation.unknown
    }
    return interfaceOrientation
  }
  
  
}
