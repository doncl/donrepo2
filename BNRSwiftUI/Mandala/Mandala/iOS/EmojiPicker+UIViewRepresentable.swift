//
//  EmojiPicker+UIViewRepresentable.swift
//  Mandala (iOS)
//
//  Created by Don Clore on 7/21/21.
//

import SwiftUI
import EmojiUtilities

extension EmojiPicker: UIViewRepresentable {
  class Coordinator {

      @Binding var emoji: Emoji?

      init(emoji: Binding<Emoji?>) {
          _emoji = emoji
      }

      @objc func emojiFieldDidChangeValue(_ emojiField: EmojiField) {
          emoji = Emoji(rawValue: emojiField.text)
      }

  }
  
  func makeUIView(context: Context) -> EmojiField {
    let emojiField = EmojiField()
    emojiField.addTarget(
        context.coordinator,
        action: #selector(Coordinator.emojiFieldDidChangeValue),
        for: .valueChanged)
    return emojiField
  }

  func updateUIView(_ emojiField: EmojiField, context: Context) {
      emojiField.text = emoji?.rawValue ?? ""
      emojiField.isEnabled = isEnabled
  }

  func makeCoordinator() -> Coordinator {
      Coordinator(emoji: $emoji)
  }
  
}
