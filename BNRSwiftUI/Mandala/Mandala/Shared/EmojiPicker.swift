//
//  EmojiPicker.swift
//  Mandala
//
//  Created by Don Clore on 7/21/21.
//

import SwiftUI
import EmojiUtilities

struct EmojiPicker: View {
  @Binding var emoji: Emoji?
  @Environment(\.isEnabled) var isEnabled  
}

struct EmojiPicker_Previews: PreviewProvider {
  static var previews: some View {
    EmojiPicker(emoji: .constant(.random()))
  }
}
