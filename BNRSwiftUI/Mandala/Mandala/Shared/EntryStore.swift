//
//  EntryStore.swift
//  Mandala
//
//  Created by Don Clore on 7/19/21.
//

import Foundation
import Combine
#if os(macOS)
import Cocoa
#endif

class EntryStore: ObservableObject {
  @Published var allEntries: [Entry] = []
  
  init(entries: [Entry]) {
    self.allEntries = entries
  }
  
  init() {
    load()
    
    #if os(macOS)
    NotificationCenter.default.addObserver(self, selector: #selector(EntryStore.save), name: NSApplication.willTerminateNotification, object: nil)
    #endif
  }
  
  func addEntry(mood: Mood) -> Entry {
    let entry = Entry(mood: mood)
    allEntries.insert(entry, at: 0)
    return entry
  }
  
  func load() {
    do {
      let url = try archiveURL()
      print("Loading entries from \(url)")
      let data = try Data(contentsOf: url)
      let unarchiver = PropertyListDecoder()
      let entries = try unarchiver.decode([Entry].self, from: data)
      allEntries = entries
    } catch let decodingError {
      print("Could not read saved entries: \(decodingError)")
    }
  }
  
  @objc func save() {
    do {
      let url = try archiveURL()
      print("Saving entries to \(url)")
      let encoder = PropertyListEncoder()
      let data = try encoder.encode(allEntries)
      try data.write(to: url)
      print("Saved all of the entries")
    } catch let CocoaError.fileReadNoSuchFile {
      // Ignore error
    } catch let encodingError {
      print("Could not save any of the entries: \(encodingError)")
    }
  }
  
  func archiveURL() throws -> URL {
    let docDir = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    return docDir.appendingPathComponent("entries", conformingTo: .propertyList)
  }
  
  func update(_ entry: Entry) {
    guard let index = indexForEntry(with: entry.id) else {
      return
    }
    allEntries[index] = entry
    allEntries.sort {
      $0.timestamp > $1.timestamp
    }
  }
  
  func remove(at offsets: IndexSet) {
    allEntries.remove(atOffsets: offsets)
  }
  
  func containsEntry(with id: Entry.ID) -> Bool {
    return indexForEntry(with: id) != nil 
  }
  
  func indexForEntry(with id: Entry.ID) -> Int? {
    return allEntries.firstIndex { $0.id == id }
  }
}
