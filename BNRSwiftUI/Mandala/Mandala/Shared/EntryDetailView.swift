//
//  EntryDetailView.swift
//  Mandala
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct EntryDetailView: View {
  #if os(iOS)
  @EnvironmentObject var store: EntryStore
  #else
  @ObservedObject var store: EntryStore
  #endif
  @State var entry: Entry
  
  var formattedTimeStamp: Text {
    return Text(entry.timestamp, style: .date)
  }
  
  var body: some View {
    if store.containsEntry(with: entry.id) {
      #if os(iOS)
      form
        .navigationTitle("Entry")
        .navigationBarTitleDisplayMode(.inline)
      #else
      form
        .padding()
        .frame(minWidth: 300)
        .navigationSubtitle(formattedTimeStamp)
      #endif
    } else {
      PlaceholderSelectionView()
    }    
  }
  
  var form: some View {
    Form {
      EmojiPicker(emoji: $entry.emoji)
        .padding()
        .background(MoodView(mood: entry.mood))
        .padding(.vertical, 8)
      
      Picker("Mood", selection: $entry.mood) {
        ForEach(Mood.allCases, id: \.self) { mood in
          mood.shortName
            .tag(mood)
        }
      }
      
      Section {
        TextField("Summary", text: $entry.summary)
        DatePicker("Date", selection: $entry.timestamp)
      }
      
      Section(header: Text("Additional Details", comment: "Form section header")) {
        #if os(iOS)
        TextEditor(text: $entry.details)
        #else
        TextEditor(text: $entry.details)
          .frame(minHeight: 200)
          .border(Color.secondary).opacity(0.2)
        #endif
      }
    }
    .onDisappear{
      store.update(entry)
    }
  }
}

struct EntryDetailView_Previews: PreviewProvider {
  static var previews: some View {
    let entry = Entry.random()
    let store = EntryStore(entries: [ entry ])
    
    #if os(iOS)
    let detail = EntryDetailView(entry: entry)
      .environmentObject(store)
    #else
    let detail = EntryDetailView( store: store, entry: entry)
    #endif
    
    NavigationView {
      #if os(macOS)
      EmptyView()
      #endif
      detail
    }
  }
}
