//
//  Entry.swift
//  Mandala
//
//  Created by Don Clore on 7/19/21.
//

import Foundation
import EmojiUtilities

struct Entry: Identifiable, Codable {
  var id = UUID()
  var mood: Mood
  var timestamp = Date()
  var summary = ""
  var details = ""
  var emoji: Emoji?
}
