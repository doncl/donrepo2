//
//  MoodView.swift
//  Mandala
//
//  Created by Don Clore on 7/19/21.
//

import SwiftUI

struct MoodView: View {
  let mood: Mood
  
  let overlayGradient = LinearGradient(
    gradient: Gradient(colors: [Color.white.opacity(0.4), .clear]),
    startPoint: .top,
    endPoint: .bottom)
  
    var body: some View {
      Color(mood.rawValue)
        .aspectRatio(contentMode: .fit)
        .overlay(overlayGradient)
        .clipShape(Circle())
    }
}

struct MoodPreview: Identifiable {
  let mood: Mood
  let id = UUID()
}

struct MoodView_Previews: PreviewProvider {
  static var cases: [MoodPreview] = Mood.allCases.map{ MoodPreview(mood: $0)}
  
    static var previews: some View {
      VStack {
        ForEach(cases) { moodPreview in 
          MoodView(mood: moodPreview.mood)
        }
      }
    }
}
