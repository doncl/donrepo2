//
//  ContentView.swift
//  Shared
//
//  Created by Don Clore on 7/19/21.
//

import SwiftUI
import Combine

struct EntryListView: View {
  #if os(iOS)
  @EnvironmentObject var store: EntryStore
  #else
  @ObservedObject var store: EntryStore
  #endif
  @State var selection: Entry.ID?
  
  var body: some View {
    List {
      ForEach(store.allEntries) { entry in
        #if os(iOS)
        let detail = EntryDetailView(entry: entry)
        #else
        let detail = EntryDetailView(store: store, entry: entry)
        #endif
        NavigationLink(destination: detail, tag: entry.id, selection: $selection) {
          #if os(iOS)
          EntryRowView(entry: entry)
          #else
          EntryRowView(entry: entry, store: store)
          #endif
        }
      }
      .onDelete(perform: { indexSet in
        delete(at: indexSet)
      })
    }
    .frame(minWidth: 250)
    .toolbar {
      #if os(iOS)
      ToolbarItem(placement: .navigationBarTrailing) {
        Menu {
          ForEach(Mood.allCases, id: \.self) { mood in
            Button {
              addEntry(mood: mood)
            } label: {
              mood.shortName
            }
          }
        } label: {
          Label("New Entry", systemImage: "plus")
        }
      }
      #else
      ToolbarItem(placement: .automatic) {
        Menu {
          ForEach(Mood.allCases, id: \.self) { mood in
            Button {
              addEntry(mood: mood)
            } label: {
              mood.shortName
            }
          }
        } label: {
          Label("New Entry", systemImage: "plus")
        }
      }
      #endif
    }
  }
  
  func delete(at offsets: IndexSet) {
    store.remove(at: offsets)
  }
  func addEntry(mood: Mood) {
    withAnimation {
      let entry = store.addEntry(mood: mood)
      selection = entry.id
    }
  }
}

struct EntryListView_Previews: PreviewProvider {
  static var previews: some View {
    let store = EntryStore(entries: [
      Entry.random(),
      Entry.random(),
      Entry.random(),
    ])
    NavigationView {
      #if os(iOS)
      EntryListView()
        .environmentObject(store)
      #else
      EntryListView(store: store)
      #endif
    }
  }
}
