//
//  MandalaApp.swift
//  Shared
//
//  Created by Don Clore on 7/19/21.
//

import SwiftUI

@main
struct MandalaApp: App {
  @StateObject var entryStore = EntryStore()

  var body: some Scene {
    WindowGroup {
      #if os(iOS)
      ContentView()
        .environmentObject(entryStore)
      #else
      ContentView(store: entryStore)
      #endif
    }
  }
}
