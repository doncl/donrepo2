//
//  Mood.swift
//  Mandala
//
//  Created by Don Clore on 7/19/21.
//

import Foundation

enum Mood: String, CaseIterable, Codable {
  case anger
  case anticipation
  case joy
  case trust
  case fear
  case surprise
  case sadness
  case disgust
}
