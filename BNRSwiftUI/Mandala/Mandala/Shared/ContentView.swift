//
//  ContentView.swift
//  Mandala
//
//  Created by Don Clore on 7/19/21.
//

import SwiftUI

struct ContentView: View {
  @Environment(\.scenePhase) var scenePhase
  #if os(iOS)
  @EnvironmentObject var store: EntryStore
  #else
  @ObservedObject var store: EntryStore
  #endif
  
  var body: some View {
    NavigationView {
      #if os(iOS)
      EntryListView()
        .navigationTitle("Mandala")
      #else
      EntryListView(store: store)
        .navigationTitle("Mandala")
      #endif
      
      PlaceholderSelectionView()
    }
    .onChange(of: scenePhase) { phase in
      if phase == .background {
        store.save()
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    let store = EntryStore(entries: [
      Entry.random(),
      Entry.random(),
      Entry.random(),
    ])
    #if os(iOS)
    ContentView()
      .environmentObject(store)
    #else
    ContentView(store: store)
    #endif
  }
}
