//
//  EntryExtensions.swift
//  Mandala
//
//  Created by Don Clore on 7/20/21.
//

import Foundation

extension Entry {
  static func random() -> Entry {
    return Entry(id: UUID(), mood: Mood.random(), timestamp: Date())
  }
}
