//
//  PlaceholderSelectionView.swift
//  Mandala
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct PlaceholderSelectionView: View {
  var body: some View {
    #if os(iOS)
      placeholder
      .navigationTitle("")
      .navigationBarTitleDisplayMode(.inline)
    #else
      placeholder
    #endif
  }
  
  var placeholder: some View {
    Text("Nothing Selected", comment: "Placeholder Text")
      .font(.title2)
      .fontWeight(.bold)
      .foregroundColor(.secondary)
      .frame(minWidth: 300, minHeight: 300)
  }
}

struct PlaceholderSelectionView_Previews: PreviewProvider {
  static var previews: some View {
    PlaceholderSelectionView()
      .previewLayout(.sizeThatFits)
  }
}
