//
//  EntryRowView.swift
//  Mandala
//
//  Created by Don Clore on 7/19/21.
//

import SwiftUI

let entryDateFormatter: DateFormatter = {
  let formatter = DateFormatter()
  formatter.dateStyle = .medium
  formatter.timeStyle = .short
  return formatter
}()


struct EntryRowView: View {
  var entry: Entry
  @Environment(\.sizeCategory) var sizeCategory: ContentSizeCategory
  
  @ScaledMetric var emojiSize = CGFloat(36)
  @ScaledMetric var emojiPadding = CGFloat(10)
  
  #if !os(iOS)
  @ObservedObject var store: EntryStore
  #endif
  
  
  var summary: Text {
    entry.summary.isEmpty ? entry.mood.longName : Text(entry.summary)
  }
  
  var emoji: Text {
    Text(entry.emoji?.rawValue ?? entry.mood.emoji)
  }
  
  let overlayGradient = LinearGradient(
    gradient: Gradient(colors: [Color.white.opacity(0.4), .clear]),
    startPoint: .top,
    endPoint: .bottom)
  
  var shared: some View {
    Group {
      emoji
        .font(.system(size: emojiSize))
        .padding(emojiPadding)
        .background(MoodView(mood: entry.mood))
        .clipShape(Circle())
      
      VStack(alignment: .leading) {
        summary
          .font(.headline)
  
        
        Text("on \(entry.timestamp, formatter: entryDateFormatter)")
          .font(.subheadline)
          .foregroundColor(.secondary)
      }
    }
  }
    
  var body: some View {
    if sizeCategory.isAccessibilityCategory {
      VStack {
        shared
      }
    } else {
      HStack {
        shared
      }
    }
  }
}

struct EntryRowView_Previews: PreviewProvider {
  static var previews: some View {
    #if !os(iOS)
    let store = EntryStore(entries: [
      Entry.random(),
      Entry.random(),
      Entry.random(),
    ])
    Group {
      EntryRowView(entry: Entry.random(), store: store)
      EntryRowView(entry: Entry.random(), store: store)
        .environment(\.sizeCategory, .accessibilityExtraExtraExtraLarge)
      EntryRowView(entry: Entry.random(), store: store)
        .environment(\.locale, Locale(identifier: "es-ES"))
    }
    .padding(.horizontal)
    .frame(width: 320, alignment: .leading)
    .previewLayout(.sizeThatFits)

    #else
    Group {
      EntryRowView(entry: Entry.random())
      EntryRowView(entry: Entry.random())
        .environment(\.sizeCategory, .accessibilityExtraExtraExtraLarge)
      EntryRowView(entry: Entry.random())
        .environment(\.locale, Locale(identifier: "es-ES"))
    }
    .padding(.horizontal)
    .frame(width: 320, alignment: .leading)
    .previewLayout(.sizeThatFits)
    #endif
  }
}
