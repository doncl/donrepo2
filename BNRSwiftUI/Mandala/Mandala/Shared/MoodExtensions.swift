//
//  MoodExtensions.swift
//  Mandala
//
//  Created by Don Clore on 7/19/21.
//

import Foundation
import SwiftUI

extension Mood {
  var shortName: Text {
    switch self {
      case .anger:
        return Text("anger", tableName: "Moods", comment: "Short name")
      case .anticipation:
        return Text("anticipation", tableName: "Moods", comment: "Short name")
      case .joy:
        return Text("joy", tableName: "Moods", comment: "Short name")
      case .trust:
        return Text("trust", tableName: "Moods", comment: "Short name")
      case .fear:
        return Text("fear", tableName: "Moods", comment: "Short name")
      case .surprise:
        return Text("surprise", tableName: "Moods", comment: "Short name")
      case .sadness:
        return Text("sadness", tableName: "Moods", comment: "Short name")
      case .disgust:
        return Text("disgust", tableName: "Moods", comment: "Short name")
    }
  }
  
  var emoji: String {
    switch self {
      case .anger:
        return "😡"
      case .anticipation:
        return "🥶"
      case .joy:
        return "😁"
      case .trust:
        return "😜"
      case .fear:
        return "😱"
      case .surprise:
        return "😳"
      case .sadness:
        return "😭"
      case .disgust:
        return "🤮"
    }
  }
  
  var longName: Text {
    switch self {      
      case .anger:
        return Text("I was angry", tableName: "Moods", comment: "Long name, past tense")
      case .anticipation:
        return Text("I was excited", tableName: "Moods", comment: "Long name, past tense")
      case .joy:
        return Text("I was happy", tableName: "Moods", comment: "Long name, past tense")
      case .trust:
        return Text("I admired", tableName: "Moods", comment: "Long name, past tense")
      case .fear:
        return Text("I was scared", tableName: "Moods", comment: "Long name, past tense")
      case .surprise:
        return Text("I was surprised", tableName: "Moods", comment: "Long name, past tense")
      case .sadness:
        return Text("I was surprised", tableName: "Moods", comment: "Long name, past tense")
      case .disgust:
        return Text("I was disgusted", tableName: "Moods", comment: "Long name, past tense")
    }
  }

  static func random() -> Mood {
    return Mood.allCases.randomElement()!
  }
}
