//
//  EmojiPicker+NSViewRepresentable.swift
//  Mandala (macOS)
//
//  Created by Don Clore on 7/21/21.
//

import Foundation

import SwiftUI
import EmojiUtilities

extension EmojiPicker: NSViewRepresentable {

    class Coordinator {

        @Binding var emoji: Emoji?

        init(emoji: Binding<Emoji?>) {
            _emoji = emoji
        }

        @objc func emojiFieldDidChangeValue(_ emojiField: EmojiField) {
            emoji = Emoji(rawValue: emojiField.stringValue)
        }

    }

    func makeNSView(context: Context) -> EmojiField {
        let emojiField = EmojiField()
        emojiField.target = context.coordinator
        emojiField.action = #selector(Coordinator.emojiFieldDidChangeValue)
        return emojiField
    }

    func updateNSView(_ emojiField: EmojiField, context: Context) {
        emojiField.stringValue = emoji?.rawValue ?? ""
        emojiField.isEnabled = isEnabled
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(emoji: $emoji)
    }

}

