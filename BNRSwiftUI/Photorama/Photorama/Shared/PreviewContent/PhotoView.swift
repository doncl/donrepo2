//
//  PhotoView.swift
//  Photorama
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct PhotoView: View {
  var photo: Photo
  @StateObject var imageLoader = ImageLoader()
  
  
  var loadingView: some View {
    ZStack {
      Color.gray
      Image(systemName: "questionmark.square.dashed")
        .font(.largeTitle)
    }
  }
  
  var errorView: some View {
    ZStack {
      Color.gray
      Image(systemName: "questionMark.square.dashed")
        .font(.largeTitle)
    }
  }
  
  var body: some View {
    Group {
      switch imageLoader.state {
        case .loading:
          loadingView
            .onAppear {
              if let url = photo.remoteURL {
                imageLoader.fetchImage(from: url)
              }
            }
        case .error:
          errorView
        case let .image(image):
          image.resizable()
            .aspectRatio(contentMode: .fill)
      }
    }
    .frame(minWidth: 0, minHeight: 0)
    .aspectRatio(1.0, contentMode: .fill)
    .clipShape(RoundedRectangle(cornerRadius: 5))
  }
}

struct PhotoView_Previews: PreviewProvider {
  static let photo = Photo(title: "Preview Photo",
                               remoteURL: nil,
                               id: UUID().uuidString,
                               dateTaken: Date())
  
  static let successfulLandscapeImageLoader
      = ImageLoader(state: .image(Image("Photo_1")))

  static let successfulPortraitImageLoader
      = ImageLoader(state: .image(Image("Photo_2")))

  static let errorImageLoader
    = ImageLoader(state: .error(ImageLoader.Failure.missingImageURL))

  static let loadingImageLoader
      = ImageLoader(state: .loading)
  

  static var previews: some View {
    Group {
      PhotoView(photo: photo, imageLoader: successfulLandscapeImageLoader)
        .previewDisplayName("Success Landscape")
      
      PhotoView(photo: photo, imageLoader: successfulPortraitImageLoader)
        .previewDisplayName("Success Portrait")
      
      PhotoView(photo: photo, imageLoader: errorImageLoader)
        .previewDisplayName("Error")
      
      PhotoView(photo: photo, imageLoader: loadingImageLoader)
        .previewDisplayName("Loading")
    }
    .previewLayout(.fixed(width: 200, height: 200))
  }
}




































