//
//  PhotoGrid.swift
//  Photorama
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct PhotoGrid: View {
  var photos: [Photo]
  
  var columns = [
    GridItem(.adaptive(minimum: 100, maximum: 200))
  ]
  
  var body: some View {
    ScrollView {
      LazyVGrid(columns: columns) {
        ForEach(photos) { photo in
          PhotoView(photo: photo)
        }
      }
      .padding()
    }
  }
}

struct PhotoGrid_Previews: PreviewProvider {
  static var previews: some View {
    let iPhoneDevice = "iPhone 12 Pro"
    
    PhotoGrid(photos: Photo.previewPhotos)
      .previewDevice(PreviewDevice(rawValue: iPhoneDevice))
    
    let iPadDevice = "iPad Pro (11-inch) (2nd generation)"
    PhotoGrid(photos: Photo.previewPhotos)
      .previewDevice(PreviewDevice(rawValue: iPadDevice))
  }
}
