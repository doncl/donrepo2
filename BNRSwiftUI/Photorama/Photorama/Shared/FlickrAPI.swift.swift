//
//  FlickrAPI.swift.swift
//  Photorama
//
//  Created by Don Clore on 7/20/21.
//

import Foundation

enum FlickrAPI {
  private static let apiKey = "a6d819499131071f158fd740860a5a88"
  
  static var interestingPhotosURL: URL {
    return flickrURL(endPoint: .interestingPhotos, parameters: ["extras": "url_z,date_taken"])
  }
    
  private enum Endpoint: String {
    case interestingPhotos = "flickr.interestingness.getList"
  }
  
  private static let baseURLString = "https://api.flickr.com/services/rest"
  
  static func photos(fromJSON data: Data) throws -> [Photo] {
    let decoder = JSONDecoder()

    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    dateFormatter.locale = Locale(identifier: "en_US_POSIX")
    dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
    decoder.dateDecodingStrategy = .formatted(dateFormatter)

    
    let response = try decoder.decode(Response.self, from: data)
    let photos = response.photosInfo.photos.filter { $0.remoteURL != nil }
    
    return photos
  }
  
  private static func flickrURL(endPoint: Endpoint, parameters: [String: String]?) -> URL {
    var components = URLComponents(string: baseURLString)!
    var queryItems = [URLQueryItem]()
    
    let baseParams = [
      "method": endPoint.rawValue,
      "format": "json",
      "nojsoncallback": "1",
      "api_key": apiKey,
    ]
    
    for (key, value) in baseParams {
      let item = URLQueryItem(name: key, value: value)
      queryItems.append(item)
    }
    
    if let additionalParams = parameters {
      for (key, value) in additionalParams {
        let item = URLQueryItem(name: key, value: value)
        queryItems.append(item)
      }
    }
    components.queryItems = queryItems
    return components.url!
  }
}

extension FlickrAPI {
  struct Response: Codable {
    enum CodingKeys: String, CodingKey {
      case photosInfo = "photos"
    }
    let photosInfo: PhotosResponse
  }
  
  struct PhotosResponse: Codable {
    enum CodingKeys: String, CodingKey {
      case photos = "photo"
    }
    let photos: [Photo]
    
  }
}
