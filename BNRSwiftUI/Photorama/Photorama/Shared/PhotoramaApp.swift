//
//  PhotoramaApp.swift
//  Shared
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

@main
struct PhotoramaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
