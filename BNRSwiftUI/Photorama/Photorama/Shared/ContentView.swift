//
//  ContentView.swift
//  Shared
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct ContentView: View {
  @StateObject var photoStore = PhotoStore()
  
  var body: some View {
    let grid = PhotoGrid(photos: photoStore.interestingPhotos)
      .onAppear {
        photoStore.fetchInterestingPhotos()
      }
    
    #if os(iOS)
    NavigationView {
      grid
        .navigationTitle("Photorama")
        .navigationBarTitleDisplayMode(.inline)
    }
    #else
    grid 
    #endif
  }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
