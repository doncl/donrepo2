//
//  ImageLoader.swift
//  Photorama (iOS)
//
//  Created by Don Clore on 7/21/21.
//

import Foundation
import SwiftUI

#if os(iOS)
import UIKit
#endif

class ImageLoader: ObservableObject {
  enum Failure: Error {
    case imageCreationError
    case missingImageURL
  }
  
  #if os(iOS)
  typealias ImageReference = UIImage
  #else
  typealias ImageReference = NSImage
  #endif
  
  enum State {
    case loading
    case error(Error)
    case image(Image)
  }
  
  @Published var state = State.loading
  
  convenience init(state: ImageLoader.State) {
    self.init()
    self.state = state 
  }
  
  func fetchImage(from url: URL) {
    let request = URLRequest(url: url)
    let task = URLSession.shared.dataTask(with: request) {
        (data, response, error) in
        do {
            let image = try self.processImageRequest(data: data, error: error)
            OperationQueue.main.addOperation {
                self.state = .image(image)
            }
        } catch {
            OperationQueue.main.addOperation {
                self.state = .error(error)
            }
        }
    }
    task.resume()
  }
  
  private func processImageRequest(data: Data?, error: Error?) throws -> Image {
    if let error = error {
      throw error
    }
    guard let imageData = data, let reference = ImageReference(data: imageData) else {
      throw Failure.imageCreationError
    }
    
    #if os(iOS)
    let image = Image(uiImage: reference)
    #else
    let image = Image(nsImage: reference)
    #endif
    
    return image 
  }
}
