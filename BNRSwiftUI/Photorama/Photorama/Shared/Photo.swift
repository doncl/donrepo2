//
//  Photo.swift
//  Photorama
//
//  Created by Don Clore on 7/20/21.
//

import Foundation

struct Photo: Identifiable, Codable {
  
  enum CodingKeys: String, CodingKey {
    case title
    case remoteURL = "url_z"
    case id
    case dateTaken = "datetaken"
  }
  
  let title: String
  let remoteURL: URL?
  let id: String
  let dateTaken: Date
}

