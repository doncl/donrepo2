//
//  PhotoStore.swift
//  Photorama
//
//  Created by Don Clore on 7/20/21.
//

import Foundation
import Combine


class PhotoStore: ObservableObject {
  enum Failure: Error {
    case unexpectedResponse
  }

  
  @Published var interestingPhotos: [Photo] = []
  
  init(photos: [Photo] = []) {
    self.interestingPhotos = photos 
  }
  
  private let session = URLSession.shared
  
  func fetchInterestingPhotos() {
    let url = FlickrAPI.interestingPhotosURL
    let request = URLRequest(url: url)
    let task = session.dataTask(with: request) { [weak self] data, response, error in
      guard let self = self else { return }
      
      let photos = try? self.processPhotosRequest(data: data, error: error)
      
      OperationQueue.main.addOperation {
        self.interestingPhotos = photos ?? []
      }
    }
    task.resume()
  }
  
  private func processPhotosRequest(data: Data?, error: Error?) throws -> [Photo] {
    if let error = error {
      throw error
    }
    
    guard let jsonData = data else {
      throw Failure.unexpectedResponse
    }
    
    return try FlickrAPI.photos(fromJSON: jsonData)
  }
}
