//
//  ContentView.swift
//  Shared
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct HomeControl: View {
    @State var color = Color.orange
    var body: some View {
        ZStack {
            Color.black.ignoresSafeArea()
            VStack(spacing: 20) {
                LightSlider(color: .orange)
                LightColorPicker()
            }
            HStack {
                Image(systemName: "lightbulb.fill")
                    .foregroundColor(.yellow)
                    .font(.title)
                VStack(alignment: .leading) {
                    Text("Hue lamp 1")
                        .bold()
                    Text("42% brightness")
                }
                Spacer()
                Button { } label: {
                    Image(systemName: "xmark.circle.fill")
                        .font(.title)
                        .foregroundColor(.gray)
                }
            }
            .padding()
            .foregroundColor(.white)
            .frame(maxWidth: .infinity,
                   maxHeight: .infinity,
                   alignment: .top)
            Button { } label: {
                Image(systemName: "gearshape.fill")
                    .font(.title)
                    .foregroundColor(.gray)
            }
            .padding()
            .frame(maxWidth: .infinity,
                   maxHeight: .infinity,
                   alignment: .bottomTrailing)
        }
    }
}
struct LightSlider: View {
    var color: Color
    var body: some View {
        ZStack(alignment: .bottom) {
            Rectangle()
                .fill(Color.gray)
            Rectangle()
                .fill(color)
                .frame(height: 150)
        }
        .frame(maxWidth: 100, maxHeight: 260)
        .overlay(
            Image(systemName: "lightbulb.fill")
                .foregroundColor(.white)
                .font(.title)
                .frame(maxHeight: .infinity, alignment: .bottom)
                .padding(.bottom)
        )
        .clipShape(RoundedRectangle(cornerRadius: 20))
    }
}
struct LightColorPicker: View {
    var body: some View {
        VStack(spacing: 6) {
            HStack {
                ColorDot(color: .yellow)
                    .overlay(
                        Image(systemName: "sun.max.fill")
                            .font(.title2)
                            .foregroundColor(.white)
                    )
                ColorDot(color: .white)
                ColorDot(color: .blue)
            }
            HStack {
                ColorDot(color: .orange)
                ColorDot(color: .green)
                ColorDot(color: .purple)
            }
        }
    }
}
struct ColorDot: View {
    var color: Color
    var body: some View {
        Circle()
            .fill(color)
            .frame(width: diameter, height: diameter)
    }
    var diameter: CGFloat { 50 }
}
struct HomeControl_Previews: PreviewProvider {
    static var previews: some View {
        HomeControl()
    }
}
