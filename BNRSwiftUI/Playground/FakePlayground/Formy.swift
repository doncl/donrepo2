//
//  Formy.swift
//  FakePlayground (iOS)
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct Formy: View {
  var body: some View {
    Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
  }
}

struct Formy_Previews: PreviewProvider {
  static var previews: some View {
      Formy()
  }
}
