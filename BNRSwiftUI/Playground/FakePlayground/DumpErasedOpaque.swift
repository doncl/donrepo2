//
//  DumpErasedOpaque.swift
//  FakePlayground (iOS)
//
//  Created by Don Clore on 7/22/21.
//

import SwiftUI

extension View {
  
}

struct DumpErasedOpaque: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct DumpErasedOpaque_Previews: PreviewProvider {
    static var previews: some View {
        DumpErasedOpaque()
    }
}
