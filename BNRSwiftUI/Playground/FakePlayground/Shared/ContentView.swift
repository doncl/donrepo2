//
//  ContentView.swift
//  Shared
//
//  Created by Don Clore on 7/19/21.
//

import SwiftUI

struct ContentView: View {
//    var body: some View {
//      // 1. Gets 320 points of space
//      HStack {
//        // 2. Subtracts "fixed size" views
//        Text("Hello")
//
//        // 3. Distributes remaining flexible space
//        Circle()
//          .fill(Color.green)
//        Circle()
//          .fill(Color.orange)
//      }
//      .font(.title)
//    }
//  var body: some View {
//    Text("Hello, world!")
//      .border(Color.green, width: 2)
//      .frame(width: 200, height: 200)
//      .border(Color.red, width: 2)
//  }
  
//  var body: some View {
//    Text("Hello, world")
//      .font(.title)
//      .frame(width: 300, height: 300, alignment: .leading)
  
//  var body: some View {
//    Text("Hello, world!")
//      .border(Color.red, width: 1)
//      .frame(maxWidth: .infinity, maxHeight: .infinity)
//      .border(Color.red, width: 1)
  

  var stack: some View {
    VStack {
      Text("One")
        .border(Color.blue, width: 1)
      Text("Two")
//        .frame( maxWidth: .infinity, maxHeight: .infinity)
        .border(Color.red, width: 1)
    }
    .frame(maxHeight: .infinity)
    .border(Color.purple, width: 1)

  }
  
  var crazyStack: some View {
    HStack {
      Text("\"Here's to the crazy ones, the misfits, the rebels.\"")
        .layoutPriority(1)

      Text("Steve Jobs")

    }
    .lineLimit(1)
  }

//  // 1. Offers a size of 320 width
//  var body: some View {
//    VStack {
//      Text("Here is a quote and it's somewhat long 12345 dfg dfg dfg.")
//      Spacer()
//        .frame(height: 100)
//      Text("--- Christian Keur")
//    }
//    .lineLimit(1)
//  }
  
  var body: some View {
    ZStack {
      Circle()
        .fill(Color.yellow)
      
      Text("Circle")
        .font(.largeTitle)
//        .foregroundColor(Color.white)
        .frame(maxWidth: .infinity, alignment: .leading)
        .padding()
      
      HStack {
        Text("Circle")
          .font(.largeTitle)
//        Spacer()
      }
    }
  }
  
//  var body: some View {
//    VStack {
//      Text("Hello")
//      Spacer()
//      Text("There")
//    }
//    .border(Color.red, width: 2)
//  }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
