//
//  TabsAndRepeatingContent.swift
//  FakePlayground (iOS)
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct TabsAndRepeatingContent: View {
  var body: some View {
    VStack {
      ForEach(1 ..< 4) { i in
        Text("\(i)")
      }
    }
  }
}

struct TabsAndRepeatingContent_Previews: PreviewProvider {
  static var previews: some View {
    TabsAndRepeatingContent()
  }
}
