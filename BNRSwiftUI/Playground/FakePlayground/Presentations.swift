//
//  Presentations.swift
//  FakePlayground (iOS)
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct Presentations: View {
  @State var isPresented = false
  
  var body: some View {
    VStack {
      Text("One")
      Text("Two")
      Button("Present") {
        isPresented = true
      }
      .popover(isPresented: $isPresented) {
        SheetView()
      }
    }
  }
}

struct SheetView: View {
  @Environment(\.presentationMode) @Binding var presentationMode: PresentationMode
  
  var body: some View {
    ZStack {
      Color.yellow.ignoresSafeArea()
      Button("Dismiss") {
        presentationMode.dismiss()
      }
    }
  }
}

struct Presentations_Previews: PreviewProvider {
  static var previews: some View {
      Presentations()
  }
}
