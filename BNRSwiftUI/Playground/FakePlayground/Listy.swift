//
//  Listy.swift
//  FakePlayground (iOS)
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct Listy: View {
  var body: some View {
    List {
      Text("Above")
      ForEach(1 ..< 9002) { i in
        Text("\(i)")
      }
      Text("Below")
    }

  }
}

struct Listy_Previews: PreviewProvider {
    static var previews: some View {
        Listy()
    }
}
