//
//  StateAndBindingView.swift
//  FakePlayground (iOS)
//
//  Created by Don Clore on 7/19/21.
//

import SwiftUI

struct StateAndBindingView: View {
  var body: some View {
    Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
  }
}

struct StateAndBindingView_Previews: PreviewProvider {
  static var previews: some View {
    StateAndBindingView()
  }
}
