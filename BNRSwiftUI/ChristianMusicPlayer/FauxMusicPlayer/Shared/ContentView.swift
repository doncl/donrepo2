//
//  ContentView.swift
//  Shared
//
//  Created by Don Clore on 7/20/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
      MusicPlayer()
    }
}

struct MusicPlayer: View {
  @State var playbackProgress = 50.0
  
  var body: some View {
    VStack() {
      Capsule()
        .fill(Color.gray)
        .frame(width: 60, height: 10)
        .padding(.bottom, 30)
      
      AlbumArtView()
      
      HStack {
        VStack(alignment: .leading) {
          Text("Song")
            .bold()
          Text("Album234234234")
        }
        Spacer()
        
        Button {} label: {
          Image(systemName: "ellipsis.circle.fill")
            .font(.title2)
        }
        
      }
      Slider(value: $playbackProgress, in: 0...100)
      Spacer()
    }
    .padding()
  }
}

struct AlbumArtView: View {
  var body: some View {
    Rectangle()
      .fill(Color.blue)
      .aspectRatio(1.0, contentMode: .fit)
      .cornerRadius(10)
      .shadow(radius: 5)
  }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

