//
//  CounterApp.swift
//  Shared
//
//  Created by Don Clore on 7/19/21.
//

import SwiftUI

@main
struct CounterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
