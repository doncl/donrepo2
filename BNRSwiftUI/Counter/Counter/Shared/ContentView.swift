//
//  ContentView.swift
//  Shared
//
//  Created by Don Clore on 7/19/21.
//

import SwiftUI


struct ContentView: View {
  @State var chickenCount: Int = 12
  @State var myTextFieldText = ""
  @State var volume = 50.0
  @State var color = Color.green
  
  var body: some View {
    VStack {
      TextField("Placeholder", text: $myTextFieldText)
      Slider(value: $volume, in: 0...100)
      Text("Volume: \(volume)")
      ColorPicker("Background", selection: $color)
    }
    .font(.largeTitle)
    .padding()
    .background(color)
  }
}

struct MyStepper: View {
  var title: String
  @Binding var value: Int
  
  var body: some View {
    print("My Stepper body is being computed")
    return HStack {
      Text(title)
      Text("(\(value))")
      Spacer()
      Button("-") {
        value -= 1
      }
      Button("+") {
        value += 1
      }
    }
    .font(.title)
  }
  
  init(_ title: String, value: Binding<Int>) {
    self._value = value
    self.title = title
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}
