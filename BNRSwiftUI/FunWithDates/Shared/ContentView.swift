//
//  ContentView.swift
//  Shared
//
//  Created by Don Clore on 7/21/21.
//

import SwiftUI

struct ContentView: View {
  let valueOfPie = 3.14
  
  var body: some View {
    Text(
      valueOfPie.formatted(.currency)
    )
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
      ContentView()
  }
}
