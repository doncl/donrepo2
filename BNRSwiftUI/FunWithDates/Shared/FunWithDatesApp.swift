//
//  FunWithDatesApp.swift
//  Shared
//
//  Created by Don Clore on 7/21/21.
//

import SwiftUI

@main
struct FunWithDatesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
