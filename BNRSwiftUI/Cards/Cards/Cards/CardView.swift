//
//  CardView.swift
//  Cards
//
//  Created by Don Clore on 7/18/21.
//

import SwiftUI

struct CardView: View, Identifiable {
  @Environment(\.upperCaseVal) var isUpperCased
  @Environment(\.accessibilityReduceMotion) var reduceMotion
  
  var card: Card
  var id = UUID()
  @State var isBackVisible = false
  
  var degrees: Double {
    isBackVisible ? 180 : 0
  }

  var body: some View {
    ZStack {
      ZStack {
        Text(isUpperCased ? card.back.uppercased() : card.back)
          .bold()
          .scaleEffect(x: -1)
          .opacity(isBackVisible ? 1 : 0)
          
        Text(isUpperCased ? card.front.uppercased() : card.front)
          .bold()
          .opacity(isBackVisible ? 0 : 1)
      }
      .font(.system(size: 24))
      .multilineTextAlignment(.center)
      
      ZStack {
       Button {
        if reduceMotion {
          isBackVisible.toggle()
        } else {
          withAnimation {
            isBackVisible.toggle()
          }
        }
       } label: {

        Image(systemName: "arrow.left.arrow.right.circle.fill")
          .font(.system(size: 36))
        }
      }
      .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottomTrailing)
    }
    .padding()
    .frame(width: 300, height: 225)
    .background(isBackVisible ? Color.yellow : card.color)
    .cornerRadius(10)
    .shadow(radius: 10)
    .padding()
    .rotation3DEffect(
      .degrees(degrees), axis: (x: 0.0, y: 1.0, z: 0.0)
    )
    .aspectRatio(4.3, contentMode: .fit)
  }
}

struct CardView_Previews: PreviewProvider {
  static var cardViews: [CardView] = [
    CardView(card: Card(front: "What is 7+7?", back: "14")),
    CardView(card: Card(front: "What is the airspeed velocity of an unladen swallow?", back: "African or European?")),

  ]
    static var previews: some View {
      ForEach(cardViews) { cardView in
        cardView 
      }
    }
}
