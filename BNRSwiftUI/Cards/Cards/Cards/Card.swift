//
//  Card.swift
//  Cards
//
//  Created by Don Clore on 7/18/21.
//

import Foundation
import SwiftUI

struct Card: Identifiable {
  var front: String
  var back: String
  var color: Color = Color.orange
  var id = UUID()
}
