//
//  ContentView.swift
//  Cards
//
//  Created by Don Clore on 7/18/21.
//

import SwiftUI

struct ContentView: View {
  @State var isShowingCreateCardView = false
  
  @State var cards: [Card] = [
//    Card(front: "What is 7+7?", back: "14"),
//    Card(front: "What is the airspeed velocity of an unladen swallow?", back: "African or European?"),
//    Card(front: "From what is cognac made?", back: "Grapes"),
  ]
  
  var body: some View {
    ZStack {
      CardDeckView(cards: cards)
      
      ZStack {
        Button {
          isShowingCreateCardView = true
        } label: {
          Image(systemName: "plus")
            .font(.headline)
        }
        .padding()
        .background(Color.orange)
        .clipShape(Circle())
        .padding([.top, .trailing])
      }
      .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
    }
    .sheet(isPresented: $isShowingCreateCardView, onDismiss: {
      print("isShowingCreateCardView = \(self.isShowingCreateCardView)")
    }, content: {
      CreateCardView(onCreate: { card in
        cards.append(card)
      })
    })
    .myUpperCaseVal(true)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    ContentView()
  }
}

struct UpperCaseKey: EnvironmentKey {
  static let defaultValue: Bool = false
}

extension EnvironmentValues {
  var upperCaseVal: Bool {
    get {
      return self[UpperCaseKey.self]
    }
    set {
      self[UpperCaseKey.self] = newValue
    }
  }
}
extension View {
  func myUpperCaseVal(_ upperCaseVal: Bool) -> some View {
    environment(\.upperCaseVal, upperCaseVal)
  }
}
