//
//  CardDeckView.swift
//  Cards
//
//  Created by Don Clore on 7/18/21.
//

import SwiftUI

struct CardDeckView: View {
  var cards: [Card]
  
  var body: some View {
    if cards.count > 0 {
      TabView {
        ForEach(cards) { card in
          CardView(card: card)
            .environment(\.upperCaseVal, true)
        }
      }
      .tabViewStyle(PageTabViewStyle())
      .background(Color.gray.ignoresSafeArea())
    } else {
      Text("Press + button to add card")
    }
  }
}

struct CardDeckView_Previews: PreviewProvider {
  static let cards = [
    Card(front: "What is 7+7?", back: "14"),
    Card(front: "What is the airspeed velocity of an unladen swallow?", back: "African or European?"),
    Card(front: "From what is cognac made?", back: "Grapes"),
  ]

  static var previews: some View {    
    CardDeckView(cards: cards)
  }
}
