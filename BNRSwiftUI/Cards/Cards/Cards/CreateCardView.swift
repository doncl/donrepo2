//
//  CreateCardView.swift
//  Cards
//
//  Created by Don Clore on 7/18/21.
//

import SwiftUI

struct CreateCardView: View {
  @Environment(\.presentationMode) @Binding var presentationMode: PresentationMode
  
  @State private var front = ""
  @State private var back = ""
  @State private var cardColor = Color.orange
  
  var isValid: Bool {
    !front.isEmpty && !back.isEmpty
  }
  
  var onCreate: (Card) -> Void
  
  var body: some View {
    Form {
      Section(header: Text("Card Details")) {
        TextField("Front", text: $front)
        TextField("Back", text: $back)
        ColorPicker("Card Color", selection: $cardColor)
      }
      Section {
        Button("Create") {
          let card = Card(front: front, back: back, color: cardColor)
          onCreate(card)
          presentationMode.dismiss()
        }
        .disabled(!isValid)
        
        Button("Cancel") {
          presentationMode.dismiss()
        }
      }
    }
  }
}

struct CreateCardView_Previews: PreviewProvider {
  static var previews: some View {
    CreateCardView(onCreate: { _ in
      
    })
  }
}
