//
//  CardsApp.swift
//  Cards
//
//  Created by Don Clore on 7/18/21.
//

import SwiftUI

@main
struct CardsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
