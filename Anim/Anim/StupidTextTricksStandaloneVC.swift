//
//  ViewController.swift
//  Anim
//
//  Created by Don Clore on 8/12/21.
//

import UIKit
import Foundation

typealias AnimatedCharacter = StupidCharacterTricks.AnimatedCharacter

class StupidTextTricksStandaloneVC: UIViewController {
	enum Constants {
		static let initialFontSize: CGFloat = 48.0
		static let interItemSpace: CGFloat = 4.0
		static let gutters: CGFloat = 16.0
	}

	var richStrings: [NSAttributedString] = []
	
	private let animatedCharFactory = StupidCharacterTricks()
	private let overallLayer: CAShapeLayer = CAShapeLayer()
	private let text: String = "The only thing we have to fear ...is me."
	var translationsComplete: Int = 0
	var simpleScalesComplete: Int = 0
	let randomAnimationsCount: Int = 3
	
	var randomIndices = Set<Int>()
		
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .white
		
		overallLayer.isGeometryFlipped = true
		view.layer.addSublayer(overallLayer)
		animatedCharFactory.generate(fromString: text, withDesiredWidth: view.bounds.width)
		animatedCharFactory.setCharacterLayers(asChildrenOfParentLayer: overallLayer)
	}
		
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		let charactersSize = animatedCharFactory.size
		let x = view.bounds.midX - (charactersSize.width / 2)
		let y = view.bounds.midY - charactersSize.height

		overallLayer.frame = CGRect(x: x, y: y, width: charactersSize.width, height: charactersSize.height)
		animatedCharFactory.setCharacterFramesAndPositions(moveOffScreen: true)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		animatedCharFactory.animateCharsToOriginalPositions(delegate: self)
	}
}

extension StupidTextTricksStandaloneVC: CAAnimationDelegate {
	func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
		guard let index = anim.value(forKey: "layerIndex") as? Int else {
			return
		}
		guard let animatedChar = animatedCharFactory.getChar(forIndex: index) else {
			return
		}
		let layer = animatedChar.glyph
		let originalPosition = animatedChar.originalPosition
		layer.position = originalPosition
		layer.removeAllAnimations()
		
		if let animationName = anim.value(forKey: "animationName") as? String {
			switch animationName {
				case "xTranslate":
					translationsComplete += 1
				case "simpleScale":
					simpleScalesComplete += 1
				default:
					break
			}
		}
		if translationsComplete == animatedCharFactory.length {
			translationsComplete = 0
			animatedCharFactory.doRandomSimpleScaling(delegate: self)
		}
		
		if simpleScalesComplete == randomAnimationsCount {
			simpleScalesComplete = 0
			animatedCharFactory.doRandomScaleAndSpin()
		}
	}
	
	private func reinitRandomIndices() {
		randomIndices.removeAll()
		for i in 0..<animatedCharFactory.length {
			randomIndices.insert(i)
		}
	}	
}
