//
//  AnimatedCharacter.swift
//  Anim
//
//  Created by Don Clore on 8/14/21.
//

import Foundation
import UIKit
import AVFoundation


extension StupidCharacterTricks {
	class AnimatedCharacter {
		let glyph: CAShapeLayer
		var yOffset: CGFloat = 0
		var width: CGFloat = 0
		var originalPosition: CGPoint = CGPoint.zero
		var color: UIColor
		
		init(glyph: CAShapeLayer, color: UIColor, yOffset: CGFloat, width: CGFloat) {
			self.color = color
			self.glyph = glyph
			self.yOffset = yOffset
			self.width = width
		}
		
		func animateToOriginalPositionInVideo(index: Int,
											  quantum: Double,
											  withDuration duration: CMTime,
											  andDelegate delegate: CAAnimationDelegate) {
			
			let timeInterval: TimeInterval = CMTimeGetSeconds(duration)
			
			let opacity = CAKeyframeAnimation(keyPath: "opacity")
			opacity.beginTime = AVCoreAnimationBeginTimeAtZero
			opacity.duration = timeInterval
			opacity.keyTimes = [0.000001, 0.000002]
			opacity.values = [0, 1]
			opacity.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
			opacity.isRemovedOnCompletion = false
			glyph.add(opacity, forKey: nil)
			
			let animation = CAKeyframeAnimation(keyPath: "position")
			animation.beginTime = AVCoreAnimationBeginTimeAtZero
			animation.duration = timeInterval
			let oldPosition = NSValue(cgPoint: glyph.position)
			let newPosition = NSValue(cgPoint: originalPosition)
			animation.values = [oldPosition, newPosition]
			let firstKeyTime: Double = Double(index + 1) * quantum
			let secondKeyTime: Double = firstKeyTime + quantum
			animation.keyTimes = [NSNumber(value: firstKeyTime), NSNumber(value: secondKeyTime)]
			animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
			animation.isRemovedOnCompletion = true
			animation.autoreverses = false
			animation.delegate = delegate
			animation.setValue(index, forKey: "layerIndex")
			animation.setValue("xTranslate", forKey: "animationName")
			glyph.add(animation, forKey: "\(index)")
		}
		
		func animateToOriginalPosition(index: Int, delegate: CAAnimationDelegate) {
			let animation = CABasicAnimation(keyPath: "position")
			animation.beginTime = CACurrentMediaTime() + TimeInterval(TimeInterval(index) * 0.1)
			animation.duration = 0.4
			let oldPosition = glyph.position
			let newPosition = originalPosition
			animation.fromValue = NSValue(cgPoint: oldPosition)
			animation.toValue = NSValue(cgPoint: newPosition)
			animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
			animation.isRemovedOnCompletion = true
			animation.autoreverses = false
			animation.delegate = delegate
			animation.setValue(index, forKey: "layerIndex")
			animation.setValue("xTranslate", forKey: "animationName")
			glyph.add(animation, forKey: "\(index)")
		}
		
		func simpleScale(ordinalPosition: Int, index: Int, delegate: CAAnimationDelegate) {
			let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
			scaleAnimation.fromValue = 0.8
			scaleAnimation.toValue = 4
			scaleAnimation.duration = 0.5
			scaleAnimation.repeatCount = 6
			scaleAnimation.autoreverses = true
			scaleAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
			scaleAnimation.beginTime = CACurrentMediaTime() + TimeInterval(ordinalPosition) * 0.4
			scaleAnimation.isRemovedOnCompletion = true
			scaleAnimation.setValue("simpleScale", forKey: "animationName")
			scaleAnimation.setValue(index, forKey: "layerIndex")
			scaleAnimation.delegate = delegate
			glyph.add(scaleAnimation, forKey: nil)
		}
		
		func scaleInVideo(ordinalPosition: Int, index: Int, duration: CMTime,
								 delegate: CAAnimationDelegate? = nil) {
			
			let timeInterval: TimeInterval = CMTimeGetSeconds(duration)
			
			let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
			scaleAnimation.keyTimes = [0.0005, 0.0007, 0.0009, 0.0011, 0.0013, 0.0015, 0.0017, 0.0019]
			scaleAnimation.values = [1, 4, 0.8, 4, 0.8, 4, 0.8, 1,]
			scaleAnimation.duration = timeInterval
			scaleAnimation.delegate = delegate
			scaleAnimation.setValue(index, forKey: "layerIndex")
			scaleAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
			scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero
			scaleAnimation.isRemovedOnCompletion = true

			glyph.add(scaleAnimation, forKey: nil)
		}
		
		func spinInVideo(ordinalPosition: Int, index: Int, duration: CMTime) {
			let timeInterval: TimeInterval = CMTimeGetSeconds(duration)
			
			let rotateAnimation = CAKeyframeAnimation(keyPath: "transform.rotation.z")
			guard let currentVal = glyph.value(forKeyPath: "transform.rotation.z") as? NSNumber else {
				return
			}
			let toValue = NSNumber(value: Float(CGFloat.pi) * 2)

			rotateAnimation.isAdditive = true
			rotateAnimation.keyTimes = [0.0019, 0.0021, 0.0023, 0.0025, 0.0027, 0.0029, 0.0031, 0.0033]
			rotateAnimation.values = [currentVal, toValue, currentVal, toValue, currentVal, toValue, currentVal, currentVal]
			rotateAnimation.fillMode = CAMediaTimingFillMode.forwards
			rotateAnimation.beginTime = AVCoreAnimationBeginTimeAtZero
			rotateAnimation.duration = timeInterval
			rotateAnimation.setValue(index, forKey: "layerIndex")
			rotateAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
			rotateAnimation.isRemovedOnCompletion = true
			glyph.add(rotateAnimation, forKey: nil)
		}
		
		func scaleAndSpin(ordinalPosition: Int, index: Int, delegate: CAAnimationDelegate? = nil) {
			let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
			scaleAnimation.fromValue = 0.8
			scaleAnimation.toValue = 4
			scaleAnimation.duration = 0.5
			scaleAnimation.repeatCount = 6
			scaleAnimation.autoreverses = true
			scaleAnimation.delegate = delegate
			scaleAnimation.setValue(index, forKey: "layerIndex")
			scaleAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
			scaleAnimation.beginTime = CACurrentMediaTime() + TimeInterval(ordinalPosition) * 0.4
			scaleAnimation.isRemovedOnCompletion = true

			let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
			if let currentVal = glyph.value(forKeyPath: "transform.rotation.z") as? NSValue {
				rotateAnimation.fromValue = currentVal
			}
			rotateAnimation.isAdditive = true
			rotateAnimation.toValue = CGFloat.pi * 2
			rotateAnimation.fillMode = CAMediaTimingFillMode.forwards
			rotateAnimation.beginTime = CACurrentMediaTime() + TimeInterval(ordinalPosition) * 0.4 + 0.5
			rotateAnimation.duration = 0.8
			rotateAnimation.repeatCount = 3
			rotateAnimation.autoreverses = true
			rotateAnimation.setValue(index, forKey: "layerIndex")
			rotateAnimation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
			rotateAnimation.isRemovedOnCompletion = true

			glyph.add(scaleAnimation, forKey: nil)
			glyph.add(rotateAnimation, forKey: nil)
		}
	}
}
