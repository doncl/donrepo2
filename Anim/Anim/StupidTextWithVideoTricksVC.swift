//
//  StupidTextWithVideoTricksVC.swift
//  Anim
//
//  Created by Don Clore on 8/13/21.
//

import UIKit
import AVKit
import AVFoundation
import Photos

class StupidTextWithVideoTricksVC: UIViewController {
	let animKeyName: String = "animationName"
	
	private let exportBtn = UIButton(type: .custom)
	
	private let animatedCharFactory = StupidCharacterTricks()
	
	enum AnimationTypes: String {
		case scale = "scale"
		case opacity = "opacity"
	}
	
	class OverlayLayer: CALayer {
		override var bounds: CGRect {
			didSet {
				print("\(#function) - bounds \(bounds)")
			}
		}
		
		override var frame: CGRect {
			didSet {
				print("\(#function) - frame \(frame)")
			}
		}
	}
	
	var videoSize: CGSize?
	var playerLayer: AVPlayerLayer?
	var player: AVPlayer?
	var syncLayer: AVSynchronizedLayer?
	var overlayLayer: OverlayLayer?
	var textLayer: CATextLayer?
	var duration: CMTime?
	var savedComposition: AVMutableComposition?
	
	var factorySetup = false
	
	var text: String {
		return "The only thing we have to fear....is me"
	}
	
	let cover: UIView = UIView()
	
	private var playerItemContext = 0
	private var playerContext = 0
	
	lazy var spinner: UIActivityIndicatorView = {
	    let a = UIActivityIndicatorView(style: .large)
	    a.translatesAutoresizingMaskIntoConstraints = false
	    return a
	}()
	
	deinit {
		NotificationCenter.default.removeObserver(self)
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		view.backgroundColor = .white
		cover.translatesAutoresizingMaskIntoConstraints = false
		spinner.translatesAutoresizingMaskIntoConstraints = false
		exportBtn.translatesAutoresizingMaskIntoConstraints = false
		cover.backgroundColor = .white
		view.addSubview(cover)
		view.addSubview(spinner)
		view.addSubview(exportBtn)
		NSLayoutConstraint.activate([
			cover.leftAnchor.constraint(equalTo: view.leftAnchor),
			cover.topAnchor.constraint(equalTo: view.topAnchor),
			cover.rightAnchor.constraint(equalTo: view.rightAnchor),
			cover.bottomAnchor.constraint(equalTo: view.bottomAnchor),
			
			spinner.centerXAnchor.constraint(equalTo: view.centerXAnchor),
			spinner.centerYAnchor.constraint(equalTo: view.centerYAnchor),
			
			exportBtn.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 16),
			exportBtn.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16),
			exportBtn.widthAnchor.constraint(equalToConstant: 100.0),
			exportBtn.heightAnchor.constraint(equalToConstant: 40.0),
		])
		
		exportBtn.layer.cornerRadius = 5
		exportBtn.addTarget(self, action: #selector(StupidTextWithVideoTricksVC.exportBtnTouched(_:)), for: .touchUpInside)
		
		guard let movieFile = Bundle.main.url(forResource: "PianoRecital", withExtension: "mp4") else {
		  return
		}
		let asset = AVURLAsset(url: movieFile)

		self.doStuffAfterAssetLoaded(asset: asset, movieFile: movieFile)
		exportBtn.backgroundColor = .purple
		exportBtn.setTitle("EXPORT", for: .normal)
		exportBtn.setTitleColor(.white, for: .normal)
	}
	
	@objc func exportBtnTouched(_ sender: UIButton) {
		print("\(#function)")
		if let player = player {
			player.rate = 0
		}

		let id = kCMPersistentTrackID_Invalid
		
		guard let movieFile = Bundle.main.url(forResource: "PianoRecital", withExtension: "mp4") else {
		  return
		}
		let asset = AVURLAsset(url: movieFile)
		
		let composition = AVMutableComposition()
		self.savedComposition = composition
		
		guard let compositionTrack = composition.addMutableTrack(withMediaType: .video, preferredTrackID: id),
			  let assetTrack = asset.tracks(withMediaType: AVMediaType.video).first else {
			return
		}
		
		do {
			let timeRange = CMTimeRange(start: .zero, duration: asset.duration)
			try compositionTrack.insertTimeRange(timeRange, of: assetTrack, at: .zero)
			
			if let audioAssetTrack = asset.tracks(withMediaType: .audio).first,
			   let compositionAudioTrack = composition.addMutableTrack(withMediaType: .audio, preferredTrackID: id) {
				try compositionAudioTrack.insertTimeRange(timeRange, of: audioAssetTrack, at: .zero)
			}
		} catch let error {
			print(error)
			return
		}
		
		compositionTrack.preferredTransform = assetTrack.preferredTransform
		let videoInfo = orientation(from: assetTrack.preferredTransform)
		
		if videoInfo.isPortrait {
			videoSize = CGSize(width: assetTrack.naturalSize.height, height: assetTrack.naturalSize.width)
		} else {
			videoSize = assetTrack.naturalSize
		}
		
		guard let videoSize = videoSize, let duration = duration else {
			return
		}
				
		let newFactory = StupidCharacterTricks()
		let scalingFactor = videoSize.width / view.bounds.width
		newFactory.generate(fromString: text, withDesiredWidth: videoSize.width, andFontScalingFactor: scalingFactor)
		let charactersSize = newFactory.size
		print("\(#function) characterSize = \(charactersSize)")

		let x = videoSize.width / 2 - (charactersSize.width / 2)
		let y = videoSize.height / 2 - charactersSize.height
		
		let freshOverlayLayer = OverlayLayer()
		freshOverlayLayer.frame = CGRect(origin: .zero, size: videoSize)
		let characterParentLayer = CALayer()
		freshOverlayLayer.addSublayer(characterParentLayer)
		characterParentLayer.frame = CGRect(x: x, y: y, width: charactersSize.width, height: charactersSize.height)
		newFactory.setCharacterLayers(asChildrenOfParentLayer: characterParentLayer)
		newFactory.setCharacterFramesAndPositions(moveOffScreen: true)
		newFactory.animateCharsToOriginalPositionsInVideo(withDuration: duration, delegate: self)
		newFactory.scaleCharsInVideo(withDuration: duration, delegate: self)
		newFactory.spinInVideo(withDuration: duration)
		
//		add(text: text, to: freshOverlayLayer, videoSize: videoSize)
				
		let videoLayer = CALayer()
		
		videoLayer.frame = CGRect(origin: .zero, size: videoSize)
		
		let outputLayer = CALayer()
		outputLayer.frame = CGRect(origin: .zero, size: videoSize)
		
		outputLayer.addSublayer(videoLayer)
		outputLayer.addSublayer(freshOverlayLayer)
		
		let videoComposition = AVMutableVideoComposition()
		videoComposition.renderSize = videoSize
		videoComposition.frameDuration = CMTime(value: 1, timescale: 30)
		videoComposition.animationTool = AVVideoCompositionCoreAnimationTool(postProcessingAsVideoLayer: videoLayer, in: outputLayer)
		
		let instruction = AVMutableVideoCompositionInstruction()
		instruction.timeRange = CMTimeRange(start: CMTime.zero, duration: composition.duration)
		videoComposition.instructions = [instruction]
		
		let layerInstruction = compositionLayerInstruction(for: compositionTrack, assetTrack: assetTrack)
		instruction.layerInstructions = [layerInstruction]
		
		guard let export = AVAssetExportSession(asset: composition, presetName: AVAssetExportPresetHighestQuality) else {
			print("Could not create export")
			return
		}
		
		let videoName = UUID().uuidString
		let exportURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(videoName).appendingPathExtension("mov")
		
		export.videoComposition = videoComposition
		export.outputFileType = .mov
		export.outputURL = exportURL
		
		export.exportAsynchronously {
		  DispatchQueue.main.async { [weak self] in
			guard let self = self else { return }
			switch export.status {
			  case .completed:
				self.saveToCameraRoll(exportURL: exportURL)
			  default:
				print("Something went wrong during export")
				print(export.error ?? "unknown error")
				break
			}
		  }
		}
	}
	
	private func saveToCameraRoll(exportURL: URL) {
		saveVideoToAlbum(outputURL: exportURL) { [weak self] result in
			switch result {
				case .success:
					print("success!")
					guard let self = self else { return }
					self.successDlg()
				case .failure(let error):
					print(error)
			}
		}
	}
	
	private func successDlg() {
		let ac = UIAlertController(title: "Success!", message: "Vid saved to camera roll", preferredStyle: .alert)
		let ok = UIAlertAction(title: "OK", style: .default)
		ac.addAction(ok)
		present(ac, animated: true)
	}
	
	func requestAuthorization(completion: @escaping ()->Void) {
			if PHPhotoLibrary.authorizationStatus() == .notDetermined {
				PHPhotoLibrary.requestAuthorization { (status) in
					DispatchQueue.main.async {
						completion()
					}
				}
			} else if PHPhotoLibrary.authorizationStatus() == .authorized{
				completion()
			}
		}



	func saveVideoToAlbum(outputURL: URL, completion: @escaping (Result<Bool, Error>) ->()) {
		requestAuthorization {
			PHPhotoLibrary.shared().performChanges({
				let request = PHAssetCreationRequest.forAsset()
				request.addResource(with: .video, fileURL: outputURL, options: nil)
			}) { (result, error) in
				DispatchQueue.main.async {
					if let error = error {
						completion(.failure(error))
					} else {
						completion(.success(true))
					}
				}
			}
		}
	}
	
	private func compositionLayerInstruction(for track: AVCompositionTrack, assetTrack: AVAssetTrack)
		-> AVMutableVideoCompositionLayerInstruction {
		
	  let instruction = AVMutableVideoCompositionLayerInstruction(assetTrack: track)
	  let transform = assetTrack.preferredTransform

	  instruction.setTransform(transform, at: .zero)

	  return instruction
	}
	
	private func doStuffAfterAssetLoaded(asset: AVAsset, movieFile: URL) {
		
		self.duration = asset.duration
				
		self.player = AVPlayer(url: movieFile)
		self.playerLayer = AVPlayerLayer(player: player)
		guard let playerLayer = playerLayer, let player = player, let currentItem = player.currentItem else {
			return
		}
		
		view.layer.addSublayer(playerLayer)
		playerLayer.frame = view.bounds
		
		currentItem.addObserver(self, forKeyPath: #keyPath(AVPlayerItem.status), options: [.old, .new],
								context: &self.playerItemContext)
		
		player.addObserver(self, forKeyPath: #keyPath(AVPlayer.timeControlStatus), options: [.old, .new],
						   context: &self.playerContext)

		
		self.syncLayer = AVSynchronizedLayer(playerItem: currentItem)
		guard let syncLayer = syncLayer else {
			return
		}

		syncLayer.frame = view.bounds
		syncLayer.zPosition = 10
		syncLayer.accessibilityLabel = "syncLayer"
		playerLayer.accessibilityLabel = "playerLayer"
		playerLayer.addSublayer(syncLayer)
		addOverlay()
		view.bringSubviewToFront(cover)
		spinner.startAnimating()
		view.bringSubviewToFront(spinner)
		view.bringSubviewToFront(exportBtn)
	}
					
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		
		
		//if let textLayer = textLayer, let attr = textLayer.string as? NSAttributedString {
		//let charactersSize = attr.size()
		
		//textLayer.frame = overlayLayer.bounds
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		guard !factorySetup else {
			return
		}
		guard let playerLayer = playerLayer, let syncLayer = syncLayer, let overlayLayer = overlayLayer,
			  let _ = player, let _ = duration else {
			return
		}
		
		playerLayer.frame = view.bounds
		syncLayer.frame = view.bounds

		factorySetup = true
		animatedCharFactory.generate(fromString: text, withDesiredWidth: view.bounds.width)
		let charactersSize = animatedCharFactory.size

		let x = view.bounds.midX - (charactersSize.width / 2)
		let y = view.bounds.midY - charactersSize.height
		
		overlayLayer.frame = CGRect(x: x, y: y, width: charactersSize.width, height: charactersSize.height)

		cover.frame = view.bounds
		view.bringSubviewToFront(cover)
		animatedCharFactory.setCharacterLayers(asChildrenOfParentLayer: overlayLayer)
		animatedCharFactory.setCharacterFramesAndPositions(moveOffScreen: true)
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		if context == &playerItemContext {
			guard let item = object as? AVPlayerItem, let player = player, let overlayLayer = overlayLayer else {
			  return
			}
			if item.status == .readyToPlay {
				DispatchQueue.main.async {
					player.play()
				}
				item.removeObserver(self, forKeyPath: #keyPath(AVPlayerItem.status))
			}

		} else if context == &playerContext {
			guard let change = change,
			  let observedPlayer = object as? AVPlayer,
			  let kind = change[NSKeyValueChangeKey.kindKey] as? NSNumber else {
			  return
			}
			print("\(#function) \(kind.intValue)")
			guard kind.intValue == 1 else { // setting change
			  return
			}
												
			if observedPlayer.timeControlStatus == .playing {
				// it is starting up
				DispatchQueue.main.async { [weak self] in
					guard let self = self else { return }
					self.playerStarted()
				}
				observedPlayer.removeObserver(self, forKeyPath: #keyPath(AVPlayer.timeControlStatus))
			}
		} else {
			super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
		}
	}
	
	func playerStarted() {
		assert(Thread.isMainThread)
		spinner.stopAnimating()
		guard let duration = duration, let overlayLayer = overlayLayer, let _ = playerLayer else {
			return
		}

		DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) { [weak self] in
			guard let self = self else { return }
			UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
				guard let self = self else { return }
				self.cover.alpha = 0
			}, completion: { [weak self] _ in
				guard let self = self else { return }
				self.cover.isHidden = true
				self.cover.removeFromSuperview()
				overlayLayer.setNeedsLayout()
				overlayLayer.layoutIfNeeded()
				overlayLayer.setNeedsDisplay()
				self.animatedCharFactory.animateCharsToOriginalPositionsInVideo(withDuration: duration, delegate: self)
				self.animatedCharFactory.scaleCharsInVideo(withDuration: duration, delegate: self)
				self.animatedCharFactory.spinInVideo(withDuration: duration)
			})
		}
	}
	
	private func addOverlay() {
		guard let syncLayer = syncLayer, let _ = playerLayer else {
			return
		}

		self.overlayLayer = OverlayLayer()
		guard let overlayLayer = overlayLayer else {
			return
		}

		overlayLayer.accessibilityLabel = "overlayLayer"
		overlayLayer.isGeometryFlipped = true
		syncLayer.addSublayer(overlayLayer)
		//add(text: text, to: overlayLayer, videoSize: playerLayer.bounds.size)
	}
	
	
	private func orientation(from transform: CGAffineTransform) -> (orientation: UIImage.Orientation, isPortrait: Bool) {
	  var assetOrientation = UIImage.Orientation.up
	  var isPortrait = false
	  if transform.a == 0 && transform.b == 1.0 && transform.c == -1.0 && transform.d == 0 {
		assetOrientation = .right
		isPortrait = true
	  } else if transform.a == 0 && transform.b == -1.0 && transform.c == 1.0 && transform.d == 0 {
		assetOrientation = .left
		isPortrait = true
	  } else if transform.a == 1.0 && transform.b == 0 && transform.c == 0 && transform.d == 1.0 {
		assetOrientation = .up
	  } else if transform.a == -1.0 && transform.b == 0 && transform.c == 0 && transform.d == -1.0 {
		assetOrientation = .down
	  }
	  
	  return (assetOrientation, isPortrait)
	}
	
	private func add(text: String, to overlayLayer: CALayer, videoSize: CGSize) {
		let attribtedString = NSAttributedString(string: text, attributes: [
			NSAttributedString.Key.font: UIFont(name  : "ArialRoundedMTBold", size: 32)!,
			NSAttributedString.Key.foregroundColor: UIColor.systemBlue,
			NSAttributedString.Key.strokeColor: UIColor.white,
			NSAttributedString.Key.strokeWidth: -3,
		])
	  
		self.textLayer = CATextLayer()
		guard let textLayer = textLayer,  let duration = duration else {
			return
		}
		
		let timeInterval = CMTimeGetSeconds(duration)

		textLayer.string = attribtedString
		textLayer.shouldRasterize = true
		textLayer.rasterizationScale = UIScreen.main.scale
		textLayer.backgroundColor = UIColor.clear.cgColor
		textLayer.alignmentMode = CATextLayerAlignmentMode.center
		textLayer.frame = CGRect(x: 0, y: videoSize.height * 0.66, width: videoSize.width, height: 150)

		let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
		scaleAnimation.fromValue = 0.8
		scaleAnimation.toValue = 1.2
		scaleAnimation.duration = 0.5
		scaleAnimation.repeatCount = Float.greatestFiniteMagnitude
		scaleAnimation.autoreverses = true
		scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
		scaleAnimation.beginTime = AVCoreAnimationBeginTimeAtZero
		scaleAnimation.setValue(AnimationTypes.scale.rawValue, forKey: animKeyName)
		scaleAnimation.delegate = self
		scaleAnimation.isRemovedOnCompletion = false
		textLayer.add(scaleAnimation, forKey: "scale")
		
		let animation = CAKeyframeAnimation(keyPath: "opacity")
		animation.values = [0, 0, 1, 1, 0]
		animation.keyTimes = [0, 0.005, 0.01, 0.02, 0.03]
		animation.duration = timeInterval
		animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.linear)
		animation.beginTime = AVCoreAnimationBeginTimeAtZero
		animation.isRemovedOnCompletion = false
		animation.setValue(AnimationTypes.opacity.rawValue, forKey: animKeyName)
		animation.delegate = self
		textLayer.add(animation, forKey: "opacity")

		overlayLayer.addSublayer(textLayer)
	}
}

extension StupidTextWithVideoTricksVC: CAAnimationDelegate {
	func animationDidStart(_ anim: CAAnimation) {
		guard let val = anim.value(forKey: animKeyName) as? String,
			  let type = AnimationTypes(rawValue: val),
			  let _ = textLayer else {
			return
		}
		print("\(#function) animation type \(type.rawValue) started")
	}
	
	func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
		guard let val = anim.value(forKey: animKeyName) as? String,
			  let type = AnimationTypes(rawValue: val),
			  let _ = textLayer else {
			return
		}
		print("\(#function) animation type \(type.rawValue) stopped")
		switch type {
			case .scale:
				break
			case .opacity:
				break
		}
	}
}
