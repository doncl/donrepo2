//
//  StupidCharacterTricks.swift
//  Anim
//
//  Created by Don Clore on 8/14/21.
//

import Foundation
import UIKit
import AVFoundation

class StupidCharacterTricks {
	struct Constants {
		static let initialFontSize: CGFloat = 48.0
		static let interItemSpace: CGFloat = 4.0
		static let gutters: CGFloat = 16.0
	}
	
	private var randomIndices = Set<Int>()
	private let randomAnimationsCount: Int = 3
	
	var attributedString: NSAttributedString {
		return attrStr
	}
	
	var length: Int {
		return attrStr.length
	}

	private var _size: CGSize = .zero {
		didSet {
			print("\(#function) - _size = \(_size)")
		}
	}
	
	var size: CGSize {
		//let sz = attrStr.size()
		let sz = _size
		let cx: CGFloat = sz.width + ((CGFloat(attrStr.length) * Constants.interItemSpace) - 1) + (Constants.gutters * 2)
		return CGSize(width: cx, height: sz.height)
	}
	
	var positionInitialized: Bool = false
	var positionRestored: Bool = false
	
	private var attrStr: NSMutableAttributedString = NSMutableAttributedString() {
		didSet {
			print("wha?")
		}
	}
	private var text: String = ""
	private var animatedChars: [AnimatedCharacter] = []
	private var colors: [UIColor] = []
	private var singleCharRichStrings: [NSAttributedString] = []
	private var portWidth: CGFloat = 1 // avoid divide-by-zero errors
	private var centerY: CGFloat = 1
	
	func setCharacterLayers(asChildrenOfParentLayer parentLayer: CALayer) {
		for char in animatedChars {
			parentLayer.addSublayer(char.glyph)
		}
	}
		
	func generate(fromString string: String,
				  withDesiredWidth desiredWidth: CGFloat,
				  andFontScalingFactor fontScalingFactor: CGFloat = 1) {
		
		guard desiredWidth > 0 else {
			return
		}
		portWidth = desiredWidth
		attrStr = NSMutableAttributedString()
		self.text = string
		let para = NSMutableParagraphStyle()
		para.alignment = NSTextAlignment.natural
		
		for char in text {
			guard let font = getRandomFont(scalingFactor: fontScalingFactor) else {
				continue
			}
			let color = UIColor.random()
			colors.append(color)
			
			let attrs: [NSAttributedString.Key: Any] = [
				NSAttributedString.Key.paragraphStyle: para,
				NSAttributedString.Key.font: font,
				NSAttributedString.Key.foregroundColor: color,
			]

			let attrChar = NSAttributedString(string: String(char), attributes: attrs)
			attrStr.append(attrChar)
		}
    
        let adjustedDesiredwith = desiredWidth - ((CGFloat(string.count) * Constants.interItemSpace) - 1) + (Constants.gutters * 2)
		
		if let tuple = attrStr.sizeFont(forWidth: adjustedDesiredwith) {
			attrStr = tuple.string
		}
		
		for i in 0..<attrStr.length {
			let attributedSubOneChar = attrStr.attributedSubstring(from: NSRange(location: i, length: 1))
			singleCharRichStrings.append(attributedSubOneChar)
		}
		_size = attrStr.size()
		print("\(#function) - _size = \(_size)")
		setPaths()
	}
	
	func setCharacterFramesAndPositions(moveOffScreen: Bool) {
		guard !positionInitialized else {
			return
		}
		positionInitialized = true
		
		let size = attrStr.size()
		let cx: CGFloat = size.width + ((CGFloat(attrStr.length) * Constants.interItemSpace) - 1) + (Constants.gutters * 2)
		var x =  (portWidth / 2) - (cx / 2)
				
		x = 0
				
		for (i, animatedChar) in animatedChars.enumerated() {
			let attrChar = singleCharRichStrings[i]
			let glyphSize = attrChar.size()
			let yOffset = animatedChar.yOffset
			let frame = CGRect(x: x, y: yOffset, width: glyphSize.width, height: glyphSize.height)
			animatedChar.glyph.frame = frame
			x += glyphSize.width + Constants.interItemSpace
			let originalPosition = animatedChar.glyph.position
			animatedChar.originalPosition = originalPosition
			if moveOffScreen {
				animatedChar.glyph.position = CGPoint(x: originalPosition.x + portWidth * 2, y: originalPosition.y)
			}
		}
	}
	
	func animateCharsToOriginalPositions(delegate: CAAnimationDelegate) {
		for (index, animatedChar) in animatedChars.enumerated() {
			animatedChar.animateToOriginalPosition(index: index, delegate: delegate)
		}
	}
	
	func scaleCharsInVideo(withDuration duration: CMTime, delegate: CAAnimationDelegate) {
		reinitRandomIndices()
		
		for i in 0 ..< randomAnimationsCount {
			guard let tuple = getRandomAnimatedChar() else {
				break
			}
			tuple.char.scaleInVideo(ordinalPosition: i, index: tuple.index,
										   duration: duration, delegate: delegate)
		}
	}
	
	func spinInVideo(withDuration duration: CMTime) {
		reinitRandomIndices()
		
		for i in 0 ..< randomAnimationsCount {
			guard let tuple = getRandomAnimatedChar() else {
				break
			}
			tuple.char.spinInVideo(ordinalPosition: i, index: tuple.index, duration: duration)
		}
	}
	
	func doRandomScaleAndSpin() {
		reinitRandomIndices()
		
		for i in 0 ..< randomAnimationsCount {
			guard let tuple = getRandomAnimatedChar() else {
				break
			}
			tuple.char.scaleAndSpin(ordinalPosition: i, index: tuple.index, delegate: nil)
		}
	}
	
	func doRandomSimpleScaling(delegate: CAAnimationDelegate) {
		reinitRandomIndices()
		
		for i in 0 ..< randomAnimationsCount {
			guard let tuple = getRandomAnimatedChar() else {
				break
			}
									
			tuple.char.simpleScale(ordinalPosition: i, index: tuple.index, delegate: delegate)
		}
	}
	
	private func reinitRandomIndices() {
		randomIndices.removeAll()
		for i in 0..<length {
			let attrChar = attrStr.attributedSubstring(from: NSRange(location: i, length: 1))
			if attrChar.string != " " {
				randomIndices.insert(i)
			}
		}
	}
	
	func getRandomAnimatedChar() -> (index: Int, char: AnimatedCharacter)? {
		guard randomIndices.count > 0 else {
			return nil
		}
		let randomIndexIndex = Int.random(in: 0..<randomIndices.count)
		let randomIndex = Array(randomIndices)[randomIndexIndex]
		randomIndices.remove(randomIndex)
		guard let animatedChar = getChar(forIndex: randomIndex) else {
			return nil
		}
		return (index: randomIndex, char: animatedChar)
	}
	
	func animateCharsToOriginalPositionsInVideo(withDuration duration: CMTime, delegate: CAAnimationDelegate) {
		guard positionInitialized, !positionRestored, length > 0 else {
			return
		}
		positionRestored = true
		let quantum = 0.0005 / Double(length)
		
		for (index, animatedChar) in animatedChars.enumerated() {
			animatedChar.animateToOriginalPositionInVideo(index: index, quantum: quantum,
														  withDuration: duration, andDelegate: delegate)
		}
	}
		
	func getChars() -> [AnimatedCharacter] {
		return animatedChars
	}
	
	func getChar(forIndex index: Int) -> AnimatedCharacter? {
		guard index < animatedChars.count else {
			return nil
		}
		return animatedChars[index]
	}
	
	private func getRandomFont(scalingFactor: CGFloat) -> UIFont? {
		let fontFamilies = UIFont.familyNames
		let randomFontFamilyName = fontFamilies[Int.random(in: 0 ..< fontFamilies.count)]
		return UIFont(name: randomFontFamilyName, size: Constants.initialFontSize * scalingFactor)
	}
	
	private func setPaths() {
		for (i, richString) in singleCharRichStrings.enumerated() {
			let tuple = makePath(fromAttributedCharacter: richString)
			let path = tuple.path
			let layer = CAShapeLayer()
			layer.fillColor = colors[i].cgColor
			layer.path = path
			layer.bounds = path.boundingBoxOfPath
			let animatedChar = AnimatedCharacter(glyph: layer, color: colors[i], yOffset: tuple.yOffset, width: tuple.width)
			animatedChars.append(animatedChar)
		}
	}
	
	private func makePath(fromAttributedCharacter attributedCharacter: NSAttributedString)
		-> (path: CGPath, yOffset: CGFloat, width: CGFloat) {
		
		var yOffset: CGFloat = 0
		var width: CGFloat = 0
		let letter = CGMutablePath()
		let line = CTLineCreateWithAttributedString(attributedCharacter)
		let runArray = CTLineGetGlyphRuns(line)
				
		for runIndex in 0 ..< 1 {
			let run: CTRun = unsafeBitCast(CFArrayGetValueAtIndex(runArray, runIndex), to: CTRun.self)
			let dictRef: CFDictionary = CTRunGetAttributes(run)
			let dict: NSDictionary = dictRef as NSDictionary
			let runFont = dict[kCTFontAttributeName as String] as! CTFont

			for runGlyphIndex in 0 ..< CTRunGetGlyphCount(run) {
				let thisGlyphRange = CFRangeMake(runGlyphIndex, 1)
				var glyph = CGGlyph()
				var position = CGPoint.zero
				CTRunGetGlyphs(run, thisGlyphRange, &glyph)
				CTRunGetPositions(run, thisGlyphRange, &position)

				let boundingBox = CTFontGetBoundingRectsForGlyphs(runFont, .default, &glyph, nil, 1)
				yOffset = boundingBox.origin.y
				width = boundingBox.size.width

				guard let pathForGlyph = CTFontCreatePathForGlyph(runFont, glyph, nil) else {
					continue
				}
				let t = CGAffineTransform(translationX: position.x, y: position.y)
				letter.addPath(pathForGlyph, transform: t)
			}
		}
		
		let path = UIBezierPath()
		path.move(to: CGPoint.zero)
		path.append(UIBezierPath(cgPath: letter))
		return (path: path.cgPath, yOffset: yOffset, width: width)
	}
}
