//
//  UIColorExtensions.swift
//  Anim
//
//  Created by Don Clore on 8/14/21.
//

import UIKit

extension UIColor {
	static func random() -> UIColor {
		let color: UIColor

		let red = CGFloat.random(in: 0..<256) / 255
		let green = CGFloat.random(in: 0..<256) / 255
		let blue = CGFloat.random(in: 0..<256) / 255

		//https://stackoverflow.com/questions/2509443/check-if-uicolor-is-dark-or-bright
		let val = ((red * 299) + (green * 587) + (blue * 114) / 1000)
		if val > 750 {
			// Comlete hack, only for demo.
			color = UIColor.black
		} else {
			color = UIColor(red: red, green: green, blue: blue, alpha: 1.0)
		}

		return color
	}
}
