//
//  NSAttributedStringExtensions.swift
//  Anim
//
//  Created by Don Clore on 8/14/21.
//

import UIKit

extension NSAttributedString {
	func sizeFont(forWidth width: CGFloat) -> (fontSize: CGFloat, string: NSMutableAttributedString)? {
		let currentSize = size()
				
		// The following code assumes that, even if all characters have different font families, they are all
		// the same fontsize - we're just finding the font at front of string.
		guard let existingFont = attribute(NSAttributedString.Key.font, at: 0, effectiveRange: nil) as? UIFont else {
			return nil
		}
		
		let existingFontSize = existingFont.pointSize
		if currentSize.width <= width {
			return (fontSize: existingFontSize, string: NSMutableAttributedString(attributedString: self))
		}
		assert(currentSize.width > 0)
		let shrinkBy = width / currentSize.width
		let newFontSize = existingFontSize * shrinkBy
		
		let newString = getNSString(usingNewSize: newFontSize, andOldString: self)
		return (fontSize: newFontSize, string: newString)
	}
	
	private func addResizedCharacter(fromOldAttributedString oldAttr: NSAttributedString,
									 toNewAttributedString newAttr: NSMutableAttributedString,
									 atIndex index: Int,
									 usingNewSize newSize: CGFloat)
	{

		guard let existingFont = oldAttr.attribute(NSAttributedString.Key.font, at: index, effectiveRange: nil) as? UIFont else {
			fatalError("Must set font")
		}

		// Get the old font description, size it up or down to the newsize, and then get the attributes for the attributed
		// substring at this location, and modify them to take the resized font.
		let fd = existingFont.fontDescriptor
		let newFont = UIFont(descriptor: fd, size: newSize)
		let attrSub = oldAttr.attributedSubstring(from: NSRange(location: index, length: 1))
		var newAttrs = attrSub.attributes(at: 0, effectiveRange: nil)
		newAttrs[NSAttributedString.Key.font] = newFont

		let start = oldAttr.string.index(oldAttr.string.startIndex, offsetBy: index)
		let end = oldAttr.string.index(start, offsetBy: 1)
		let char = oldAttr.string[start ..< end]
		let charString = String(char)
		let newAttributedStringForOneCharacter = NSAttributedString(string: charString, attributes: newAttrs)
		newAttr.append(newAttributedStringForOneCharacter)
	}

	private func getNSString(usingNewSize size: CGFloat, andOldString oldNS: NSAttributedString) -> NSMutableAttributedString {
		let newString = NSMutableAttributedString()

		for i in 0 ..< oldNS.length {
			addResizedCharacter(fromOldAttributedString: oldNS, toNewAttributedString: newString, atIndex: i, usingNewSize: size)
		}
		return newString
	}
}
