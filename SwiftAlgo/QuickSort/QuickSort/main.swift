//
//  main.swift
//  QuickSort
//
//  Created by Don Clore on 8/8/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

//// NAIVE QUICKSORT
//let a = [12, 0, 3, 9, 2, 18, 8, 27, 1, 5, 8, -1, 21]
//let b = quicksortNaive(a)
//print(b)
//

//// QUICKSORT WITH LOMUTO PARTITIONING SCHEME
//var list = [12, 0, 3, 9, 2, 21, 18, 27, 1, 5, 8, -1, 8]
//quicksortLomutu(&list, low: 0, high: list.count - 1)
//print(list)

// QUICKSORT WITH HOARE PARTITIONING SCHEME

//var list2 = [12, 0, 3, 9, 2, 21, 18, 27, 1, 5, 8, -1, 8]
//quicksortHoare(&list2, low: 0, high: list2.count - 1)
//print(list2)

// QUICKSORT WITH MEDIAN OF THREE PARITIONING SCHEME
//var list3 = [12, 0, 3, 9, 2, 21, 18, 27, 1, 5, 8, -1, 8]
//quicksortMedian(&list3, low: 0, high: list3.count - 1)
//print(list3)

// QUICKSORT WITH DUTCHFLAG PARITIONING SCHEME
var list4 = [12, 0, 3, 9, 2, 21, 18, 27, 1, 5, 8, -1, 8]
quicksortDutchFlag(&list4, low: 0, high: list4.count - 1)
print(list4)

