//
//  quicksortHoare.swift
//  QuickSort
//
//  Created by Don Clore on 8/8/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

public func paritionHoare<T: Comparable>(_ a: inout [T], low: Int, high: Int) -> Int {
  let pivot = a[low]
  var i = low - 1
  var j = high + 1
  
  while true {
    repeat {
      j -= 1
    } while a[j] > pivot
    
    repeat {
      i += 1
    } while a[i] < pivot
    
    if i < j {
      a.swapAt(i, j)
    } else {
      return j
    }
  }
}

public func quicksortHoare<T: Comparable>(_ a: inout [T], low: Int, high: Int) {
  if low < high {
    let p = paritionHoare(&a, low: low, high: high)
    quicksortHoare(&a, low: low, high: p)
    quicksortHoare(&a, low: p + 1, high: high)
  }
}
