//
//  QuickSort.swift
//  QuickSort
//
//  Created by Don Clore on 8/8/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

public func quicksortNaive<T: Comparable>(_ a: [T]) -> [T] {
  guard a.count > 1 else {
    return a
  }
  
  let pivot = a[a.count / 2]
  let less = a.filter { $0 < pivot }
  let equal = a.filter { $0 == pivot }
  let greater = a.filter { $0 > pivot }
  
  return quicksortNaive(less) + equal + quicksortNaive(greater)
}

public func partitionLomuto<T: Comparable>(_ a: inout [T], low: Int, high: Int) -> Int {
  let pivot = a[high]
  
  var i = low
  for j in low..<high {
    if a[j] <= pivot {
      a.swapAt(i, j)
      i += 1
    }
  }
  
  a.swapAt(i, high)
  return i
}

public func quicksortLomutu<T: Comparable>(_ a: inout [T], low: Int, high: Int) {
  if low < high {
    let pivot = partitionLomuto(&a, low: low, high: high)
    quicksortLomutu(&a, low: low, high: pivot - 1)
    quicksortLomutu(&a, low: pivot + 1, high: high)
  }
}
