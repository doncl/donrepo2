//
//  BinaryTree.swift
//  BinaryTree
//
//  Created by Don Clore on 8/1/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

public class BinaryNode<Element> {
  public var value: Element
  public var leftChild: BinaryNode?
  public var rightChild: BinaryNode?
  
  public init(value: Element) {
    self.value = value
  }
}

extension BinaryNode: CustomStringConvertible {

  public var description: String {
    diagram(for: self)
  }
  
  private func diagram(for node: BinaryNode?,
                       _ top: String = "",
                       _ root: String = "",
                       _ bottom: String = "") -> String {
    guard let node = node else {
      return root + "nil\n"
    }
    if node.leftChild == nil && node.rightChild == nil {
      return root + "\(node.value)\n"
    }
    return diagram(for: node.rightChild,
                   top + " ", top + "┌──", top + "│ ")
         + root + "\(node.value)\n"
         + diagram(for: node.leftChild,
                   bottom + "│ ", bottom + "└──", bottom + " ")
  }
}

extension BinaryNode {
  public func traverseInOrder(visit: (Element) -> Void) {
    leftChild?.traverseInOrder(visit: visit)
    visit(value)
    rightChild?.traverseInOrder(visit: visit)
  }
  
  public func traversePreOrder(visit: (Element) -> Void) {
    visit(value)
    leftChild?.traversePreOrder(visit: visit)
    rightChild?.traversePreOrder(visit: visit)
  }
  
  public func traversePostOrder(visit: (Element) -> Void) {
    leftChild?.traversePostOrder(visit: visit)
    rightChild?.traversePostOrder(visit: visit)
    visit(value)
  }
}

extension BinaryNode {
  func traverseForSerialization(visit: (Element?) -> Void) {
    visit(value)
    
    if let leftChild = leftChild {
      leftChild.traverseForSerialization(visit: visit)
    } else {
      visit(nil)
    }
    
    if let rightChild = rightChild {
      rightChild.traverseForSerialization(visit: visit)
    } else {
      visit(nil)
    }
  }
  
  func serialize() -> [Element?] {
    var array: [Element?] = []
    
    traverseForSerialization(visit: {
      array.append($0)
    })
    return array
  }
  
  static func deserialize(_ array: [Element?]) -> BinaryNode<Element>? {
    var reversed = Array(array.reversed())
    return deserialize(&reversed)
  }
  
  private static func deserialize(_ array: inout [Element?]) -> BinaryNode<Element>? {
    guard array.count > 0,  let value = array.removeLast() else {
      return nil
    }
    
    let node = BinaryNode(value: value)
    node.leftChild = deserialize(&array)
    node.rightChild = deserialize(&array)
    
    return node
  }
}
