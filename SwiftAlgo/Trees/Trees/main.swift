//
//  main.swift
//  Trees
//
//  Created by Don Clore on 8/1/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

func makeBeverageTree() -> TreeNode<String> {
  let tree = TreeNode("Beverages")

  let hot = TreeNode("hot")
  let cold = TreeNode("cold")

  let tea = TreeNode("tea")
  let coffee = TreeNode("coffee")
  let chocolate = TreeNode("cocoa")

  let blackTea = TreeNode("black")
  let greenTea = TreeNode("green")
  let chaiTea = TreeNode("chai")

  let soda = TreeNode("soda")
  let milk = TreeNode("milk")

  let gingerAle = TreeNode("ginger ale")
  let bitterLemon = TreeNode("bitter lemon")

  tree.add(hot)
  tree.add(cold)

  hot.add(tea)
  hot.add(coffee)
  hot.add(chocolate)

  cold.add(soda)
  cold.add(milk)

  tea.add(blackTea)
  tea.add(greenTea)
  tea.add(chaiTea)

  soda.add(gingerAle)
  soda.add(bitterLemon)

  return tree
}


// example of creating a tree
//let beverages = TreeNode("Beverages")
//let hot = TreeNode("Hot")
//let cold = TreeNode("Cold")
//
//beverages.add(hot)
//beverages.add(cold)

// example of depth-first traversal
//let tree = makeBeverageTree()
//tree.forEachDepthFirst(visit: { print($0.value) })


// example of level order traversal
//let tree = makeBeverageTree()
//tree.forEachLevelOrder(visit: {print($0.value) })


// example of searching for a node
//let tree = makeBeverageTree()
//
//if let searchResult1 = tree.search("ginger ale") {
//  print("Found node: \(searchResult1.value)")
//}
//if let searchResult2 = tree.search("WKD Blue") {
//  print(searchResult2.value)
//} else {
//  print("Couldn't find WKD Blue")
//}

// example of printing in level order
var root = TreeNode(15)
var one = TreeNode(1)
var seventeen = TreeNode(17)
var twenty = TreeNode(20)

root.children.append(contentsOf: [one, seventeen, twenty])

one.children.append(contentsOf: [TreeNode(1), TreeNode(5), TreeNode(0)])

seventeen.add(TreeNode(2))

twenty.children.append(contentsOf: [TreeNode(5), TreeNode(7)])

root.printBreadthFirst()
