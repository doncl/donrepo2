//
//  TreeNode.swift
//  Trees
//
//  Created by Don Clore on 8/1/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

public class TreeNode<T> {
  public var value: T
  public var children: [TreeNode] = []
  
  public init(_ value: T) {
    self.value = value
  }
  
  public func add(_ child: TreeNode) {
    children.append(child)
  }
  public func toString() -> String {
    return String(describing: value)
  }
}

extension TreeNode {
  public func forEachDepthFirst(visit: (TreeNode) -> Void) {
    visit(self)
    children.forEach {
      $0.forEachDepthFirst(visit: visit)
    }
  }
  
  public func forEachLevelOrder(visit: (TreeNode) -> ()) {
    visit(self)
    var queue = Queue<TreeNode>()
    children.forEach { queue.enqueue($0)}
    while let node = queue.dequeue() {
      visit(node)
      node.children.forEach({ queue.enqueue($0)})
    }
  }
}

extension TreeNode where T: Equatable {
  public func search(_ value: T) -> TreeNode? {
    var result: TreeNode?
    forEachLevelOrder(visit: { node in
      if node.value == value {
        result = node
      }
    })
    
    return result
  }
}

extension TreeNode {
  public func printBreadthFirst() {
    var q: Queue<TreeNode<T>> = Queue<TreeNode<T>>()
  
    var nodesLeftInCurrentLevel = 0
    
    q.enqueue(self)
    while !q.isEmpty {
      nodesLeftInCurrentLevel = q.count
      
      while nodesLeftInCurrentLevel > 0 {
        guard let node = q.dequeue() else {
          break
        }
        print("\(node.value) ", terminator: "")
        node.children.forEach {
          q.enqueue($0)
        }
        nodesLeftInCurrentLevel -= 1
      }
      print()
    }
  }
}
