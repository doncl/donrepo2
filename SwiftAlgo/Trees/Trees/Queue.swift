//
//  Queue.swift
//  Trees
//
//  Created by Don Clore on 8/1/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

struct Queue<T> {
  private var left: [T] = []
  private var right: [T] = []
  
  public var count: Int {
    return left.count + right.count 
  }
  
  public var isEmpty: Bool {
    return left.isEmpty && right.isEmpty
  }

  public var peek: T? {
    return !left.isEmpty ? left.last : right.first
  }
  
  public init() {}
  
  
  public mutating func enqueue(_ element: T) {
    right.append(element)
  }
  
  @discardableResult
  public mutating func dequeue() -> T? {
    if left.isEmpty {
      left = right.reversed()
      right.removeAll()
    }
    return left.popLast()
  }
}

extension Queue: CustomStringConvertible {
  public var description: String {
    return String(describing: left.reversed() + right)
  }
}
