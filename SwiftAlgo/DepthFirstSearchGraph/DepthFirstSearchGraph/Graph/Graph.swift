//
//  Graph.swift
//  Graphs
//
//  Created by Don Clore on 8/8/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

public enum EdgeType {
  case directed
  case undirected
}

public protocol Graph {
  associatedtype Element
  
  var allVertices: [Vertex<Element>] { get }
  func createVertex(data: Element) -> Vertex<Element>
  func addDirectedEdge(from source: Vertex<Element>, to destination: Vertex<Element>, weight: Double?)
  func addUndirectedEdge(between source: Vertex<Element>, and destination: Vertex<Element>, weight: Double?)
  func add(_ edge: EdgeType, from source: Vertex<Element>, to destination: Vertex<Element>, weight: Double?)
  func edges(from source: Vertex<Element>) -> [Edge<Element>]
  func weight(from source: Vertex<Element>, to destination: Vertex<Element>) -> Double?
}

extension Graph {
  public func addUndirectedEdge(between source: Vertex<Element>, and destination: Vertex<Element>, weight: Double?) {
    addDirectedEdge(from: source, to: destination, weight: weight)
    addDirectedEdge(from: destination, to: source, weight: weight)
  }
  
  public func add(_ edge: EdgeType, from source: Vertex<Element>, to destination: Vertex<Element>, weight: Double?) {
    switch edge {
      case .directed:
        addDirectedEdge(from: source, to: destination, weight: weight)
      case .undirected:
        addUndirectedEdge(between: source, and: destination, weight: weight)
    }
  }
}

extension Graph where Element: Hashable {
  public func numberOfPaths(from source: Vertex<Element>, to destination: Vertex<Element>) -> Int {
    var numberOfPaths = 0
    var visited: Set<Vertex<Element>> = []
    paths(from: source, to: destination, visited: &visited, pathCount: &numberOfPaths)
    return numberOfPaths
  }
  
  func paths(from source: Vertex<Element>, to destination: Vertex<Element>,
             visited: inout Set<Vertex<Element>>, pathCount: inout Int) {
    
    visited.insert(source)
    if source == destination {
      pathCount += 1
    } else {
      let neighbors = edges(from: source)
      for edge in neighbors {
        if !visited.contains(edge.destination) {
          paths(from: edge.destination, to: destination, visited: &visited, pathCount: &pathCount)
        }
      }
    }
    visited.remove(source)
  }
}

extension Graph where Element: Hashable {
  func depthFirstSearch(from source: Vertex<Element>) -> [Vertex<Element>] {
    var stack: Stack<Vertex<Element>> = []
    var pushed: Set<Vertex<Element>> = []
    var visited: [Vertex<Element>] = []
    
    stack.push(source)
    pushed.insert(source)
    visited.append(source)
    
    outer: while let vertex = stack.peek() {
      let neighbors = edges(from: vertex)
      guard !neighbors.isEmpty else {
        stack.pop()
        continue
      }
      for edge in neighbors {
        if !pushed.contains(edge.destination) {
          stack.push(edge.destination)
          pushed.insert(edge.destination)
          visited.append(edge.destination)
          continue outer
        }
      }
      stack.pop()
    }
    
    return visited
  }
  
  func dfs(from start: Vertex<Element>) -> [Vertex<Element>] {
    var visited: [Vertex<Element>] = []
    var pushed: Set<Vertex<Element>> = []
    
    dfs(from: start, visited: &visited, pushed: &pushed)
    
    return visited
  }
  
  private func dfs(from source: Vertex<Element>, visited: inout [Vertex<Element>], pushed: inout Set<Vertex<Element>>) {
    pushed.insert(source)
    visited.append(source)
    
    let neighbors = edges(from: source)
    for edge in neighbors {
      if !pushed.contains(edge.destination) {
        dfs(from: edge.destination, visited: &visited, pushed: &pushed)
      }
    }
  }
}

extension Graph where Element: Hashable {    
  private func hasCycle(from source: Vertex<Element>, pushed: inout Set<Vertex<Element>>) -> Bool {
    pushed.insert(source)
    
    let neighbors = edges(from: source)
    for edge in neighbors {
      if !pushed.contains(edge.destination) && hasCycle(from: edge.destination, pushed: &pushed) {
        return true
      } else if pushed.contains(edge.destination) {
        return true
      }
    }
    
    pushed.remove(source)
    return false
  }
}
