//
//  challenges.swift
//  Stacks
//
//  Created by Don Clore on 8/1/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

func balanceParens(input: String) -> Bool {
  var s = Stack<Character>()
  for char in input {
    if char == "(" {
      s.push(char)
    }
    if char == ")" {
      guard let matching = s.pop(), matching == "(" else {
        return false
      }
    }
  }
  return s.isEmpty
}
