//
//  main.swift
//  AVLTrees
//
//  Created by Don Clore on 8/2/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

// example of repeated insertions in sequence

//var tree = AVLTree<Int>()
//for i in 0..<15 {
//  tree.insert(i)
//}
//
//print(tree)


// example of removing a value
var tree = AVLTree<Int>()
tree.insert(15)
tree.insert(10)
tree.insert(16)
tree.insert(18)
print(tree)
tree.remove(10)
print(tree)
