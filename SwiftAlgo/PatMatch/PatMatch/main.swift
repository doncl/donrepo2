//
//  main.swift
//  PatMatch
//
//  Created by Don Clore on 8/9/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

enum PatMatch {
  case noassociated
  case associated(Int)
}

func returnEnum() -> PatMatch {
  return .associated(5)
}

@discardableResult
func matchEnum(pathMatch: PatMatch) -> Bool {
  if case PatMatch.associated(let value) = pathMatch {
    print("Value matched = \(value)")
    return true
  }
  return false
}


let e = returnEnum()

matchEnum(pathMatch: e)
