//
//  BTree.swift
//  BTree
//
//  Created by Don Clore on 8/9/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

class BTreeNode<Key: Comparable, Value> {
  unowned var owner: BTree<Key, Value>
  
  fileprivate var keys = [Key]()
  fileprivate var values = [Value]()
  var children: [BTreeNode]?
  
  var isLeaf: Bool {
    return children == nil
  }
  
  var numberOfKeys: Int {
    return keys.count
  }
  
  init(owner: BTree<Key, Value>) {
    self.owner = owner
  }
  
  convenience init(owner: BTree<Key, Value>, keys: [Key], values: [Value], children: [BTreeNode]? = nil) {
    self.init(owner: owner)
    self.keys += keys
    self.values += values
    self.children = children
  }
}

extension BTreeNode {
  func value(for key: Key) -> Value? {
    var index = keys.startIndex
    while (index + 1) < keys.endIndex && keys[index] < key {
      index = (index + 1)
    }
    
    if key == keys[index] {
      return values[index]
    } else if key < keys[index] {
      return children?[index].value(for: key)
    } else {
      return children?[(index + 1)].value(for: key)
    }
  }
}

extension BTreeNode {
  func traverseKeysInOrder(_ process: (Key) ->()) {
    for i in 0..<numberOfKeys {
      children?[i].traverseKeysInOrder(process)
      process(keys[i])
    }
    
    children?.last?.traverseKeysInOrder(process)
  }
}

extension BTreeNode {
  func insert(_ value: Value, for key: Key) {
    var index = keys.startIndex
    
    while index < keys.endIndex && keys[index] < key {
      index += 1
    }
    
    if index < keys.endIndex && keys[index] == key {
      values[index] = value
      return
    }
    
    if isLeaf {
      keys.insert(key, at: index)
      values.insert(value, at: index)
      owner.numberOfKeys += 1
    } else {
      guard let children = children else {
        return
      }
      children[index].insert(value, for: key)
      if children[index].numberOfKeys > owner.order * 2 {
        split(child: children[index], atIndex: index)
      }
    }
  }
  
  private func split(child: BTreeNode, atIndex index: Int) {
    let middleIndex = child.numberOfKeys / 2
    keys.insert(child.keys[middleIndex], at: index)
    values.insert(child.values[middleIndex], at: index)
    child.keys.remove(at: middleIndex)
    child.values.remove(at: middleIndex)
    
    let childKeys = child.keys[child.keys.indices.suffix(from: middleIndex)]
    let childValues = child.values[child.values.indices.suffix(from: middleIndex)]
    let rightSibling = BTreeNode(owner: owner,
                                 keys: Array(childKeys),
                                 values: Array(childValues))
    
    child.keys.removeSubrange(child.keys.indices.suffix(from: middleIndex))
    child.values.removeSubrange(child.values.indices.suffix(from: middleIndex))
    
    assert(children != nil)
    if var children = children {
      children.insert(rightSibling, at: index + 1)
    }
    
    if var children = child.children {
      rightSibling.children = Array(children[children.indices.suffix(from: middleIndex + 1)])
      children.removeSubrange(children.indices.suffix(from: middleIndex + 1))
    }
  }
}

private enum BTreeNodePosition {
  case left
  case right
}

extension BTreeNode {
  private var inorderPredecessor: BTreeNode {
    if isLeaf {
      return self
    } else {
      return children!.last!.inorderPredecessor
    }
  }
  
 
 func remove(_ key: Key) {
   var index = keys.startIndex

   while (index + 1) < keys.endIndex && keys[index] < key {
     index += 1
   }

   if keys[index] == key {
     if isLeaf {
       keys.remove(at: index)
       values.remove(at: index)
       owner.numberOfKeys -= 1
     } else {
       let predecessor = children![index].inorderPredecessor
       keys[index] = predecessor.keys.last!
       values[index] = predecessor.values.last!
       children![index].remove(keys[index])
       if children![index].numberOfKeys < owner.order {
         fix(childWithTooFewKeys: children![index], atIndex: index)
       }
     }
   } else if key < keys[index] {
     // Go to left child
     if let leftChild = children?[index] {
       leftChild.remove(key)
       if leftChild.numberOfKeys < owner.order {
         fix(childWithTooFewKeys: leftChild, atIndex: index)
       }
     } else {
       print("the key:\(key) is not in the tree.")
     }
   } else {
     // we should go to right child.
     if let rightChild = children?[index + 1] {
       rightChild.remove(key)
       if rightChild.numberOfKeys < owner.order {
         fix(childWithTooFewKeys: rightChild, atIndex: index + 1)
       }
     } else {
       print("The key:\(key) is not in the tree")
     }
   }
 }
  
  private func fix(childWithTooFewKeys child: BTreeNode, atIndex index: Int) {
    precondition(child.numberOfKeys < owner.order)
    
    if (index - 1) >= 0 && children![index - 1].numberOfKeys > owner.order {
      move(keyAtIndex: (index - 1), to: child, from: children![(index - 1)], at: .left)
    } else if (index + 1) < children!.count && children![(index + 1)].numberOfKeys > owner.order {
      move(keyAtIndex: index, to: child, from: children![index + 1], at: .right)
    } else if (index - 1) >= 0 {
      merge(child: child, atIndex: index, to: .left)
    } else {
      merge(child: child, atIndex: index, to: .right)
    }
  }
  
  private func move(keyAtIndex index: Int, to targetNode: BTreeNode, from node: BTreeNode, at position: BTreeNodePosition) {
    switch position {
    case .left:
      targetNode.keys.insert(keys[index], at: targetNode.keys.startIndex)
      targetNode.values.insert(values[index], at: targetNode.values.startIndex)
      keys[index] = node.keys.last!
      values[index] = node.values.last!
      node.keys.removeLast()
      node.values.removeLast()
      if !targetNode.isLeaf {
        targetNode.children!.insert(node.children!.last!, at: targetNode.children!.startIndex)
        node.children!.removeLast()
      }
      
    case .right:
      targetNode.keys.insert(keys[index], at: targetNode.keys.endIndex)
      targetNode.values.insert(values[index], at: targetNode.values.endIndex)
      keys[index] = node.keys.first!
      values[index] = node.values.first!
      node.keys.removeFirst()
      node.values.removeFirst()
      if !targetNode.isLeaf {
        targetNode.children!.insert(node.children!.first!, at: targetNode.children!.endIndex)
        node.children!.removeFirst()
      }
    }
  }
  
  private func merge(child: BTreeNode, atIndex index: Int, to position: BTreeNodePosition) {
    switch position {
    case .left:
      // We can merge to the left sibling
      children![index - 1].keys = children![index - 1].keys + [keys[index - 1]] + child.keys
      
      children![index - 1].values = children![index - 1].values + [values[index - 1]] + child.values
      
      keys.remove(at: index - 1)
      values.remove(at: index - 1)
      
      if !child.isLeaf {
        children![index - 1].children = children![index - 1].children! + child.children!
      }
      
    case .right:
      // we should merge to the right sibling
      
      children![index + 1].keys = child.keys + [keys[index]] + children![index + 1].keys
      
      children![index + 1].values = child.values + [values[index]] + children![index + 1].values
      
      keys.remove(at: index)
      values.remove(at: index)
      
      if !child.isLeaf {
        children![index + 1].children = child.children! + children![index + 1].children!
      }
    }
    children!.remove(at: index)
  }
}

extension BTreeNode {
  var inorderArrayFromKeys: [Key]  {
    var array = [Key]()
    
    for i in 0..<numberOfKeys {
      if let returnedArray = children?[i].inorderArrayFromKeys {
        array += returnedArray
      }
      array += [keys[i]]
    }
    
    if let returnedArray = children?.last?.inorderArrayFromKeys {
      array += returnedArray
    }
        
    return array
  }
}

extension BTreeNode: CustomStringConvertible {
  var description: String {
    var str = "\(keys)"
    
    if !isLeaf, let children = children {
      for child in children {
        str += child.description
      }
    }
    return str
  }
}


public class BTree<Key: Comparable, Value> {
  public let order: Int
  public fileprivate(set) var numberOfKeys: Int = 0
  var rootNode: BTreeNode<Key, Value>!
  
  public init?(order: Int) {
    guard order > 0 else {
      print("Order has to be greater than 0.")
      return nil
    }
    self.order = order
    rootNode = BTreeNode<Key, Value>(owner: self)
  }
}

extension BTree {
  public func traverseKeysInOrder(_ process: (Key) -> ()) {
    rootNode.traverseKeysInOrder(process)
  }
}

extension BTree {
  public subscript (key: Key) -> Value? {
    return value(for: key)
  }
}

extension BTree {
  public func value(for key: Key) -> Value? {
    guard rootNode.numberOfKeys > 0 else {
      return nil
    }
    return rootNode.value(for: key)
  }
}

extension BTree {
  public func insert(_ value: Value, for key: Key) {
    rootNode.insert(value, for: key)
    
    if rootNode.numberOfKeys > order * 2 {
      splitRoot()
    }
  }
  
  private func splitRoot() {
    let middleIndexOfOldRoot = rootNode.numberOfKeys / 2
    
    let newRoot = BTreeNode(owner: self, keys: [rootNode.keys[middleIndexOfOldRoot]],
                            values: [rootNode.values[middleIndexOfOldRoot]],
                            children: [rootNode])
    
    rootNode.keys.remove(at: middleIndexOfOldRoot)
    rootNode.values.remove(at: middleIndexOfOldRoot)
    
    let rightKeys = rootNode.keys[rootNode.keys.indices.suffix(from: middleIndexOfOldRoot)]
    let rightValues = rootNode.values[rootNode.values.indices.suffix(from: middleIndexOfOldRoot)]
    let newRightChild = BTreeNode(owner: self, keys: Array(rightKeys), values: Array(rightValues))
    
    rootNode.keys.removeSubrange(rootNode.keys.indices.suffix(from: middleIndexOfOldRoot))
    rootNode.values.removeSubrange(rootNode.values.indices.suffix(from: middleIndexOfOldRoot))
    
    if var rootKids = rootNode.children {
      newRightChild.children = Array(rootKids[rootKids.indices.suffix(from: middleIndexOfOldRoot + 1)])
      rootKids.removeSubrange(rootKids.indices.suffix(from: middleIndexOfOldRoot + 1))
    }
    
    newRoot.children!.append(newRightChild)
    rootNode = newRoot
  }
}

extension BTree {
  public func remove(_ key: Key) {
    guard rootNode.numberOfKeys > 0 else {
      return
    }
    
    rootNode.remove(key)
    
    if rootNode.numberOfKeys == 0 && !rootNode.isLeaf {
      rootNode = rootNode.children!.first!
    }
  }
}

extension BTree {
  public var inorderArrayFromKeys: [Key] {
    return rootNode.inorderArrayFromKeys
  }
}

extension BTree: CustomStringConvertible {
  public var description: String {
    return rootNode.description
  }
}
