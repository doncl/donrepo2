//
//  MergeSequences.swift
//  MergeSort
//
//  Created by Don Clore on 8/7/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

func merge<T: Sequence>(first: T, second: T) -> AnySequence<T.Element> where T.Element: Comparable {
  var result: [T.Element] = []
  
  var firstIterator = first.makeIterator()
  var secondIterator = second.makeIterator()
  
  var firstNextValue = firstIterator.next()
  var secondNextValue = secondIterator.next()
  
  while let first = firstNextValue, let second = secondNextValue {
    if first < second {
      result.append(first)
      firstNextValue = firstIterator.next()
    } else if second < first {
      result.append(second)
      secondNextValue = secondIterator.next()
    } else {
      result.append(first)
      firstNextValue = firstIterator.next()
      result.append(second)
      secondNextValue = secondIterator.next()
    }
  }
  
  while let first = firstNextValue {
    result.append(first)
    firstNextValue = firstIterator.next()
  }
  
  while let second = secondNextValue {
    result.append(second)
    secondNextValue = secondIterator.next()
  }
  
  return AnySequence<T.Element>(result)
}
