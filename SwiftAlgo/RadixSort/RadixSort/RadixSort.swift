//
//  RadixSort.swift
//  RadixSort
//
//  Created by Don Clore on 8/8/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

extension Array where Element == Int {
  public mutating func radixSort() {
    let base = 10
    var done = false
    var digits = 1
    
    while !done {
      done = true 
      var buckets: [[Int]] = [[Int]](repeating: [], count: base)
      
      forEach { number in
        let remainingPart = number / digits
        let digit = remainingPart % base
        buckets[digit].append(number)
        if remainingPart > 0 {
          done = false
        }
      }
      
      digits *= base
      self = buckets.flatMap { $0 }
    }
  }
}
