//
//  main.swift
//  Queues
//
//  Created by Don Clore on 8/1/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

var q = Queue<String>()

q.enqueue("Ray")
q.enqueue("Brian")
q.enqueue("Eric")

print(q)
q.dequeue()
print(q)
if let val = q.peek {
  print("peek = \(val)")
} else {
  print("queue is empty")
}
