//
//  GroupElements.swift
//  NSquaredSorts
//
//  Created by Don Clore on 8/5/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation
extension MutableCollection
  where Self: BidirectionalCollection, Element: Equatable {

  mutating func rightAlign(value: Element) {
    var left = startIndex
    var right = index(before: endIndex)

    while left < right {
      while self[right] == value {
        formIndex(before: &right)
      }
      while self[left] != value {
        formIndex(after: &left)
      }

      guard left < right else {
        return
      }
      swapAt(left, right)
    }
  }
}

