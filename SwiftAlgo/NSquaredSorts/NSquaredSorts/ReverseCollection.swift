//
//  ReverseCollection.swift
//  NSquaredSorts
//
//  Created by Don Clore on 8/5/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

extension MutableCollection where Self: BidirectionalCollection {
  mutating func reverse() {
    var left = startIndex
    var right = index(before: endIndex)
    
    while left < right {
      swapAt(left, right)
      formIndex(after: &left)
      formIndex(before: &right)
    }
  }
}
