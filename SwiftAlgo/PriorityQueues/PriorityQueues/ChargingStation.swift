//
//  ChargingStation.swift
//  PriorityQueues
//
//  Created by Don Clore on 8/5/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

enum DestinationResult {
  /// Able to reach your destination with the minimum number of stops.
  case reachable(rechargeStops: Int)
  
  // Unable to reach your destination
  case unreachable
}

struct ChargingStation {
  // distance from start location
  let distance: Int
  
  // The amount of electricity the station has to charge a car.
  /// 1 capacity = 1 mile.
  let chargeCapacity: Int
}


func minRechargeStops(target: Int, startCharge: Int, stations: [ChargingStation]) -> DestinationResult {
  guard startCharge <= target else {
    return .reachable(rechargeStops: 0)
  }
  
  var minStops = -1
  
  var currentCharge = 0
  
  var currentStation = 0
  
  var chargePriority = PriorityQueue(sort: >, elements: [startCharge])
  
  while !chargePriority.isEmpty {
    guard let charge = chargePriority.dequeue() else {
      return .unreachable
    }
    
    currentCharge += charge
    
    minStops += 1
    
    if currentCharge >= target {
      return .reachable(rechargeStops: minStops)
    }
    
    while currentStation < stations.count && currentCharge >= stations[currentStation].distance {
      let distance = stations[currentStation].chargeCapacity
      chargePriority.enqueue(distance)
      currentStation += 1
    }
  }
  
  return .unreachable
}
