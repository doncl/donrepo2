//
//  Person.swift
//  PriorityQueues
//
//  Created by Don Clore on 8/5/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

struct Person: Equatable {
  let name: String
  let age: Int
  let isMilitary: Bool
}


func tswiftSort(person1: Person, person2: Person) -> Bool {
  if person1.isMilitary == person2.isMilitary {
    return person1.age > person2.age
  }
  return person1.isMilitary
}
