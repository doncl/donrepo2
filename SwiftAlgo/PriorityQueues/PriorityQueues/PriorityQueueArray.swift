//
//  ArrayPriorityQueue.swift
//  PriorityQueues
//
//  Created by Don Clore on 8/5/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

public struct PriorityQueueArray<T: Equatable>: Queue {
  private var elements: [T] = []
  let sort: (Element, Element) -> Bool
  
  public var isEmpty: Bool {
    return elements.isEmpty
  }
  
  public var peek: T? {
    return elements.first
  }
    
  public init(sort: @escaping (Element, Element) -> Bool, elements: [Element] = []) {
    self.sort = sort
    self.elements = elements
    self.elements.sort(by: sort)
  }
  
  @discardableResult
  public mutating func enqueue(_ element: T) -> Bool {
    for (index, otherElement) in elements.enumerated() {
      if sort(element, otherElement) {
        elements.insert(element, at: index)
        return true
      }
    }
    elements.append(element)
    return true
  }
  
  public mutating func dequeue() -> T? {
    return isEmpty ? nil : elements.removeFirst()
  }
}

extension PriorityQueueArray: CustomStringConvertible {
  public var description: String {
    String(describing: elements)
  }
}
