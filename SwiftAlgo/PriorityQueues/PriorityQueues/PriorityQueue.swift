//
//  PriorityQueue.swift
//  PriorityQueues
//
//  Created by Don Clore on 8/3/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

struct PriorityQueue<Element: Equatable>: Queue {
  private var heap: Heap<Element>
  
  init(sort: @escaping (Element, Element) -> Bool, elements: [Element] = []) {
    heap = Heap(sort: sort, elements: elements)
  }
  
  var isEmpty: Bool {
    heap.isEmpty
  }
  
  var peek: Element? {
    heap.peek()
  }
  
  @discardableResult
  mutating func enqueue(_ element: Element) -> Bool {
    heap.insert(element)
    return true
  }
  
  mutating func dequeue() -> Element? {
    heap.remove()
  }
}
