
//
//  Vertex.swift
//  Graphs
//
//  Created by Don Clore on 8/8/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

public struct Vertex<T> {
  public let index: Int
  public let data: T
}

extension Vertex: Hashable where T: Hashable {}

extension Vertex: Equatable where T: Equatable {}

extension Vertex: CustomStringConvertible {
  public var description: String {
    return "\(index): \(data)"
  }
}

