//
//  Queue.swift
//  PriorityQueues
//
//  Created by Don Clore on 8/3/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Foundation

public protocol Queue {
  associatedtype Element
  mutating func enqueue(_ element: Element) -> Bool
  mutating func dequeue() -> Element?
  var isEmpty: Bool { get }
  var peek: Element? { get }
}

