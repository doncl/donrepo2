/// Copyright (c) 2019 Razeware LLC
///
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
///
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
///
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
///
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import UIKit
import AVFoundation

class DepthVideoViewController: UIViewController {
  @IBOutlet weak var previewView: UIImageView!
  @IBOutlet weak var previewModeControl: UISegmentedControl!
  @IBOutlet weak var filterControl: UISegmentedControl!
  @IBOutlet weak var depthSlider: UISlider!

  var sliderValue: CGFloat = 0.0
  var previewMode = PreviewMode.original
  var filter = FilterType.comic
  let session = AVCaptureSession()
  let dataOutputQueue = DispatchQueue(label: "video data queue",
                                      qos: .userInitiated,
                                      attributes: [],
                                      autoreleaseFrequency: .workItem)
  let background: CIImage! = CIImage(image: UIImage(named: "earth-rise")!)
  var depthMap: CIImage?
  var mask: CIImage?
  var scale: CGFloat = 0.0
  var depthFilters = DepthImageFilters()

  override func viewDidLoad() {
    super.viewDidLoad()

    filterControl.isHidden = true
    depthSlider.isHidden = true

    previewMode = PreviewMode(rawValue: previewModeControl.selectedSegmentIndex) ?? .original
    filter = FilterType(rawValue: filterControl.selectedSegmentIndex) ?? .comic
    sliderValue = CGFloat(depthSlider.value)

    configureCaptureSession()

    session.startRunning()
  }
}

// MARK: - Helper Methods
extension DepthVideoViewController {
  func configureCaptureSession() {
    guard let camera = AVCaptureDevice.default(.builtInDualCamera, for: .video, position: .unspecified) else {
      fatalError("No depth video camera available")
    }

    session.sessionPreset = .photo

    do {
      let cameraInput = try AVCaptureDeviceInput(device: camera)
      session.addInput(cameraInput)
    } catch {
      fatalError(error.localizedDescription)
    }

    let videoOutput = AVCaptureVideoDataOutput()
    videoOutput.setSampleBufferDelegate(self, queue: dataOutputQueue)
    videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]

    session.addOutput(videoOutput)

    let videoConnection = videoOutput.connection(with: .video)
    videoConnection?.videoOrientation = .portrait
    
    // 1
    let depthOutput = AVCaptureDepthDataOutput()
    // 2
    depthOutput.setDelegate(self, callbackQueue: dataOutputQueue)
    // 3
    depthOutput.isFilteringEnabled = true
    // 4
    session.addOutput(depthOutput)
    // 5
    let depthConnection = depthOutput.connection(with: .depthData)
    // 6
    depthConnection?.videoOrientation = .portrait
    
    // 1
    let outputRect = CGRect(x: 0, y: 0, width: 1, height: 1)
    let videoRect = videoOutput
      .outputRectConverted(fromMetadataOutputRect: outputRect)
    let depthRect = depthOutput
      .outputRectConverted(fromMetadataOutputRect: outputRect)

    // 2
    scale =
      max(videoRect.width, videoRect.height) /
      max(depthRect.width, depthRect.height)

    // 3
    do {
      try camera.lockForConfiguration()

      // 4
      if let format = camera.activeDepthDataFormat,
        let range = format.videoSupportedFrameRateRanges.first  {
        camera.activeVideoMinFrameDuration = range.minFrameDuration
      }

      // 5
      camera.unlockForConfiguration()
    } catch {
      fatalError(error.localizedDescription)
    }
  }
}

// MARK: - Capture Video Data Delegate Methods
extension DepthVideoViewController: AVCaptureVideoDataOutputSampleBufferDelegate {
  func captureOutput(_ output: AVCaptureOutput,
                     didOutput sampleBuffer: CMSampleBuffer,
                     from connection: AVCaptureConnection) {
    let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)
    let image = CIImage(cvPixelBuffer: pixelBuffer!)

    let previewImage: CIImage

    switch (previewMode, filter, mask) {
    case (.original, _, _):
      previewImage = image
    case (.depth, _, _):
      previewImage = depthMap ?? image
    case (.mask, _, let mask?):
      previewImage = mask
    case (.filtered, .comic, let mask?):
      previewImage = depthFilters.comic(image: image, mask: mask)
    case (.filtered, .greenScreen, let mask?):
      previewImage = depthFilters.greenScreen(
        image: image,
        background: background,
        mask: mask)
    case (.filtered, .blur, let mask?):
      previewImage = depthFilters.blur(image: image, mask: mask)
    default:
      previewImage = image
    }

    let displayImage = UIImage(ciImage: previewImage)
    DispatchQueue.main.async { [weak self] in
      self?.previewView.image = displayImage
    }
  }
}

// MARK: - Slider Methods
extension DepthVideoViewController {
  @IBAction func sliderValueChanged(_ sender: UISlider) {
    sliderValue = CGFloat(depthSlider.value)
  }
}

// MARK: - Segmented Control Methods
extension DepthVideoViewController {
  @IBAction func previewModeChanged(_ sender: UISegmentedControl) {
    previewMode = PreviewMode(rawValue: previewModeControl.selectedSegmentIndex) ?? .original

    switch previewMode {
    case .mask, .filtered:
      filterControl.isHidden = false
      depthSlider.isHidden = false
    case .depth, .original:
      filterControl.isHidden = true
      depthSlider.isHidden = true
    }
  }

  @IBAction func filterTypeChanged(_ sender: UISegmentedControl) {
    filter = FilterType(rawValue: filterControl.selectedSegmentIndex) ?? .comic
  }
}

// MARK: - Capture Depth Data Delegate Methods
extension DepthVideoViewController: AVCaptureDepthDataOutputDelegate {
  func depthDataOutput(
    _ output: AVCaptureDepthDataOutput,
    didOutput depthData: AVDepthData,
    timestamp: CMTime,
    connection: AVCaptureConnection) {
    
    // 1
    guard previewMode != .original else {
      return
    }

    var convertedDepth: AVDepthData
    // 2
    let depthDataType = kCVPixelFormatType_DisparityFloat32
    if depthData.depthDataType != depthDataType {
      convertedDepth = depthData.converting(toDepthDataType: depthDataType)
    } else {
      convertedDepth = depthData
    }

    // 3
    let pixelBuffer = convertedDepth.depthDataMap
    // 4
    pixelBuffer.clamp()
    // 5
    let depthMap = CIImage(cvPixelBuffer: pixelBuffer)

    if previewMode == .mask || previewMode == .filtered {
      switch filter {
      case .comic:
        mask = depthFilters.createHighPassMask(
          for: depthMap,
          withFocus: sliderValue,
          andScale: scale)
      case .greenScreen:
        mask = depthFilters.createHighPassMask(
          for: depthMap,
          withFocus: sliderValue,
          andScale: scale,
          isSharp: true)
      case .blur:
        mask = depthFilters.createBandPassMask(
          for: depthMap,
          withFocus: sliderValue,
          andScale: scale)
      }
    }
    
    // 6
    DispatchQueue.main.async {
      self.depthMap = depthMap
    }
  }
}
