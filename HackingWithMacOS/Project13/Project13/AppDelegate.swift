//
//  AppDelegate.swift
//  Project13
//
//  Created by Don Clore on 7/18/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



  func applicationDidFinishLaunching(_ aNotification: Notification) {
    NSColorPanel.shared.showsAlpha = true 
  }

  func applicationWillTerminate(_ aNotification: Notification) {
    // Insert code here to tear down your application
  }


}

