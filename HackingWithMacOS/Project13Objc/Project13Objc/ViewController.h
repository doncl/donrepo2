//
//  ViewController.h
//  Project13Objc
//
//  Created by Don Clore on 7/18/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController
@property (strong) IBOutlet NSImageView *imageView;
@property (strong) IBOutlet NSTextView *caption;
@property (strong) IBOutlet NSPopUpButton *fontName;
@property (strong) IBOutlet NSPopUpButton *fontSize;
@property (strong) IBOutlet NSColorWell *fontColor;
@property (strong) IBOutlet NSPopUpButton *backgroundImage;
@property (strong) IBOutlet NSColorWell *backgroundColorStart;
@property (strong) IBOutlet NSColorWell *backgroundColorEnd;
@property (strong) IBOutlet NSSegmentedControl *dropShadowStrength;
@property (strong) IBOutlet NSSegmentedControl *dropShadowTarget;
@end

