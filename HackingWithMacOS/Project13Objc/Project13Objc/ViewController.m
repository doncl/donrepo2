//
//  ViewController.m
//  Project13Objc
//
//  Created by Don Clore on 7/18/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "ViewController.h"
#import "Document.h"
#import "Screenshot.h"
#import <CoreGraphics/CoreGraphics.h>

@interface ViewController() <NSTextViewDelegate>
@end

@implementation ViewController
{
  NSImage *_screenshotImage;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  NSClickGestureRecognizer *click = [[NSClickGestureRecognizer alloc] initWithTarget:self action:@selector(importScreenshot)];
  [self.imageView addGestureRecognizer:click];
  [self loadFonts];
  [self loadBackgroundImages];
}

- (void)viewWillAppear {
  [super viewWillAppear];
  [self updateUI];
  [self generatePreview];
}

- (void)setRepresentedObject:(id)representedObject {
  [super setRepresentedObject:representedObject];

  // Update the view, if already loaded.
}
                                     
#pragma IBAction stuff.
- (IBAction)changeFontSize:(NSMenuItem *)sender {
  NSInteger tag = self.fontSize.selectedTag;
  Screenshot *screenshot = [self getDocumentScreenshot];
  if (screenshot) {
    screenshot.captionFontSize = (CGFloat) tag;
  }
  [self generatePreview];
}

- (IBAction)changeFontColor:(id)sender {
  NSColor *color = self.fontColor.color;
  Screenshot *screenshot = [self getDocumentScreenshot];
  if (screenshot) {
    screenshot.captionColor = color;
  }

  [self generatePreview];
}

- (IBAction)changeBackgroundImage:(NSMenuItem *)sender {
  Screenshot *screenshot = [self getDocumentScreenshot];
  if (screenshot) {
    if (self.backgroundImage.selectedTag == 999) {
      screenshot.backgroundImage = @"";
    } else {
      NSString *title = @"";
      if (self.backgroundImage.titleOfSelectedItem) {
        title = self.backgroundImage.titleOfSelectedItem;
      }
      screenshot.backgroundImage = title;
    }
  }

  [self generatePreview];
}

- (IBAction)changeBackgroundColorStart:(NSColorWell *)sender {
  NSColor *start = self.backgroundColorStart.color;
  Screenshot *screenshot = [self getDocumentScreenshot];
  if (screenshot) {
    screenshot.backgroundColorStart = start;
  }

  [self generatePreview];
}

- (IBAction)changeBackgroundColorEnd:(NSColorWell *)sender {
  NSColor *end = self.backgroundColorEnd.color;
  Screenshot *screenshot = [self getDocumentScreenshot];
  if (screenshot) {
    screenshot.backgroundColorEnd = end;
  }

  [self generatePreview];
}

- (IBAction)changeDropShadowStrength:(NSSegmentedControl *)sender {
  NSInteger strength = self.dropShadowStrength.selectedSegment;
  Screenshot *screenshot = [self getDocumentScreenshot];
  if (screenshot) {
    screenshot.dropShadowStrength = strength;
  }

  [self generatePreview];
}

- (IBAction)changeDropShadowTarget:(NSSegmentedControl *)sender {
  NSInteger target = self.dropShadowTarget.selectedSegment;
  Screenshot *screenshot = [self getDocumentScreenshot];
  if (screenshot) {
    screenshot.dropShadowTarget = target;
  }
  [self generatePreview];
}

- (void)updateUI {
  Screenshot *screenshot = [self getDocumentScreenshot];
  if (screenshot) {
    self.caption.string = screenshot.caption;
    NSString *captionFontName = screenshot.captionFontName;
    if (captionFontName) {
      [self.fontName selectItemWithTitle:captionFontName];
    }
    [self.fontSize selectItemAtIndex:screenshot.captionFontSize];
    NSColor *captionColor = screenshot.captionColor;
    if (captionColor) {
      self.fontColor.color = captionColor;
    }
    NSString *backgroundImage = screenshot.backgroundImage;
    if (backgroundImage && ![backgroundImage isEqualToString:@""]) {
      [self.backgroundImage selectItemWithTitle:backgroundImage];
    }
       
    NSColor *backgroundColorStart = screenshot.backgroundColorStart;
    if (backgroundColorStart) {
      self.backgroundColorStart.color = backgroundColorStart;
    }
    
    NSColor *backgroundColorEnd = screenshot.backgroundColorEnd;
    if (backgroundColorEnd) {
      self.backgroundColorEnd.color = backgroundColorEnd;
    }
    
    self.dropShadowStrength.selectedSegment = screenshot.dropShadowStrength;
    self.dropShadowTarget.selectedSegment = screenshot.dropShadowTarget;
  }
}

- (IBAction)export:(id)sender {
  NSImage *image = self.imageView.image;
  if (!image) {
    return;
  }
  NSData *tiffData = image.TIFFRepresentation;
  if (!tiffData) {
    return;
  }
  NSBitmapImageRep * imageRep = [[NSBitmapImageRep alloc] initWithData:tiffData];
  if (!imageRep) {
    return;
  }
  NSData *pngData = [imageRep representationUsingType:NSBitmapImageFileTypePNG properties:@{}];
  if (!pngData) {
    return;
  }
  
  NSSavePanel *panel = [NSSavePanel new];
  panel.allowedFileTypes = @[@"png"];
  [panel beginWithCompletionHandler:^(NSModalResponse result){
    if (result == NSModalResponseOK) {
      NSURL *url = panel.URL;
      if (!url) {
        return;
      }
      NSError *err;
      [pngData writeToURL:url options:NSDataWritingAtomic error:&err];
      if (err) {
        NSLog(@"Error writing pngData = %@", [err localizedDescription]);
      }
    }
  }];
}


#pragma mark - methods painstakingly and possibly shoddily translated from Swift code in book.
- (void)clearBackground:(CGContextRef)context rect:(CGRect)rect  {
  CGColorRef color = [NSColor whiteColor].CGColor;
  CGContextSetFillColorWithColor(context, color);
  CGContextFillRect(context, rect);
}

- (void)drawBackgroundImage:(CGRect)rect {
  if (self.backgroundImage.selectedTag == 999) {
    return;
  }
  NSString *title = self.backgroundImage.titleOfSelectedItem;
  if (!title) {
    return;
  }
  NSImage *image = [NSImage imageNamed:title];
  if (!image) {
    return;
  }
  [image drawInRect:rect fromRect:CGRectZero operation:NSCompositingOperationSourceOver fraction:1.0];
}

- (void)importScreenshot {
  NSOpenPanel *panel = [NSOpenPanel new];
  panel.allowedFileTypes = @[@"jpg", @"jpeg", @"png"];
  
  __weak typeof(self) weakSelf = self;
  [panel beginWithCompletionHandler:^(NSModalResponse result) {
    if (!weakSelf) {
      return;
    }
    __strong typeof(self) strongSelf = weakSelf;
    if (result == NSModalResponseOK) {
      NSURL *imageURL = panel.URL;
      if (!imageURL) {
        return;
      }
      strongSelf->_screenshotImage = [[NSImage alloc] initWithContentsOfURL:imageURL];
      [strongSelf generatePreview];
    }
  }];
}

- (void)drawScreenshot:(CGContextRef)ctxt rect:(CGRect)rect captionOffset:(CGFloat)captionOffset {
  if (!_screenshotImage) {
    return;
  }
  _screenshotImage.size = CGSizeMake(891, 1584);
  CGFloat offsetY = 314.0 - captionOffset;
  CGPoint pt = CGPointMake(176, offsetY);
  [_screenshotImage drawAtPoint:pt fromRect:CGRectZero operation:NSCompositingOperationSourceOver fraction:1.0];
}

- (void)drawColorOverlay:(CGRect)rect {
  NSColor *startingColor = self.backgroundColorStart.color;
  NSColor *endingColor = self.backgroundColorEnd.color;
  if (!startingColor || !endingColor) {
    return;
  }
  NSGradient *gradient = [[NSGradient alloc] initWithStartingColor:startingColor endingColor:endingColor];
  if (!gradient) {
    return;
  }
  [gradient drawInRect:rect angle:-90.0];
}

- (void) loadFonts {
  NSURL *fontFile = [[NSBundle mainBundle] URLForResource:@"fonts" withExtension:nil];
  if (!fontFile) {
    return;
  }
  NSError *err;
  NSString *fonts = [NSString stringWithContentsOfURL:fontFile encoding:NSUTF8StringEncoding error:&err];
  if (err || !fonts) {
    return;
  }
  NSArray<NSString *> *fontNames = [fonts componentsSeparatedByString:@"\n"];
  for (NSString *font in fontNames) {
    if ([font hasPrefix:@" "]) {
      // this is a font variation
      NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:font action:@selector(changeFontName:) keyEquivalent:@""];
      item.target = self;
      if (self.fontName.menu) {
        [self.fontName.menu addItem:item];
      }
    } else {
      // this is a font family
      NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:font action:nil keyEquivalent:@""];
      item.target = self;
      [item setEnabled:false];
      if (self.fontName.menu) {
        [self.fontName.menu addItem:item];
      }
    }
  }
}

- (void)loadBackgroundImages {
  NSArray<NSString *> *allImages = @[@"Antique Wood", @"Autumn Leaves", @"Autumn Sunset", @"Autumn by the Lake",
                                     @"Beach and Palm Tree", @"Blue Skies", @"Bokeh (Blue)", @"Bokeh (Golden)",
                                     @"Bokeh (Green)", @"Bokeh (Orange)", @"Bokeh (Rainbow)", @"Bokeh (White)",
                                     @"Burning Fire", @"Cherry Blossom", @"Coffee Beans", @"Cracked Earth",
                                     @"Geometric Pattern 1", @"Geometric Pattern 2", @"Geometric Pattern 3",
                                     @"Geometric Pattern 4", @"Grass", @"Halloween", @"In the Forest", @"Jute Pattern",
                                     @"Polka Dots (Purple)", @"Polka Dots (Teal)", @"Red Bricks", @"Red Hearts",
                                     @"Red Rose", @"Sandy Beach", @"Sheet Music", @"Snowy Mountain", @"Spruce Tree Needles",
                                     @"Summer Fruits", @"Swimming Pool", @"Tree Silhouette", @"Tulip Field", @"Vintage Floral",
                                     @"Zebra Stripes"];


  for (NSString *image in allImages) {
    NSMenuItem *item = [[NSMenuItem alloc] initWithTitle:image action:@selector(changeBackgroundImage:) keyEquivalent:@""];
    item.target = self;
    if (self.backgroundImage.menu) {
      [self.backgroundImage.menu addItem:item];
    }
  }
}

- (void)changeFontName:(NSMenuItem *)sender {
  NSString *captionFontName = self.fontName.titleOfSelectedItem;
  if (!captionFontName) {
    captionFontName = @"";
  }
  Screenshot *screenshot = [self getDocumentScreenshot];
  if (screenshot) {
    screenshot.captionFontName = captionFontName;
  }
  [self generatePreview];
}


- (void)generatePreview {
  NSSize size = NSMakeSize(1242, 2208);
  
  __weak typeof(self) weakSelf = self;
  NSImage *image = [NSImage imageWithSize:size flipped:NO drawingHandler:^BOOL(NSRect rect) {
    if (!weakSelf) {
      return NO;
    }
    __strong typeof(self) strongSelf = weakSelf;
    NSGraphicsContext *ctx = [NSGraphicsContext currentContext];
    if (!ctx) {
      return NO;
    }
    CGContextRef cgCtxt = (__bridge CGContextRef)(ctx);
    [strongSelf clearBackground:cgCtxt rect:rect];
    [strongSelf drawBackgroundImage:rect];
    [strongSelf drawColorOverlay:rect];
    CGFloat captionOffset = [self drawCaption:cgCtxt rect:rect];
    [strongSelf drawDevice:cgCtxt rect:rect captionOffset:captionOffset];
    [strongSelf drawScreenshot:cgCtxt rect:rect captionOffset:captionOffset];
    
    return YES;
  }];
  self.imageView.image = image;
}

- (NSDictionary<NSAttributedStringKey, NSObject *> *)createCaptionAttributes {
  NSMutableParagraphStyle *ps = [NSMutableParagraphStyle new];
  ps.alignment = NSTextAlignmentCenter;
  
  NSDictionary<NSNumber *, NSNumber *> *fontSizes = @{
    @(0.0): @(48.0),
    @(1.0): @(56.0),
    @(2.0): @(64),
    @(3.0): @(72),
    @(4.0): @(80.0),
    @(5.0): @(96.0),
    @(6.0): @(128.0),
  };
  
  NSInteger selectedTag = self.fontSize.selectedTag;
  NSNumber *nsTag = [NSNumber numberWithInteger:selectedTag];
  NSNumber *nsFontSize = fontSizes[nsTag];
  if (!nsFontSize) {
    return nil;
  }
  CGFloat fontSize = [nsFontSize doubleValue];

  NSString *unTrimmedName = self.fontName.selectedItem.title;
  NSString *selectedFontName = [unTrimmedName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  if (!selectedFontName) {
    return nil;
  }
  NSFont *font = [NSFont fontWithName:selectedFontName size:fontSize];
  if (!font) {
    return nil;
  }
  
  NSColor *color = self.fontColor.color;
  
  return @{
    NSParagraphStyleAttributeName: ps,
    NSFontAttributeName: font,
    NSForegroundColorAttributeName: color,
  };
}

- (void)setShadow {
  NSShadow *shadow = [NSShadow new];
  shadow.shadowOffset = CGSizeZero;
  shadow.shadowColor = [NSColor blackColor];
  shadow.shadowBlurRadius = 50.0;
  
  [shadow set];
}

- (CGFloat)drawCaption:(CGContextRef)context rect:(CGRect)rect {
  if ([self shouldDoShadow]) {
    [self setShadow];
  }
  
  // pull out the string to render
  NSString *string = @"";
  if (self.caption.textStorage.string) {
    string = self.caption.textStorage.string;
  }
  
  // Inset the rendering rect to keep the text off the edges.
  CGRect insetRect = CGRectInset(rect, 40, 40);
  
  // combine the user's text with their attributes to create an attributed string.
  NSDictionary<NSAttributedStringKey, NSObject *> *captionAttributes = [self createCaptionAttributes];

  NSAttributedString *attributedString = [[NSAttributedString alloc] initWithString:string attributes:captionAttributes];
  
  // draw the string in the inset rect
  [attributedString drawInRect:insetRect];
  
  // if the shadow is set to "strong" then we'll draw the string again to make the shadow deeper
  if ([self shadowIsSetToStrong]) {
    [attributedString drawInRect:insetRect];
  }
  
  // clear the shadow so it doesn't affect other stuff.
  NSShadow *noShadow = [NSShadow new];
  [noShadow set];
  
  // calculate how much space this attributed string needs
  CGSize availableSpace = CGSizeMake(insetRect.size.width, CGFLOAT_MAX);
  CGRect textFrame = [attributedString boundingRectWithSize:availableSpace
                                                    options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading];
  
  // send the height back to our caller
  
  return textFrame.size.height;
}

- (void)drawDevice:(CGContextRef)context rect:(CGRect)rect captionOffset:(CGFloat)captionOffset {
  NSImage *image = [NSImage imageNamed:@"iPhone"];
  if (!image) {
    return;
  }
  CGFloat offsetX = (rect.size.width - image.size.width) / 2.0;
  CGFloat offsetY = (rect.size.height - image.size.height) / 2.0;
  offsetY -= captionOffset;

  if ([self shouldDoShadow]) {
    [self setShadow];
  }
  
  CGPoint point = CGPointMake(offsetX, offsetY);
  [image drawAtPoint:point fromRect:CGRectZero operation:NSCompositingOperationSourceOver fraction:1.0];
  
  if ([self shadowIsSetToStrong]) {
    // create a stronger drop shadow by drawing again.
    [image drawAtPoint:point fromRect:CGRectZero operation:NSCompositingOperationSourceOver fraction:1.0];
  }
  
  // clear the shadow so it doesn't affect other stuff (Paul said)
  NSShadow *shadow = [NSShadow new];
  [shadow set];
}

- (BOOL)shadowIsSetToStrong {
  if (self.dropShadowStrength.selectedSegment != 2) {
    return NO;
  }
  return (self.dropShadowTarget.selectedSegment == 0 || self.dropShadowTarget.selectedSegment == 2);
}

- (BOOL)shouldDoShadow {
  if (self.dropShadowStrength.selectedSegment == 0) {
    return NO;
  }
  return (self.dropShadowTarget.selectedSegment == 1 || self.dropShadowTarget.selectedSegment == 2);
}

// This was done as a computed var in the original Swift code - this seems semantically equivalent to what Paul wrote.
- (Document * _Nonnull)getDocument {
  Document *oughtToBeDocument = self.view.window.windowController.document;
  NSAssert(oughtToBeDocument != nil, @"Unexpected nil document");
  return oughtToBeDocument;
}

- (Screenshot * _Nullable)getDocumentScreenshot {
  return [self getDocument].screenshot;
}

#pragma mark - NSTextViewDelegate
- (void)textDidChange:(NSNotification *)notification
{
  Screenshot *screenshot = [self getDocumentScreenshot];
  if (screenshot) {
    screenshot.caption = self.caption.string;
  }
  [self generatePreview];
}
@end
