//
//  Screenshot.h
//  Project13Objc
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface Screenshot : NSObject<NSCoding>
@property (nonatomic, nullable) NSString *caption;
@property (nonatomic, nullable) NSString *captionFontName;
@property (nonatomic) NSInteger captionFontSize;
@property (nonatomic, nullable) NSColor *captionColor;
@property (nonatomic, nullable) NSString *backgroundImage;
@property (nonatomic, nullable) NSColor *backgroundColorStart;
@property (nonatomic, nullable) NSColor *backgroundColorEnd;
@property (nonatomic) NSInteger dropShadowStrength;
@property (nonatomic) NSInteger dropShadowTarget;


@end

NS_ASSUME_NONNULL_END
