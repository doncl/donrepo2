//
//  Document.h
//  Project13Objc
//
//  Created by Don Clore on 7/18/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class Screenshot;

@interface Document : NSDocument
@property (nonatomic, nullable) Screenshot *screenshot;

@end

