//
//  Document.m
//  Project13Objc
//
//  Created by Don Clore on 7/18/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "Document.h"
#import "Screenshot.h"

@interface Document ()
@end

@implementation Document
- (instancetype)init {
    self = [super init];
    if (self) {
    // Add your subclass-specific initialization here.
    }
    return self;
}

+ (BOOL)autosavesInPlace {
  return YES;
}

- (void)makeWindowControllers {
  // Override to return the Storyboard file name of the document.
  [self addWindowController:[[NSStoryboard storyboardWithName:@"Main" bundle:nil] instantiateControllerWithIdentifier:@"Document Window Controller"]];
}

- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError {
  return [NSKeyedArchiver archivedDataWithRootObject:self.screenshot requiringSecureCoding:NO error:outError];
}


- (BOOL)readFromData:(NSData *)data ofType:(NSString *)typeName error:(NSError **)outError {
  Screenshot *loadedScreenshot = [NSKeyedUnarchiver unarchivedObjectOfClass:Screenshot.class fromData:data error:outError];
  if (*outError != nil) {
    return NO;
  }
  self.screenshot = loadedScreenshot;
  return YES;
}

@end
