//
//  Screenshot.m
//  Project13Objc
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "Screenshot.h"

NSString *const CaptionKey = @"caption";
NSString *const CaptionFontNameKey = @"captionFontName";
NSString *const CaptionFontSizeKey = @"captionFontSize";
NSString *const CaptionColorKey = @"captionColor";
NSString *const BackgroundImageKey = @"backgroundImage";
NSString *const BackgroundColorStartKey = @"backgroundColorStart";
NSString *const BackgroundColorEndKey = @"backgroundColorEnd";
NSString *const DropShadowStrengthKey = @"dropShadowStrength";
NSString *const DropShadowTargetKey = @"dropShadowTarget";

// OK, there was a lot of fumbling with simple stuff like this.  At this juncture, I'm not quick, and have gotten awful used to
// Swift.

@implementation Screenshot
- (void)encodeWithCoder:(nonnull NSCoder *)coder {
  [coder encodeObject:self.caption forKey:CaptionKey];
  [coder encodeObject:self.captionFontName forKey:CaptionFontNameKey];
  [coder encodeInt:(int)self.captionFontSize forKey:CaptionFontSizeKey];
  [coder encodeObject:self.captionColor forKey:CaptionColorKey];
  [coder encodeObject:self.backgroundImage forKey:BackgroundImageKey];
  [coder encodeObject:self.backgroundColorStart forKey:BackgroundColorStartKey];
  [coder encodeObject:self.backgroundColorEnd forKey:BackgroundColorEndKey];
  [coder encodeInt:(int)self.dropShadowStrength forKey:DropShadowStrengthKey];
  [coder encodeInt:(int)self.dropShadowTarget forKey:DropShadowTargetKey];
  
}

- (nullable instancetype)initWithCoder:(nonnull NSCoder *)coder {
  self = [super init];
  if (self) {
    self.caption = [coder decodeObjectOfClass:NSString.class forKey:CaptionKey];
    self.captionFontName = [coder decodeObjectOfClass:NSString.class forKey:CaptionFontNameKey];
    self.captionFontSize = [coder decodeIntegerForKey:CaptionFontSizeKey];
    self.captionColor = [coder decodeObjectOfClass:NSColor.class forKey:CaptionColorKey];
    self.backgroundImage = [coder decodeObjectOfClass:NSImage.class forKey:BackgroundImageKey];
    self.backgroundColorStart = [coder decodeObjectOfClass:NSColor.class forKey:BackgroundColorStartKey];
    self.backgroundColorEnd = [coder decodeObjectOfClass:NSColor.class forKey:BackgroundColorEndKey];
    self.dropShadowStrength = [coder decodeIntegerForKey:DropShadowStrengthKey];
    self.dropShadowTarget = [coder decodeIntegerForKey:DropShadowTargetKey];
  }
  return self;
}

@end
