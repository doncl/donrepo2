import QuartzCore
import UIKit
import PlaygroundSupport

public class EasingFunctionGraph: UIView  {

	private var x1: CGFloat
	private var y1: CGFloat
	private var x2: CGFloat
	private var y2: CGFloat
	
	public init(x1: CGFloat, y1: CGFloat, x2: CGFloat, y2: CGFloat) {
		self.x1 = x1
		self.x2 = x2
		self.y1 = y1
		self.y2 = y2
		
		super.init(frame: CGRect(x: 0, y: 0, width: 320, height: 320))
		
		backgroundColor = UIColor.white
		
	}
	
	public required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	override public func draw(_ rect: CGRect) {
		let path = UIBezierPath()
		
		UIColor.blue.setStroke()
		path.lineWidth = 3.0
		path.move(to: CGPoint(x: 0, y: 0))
		path.addCurve(to: CGPoint(x: 1 * rect.maxX, y: 1 * rect.maxY), controlPoint1: CGPoint(x: x1 * rect.maxX, y: y1 *               rect.maxY), controlPoint2: CGPoint(x: x2 * rect.maxX, y: y2 * rect.maxY))
		path.stroke()
		
	}
}

let firstLine = EasingFunctionGraph(x1: 0.25, y1: 0.1, x2: 0.25, y2: 1.0)
firstLine.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI_2))

PlaygroundPage.current.liveView = firstLine
