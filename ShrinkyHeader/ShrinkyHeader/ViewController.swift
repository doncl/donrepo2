//
//  ViewController.swift
//  ShrinkyHeader
//
//  Created by Don Clore on 4/16/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  @IBOutlet var headerViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet var headerView: UIView!
  
  let headerViewMaxHeight: CGFloat = 250
  let headerViewMinHeight: CGFloat = 44 + UIApplication.shared.statusBarFrame.height

  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }


}

extension ViewController: UIScrollViewDelegate {
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    let y: CGFloat = scrollView.contentOffset.y
    let newHeaderViewHeight: CGFloat = headerViewHeightConstraint.constant - y
    if newHeaderViewHeight > headerViewMaxHeight {
      headerViewHeightConstraint.constant = headerViewMaxHeight
    } else if newHeaderViewHeight < headerViewMinHeight {
      headerViewHeightConstraint.constant = headerViewMinHeight
    } else {
      headerViewHeightConstraint.constant = newHeaderViewHeight
      scrollView.contentOffset.y = 0 // block scroll view
    }
  }
}

