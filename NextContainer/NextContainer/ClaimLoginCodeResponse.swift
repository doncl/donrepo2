//
//  ClaimLoginCodeResponse.swift
//  PasswordlessLogin
//
//  Created by Don Clore on 3/12/20.
//  Copyright © 2020 theMaven Network. All rights reserved.
//

import Foundation


enum LoginCodeSource: String, Codable {
  case none = "None"
  case login = "Login"
  case joinfree = "JoinFree"
  case subscribe = "Subscribe"
}

enum LoginCodeOperation: String, Codable {
  case none = "None"
  case register = "Register"
  case login = "Login"
  case newnotificationsubscription = "NewNotificationSubscription"
}


struct ClaimLoginCodeResponse: Codable, JSONCoding {
  typealias codableType = ClaimLoginCodeResponse
  
  struct UserLoginResponse: Codable, JSONCoding {
    typealias codableType = UserLoginResponse
    
    let loginToken: String
    let id: String
    let email: String
  }
  
  let isValid: Bool
  let operation: LoginCodeOperation
  let source: LoginCodeSource
  let userLogin: UserLoginResponse
}
