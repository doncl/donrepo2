//
//  ClaimLoginCodeRequest.swift
//  PasswordlessLogin
//
//  Created by Don Clore on 3/12/20.
//  Copyright © 2020 theMaven Network. All rights reserved.
//

import Foundation


struct ClaimLoginCodeRequest: Codable, JSONCoding  {
  typealias codableType = ClaimLoginCodeRequest
  
  let loginCodeId: String
  var code: String?
  var shortCode: String?
}
