//
//  InitialLoginPaneVC.swift
//  PasswordlessLogin
//
//  Created by Don Clore on 3/12/20.
//  Copyright © 2020 theMaven Network. All rights reserved.
//

import UIKit

class InitialLoginPaneVC: LoginPaneVC {
  override var nextPaneType: PasswordlessLogin.PaneType {
    let paneType: PasswordlessLogin.PaneType = PasswordlessLogin.PaneType.waitingforcode
    return paneType
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    loginLabel.text = "LOGIN"
    infoLabel.text = "Continue with your Email to\nlogin or create an account"
    infoLabel.numberOfLines = 2
    userInputTextField.placeholder = "Email Address"
    userInputTextField.delegate = self
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  @objc override func nextButtonHandler() {
    stopSpinner()
    let paneType = self.nextPaneType
    self.delegate?.nextButtonPressed(nextPane: paneType)
  }
}

extension InitialLoginPaneVC: UITextFieldDelegate {
  private func isValidEmail(_ email: String) -> Bool {
      let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

      let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
      return emailPred.evaluate(with: email)
  }
    
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if let text = textField.text {
      let nsText = NSString(string: text)
      let newText = nsText.replacingCharacters(in: range, with: string)
      contract()
      validateEmailText(newText)
    }
    return true
  }
  
  private func validateEmailText(_ newText: String) {
    let enabled: Bool = isValidEmail(newText)
    enableNextButton(enabled: enabled)
  }
}
