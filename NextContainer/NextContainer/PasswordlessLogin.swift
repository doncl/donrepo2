//
//  ViewController.swift
//  NextContainer
//
//  Created by Don Clore on 3/18/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class PasswordlessLogin: UIViewController {
  enum PaneType {
    case initiallogin
    case waitingforcode
    case done
  }
  
  let loginSize: CGSize = CGSize(width: 375, height: 420)
  
  // MARK: Subview properties
  lazy var blurBack: UIImageView = {
    let imageView = UIImageView(image: UIImage(named: "blurback"))
    imageView.contentMode = .scaleAspectFill
    imageView.translatesAutoresizingMaskIntoConstraints = false
    return imageView
  }()
  
  lazy var blurView: UIVisualEffectView = {
    let blurEffect: UIBlurEffect
    if #available(iOS 13.0, *) {
      blurEffect = UIBlurEffect(style: UIBlurEffect.Style.systemChromeMaterialDark)
    } else {
      // Fallback on earlier versions
      blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
    }

    let blurView = UIVisualEffectView(effect: blurEffect)
    blurView.translatesAutoresizingMaskIntoConstraints = false
    blurView.clipsToBounds = true
    blurView.accessibilityIdentifier = "blur"
    return blurView
  }()
  
  lazy var vibrancyView: UIVisualEffectView = {
    var vibrancyEffect: UIVibrancyEffect?
    
    if let blurEffect = blurView.effect as? UIBlurEffect {
      if #available(iOS 13.0, *) {
        vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect, style: .fill)
      } else {
        // Fallback on earlier versions
        vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
      }
    }

    let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
    vibrancyView.translatesAutoresizingMaskIntoConstraints = false
    vibrancyView.accessibilityIdentifier = "vibrancy"
    return vibrancyView
  }()
  
    
  lazy var paneFrame: NextContainer = {
    let pane = InitialLoginPaneVC()
    currentPane = pane
    pane.delegate = self
    let next = NextContainer(currentViewController: pane)
    return next
  }()
  
  weak var currentPane: UIViewController?
  
  

  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.addSubview(blurBack)
    view.addSubview(blurView)
    view.backgroundColor = .clear
    blurView.contentView.addSubview(vibrancyView)
    addChild(paneFrame)
    view.addSubview(paneFrame.view)
    paneFrame.didMove(toParent: self)
    
    paneFrame.view.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      blurBack.topAnchor.constraint(equalTo: view.topAnchor),
      blurBack.leftAnchor.constraint(equalTo: view.leftAnchor),
      blurBack.rightAnchor.constraint(equalTo: view.rightAnchor),
      blurBack.bottomAnchor.constraint(equalTo: view.bottomAnchor),
      
      blurView.topAnchor.constraint(equalTo: view.topAnchor),
      blurView.leftAnchor.constraint(equalTo: view.leftAnchor),
      blurView.rightAnchor.constraint(equalTo: view.rightAnchor),
      blurView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
      
      vibrancyView.topAnchor.constraint(equalTo: blurView.topAnchor),
      vibrancyView.leftAnchor.constraint(equalTo: blurView.leftAnchor),
      vibrancyView.rightAnchor.constraint(equalTo: blurView.rightAnchor),
      vibrancyView.bottomAnchor.constraint(equalTo: blurView.bottomAnchor),
            
      paneFrame.view.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      paneFrame.view.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      paneFrame.view.heightAnchor.constraint(equalToConstant: loginSize.height),
      paneFrame.view.widthAnchor.constraint(equalToConstant: loginSize.width),
    ])
  }
}

extension PasswordlessLogin: LoginPaneVCDelegate {
  func closeButtonPressed() {
    dismiss(animated: true, completion: nil)
  }
    
  func nextButtonPressed(nextPane: PaneType) {
    switch nextPane {
    case .initiallogin:
      let pane = InitialLoginPaneVC()
      currentPane = pane
      pane.delegate = self
      paneFrame.setNextViewController(nextViewController: pane)
      break
    case .waitingforcode:
      let waitingForCode = WaitingForCodePaneVC()
      currentPane = waitingForCode
      waitingForCode.delegate = self
      paneFrame.setNextViewController(nextViewController: waitingForCode)
    case .done:
      dismiss(animated: true)
    }
  }
}

