//
//  LoginPaneVC.swift
//  PasswordlessLogin
//
//  Created by Don Clore on 3/11/20.
//  Copyright © 2020 theMaven Network. All rights reserved.
//

import UIKit

protocol LoginPaneVCDelegate: class {
  func closeButtonPressed()
  func nextButtonPressed(nextPane: PasswordlessLogin.PaneType)
}

class LoginPaneVC: UIViewController {
  static let loginSize: CGSize = CGSize(width: 375, height: 368)
  static let expansionDelta: CGFloat = 52.0
  static let cornerRadius: CGFloat = 16.0
  static let topPad: CGFloat = 34
  let leftPad: CGFloat = 38.67
  static let closeButtonLeftPad: CGFloat = 26.0
  static let closeButtonDim: CGFloat = 32.0
  let loginToTextVertPad: CGFloat = 50.0
  let infoTextSize: CGSize = CGSize(width: 200, height: 40)
  let topLineY: CGFloat = 188.0
  let lineMargins: CGFloat = 32.0
  let lineHeight: CGFloat = 1.0
  let lineYDelta: CGFloat = 72.0
  let emailTextFieldHeight: CGFloat = 32.0
  let emailTextFieldWidth: CGFloat = 311.0
  let emailTextFieldY: CGFloat = 208.0
  static let nextButtonY: CGFloat = 292.0
  let nextButtonSize: CGSize = CGSize(width: 47, height: 20)
  let nextButtonLeft: CGFloat = 277.0
  let chevronSize: CGSize = CGSize(width: 12, height: 20)
  let errorMessageHeight: CGFloat = 20
  let errorMessageTopPad: CGFloat = 32.0
  
  var loginPaneHeightConstraint: NSLayoutConstraint = NSLayoutConstraint()
  var nextButtonTopConstraint: NSLayoutConstraint = NSLayoutConstraint()
  var emailTextFieldTopConstraint: NSLayoutConstraint = NSLayoutConstraint()
  
  var expanded = false
  
  weak var delegate: LoginPaneVCDelegate?
  
  lazy var spinner: UIActivityIndicatorView = {
    let spinner = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
    spinner.isHidden = true
    if #available(iOS 13.0, *) {
      spinner.style = .large
    } else {
      spinner.style = .gray
    }
    return spinner
  }()
  
  lazy var loginPane: UIView = {
    let loginPane = UIView()
    loginPane.translatesAutoresizingMaskIntoConstraints = false
    let color = UIColor(named: "loginBackground")
    loginPane.backgroundColor = color
    loginPane.accessibilityIdentifier = "login"
    loginPane.layer.cornerRadius = LoginPaneVC.cornerRadius
    return loginPane
  }()
  
  lazy var closeButton: UIImageView = {
    let button = UIImageView()
    button.translatesAutoresizingMaskIntoConstraints = false
    let image = UIImage(named: "ic_clear")
    button.image = image
    let color = UIColor(named: "loginTextLight")
    button.tintColor = color
    let tap = UITapGestureRecognizer(target: self, action: #selector(LoginPaneVC.closeButtonPressed(_:)))
    button.isUserInteractionEnabled = true
    button.addGestureRecognizer(tap)
    return button
  }()
  
  
  lazy var loginLabel: UILabel = {
    let label: UILabel = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont(name: "IBMPlexSans-SemiBold", size: 18)
    label.textColor = UIColor(named: "loginColor")
    let para = NSMutableParagraphStyle()
    para.lineHeightMultiple = 0.96
    label.attributedText = NSMutableAttributedString(string: "",
                                                     attributes: [NSAttributedString.Key.paragraphStyle: para])

    label.textAlignment = NSTextAlignment.center
    label.sizeToFit()
    return label
  }()
  
  lazy var infoLabel: UILabel = {
    let label: UILabel = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont(name: "IBMPlexSans-Regular", size: 16)
    label.textColor = UIColor(named: "loginTextLight")
    label.numberOfLines = 2
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.textAlignment = NSTextAlignment.center
    let para = NSMutableParagraphStyle()
    para.lineHeightMultiple = 0.96
    label.attributedText = NSMutableAttributedString(string: "",
                                                     attributes: [NSAttributedString.Key.paragraphStyle: para])

    label.sizeToFit()
    return label
  }()
  
  lazy var userInputTextField: UITextField = {
    let tx: UITextField = UITextField()
    tx.translatesAutoresizingMaskIntoConstraints = false
    tx.backgroundColor = .clear
    tx.font = UIFont(name: "IBMPlexSans-SemiBold", size: 18)
    tx.textAlignment = NSTextAlignment.center
    tx.autocorrectionType = .no
    tx.autocapitalizationType = .none
    tx.placeholder = "Email Address"
    let para = NSMutableParagraphStyle()
    para.lineHeightMultiple = 1.37
    tx.attributedText = NSMutableAttributedString(string: "", attributes: [NSAttributedString.Key.paragraphStyle: para])
    return tx
  }()
  
  lazy var nextButton: UILabel = {
    let nextButton: UILabel = UILabel()
    nextButton.translatesAutoresizingMaskIntoConstraints = false
    let color = UIColor(named: "enabledNextColor")
    
    let para = NSMutableParagraphStyle()
    para.lineHeightMultiple = 0.96
    nextButton.attributedText = NSMutableAttributedString(string: "NEXT",
                                                          attributes: [NSAttributedString.Key.paragraphStyle: para])

    nextButton.backgroundColor = .clear
    nextButton.font = UIFont(name: "IBMPlexSans-Bold", size: 16)
    nextButton.textColor = color
    let tap = UITapGestureRecognizer(target: self, action: #selector(LoginPaneVC.nextButtonPressed(_:)))
    nextButton.addGestureRecognizer(tap)
    nextButton.isUserInteractionEnabled = true
    return nextButton
  }()
  
  lazy var rightChevron: UIImageView = {
    let chevron = UIImageView()
    chevron.contentMode = .scaleAspectFill
    let image = UIImage(named: "loginNextArrow")
    chevron.image = image?.withRenderingMode(.alwaysTemplate)
    let color = UIColor(named: "enabledNextColor")
    chevron.tintColor = color
    chevron.translatesAutoresizingMaskIntoConstraints = false
    chevron.isUserInteractionEnabled = true
    let tap = UITapGestureRecognizer(target: self, action: #selector(LoginPaneVC.nextButtonPressed(_:)))
    chevron.addGestureRecognizer(tap)
    return chevron
  }()
  
  lazy var errorMessage: UILabel = {
    let message: UILabel = UILabel()
    message.translatesAutoresizingMaskIntoConstraints = false
    message.textColor = UIColor(named: "errorColor")
    message.font = UIFont(name: "IBMPlexSans-Regular", size: 16)
    message.textAlignment = NSTextAlignment.center
    let para = NSMutableParagraphStyle()
    para.lineHeightMultiple = 0.96
    message.attributedText = NSMutableAttributedString(string: "",
                                                     attributes: [NSAttributedString.Key.paragraphStyle: para])

    message.isHidden = true
    return message
  }()
  
  var nextPaneType: PasswordlessLogin.PaneType {
    return .done
  }

  
  let topLine: CALayer = CALayer()
  let bottomLine: CALayer = CALayer()
    
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .clear
    
    view.addSubview(loginPane)
    
    loginPane.addSubview(closeButton)
    loginPane.addSubview(loginLabel)
    loginPane.addSubview(infoLabel)
    loginPane.addSubview(userInputTextField)
    loginPane.addSubview(errorMessage)
    loginPane.addSubview(nextButton)
    loginPane.addSubview(rightChevron)
    
    loginPane.layer.addSublayer(topLine)
    loginPane.layer.addSublayer(bottomLine)
    topLine.backgroundColor = UIColor(named: "loginLine")?.cgColor
    bottomLine.backgroundColor = UIColor(named: "loginLine")?.cgColor
    
    loginPaneHeightConstraint = loginPane.heightAnchor.constraint(equalToConstant: LoginPaneVC.loginSize.height)
    nextButtonTopConstraint = nextButton.topAnchor.constraint(equalTo: loginPane.topAnchor, constant: LoginPaneVC.nextButtonY)
    emailTextFieldTopConstraint = userInputTextField.topAnchor.constraint(equalTo: loginPane.topAnchor, constant: emailTextFieldY)
    
    NSLayoutConstraint.activate([
      
      loginPane.topAnchor.constraint(equalTo: view.topAnchor),
      loginPane.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      loginPane.widthAnchor.constraint(equalToConstant: LoginPaneVC.loginSize.width),
      loginPaneHeightConstraint,
      
      closeButton.topAnchor.constraint(equalTo: loginPane.topAnchor, constant: LoginPaneVC.topPad),
      closeButton.leftAnchor.constraint(equalTo: loginPane.leftAnchor, constant: LoginPaneVC.closeButtonLeftPad),
      closeButton.widthAnchor.constraint(equalToConstant: LoginPaneVC.closeButtonDim),
      closeButton.heightAnchor.constraint(equalToConstant: LoginPaneVC.closeButtonDim),
      
      loginLabel.centerXAnchor.constraint(equalTo: loginPane.centerXAnchor),
      loginLabel.centerYAnchor.constraint(equalTo: closeButton.centerYAnchor),
      
      infoLabel.centerXAnchor.constraint(equalTo: loginPane.centerXAnchor),
      infoLabel.topAnchor.constraint(equalTo: loginLabel.bottomAnchor, constant: loginToTextVertPad),
      infoLabel.widthAnchor.constraint(equalTo: loginPane.widthAnchor, constant: -16),
      
      emailTextFieldTopConstraint,
      userInputTextField.centerXAnchor.constraint(equalTo: loginPane.centerXAnchor),
      userInputTextField.heightAnchor.constraint(equalToConstant: emailTextFieldHeight),
      userInputTextField.widthAnchor.constraint(equalToConstant: emailTextFieldWidth),
      
      errorMessage.topAnchor.constraint(equalTo: userInputTextField.bottomAnchor, constant: errorMessageTopPad),
      errorMessage.centerXAnchor.constraint(equalTo: loginPane.centerXAnchor),
      errorMessage.heightAnchor.constraint(equalToConstant: errorMessageHeight),
      errorMessage.widthAnchor.constraint(equalTo: userInputTextField.widthAnchor),
      
      nextButton.leftAnchor.constraint(equalTo: loginPane.leftAnchor, constant: nextButtonLeft),
      nextButtonTopConstraint,
      nextButton.widthAnchor.constraint(equalToConstant: nextButtonSize.width),
      nextButton.heightAnchor.constraint(equalToConstant: nextButtonSize.height),
      
      rightChevron.rightAnchor.constraint(equalTo: loginPane.rightAnchor, constant: -lineMargins),
      rightChevron.centerYAnchor.constraint(equalTo: nextButton.centerYAnchor),
      rightChevron.widthAnchor.constraint(equalToConstant: chevronSize.width),
      rightChevron.heightAnchor.constraint(equalToConstant: chevronSize.height),
    ])
    
    // DEBUG stuff for the growing/shrinking view.  Leave disabled.
    
//    let tap = UITapGestureRecognizer(target: self, action: #selector(LoginPaneVC.backgroundTapped(_:)))
//    view.addGestureRecognizer(tap)
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    let width: CGFloat = loginPane.bounds.width - (lineMargins * 2)
    let topLineFrame: CGRect = CGRect(x: lineMargins, y: topLineY, width: width, height: lineHeight)
    let bottomLineFrame: CGRect = topLineFrame.offsetBy(dx: 0, dy: lineYDelta)
    topLine.frame = topLineFrame
    bottomLine.frame = bottomLineFrame
    loginPane.layer.cornerRadius = LoginPaneVC.cornerRadius
  }
}

extension LoginPaneVC {
//  @objc func backgroundTapped(_ sender: UITapGestureRecognizer) {
//    if self.expanded {
//      self.contract()
//    } else {
//      self.expandWithError(error: "Whatever error message here")
//    }
//  }
  
  func expandWithError(error: String) {
    errorMessage.text = error
    errorMessage.alpha = 0
    UIView.animate(withDuration: 0.4, delay: 0.0, options: [.curveEaseInOut], animations: { [weak self] in
      guard let self = self else { return }
      let newHeight = LoginPaneVC.loginSize.height + LoginPaneVC.expansionDelta
      self.loginPaneHeightConstraint.constant = newHeight
      self.nextButtonTopConstraint.constant = LoginPaneVC.nextButtonY + LoginPaneVC.expansionDelta
      self.errorMessage.isHidden = false
      self.errorMessage.alpha = 1
      self.view.layoutIfNeeded()
      self.loginPane.layoutIfNeeded()
    }, completion: { [weak self] _ in
      guard let self = self else { return }
      self.expanded = true
    })
  }
  
  func contract() {
    UIView.animate(withDuration: 0.4, delay: 0.0, options: [.curveEaseInOut], animations: { [weak self] in
      guard let self = self else { return }
      self.errorMessage.text = ""
      self.errorMessage.isHidden = true
      self.errorMessage.alpha = 0
      self.loginPaneHeightConstraint.constant = LoginPaneVC.loginSize.height
      self.nextButtonTopConstraint.constant = LoginPaneVC.nextButtonY
      self.view.layoutIfNeeded()
      self.loginPane.layoutIfNeeded()
    }, completion: { [weak self] _ in
      guard let self = self else { return }
      self.expanded = false
    })
  }
  
  func simpleError(_ error: String) {
    let ac = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
    let ok = UIAlertAction(title: "OK", style: .default)
    ac.addAction(ok)
    present(ac, animated: true)
  }
  
  
  @objc func nextButtonPressed(_ sender: UITapGestureRecognizer) {
    print("\(#function)")
    spinner.center = view.center
    spinner.isHidden = false
    spinner.startAnimating()
    loginPane.addSubview(spinner)
    loginPane.bringSubviewToFront(spinner)
    
    nextButtonHandler()
  }
  
  func stopSpinner() {
    spinner.stopAnimating()
    spinner.isHidden = true
    spinner.removeFromSuperview()
  }
  
  @objc func nextButtonHandler() {
    stopSpinner()
  }
   
  @objc func closeButtonPressed(_ sender: UITapGestureRecognizer) {
    spinner.stopAnimating()
    spinner.isHidden = true
    spinner.removeFromSuperview()
    delegate?.closeButtonPressed()
  }
  
  func enableNextButton(enabled: Bool) {
    let color: UIColor? = enabled ? UIColor(named: "enabledNextColor") : UIColor(named: "nextColor")
    nextButton.isUserInteractionEnabled = enabled
    nextButton.textColor = color
    rightChevron.isUserInteractionEnabled = enabled
    rightChevron.tintColor = color
  }
}

