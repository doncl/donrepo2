//
//  CreateLoginCodeRequest.swift
//  PasswordlessLogin
//
//  Created by Don Clore on 3/12/20.
//  Copyright © 2020 theMaven Network. All rights reserved.
//

import Foundation


enum LoginCodeDevice: String, Codable  {
  case web = "Web"
  case ios = "IOSApp"
  case android = "AndroidApp"
}

struct CreateLoginCodeRequest: Codable, JSONCoding {
  typealias codableType = CreateLoginCodeRequest
   
  let operation: LoginCodeOperation = LoginCodeOperation.register
  let email: String
  let device: LoginCodeDevice = LoginCodeDevice.ios
  let channelKey: String?
}
