//
//  NextContainer.swift
//  Duwamish
//
//  Created by Don Clore on 3/19/20.
//  Copyright © 2020 theMaven Network. All rights reserved.
//

import UIKit

class NextContainer: UIViewController {
  var currentViewController: UIViewController
  
  init(currentViewController: UIViewController) {
    self.currentViewController = currentViewController
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
      
    adopt(child: currentViewController)
  }
  
  func setNextViewController(nextViewController: UIViewController) {
    let prev = currentViewController
    let next = nextViewController
    adopt(child:next)
    
    next.view.alpha = 0.0
    next.view.transform = CGAffineTransform(translationX: 375, y: 0)
    UIView.animate(withDuration: 0.400, delay: 0, options: [.curveEaseInOut], animations: {
      next.view.transform = .identity
      next.view.alpha = 1.0
      prev.view.transform = CGAffineTransform(translationX: -375, y: 0)
      prev.view.alpha = 0
    }, completion: { [weak self] _ in
      guard let self = self else { return }
      prev.remove()
      self.currentViewController = next
    })
  }
}

extension UIViewController {
  func adopt(child: UIViewController) {
    child.loadViewIfNeeded()
    child.view.translatesAutoresizingMaskIntoConstraints = false
    addChild(child)
    view.addSubview(child.view)
    child.didMove(toParent: self)
    
    NSLayoutConstraint.activate([
      child.view.topAnchor.constraint(equalTo: view.topAnchor),
      child.view.leftAnchor.constraint(equalTo: view.leftAnchor),
      child.view.rightAnchor.constraint(equalTo: view.rightAnchor),
      child.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])
    child.view.frame = view.bounds
  }
  
  func remove() {
    guard parent != nil else {
      return
    }
    willMove(toParent: nil)
    view.removeConstraints(view.constraints)
    view.removeFromSuperview()
    removeFromParent()
  }
}
