//
//  CreateLoginCodeResponse.swift
//  PasswordlessLogin
//
//  Created by Don Clore on 3/12/20.
//  Copyright © 2020 theMaven Network. All rights reserved.
//

import Foundation


struct CreateLoginCodeResponse: Codable, JSONCoding {
  typealias codableType = CreateLoginCodeResponse
  
  let isValid: Bool
  let operation: LoginCodeOperation
  let loginCodeId: String
}
