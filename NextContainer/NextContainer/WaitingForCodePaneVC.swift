//
//  WaitingForCodePaneVC.swift
//  PasswordlessLogin
//
//  Created by Don Clore on 3/12/20.
//  Copyright © 2020 theMaven Network. All rights reserved.
//

import UIKit

class WaitingForCodePaneVC: LoginPaneVC {
  override var nextPaneType: PasswordlessLogin.PaneType {
    return .initiallogin
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    loginLabel.text = "LOGIN"
    infoLabel.text = "A verification email has been sent. Please\nenter the verification code from the email"
    infoLabel.numberOfLines = 2
    userInputTextField.placeholder = "XXXXXX"
    userInputTextField.delegate = self
  }
    
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  @objc override func nextButtonHandler() {
    let paneType = self.nextPaneType
    self.stopSpinner()
    self.delegate?.nextButtonPressed(nextPane: paneType)
  }
}

extension WaitingForCodePaneVC: UITextFieldDelegate {
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    if let text = textField.text {
      let nsText = NSString(string: text)
      let newText = nsText.replacingCharacters(in: range, with: string)
      let enabled: Bool = isValidNumericCode(newText)
      enableNextButton(enabled: enabled)
    }
    return true
  }
  
  private func isValidNumericCode(_ text: String) -> Bool {
    guard let _ = Int(text), text.count == 6 else {
      return false
    }
    return true
  }
}
