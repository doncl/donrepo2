//
//  AboutMeApp.swift
//  AboutMe
//
//  Created by Don Clore on 6/7/22.
//

import SwiftUI

@main
struct AboutMeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
