//
//  ViewController.swift
//  MyMaps
//
//  Created by Don Clore on 3/25/22.
//

import UIKit


class ViewController: UIViewController {
  let floaty = FloatingPlayer()
  var originalFrame: CGRect?
  
  lazy var animator:UIDynamicAnimator = {
      return UIDynamicAnimator(referenceView: view)
  }()
  
  lazy var floatyPhysicalProperties: UIDynamicItemBehavior = {
    let props = UIDynamicItemBehavior(items: [floaty])
    props.allowsRotation = false
    props.resistance = 8
    props.density = 0.02
    return props
  }()

  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
    view.addSubview(floaty)
    
    let pan: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(ViewController.panGestureHandler(_:)))
    floaty.addGestureRecognizer(pan)
  }
  
  private func addBehaviors() {
    let scale = CGAffineTransform(scaleX: 0.5, y: 0.5)
    for vertical in [\UIEdgeInsets.left, \UIEdgeInsets.right] {
      for horizontal in [\UIEdgeInsets.top, \UIEdgeInsets.bottom] {
        let springField = UIFieldBehavior.springField()
        springField.position = CGPoint(x: view.layoutMargins[keyPath: horizontal], y: view.layoutMargins[keyPath: vertical])
        springField.region = UIRegion(size: view.bounds.size.applying(scale))
        animator.addBehavior(springField)
        springField.addItem(floaty)
      }
    }
    let parentViewBoundsCollision = UICollisionBehavior(items: [floaty])
    parentViewBoundsCollision.translatesReferenceBoundsIntoBoundary = true
    animator.addBehavior(parentViewBoundsCollision)
    animator.addBehavior(floatyPhysicalProperties)
    //animator.setValue(true, forKey: "debugEnabled")
  }

  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    let width = view.bounds.width * 0.3
    let height = view.bounds.height * 0.9
    
    
    floaty.frame = CGRect(x: view.center.x - (width / 2), y: view.center.y - (height / 2), width: width, height: height)
    addBehaviors()
  }
  
  @objc func panGestureHandler(_ sender: UIPanGestureRecognizer) {
    switch sender.state {
      case .possible:
        break
      case .began:
        originalFrame = floaty.frame
        break
      case .changed:
        let translation: CGPoint = sender.translation(in: view)
        if let originalFrame = originalFrame {
          floaty.frame = originalFrame.translated(byXOffset: translation.x, andYOffset: translation.y)
        }
        break
      case .ended, .cancelled:
        let velocity = sender.velocity(in: view)
        floatyPhysicalProperties.addLinearVelocity(velocity, for: floaty)
        originalFrame = nil
      case .failed:
        break
      @unknown default:
        break
    }
  }
}


extension CGRect {
  var center: CGPoint {
    return CGPoint(x: midX, y: midY)
  }
  
  func translated(byXOffset xOffset: CGFloat, andYOffset yOffset: CGFloat) -> CGRect {
    return CGRect(x: origin.x + xOffset, y: origin.y + yOffset, width: width, height: height)
  }

}

extension ViewController: UIDynamicAnimatorDelegate {
  func dynamicAnimatorWillResume(_ animator: UIDynamicAnimator) {
    print("\(#function)")
  }

  func dynamicAnimatorDidPause(_ animator: UIDynamicAnimator) {
    print("\(#function)")
  }
}
