//
//  FloatingPlayer.swift
//  MyMaps
//
//  Created by Don Clore on 3/25/22.
//

import UIKit
import AVFoundation
import AVKit

class FloatingPlayer: UIView {
  var playerLayer: AVPlayerLayer = AVPlayerLayer()
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    guard let url = Bundle.main.url(forResource: "video0", withExtension: ".mp4") else {
      fatalError()
    }
    
    let avPlayer: AVPlayer = AVPlayer(url: url)
    playerLayer.player = avPlayer
    layer.addSublayer(playerLayer)
    playerLayer.frame = bounds
    avPlayer.play()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    playerLayer.frame = layer.bounds
  }
  
}
