//
//  Service.swift
//  CHTTPParser
//
//  Created by Don Clore on 2/2/20.
//

import Foundation
import FileKit

public struct AutoSuggestService {
  public struct ResultSet: Codable {
    let labels: [LabelEntity]
    let artists: [ArtistEntity]
    let releases: [ReleaseEntity]
  }
  
  struct Models {
    var labelsTrie: Trie<String, LabelEntity> = Trie<String, LabelEntity>()
    var artistsTrie: Trie<String, ArtistEntity> = Trie<String, ArtistEntity>()
    var releasesTrie: Trie<String, ReleaseEntity> = Trie<String, ReleaseEntity>()
     
    static var instance: Models!
       
    fileprivate init() {
      let projectFolderURL = FileKit.projectFolderURL
      loadLabels(projectFolderURL)
      loadArtists(projectFolderURL)
      loadReleases(projectFolderURL)
    }
        
    private func loadLabels(_ projectFolderURL: URL) {
      let labelsFile = projectFolderURL.appendingPathComponent("smallLabelsFile.json")
      
      do {
        let data = try Data(contentsOf: labelsFile)
        let decoder = JSONDecoder()
        let labelList = try decoder.decode(LabelList.self, from: data)
        let labels = labelList.labels
        for label in labels {
          insertLabelInTrie(label)
        }
        print("Label count = \(labels.count)")
      } catch let error {
        print(error.localizedDescription)
      }
    }
        
    fileprivate func loadArtists(_ projectFolderURL: URL) {
      let artistsFile = projectFolderURL.appendingPathComponent("smallArtistsFile.json")
      
      do {
        let data = try Data(contentsOf: artistsFile)
        let decoder = JSONDecoder()
        let artistList = try decoder.decode(ArtistList.self, from: data)
        let artists = artistList.artists
        for artist in artists {
          insertArtistInTrie(artist)
        }
        print("Artist count = \(artists.count)")
      } catch let error {
        print(error.localizedDescription)
      }
    }
    
    private func loadReleases(_ projectFolderURL: URL) {
      let releasesFile = projectFolderURL.appendingPathComponent("smallReleasesFile.json")
      do {
        let data = try Data(contentsOf: releasesFile)
        let decoder = JSONDecoder()
        let releasesList = try decoder.decode(ReleasesList.self, from: data)
        let releases = releasesList.releases
        for release in releases {
          insertReleaseInTrie(release)
        }
        print("Releases count = \(releases.count)")
      } catch let error {
        print(error.localizedDescription)
      }
    }
 }
  
  static var instance: AutoSuggestService!
  static var initialized = false
  
  static func initialize() {
    guard !initialized else {
      return
    }
    defer {
      initialized = true
    }
    Models.instance = Models()
    instance = AutoSuggestService()
  }
  
  static func getResult(for prefix: String) -> ResultSet {
    let labelsResult = Models.instance.labelsTrie.collections(startingWith: prefix)
    let artistsResult = Models.instance.artistsTrie.collections(startingWith: prefix)
    let releasesResult = Models.instance.releasesTrie.collections(startingWith: prefix)
    return ResultSet(labels: labelsResult, artists: artistsResult, releases: releasesResult)
  }
}
