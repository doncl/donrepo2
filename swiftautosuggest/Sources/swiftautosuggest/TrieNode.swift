public class TrieNode<Key: Hashable, U> {
  
  public var key: Key?
  public weak var parent: TrieNode?
  public var children: [Key: TrieNode] = [:]
  public var isTerminating = false
  public var body: U?
  
  public init(key: Key?, parent: TrieNode?, body: U?) {
    self.key = key
    self.parent = parent
    self.body = body
  }
}
