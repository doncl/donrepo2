public class Trie<CollectionType: Collection, U> where CollectionType.Element: Hashable {
  
  public typealias Node = TrieNode<CollectionType.Element, U>
  
  private let root = Node(key: nil, parent: nil, body: nil)
  
  public init() {}
  
  public func insert(_ collection: CollectionType, body: U) {
    var current = root
    for element in collection {
      if current.children[element] == nil {
        current.children[element] = Node(key: element, parent: current, body: body)
      }
      current = current.children[element]!
    }
    current.isTerminating = true
  }
  
  public func contains(_ collection: CollectionType) -> Bool {
    var current = root
    for element in collection {
      guard let child = current.children[element] else {
        return false
      }
      current = child
    }
    return current.isTerminating
  }
  
  public func remove(_ collection: CollectionType) {
    var current = root
    for element in collection {
      guard let child = current.children[element] else {
        return
      }
      current = child
    }
    guard current.isTerminating else {
      return
    }
    current.isTerminating = false
    while let parent = current.parent, current.children.isEmpty && !current.isTerminating {
      parent.children[current.key!] = nil
      current = parent
    }
  }
}

public extension Trie where CollectionType: RangeReplaceableCollection {
  
  func collections(startingWith prefix: CollectionType) -> [U] {
    var current = root
    for element in prefix {
      guard let child = current.children[element] else {
        return []
      }
      current = child
    }
    return collections(startingWith: prefix, after: current)
  }
  
  private func collections(startingWith prefix: CollectionType, after node: Node) -> [U] {
    
    // 1
    var results: [U] = []
    
    if node.isTerminating {
      if let body = node.body {
        results.append(body)
      }
    }
    
    // 2
    for child in node.children.values {
      var prefix = prefix
      prefix.append(child.key!)
      results.append(contentsOf: collections(startingWith: prefix, after: child))
    }
    
    return results
  }
}

