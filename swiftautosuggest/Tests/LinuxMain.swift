import XCTest

import swiftautosuggestTests

var tests = [XCTestCaseEntry]()
tests += swiftautosuggestTests.allTests()
XCTMain(tests)
