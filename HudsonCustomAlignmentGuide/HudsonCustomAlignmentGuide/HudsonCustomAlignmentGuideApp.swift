//
//  HudsonCustomAlignmentGuideApp.swift
//  HudsonCustomAlignmentGuide
//
//  Created by Don Clore on 2/18/21.
//

import SwiftUI

@main
struct HudsonCustomAlignmentGuideApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
