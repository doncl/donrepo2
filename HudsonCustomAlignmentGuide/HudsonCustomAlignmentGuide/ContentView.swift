//
//  ContentView.swift
//  HudsonCustomAlignmentGuide
//
//  Created by Don Clore on 2/18/21.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    HStack(alignment: .midAccountAndName) {
      VStack {
          Text("@twostraws")
          Image("PaulHudson")
              .resizable()
              .frame(width: 64, height: 64)
      }

      VStack {
          Text("Full name:")
          Text("PAUL HUDSON")
              .font(.largeTitle)
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

extension VerticalAlignment {
    enum MidAccountAndName: AlignmentID {
        static func defaultValue(in d: ViewDimensions) -> CGFloat {
            d[.top]
        }
    }

    static let midAccountAndName = VerticalAlignment(MidAccountAndName.self)
}
