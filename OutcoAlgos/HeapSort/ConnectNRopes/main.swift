//
//  main.swift
//  ConnectNRopes
//
//  Created by Don Clore on 11/21/20.
//

import Foundation

var ropes: [Int] = [4,3,2,6]

ropes.heapify(sort: <)

var sum = 0

while ropes.count > 1 {
  print(ropes)

  guard let smallest = ropes.removePeak(sort: <) else {
    fatalError()
  }

  guard let nextSmallest = ropes.removePeak(sort: <) else {
    fatalError()
  }

  let currentCost = smallest + nextSmallest
  sum += currentCost

  ropes.heapInsert(element: currentCost, usingSort: <)
}

print(sum)


ropes = [4, 3, 2, 6, 5, 7, 12]
sum = 0
ropes.heapify(sort: <)
var heapSize = ropes.count

while heapSize > 1 {
  guard let oldPeak = ropes.removePeak(sort: <) else {
    fatalError()
  }
  
  heapSize -= 1
  
  let currentCost = ropes[0] + oldPeak
  sum += currentCost
  ropes[0] = currentCost
  ropes.siftDown(fromIndex: 0, usingSort: <, upToSize: ropes.count - 1)
}

print(sum)

