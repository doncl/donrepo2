//
//  HeapSort.swift
//  HeapSort
//
//  Created by Don Clore on 11/21/20.
//

import Foundation

public extension Array where Element: Comparable {
  func leftChildIndex(ofParentAt index: Int) -> Int {
    return (index * 2) + 1
  }
  
  func rightChildIndex(ofParentAt index: Int) -> Int {
    return (index * 2) + 2
  }
  
  func parentIndex(ofChildAt index: Int) -> Int {
    return (index - 1) / 2
  }
  
//  mutating func bubbleUp(fromIndex index: Int, usingSort sort: (Element, Element) -> Bool) {
//    var child = index
//    var parent = parentIndex(ofChildAt: child)
//    while child > 0 && sort(self[child], self[parent]) {
//      swapAt(child, parent)
//      child = parent
//      parent = parentIndex(ofChildAt: child)
//    }
//  }
//
//  mutating func bubbleDown(fromIndex index: Int, usingSort sort: (Element, Element) -> Bool, upToSize size: Int) {
//    var parent = index
//
//    while true {
//      let left = leftChildIndex(ofParentAt: parent)
//      let right = rightChildIndex(ofParentAt: parent)
//
//      var candidate = parent
//
//      if left < size && sort(self[left], self[candidate]) {
//        candidate = left
//      }
//
//      if right < size && sort(self[right], self[candidate]) {
//        candidate = right
//      }
//
//      if candidate == parent {
//        return
//      }
//
//      swapAt(parent, candidate)
//      parent = candidate
//    }
//  }
  
  mutating func heapify(sort: (Element, Element) -> Bool) {
    let mid = self.count / 2 - 1
    for i in stride(from: mid, through: 0, by: -1) {
//      bubbleDown(fromIndex: i, usingSort: sort, upToSize: self.count)
      siftDown(fromIndex: i, usingSort: sort, upToSize: self.count)
    }
  }
  
  mutating func removePeak(sort: (Element, Element) -> Bool) -> Element? {
    guard count > 0 else {
      return nil
    }
    
    swapAt(0, count - 1)
    let formerPeak = self[count - 1]
    removeLast()
    
//    bubbleDown(fromIndex: 0, usingSort:sort, upToSize: count)
    siftDown(fromIndex: 0, usingSort:sort, upToSize: count)
    return formerPeak
  }
  
  mutating func heapInsert(element: Element, usingSort sort: (Element, Element) -> Bool) {
    append(element)
    //bubbleUp(fromIndex: count - 1, usingSort: sort)
    siftUp(fromIndex: count - 1, usingSort: sort)
  }
  
  mutating func heapSort() {
    heapify(sort: >)
    let reversedIndices = indices.reversed()
    for index in reversedIndices {
      swapAt(0, index)
//      bubbleDown(fromIndex: 0, usingSort: >, upToSize: index)
      siftDown(fromIndex: 0, usingSort: >, upToSize: index)
    }
  }
}
