//
//  Sift.swift
//  HeapSort
//
//  Created by Don Clore on 11/21/20.
//

import Foundation

public extension Array where Element: Comparable {
  mutating func siftUp(fromIndex index: Int, usingSort sort: (Element, Element) -> Bool) {
  }
  
  mutating func siftDown(fromIndex index: Int, usingSort sort: (Element, Element) -> Bool, upToSize size: Int) {
  }
}
