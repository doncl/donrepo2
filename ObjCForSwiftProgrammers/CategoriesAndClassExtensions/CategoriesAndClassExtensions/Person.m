//
//  Person.m
//  CategoriesAndClassExtensions
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "Person.h"
#import "Person+GoCrazy.h"

@interface Person ()
@property (readwrite) NSString *name;
@end

@implementation Person
- (void)printGreeting {
  if (self.name) {
    NSLog(@"Hello %@", self.name);
  } else {
    NSLog(@"I don't know your name, but Hi!");
  }
}

- (void)setName {
  self.name = @"Irving";
}

- (void)goCrazy {
  for (int i = 0; i < 1000; i++) {
    NSLog(@"whoop, whoop, whhop!");
  }
}

@end
