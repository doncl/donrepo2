//
//  Person.h
//  CategoriesAndClassExtensions
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSObject
@property (nonatomic, readonly) NSString *name;

- (void)printGreeting;
- (void)setName;
@end

NS_ASSUME_NONNULL_END
