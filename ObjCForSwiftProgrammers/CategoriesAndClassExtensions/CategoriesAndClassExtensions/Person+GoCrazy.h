//
//  Person+GoCrazy.h
//  CategoriesAndClassExtensions
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "Person.h"

NS_ASSUME_NONNULL_BEGIN

@interface Person ()
- (void)goCrazy;
@end

NS_ASSUME_NONNULL_END
