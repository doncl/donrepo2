//
//  NSString+Trimming.m
//  CategoriesAndClassExtensions
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "NSString+Trimming.h"

@implementation NSString (Trimming)
- (NSString *)dc_stringByTrimming {
  return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}
@end
