//
//  SFQuote.h
//  SciFiQuotes
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SFQuote : NSObject
@property NSString *text;
@property NSString *person;

- (nullable instancetype)initWithLine:(NSString *)line;
@end

NS_ASSUME_NONNULL_END
