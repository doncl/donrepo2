//
//  SFApp.m
//  SciFiQuotes
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "SFApp.h"
#import "SFQuote.h"

@implementation SFApp
- (instancetype)initWithFile:(NSString *)filename {
  if (self = [super init]) {
    // load the quotes filename we were given
    NSError *error;
    
    NSString *contents = [NSString stringWithContentsOfFile:filename encoding:NSUTF8StringEncoding error:&error];
    if (error) {
      // Something went wrong -- bail out
      NSLog(@"Fatal error: %@", [error localizedDescription]);
      exit(0);
    }
    
    // Still here? we're good to go!
    NSArray<NSString *> *lines = [contents componentsSeparatedByString:@"\n"];
    
    // create an empty array to hold the SFQuotes
    self.quotes = [NSMutableArray arrayWithCapacity:[lines count]];
    
    for (NSString *line in lines) {
      SFQuote *quote = [[SFQuote alloc] initWithLine:line];
      
      if (quote) {
        // we got a valid quote back; add it
        [self.quotes addObject:quote];
      }
    }
  }
  return self;
}

- (void)printQuote {
  NSInteger random = arc4random_uniform((u_int32_t) [self.quotes count]);
  SFQuote *selected = self.quotes[random];
  
  printf("%s\n", [selected.text cStringUsingEncoding:NSUTF8StringEncoding]);
  printf("\t - %s\n", [selected.person cStringUsingEncoding:NSUTF8StringEncoding]);
  
}
@end
