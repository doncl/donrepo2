//
//  SFQuote.m
//  SciFiQuotes
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "SFQuote.h"

@implementation SFQuote
- (nullable instancetype)initWithLine:(NSString *)line {
  self = [super init];
  if (self) {
    NSArray<NSString *> *split = [line componentsSeparatedByString:@"/"];
    
    if ([split count] != 2) {
      return nil;
    }
    
    self.text = split[0];
    self.person = split[1];
  }
  

  return self;
}
@end
