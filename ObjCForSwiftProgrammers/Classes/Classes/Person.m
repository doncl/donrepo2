//
//  Person.m
//  Classes
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "Person.h"

@implementation Person
- (void)printGreeting:(NSString *)greeting {
  NSLog(@"%@", greeting);
}
@end
