//
//  Person.h
//  Properties
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSObject
- (void)printGreeting;
@end

NS_ASSUME_NONNULL_END
