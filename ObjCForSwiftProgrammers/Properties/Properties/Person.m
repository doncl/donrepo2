//
//  Person.m
//  Properties
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "Person.h"

@interface Person ()
@property NSString *name;
@end

@implementation Person
{
  NSString *_privateVar;
}

- (void)printGreeting {
  NSLog(@"Hello, %@!", self.name);
}

@end
