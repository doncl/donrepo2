//
//  SCApp.m
//  SwfityCommits
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "SCApp.h"

@implementation SCApp
- (void)fetchCommitsForRepo:(NSString *)repo {
  NSString *urlString = [NSString stringWithFormat:@"https://api.github.com/repos/%@/commits", repo];
  NSURL *url = [NSURL URLWithString:urlString];
  if (!url) {
    return;
  }
  
  NSError *error;
  NSData *data = [NSData dataWithContentsOfURL:url options:NSDataReadingUncached error:&error];
  if (error) {
    NSLog(@"Fatal error 1: %@", [error localizedDescription]);
    exit(0);
  }
  
  // Decode the NSData into an array of dictionaries
  NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
  if (error) {
    NSLog(@"Fatal error 2: %@", [error localizedDescription]);
    exit(0);
  }
  
  for (NSDictionary *entry in json) {
    NSString *name = entry[@"commit"][@"author"][@"name"];
    NSString *message = entry[@"commit"][@"message"];
    
    // remove line breaks for easier processing
    
    message = [message stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    // print it all out
    printf("%s: %s\n\n", [name cStringUsingEncoding:NSUTF8StringEncoding], [message cStringUsingEncoding:NSUTF8StringEncoding]);
  }
}
@end
