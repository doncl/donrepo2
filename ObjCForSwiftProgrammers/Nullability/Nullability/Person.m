//
//  Person.m
//  Nullability
//
//  Created by Don Clore on 7/19/20.
//  Copyright © 2020 Don Clore. All rights reserved.
//

#import "Person.h"

@implementation Person
- (instancetype)initWithName:(NSString*)name { if (self = [super init]) {
self.name = name; }
   return self;
}
- (NSString*)fetchGreetingForTime:(NSString*)time {
  NSString *str = nil;
  self.name = str;
  return [NSString stringWithFormat:@"Good %@, %@!", time, self.name];
}

@end
