﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Newtonsoft.Json;
using ReduceCorpusSize;

namespace TestParsingReducedFiles
{
    public class Program
    {

        public static void Main(string[] args)
        {
            ParseXmlFiles();
            ParseJsonFiles();
        }

        private static void ParseXmlFiles()
        {
            var labelBaseName = "smallLabelsFile";
            var artistBaseName = "smallArtistsFile";
            var releaseBaseName = "smallReleasesFile";

            ParseAndCount($"{labelBaseName}.xml", "labels/label");
            ParseAndCount($"{artistBaseName}.xml", "artists/artist");
            ParseAndCount($"{releaseBaseName}.xml", "releases/release");
        }

        static void ParseAndCount(string fileName, string xPath)
        {
            var doc = new XmlDocument();            
            doc.Load(fileName);
            var childDocuments = doc.SelectNodes(xPath);
            Console.WriteLine($"{fileName} as {childDocuments.Count} in it");
        }

        static void ParseJsonFiles()
        {
            var labelBaseName = "smallLabelsFile";
            var artistBaseName = "smallArtistsFile";
            var releaseBaseName = "smallReleasesFile";

            ParseAndCountJson<FileOReleases>($"{releaseBaseName}.json");
            ParseAndCountJson<FileOArtists>($"{artistBaseName}.json");            
            ParseAndCountJson<FileOLabels>($"{labelBaseName}.json");
        }

        static void ParseAndCountJson<T>(string fileName) where T: class, ISmallJsonFile
        {
            var jsonString = File.ReadAllText(fileName);
            var doc = JsonConvert.DeserializeObject<T>(jsonString);
            var count = doc.GetRecordCount();
            Console.WriteLine($"{fileName} has {count} records in it.");
            doc = null;
            jsonString = null;
        }
    }
}

interface ISmallJsonFile
{
    int GetRecordCount();
}
class FileOReleases : ISmallJsonFile
{
    [JsonProperty("releases")] 
    public List<ReleaseEntity> Releases;

    public int GetRecordCount()
    {
        return Releases.Count;
    }
}

class FileOArtists : ISmallJsonFile
{
    [JsonProperty("artists")] 
    public List<ArtistEntity> Artists;

    public int GetRecordCount()
    {
        return Artists.Count;
    }
}

class FileOLabels : ISmallJsonFile
{
    [JsonProperty("labels")]
    public List<LabelEntity> Labels;

    public int GetRecordCount()
    {
        return Labels.Count;
    }
}
