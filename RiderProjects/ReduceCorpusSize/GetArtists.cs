using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace ReduceCorpusSize
{
    public partial class Program
    {
        public static List<ArtistEntity> GetArtists()
        {
            var artists = new List<ArtistEntity>();
            try
            {
                var serializer = new XmlSerializer(typeof(ArtistEntity));
                using (var fs = File.OpenRead("discog/discogs_20200101_artists.xml"))
                using (var reader = XmlReader.Create(fs))
                {
                    try
                    {
                        StringBuilder bldr = null;

                        while (reader.Read())
                        {
                            switch (reader.NodeType)
                            {
                                case XmlNodeType.Element:
                                    if (reader.Name == "artists")
                                    {
                                        break;
                                    }

                                    if (reader.Name == "artist")
                                    {
                                        System.Diagnostics.Debug.Assert(bldr == null);
                                        bldr = new StringBuilder();
                                    }
                                    

                                    bldr.Append("<" + reader.Name);
                                    while (reader.MoveToNextAttribute()) // Read the attributes.
                                        bldr.Append(" " + reader.Name + "='" + reader.Value + "'");

                                    reader.MoveToElement();
                                    if (reader.IsEmptyElement)
                                    {
                                        bldr.AppendLine("/>");
                                    }
                                    else
                                    {
                                        bldr.AppendLine(">");
                                    }

                                    break;
                                case XmlNodeType.Text:
                                    var val = HttpUtility.UrlDecode(reader.Value);
                                    bldr.Append(val);
                                    break;
                                case XmlNodeType.EndElement:
                                    bldr.Append("</" + reader.Name);
                                    bldr.AppendLine(">");
                                    if (reader.Name == "artist")
                                    {
                                        var entityString = bldr.ToString();
                                        bldr = null;
                                        //Console.WriteLine(entityString);
                                        try
                                        {
                                            using (var sr = new StringReader(entityString))
                                            {
                                                var entity = serializer.Deserialize(sr) as ArtistEntity;
                                                if (entity != null)
                                                {
                                                    artists.Add(entity);
                                                    if (artists.Count == 10000)
                                                    {
                                                        return artists;
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            var newEx = ex;
                                            // Console.Write(entityString);
                                        }
                                    }

                                    break;
                            }
                        }
                    }
                    catch (XmlException xe)
                    {
                        Console.WriteLine(xe);
                        var state = reader.ReadState;
                        Console.WriteLine(state.ToString());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        var state = reader.ReadState;
                        Console.WriteLine(state.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }            
            
            return artists;
        }

        public static void WriteArtistsFile(List<ArtistEntity> artists)
        {
            var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            var serializer = new XmlSerializer(typeof(ArtistEntity));
            var jsonSettings = new JsonSerializerSettings
            {
                
            };
            var count = artists.Count;
            
            using (var jw = File.CreateText("smallArtistsFile.json"))
            using (var tw = File.CreateText("smallArtistsFile.xml"))
            {
                tw.WriteLine("<artists>");
                jw.WriteLine("{");
                jw.WriteLine("\"artists\": [");
                for (var i = 0; i < count; i++) {
                    var artist = artists[i];
                    
                    using (var writer = XmlWriter.Create(tw, settings))
                    {
                        serializer.Serialize(writer, artist, emptyNamespaces);
                    }

                    var artistJson = JsonConvert.SerializeObject(artist, Newtonsoft.Json.Formatting.Indented);
                    jw.Write(artistJson);
                    if (i < count - 1)
                    {
                        jw.Write(",");                        
                    }
                    jw.WriteLine();
                }
                tw.WriteLine();
                tw.WriteLine("</artists>");
                
                jw.WriteLine("]");
                jw.WriteLine("}");
            }
        }
    }
}    