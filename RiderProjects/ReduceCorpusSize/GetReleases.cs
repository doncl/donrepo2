using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;

namespace ReduceCorpusSize
{
    public partial class Program
    {
        public static List<ReleaseEntity> GetReleases()
        {
            var releases = new List<ReleaseEntity>();
            try
            {
                var serializer = new XmlSerializer(typeof(ReleaseEntity));
                using (var fs = File.OpenRead("discog/discogs_20200101_releases.xml"))
                using (var reader = XmlReader.Create(fs))
                {
                    try
                    {
                        StringBuilder bldr = null;

                        while (reader.Read())
                        {
                            switch (reader.NodeType)
                            {
                                case XmlNodeType.Element:
                                    if (reader.Name == "releases")
                                    {
                                        break;
                                    }

                                    if (reader.Name == "release")
                                    {
                                        System.Diagnostics.Debug.Assert(bldr == null);
                                        bldr = new StringBuilder();
                                    }
                                    

                                    bldr.Append("<" + reader.Name);
                                    while (reader.MoveToNextAttribute()) // Read the attributes.
                                        bldr.Append(" " + reader.Name + "='" + reader.Value + "'");

                                    reader.MoveToElement();
                                    if (reader.IsEmptyElement)
                                    {
                                        bldr.AppendLine("/>");
                                    }
                                    else
                                    {
                                        bldr.AppendLine(">");
                                    }

                                    break;
                                case XmlNodeType.Text:
                                    var val = HttpUtility.UrlDecode(reader.Value);
                                    bldr.Append(val);
                                    break;
                                case XmlNodeType.EndElement:
                                    bldr.Append("</" + reader.Name);
                                    bldr.AppendLine(">");
                                    if (reader.Name == "release")
                                    {
                                        var entityString = bldr.ToString();
                                        bldr = null;
                                        // Console.WriteLine(entityString);
                                        try
                                        {
                                            using (var sr = new StringReader(entityString))
                                            {
                                                var entity = serializer.Deserialize(sr) as ReleaseEntity;
                                                if (entity != null)
                                                {
                                                    releases.Add(entity);
                                                    if (releases.Count == 10000)
                                                    {
                                                        return releases;
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            var newEx = ex;
                                            // Console.Write(entityString);
                                        }
                                    }

                                    break;
                            }
                        }
                    }
                    catch (XmlException xe)
                    {
                        var newEx = xe;                        
                        // Console.WriteLine(xe);
                        // var state = reader.ReadState;
                        // Console.WriteLine(state.ToString());
                    }
                    catch (Exception ex)
                    {
                        var newEx = ex;
                        // Console.WriteLine(ex);
                        // var state = reader.ReadState;
                        // Console.WriteLine(state.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                var newEx = ex;                
                // Console.WriteLine(ex);
            }            
            
            return releases;
        }

        public static void WriteReleasesFiles(List<ReleaseEntity> releases)
        {
            var serializer = new XmlSerializer(typeof(ReleaseEntity));
            var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            var count = releases.Count;
            
            using (var jw = File.CreateText("smallReleasesFile.json"))
            using (var tw = File.CreateText("smallReleasesFile.xml"))
            {
                tw.WriteLine("<releases>");
                jw.WriteLine("{");
                jw.WriteLine("\"releases\": [");
                
                for (var i = 0; i < releases.Count; i++)
                {
                    var release = releases[i];
                    using (var writer = XmlWriter.Create(tw, settings))
                    {
                        serializer.Serialize(writer, release, emptyNamespaces);                        
                    }

                    var releaseJson = JsonConvert.SerializeObject(release, Formatting.Indented);
                    jw.Write(releaseJson);
                    if (i < count - 1)
                    {
                        jw.Write(",");
                    }

                    jw.WriteLine();
                }
                tw.WriteLine();
                tw.WriteLine("</releases>");
                
                jw.WriteLine("]");
                jw.WriteLine("}");                
            }            
        }
    }
}