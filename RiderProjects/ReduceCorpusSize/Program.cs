﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Web;
using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;

namespace ReduceCorpusSize
{
    public partial class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Parsing and extracting 10000 Artist records");
            var artists = GetArtists();
            Console.WriteLine("Writing 10000 Artist records");            
            WriteArtistsFile(artists);
            
            Console.WriteLine("Parsing and extracting 10000 Label records");            
            var labelEntities = GetLabels();
            Console.WriteLine("Writing 10000 Label records");            
            WriteLabelFiles(labelEntities);
            
            Console.WriteLine("Parsing and extracting 10000 Releases records");            
            var releaseEntities = GetReleases();
            Console.WriteLine("Writing 10000 Releases records");            
            WriteReleasesFiles(releaseEntities);    
        }

        private static void WriteLabelFiles(List<LabelEntity> labelEntities)
        {
            var serializer = new XmlSerializer(typeof(LabelEntity));
            var emptyNamespaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.OmitXmlDeclaration = true;

            var count = labelEntities.Count;
            
            using (var jw = File.CreateText("smallLabelsFile.json"))            
            using (var tw = File.CreateText("smallLabelsFile.xml"))
            {
                tw.WriteLine("<labels>");
                jw.WriteLine("{");
                jw.WriteLine("\"labels\": [");
                for ( var i = 0; i < labelEntities.Count; i++)
                {
                    var label = labelEntities[i];
                    using (var writer = XmlWriter.Create(tw, settings))
                    {
                        serializer.Serialize(writer, label, emptyNamespaces);                        
                    }

                    var labelJson = JsonConvert.SerializeObject(label, Formatting.Indented);
                    jw.Write(labelJson);
                    if (i < count - 1)
                    {
                        jw.Write(",");
                    }

                    jw.WriteLine();
                }
                tw.WriteLine();
                tw.WriteLine("</labels>");
             
                jw.WriteLine("]");
                jw.WriteLine("}");                
            }
        }
        
        private static List<LabelEntity> GetLabels()
        {
            var labelEntities = new List<LabelEntity>();            
            try
            {
                var serializer = new XmlSerializer(typeof(LabelEntity));
                using (var fs = File.OpenRead("discog/discogs_20200101_labels.xml"))
                using (var reader = XmlReader.Create(fs))
                {
                    try
                    {
                        StringBuilder bldr = null;
                        var inSubLabelsBlock = false;

                        while (reader.Read())
                        {
                            switch (reader.NodeType)
                            {
                                case XmlNodeType.Element:
                                    if (reader.Name == "labels")
                                    {
                                        break;
                                    }

                                    if (reader.Name == "sublabels")
                                    {
                                        inSubLabelsBlock = true;
                                    }
                                    else if (reader.Name == "label" && inSubLabelsBlock == false)
                                    {
                                        System.Diagnostics.Debug.Assert(bldr == null);
                                        bldr = new StringBuilder();
                                    }

                                    // if (reader.Name == "image")
                                    // {
                                    //     Console.Write("foo");
                                    // }


                                    bldr.Append("<" + reader.Name);
                                    while (reader.MoveToNextAttribute()) // Read the attributes.
                                        bldr.Append(" " + reader.Name + "='" + reader.Value + "'");

                                    reader.MoveToElement();
                                    if (reader.IsEmptyElement)
                                    {
                                        bldr.AppendLine("/>");
                                    }
                                    else
                                    {
                                        bldr.AppendLine(">");
                                    }

                                    break;
                                case XmlNodeType.Text:
                                    var val = HttpUtility.UrlDecode(reader.Value);
                                    bldr.Append(val);
                                    break;
                                case XmlNodeType.EndElement:
                                    bldr.Append("</" + reader.Name);
                                    bldr.AppendLine(">");
                                    if (reader.Name == "sublabels")
                                    {
                                        inSubLabelsBlock = false;
                                    }

                                    if (reader.Name == "label" && inSubLabelsBlock == false)
                                    {
                                        var entityString = bldr.ToString();
                                        bldr = null;
                                        //Console.WriteLine(entityString);
                                        try
                                        {
                                            using (var sr = new StringReader(entityString))
                                            {
                                                var entity = serializer.Deserialize(sr) as LabelEntity;
                                                if (entity != null)
                                                {
                                                    labelEntities.Add(entity);
                                                    if (labelEntities.Count == 10000)
                                                    {
                                                        return labelEntities;
                                                    }
                                                }
                                            }
                                        }
                                        catch (Exception ex)
                                        {
                                            var newEx = ex;
                                            //Console.Write(entityString);
                                        }
                                    }

                                    break;
                            }
                        }
                    }
                    catch (XmlException xe)
                    {
                        Console.WriteLine(xe);
                        var state = reader.ReadState;
                        Console.WriteLine(state.ToString());
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        var state = reader.ReadState;
                        Console.WriteLine(state.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return labelEntities;
        }
    }
}