# Welcome to Dive Into SpriteKit!

This is a wholly new kind of book for me, or indeed anybody as far as I can tell. It’s very likely there are small mistakes scattered around, and I hope you’ll be patient with me as I correct them in the early releases – if you spot any, please do let me know!

The source code for this book has three components:

1. The assets for each project are stored in the Assets folder. I’ll be referring to these files frequently in each project.
2. The empty template you should use for all your games is in the DiveIntoSpriteKit folder. You should just copy that whole thing somewhere else when starting a new project.
3. The ProjectSourceCode folder contains something that I hadn’t planned on doing, but I wanted to give it a try: it’s complete source code for all projects in the book.

Now, you might think “why is supplying project source code so hard?”

Well, keep in mind that this book has *you* make choices – there is no longer a single copy of the source code.

And remember that each chapter is kept short, meaning that there are lots of stages along the way.

To work around these problems I built a simple iPad app that lets you read the book inside the app. Even better, as you progress, it always shows you the latest version of your project’s source code, so you can compare your code against mine even as you make choices.

*Even* even better it contains all the steps of all the projects as individually compilable SpriteKit game scenes, so you can press Play in my app at any time to see how the game looks at that point.

Now, all this took a huge amount of extra effort to produce, and it’s likely if not guaranteed that there are huge bugs in this –this really is experimental.

However, what *is* guaranteed is that the ProjectSourceCode app will take a long time to compile – perhaps as long as 10 minutes. This is because it contains over 1000 SpriteKit scene variations to cope with all the steps in all the projects, so I suggest you start building then go off and find something else to do while Xcode works.
