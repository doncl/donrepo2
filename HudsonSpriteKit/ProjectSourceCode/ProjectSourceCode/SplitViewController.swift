//
//  SplitViewController.swift
//  ProjectSourceCode
//
//  Created by Paul Hudson on 17/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import UIKit

class SplitViewController: UISplitViewController {
    var shouldBeObviousWarningShown = false

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if shouldBeObviousWarningShown == false {
            let copyrightAlert = UIAlertController(title: "Important!", message: "This project contains the entire text of Dive Into SpriteKit plus all project source code at each step, and as such is copyrighted just like the book itself.\n\nWhile you may distribute your final game – you made it, after all! – please do *not* distribute any part of this project.", preferredStyle: .alert)
            copyrightAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            present(copyrightAlert, animated: true)
            shouldBeObviousWarningShown = true
        }
    }
}
