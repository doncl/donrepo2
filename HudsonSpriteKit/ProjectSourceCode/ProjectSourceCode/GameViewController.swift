//
//  GameViewController.swift
//  ProjectSourceCode
//
//  Created by Paul Hudson on 17/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import SpriteKit
import UIKit

class GameViewController: UIViewController {
    var scene: SKScene!

    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if let view = self.view as! SKView? {
            

                // Set the scale mode to scale to fit the window
                scene.scaleMode = .aspectFill

                // Present the scene
                view.presentScene(scene)
//            }


            view.preferredFramesPerSecond = 120
            view.ignoresSiblingOrder = true
            view.showsFPS = true
            view.showsNodeCount = true
        }
    }

    @IBAction func exitTapped(_ sender: Any) {
        // HIDEOUS HACKS ALERT!
        // We don't want to mess with the game scenes because
        // they need to be as close to what readers see as possible.
        // So: we do a little digging inside our game scenes when
        // this view controller is dismissed, invalidating any
        // timers we find.
        let possibleTimers = ["timer", "gameTimer"]
        let invalidate = NSSelectorFromString("invalidate")

        for possible in possibleTimers {
            let sel = NSSelectorFromString(possible)
            if scene.responds(to: sel) {
                let maybeTimer = scene.perform(sel)

                if maybeTimer?.takeUnretainedValue().responds(to: invalidate) ?? false {
                    maybeTimer?.takeUnretainedValue().perform(invalidate)
                }

            }
        }

        dismiss(animated: true)
    }

}
