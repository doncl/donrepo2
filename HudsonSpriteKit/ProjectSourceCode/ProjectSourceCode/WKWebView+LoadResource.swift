//
//  WKWebView+LoadChapter.swift
//  ProjectSourceCode
//
//  Created by Paul Hudson on 17/10/2017.
//  Copyright © 2017 Paul Hudson. All rights reserved.
//

import Foundation
import WebKit

extension WKWebView {
    func loadChapter(name: String) {
        guard let path = Bundle.main.url(forResource: name, withExtension: nil) else {
            fatalError("Attempted to load an unknown resource: \(name)")
        }

        let body = try! String(contentsOf: path)
        let html = "\(header)\(body)\(footer)"

        webView.loadHTMLString(html, baseURL: nil)
    }
}
