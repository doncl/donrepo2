//
//  ViewController.swift
//  SendEmail
//
//  Created by Don Clore on 9/10/21.
//

import UIKit
import skpsmtpmessage

class ViewController: UIViewController {
	
	let btn = UIButton()

	override func viewDidLoad() {
		super.viewDidLoad()
		
		btn.translatesAutoresizingMaskIntoConstraints = false
		view.addSubview(btn)
		
		NSLayoutConstraint.activate([
			btn.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
			btn.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
			btn.widthAnchor.constraint(equalToConstant: 200),
			btn.heightAnchor.constraint(equalToConstant: 60),
			
		])
		
		btn.setTitleColor(UIColor.blue, for: .normal)
		
		btn.setTitle("Send Email", for: .normal)
		btn.addTarget(self, action: #selector(ViewController.btnPressed(_:)), for: .touchUpInside)
	}


	@objc func btnPressed(_ sender: UIButton) {
		print("\(#function)")
		
		let msg = SKPSMTPMessage()
		msg.fromEmail = "cloredon42@gmail.com"
		//msg.toEmail = "mapes@adobe.com,jeffa@adobe.com"
		//msg.toEmail = "dclore@adobe.com"
		msg.toEmail = "cloredon42@gmail.com"
        //msg.ccEmail = "dclore@adobe.com"
		
//		msg.relayHost = "smtp.gmail.com"
		msg.relayHost = "smtp-relay.sendinblue.com"
		msg.relayPorts = [587]
		msg.requiresAuth = true
		msg.login = "cloredon42@gmail.com"
		//msg.pass = "oyvsxgmgmacgqpzl"
		msg.pass = "ZNrfaUh6YJ8FwARH"
		msg.wantsSecure = true
		msg.subject = "test msg"
		msg.delegate = self
		
//		let plainParts: [String: Any] = [
//			kSKPSMTPPartContentTypeKey: "text/plain;charset=UTF-8",
//			kSKPSMTPPartMessageKey: "If you can read this, then I'm successfully sending email to you from an iPhone app," +
//			" using my gmail account as an SMTP relay.  Need an Adobe server for this, but the iOS part of the code is " +
//			"mostly a solved problem.   Need to figure out how to send attachment, or I can just paste the entire log into this"
//		]
//		let plainParts: [String: Any] = [
//			kSKPSMTPPartContentTypeKey: "text/plain;charset=UTF-8",
//			kSKPSMTPPartMessageKey: "This needs Postfix",
//		]
		
//		let plainParts: [String: Any] = [
//			kSKPSMTPPartContentTypeKey: "text/plain;charset=UTF-8",
//			kSKPSMTPPartMessageKey: "If you can read this, then I'm successfully sending email to you from an iPhone app," +
//			" using a free plan from SendInBlue, an SMTP provider.  This is an alternative to getting something official" +
//			" from Adobe."
//		]
		
		let plainParts: [String: Any] = [
			kSKPSMTPPartContentTypeKey: "text/plain;charset=UTF-8",
			kSKPSMTPPartMessageKey: "Just a test msg.",
		]


		msg.parts = [plainParts]
		
		msg.send()
		
	}
}

extension ViewController: SKPSMTPMessageDelegate {
	func messageSent(_ message: SKPSMTPMessage!) {
		print("\(#function)")
	}
	
	func messageFailed(_ message: SKPSMTPMessage!, error: Error!) {
		print("\(#function), error = \(error)")
	}
	
	
}
