//
//  DataFetcher.swift
//  Chapter_4
//
//  Created by Wals, Donny on 20/02/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import Foundation
import UIKit
import Combine

struct DataProvider {
  let dataSubject = CurrentValueSubject<[CardModel], Never>([])

  func fetch() {
    let cards = (0..<20).map { i in
      CardModel(title: "Title \(i)", subTitle: "Subtitle \(i)", imageName: "image_\(i)")
    }

    dataSubject.value = cards
  }

  func fetchImage(named imageName: String) -> AnyPublisher<UIImage?, URLError> {
    let url = URL(string: "https://imageserver.com/\(imageName)")!

    return URLSession.shared.dataTaskPublisher(for: url)
      .map { result in
        return UIImage(data: result.data)
      }
      .eraseToAnyPublisher()
  }
}
