//
//  Model.swift
//  Chapter_4
//
//  Created by Wals, Donny on 20/02/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import Foundation

struct CardModel: Hashable {
  let title: String
  let subTitle: String
  let imageName: String
}
