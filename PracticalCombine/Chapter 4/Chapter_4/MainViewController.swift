//
//  MainViewController.swift
//  Chapter_4
//
//  Created by Wals, Donny on 20/02/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import Foundation
import UIKit
import Combine

class MainViewController: UIViewController {
  let collectionView: UICollectionView

  let dataProvider = DataProvider()
  var cancellables = Set<AnyCancellable>()

  let themeManager: ThemeManager

  lazy var datasource: UICollectionViewDiffableDataSource<Int, CardModel> = {
    let datasource = UICollectionViewDiffableDataSource<Int, CardModel>.init(collectionView: collectionView) { collectionView, indexPath, item in
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CardCell.identifier, for: indexPath) as! CardCell

      cell.cancellable = self.dataProvider.fetchImage(named: item.imageName)
        .sink(receiveCompletion: { completion in
          // handle errors if needed
        }, receiveValue: { image in
          cell.imageView.image = image
        })

      cell.titleLabel.text = item.title
      cell.subtitleLabel.text = item.subTitle

      return cell
    }

    return datasource
  }()

  init(themeManager: ThemeManager) {
    self.themeManager = themeManager

    collectionView = UICollectionView(frame: .zero, collectionViewLayout: Self.collectionViewLayout)
    collectionView.register(CardCell.self, forCellWithReuseIdentifier: CardCell.identifier)
    collectionView.contentInsetAdjustmentBehavior = .scrollableAxes
    collectionView.alwaysBounceVertical = true

    super.init(nibName: nil, bundle: nil)

    collectionView.dataSource = datasource
  }

  required init?(coder: NSCoder) {
    fatalError("Not implemented")
  }

  override func loadView() {
    view = collectionView
    view.backgroundColor = .red
  }

  override func viewDidLoad() {
    dataProvider.dataSubject
      .debounce(for: 0.3, scheduler: DispatchQueue.main)
      .sink(receiveValue: self.applySnapshot)
      .store(in: &cancellables)

    dataProvider.fetch()

    themeManager.themeSubject.sink(receiveValue: { style in
      switch style {
      case .system:
        self.overrideUserInterfaceStyle = .unspecified
      case .dark:
        self.overrideUserInterfaceStyle = .dark
      case .light:
        self.overrideUserInterfaceStyle = .light
      }
    }).store(in: &cancellables)
  }

  func applySnapshot(_ models: [CardModel]) {
    var snapshot = NSDiffableDataSourceSnapshot<Int, CardModel>()
    snapshot.appendSections([0])

    snapshot.appendItems(models)

    datasource.apply(snapshot)
  }
}

extension MainViewController {
  static var collectionViewLayout: UICollectionViewCompositionalLayout {
    let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
    let item = NSCollectionLayoutItem(layoutSize: itemSize)

    let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(60))
    let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
    group.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 16, bottom: 0, trailing: 16)

    let section = NSCollectionLayoutSection(group: group)
    section.interGroupSpacing = 15

    let layout = UICollectionViewCompositionalLayout(section: section)

    return layout
  }
}
