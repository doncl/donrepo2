//
//  SceneDelegate.swift
//  Chapter_4
//
//  Created by Wals, Donny on 20/02/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

  var window: UIWindow?

  func scene(_ scene: UIScene,
             willConnectTo session: UISceneSession,
             options connectionOptions: UIScene.ConnectionOptions) {

    guard let windowScene = (scene as? UIWindowScene) else { return }

    let themeManager = ThemeManager()

    window = UIWindow(windowScene: windowScene)
    window?.rootViewController = MainViewController(themeManager: themeManager)
    window?.makeKeyAndVisible()
  }
}

