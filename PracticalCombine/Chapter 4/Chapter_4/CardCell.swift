//
//  CardCell.swift
//  Chapter_4
//
//  Created by Wals, Donny on 20/02/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import Foundation
import UIKit
import Combine

class CardCell: UICollectionViewCell {
  static let identifier = "CardCell"

  var cancellable: Cancellable?
  let imageView = UIImageView()

  let titleLabel = UILabel()
  let subtitleLabel = UILabel()

  override init(frame: CGRect) {
    super.init(frame: frame)

    titleLabel.numberOfLines = 2
    titleLabel.font = UIFont.preferredFont(forTextStyle: .largeTitle)

    subtitleLabel.numberOfLines = 2
    subtitleLabel.font = UIFont.preferredFont(forTextStyle: .body)

    let vStack = UIStackView.vertical(arrangedSubviews: [titleLabel, subtitleLabel])
    let hStack = UIStackView.horizontal(arrangedSubviews: [imageView, vStack])

    hStack.translatesAutoresizingMaskIntoConstraints = false
    imageView.translatesAutoresizingMaskIntoConstraints = false

    contentView.addSubview(hStack)

    NSLayoutConstraint.activate([
      hStack.topAnchor.constraint(equalTo: contentView.topAnchor),
      hStack.rightAnchor.constraint(equalTo: contentView.rightAnchor),
      hStack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
      hStack.leftAnchor.constraint(equalTo: contentView.leftAnchor),

      imageView.heightAnchor.constraint(equalTo: contentView.heightAnchor),
      imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 16/9)])

    contentView.backgroundColor = .systemBackground
    contentView.layer.cornerRadius = 5
    contentView.clipsToBounds = true
  }

  required init?(coder: NSCoder) {
    fatalError("Not implemented")
  }
}

extension UIStackView {
  static func horizontal(arrangedSubviews: [UIView]) -> UIStackView {
    let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
    stackView.axis = .horizontal
    return stackView
  }

  static func vertical(arrangedSubviews: [UIView]) -> UIStackView {
    let stackView = UIStackView(arrangedSubviews: arrangedSubviews)
    stackView.axis = .vertical
    return stackView
  }
}
