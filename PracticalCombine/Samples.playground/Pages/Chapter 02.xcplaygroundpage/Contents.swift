import Foundation
import UIKit
import Combine
import PlaygroundSupport
PlaygroundPage.current.needsIndefiniteExecution = true

sample {
  let publisher = [1, 2, 3].publisher
}

sample {
  let myUrl = URL(string: "https://www.donnywals.com")!
  let publisher = URLSession.shared.dataTaskPublisher(for: myUrl)
}

sample {
  let publisher = NotificationCenter.default.publisher(for: UIResponder.keyboardWillShowNotification)
}

sample {
  [1, 2, 3].publisher.sink(receiveCompletion: { completion in
    print("publisher completed: \(completion)")
  }, receiveValue: { value in
    print("received a value: \(value)")
  })
}

sample {
  [1, 2, 3].publisher.sink(receiveValue: { value in
    print("received a value: \(value)")
  })
}

sample {
  class User {
    var email = "default"
  }

  var user = User()
  ["test@email.com"].publisher.assign(to: \.email, on: user)
  print(user.email)
}

sample {
  let myNotification = Notification.Name("com.donnywals.customNotification")
  func listenToNotifications() {
    NotificationCenter.default.publisher(for: myNotification).sink(receiveValue: { notification in
      print("Received a notification!")
    })

    NotificationCenter.default.post(Notification(name: myNotification))
  }

  listenToNotifications()
  NotificationCenter.default.post(Notification(name: myNotification))
}

sample {
  let myNotification = Notification.Name("com.donnywals.customNotification")
  var subscription: AnyCancellable?

  func listenToNotifications() {
    subscription = NotificationCenter.default.publisher(for: myNotification).sink(receiveValue: { notification in
      print("Received a notification!")
    })

    NotificationCenter.default.post(Notification(name: myNotification))
  }

  listenToNotifications()
  NotificationCenter.default.post(Notification(name: myNotification))
}

sample {
  let myNotification = Notification.Name("com.donnywals.customNotification")
  var subscriptions = Set<AnyCancellable>()

  func listenToNotifications() {
    NotificationCenter.default.publisher(for: myNotification).sink(receiveValue: { notification in
      print("Received a notification!")
    }).store(in: &subscriptions)

    NotificationCenter.default.post(Notification(name: myNotification))
  }

  listenToNotifications()
  NotificationCenter.default.post(Notification(name: myNotification))
}

sample {
  let myNotification = Notification.Name("com.donnywals.customNotification")
  var cancellables = Set<AnyCancellable>()

  func testInts() {
    let publisher = URLSession.shared.dataTaskPublisher(for: URL(string: "https://donnywals.com")!)
    publisher.sink(receiveCompletion: { _ in print(cancellables.count) }, receiveValue: { int in
      print("Received a response!")
    }).store(in: &cancellables)

    print(cancellables.count)
  }

  testInts()
}
