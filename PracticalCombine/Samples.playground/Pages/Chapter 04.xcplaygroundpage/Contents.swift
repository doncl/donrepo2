import Combine
import Foundation
import UIKit

var cancellables = Set<AnyCancellable>()

sample {
  var cancellables = Set<AnyCancellable>()

  let notificationCenter = NotificationCenter.default
  let notificationName = UIResponder.keyboardWillShowNotification
  let publisher = notificationCenter.publisher(for: notificationName)

  publisher
    .sink(receiveValue: { notification in
      print(notification)
    }).store(in: &cancellables)

  notificationCenter.post(Notification(name: notificationName))
}

sample {
  var cancellables = Set<AnyCancellable>()

  let notificationSubject = PassthroughSubject<Notification, Never>()
  let notificationName = UIResponder.keyboardWillShowNotification
  let notificationCenter = NotificationCenter.default

  notificationCenter.addObserver(forName: notificationName, object: nil, queue: nil) { notification in
    notificationSubject.send(notification)
  }

  notificationSubject
    .sink(receiveValue: { notification in
      print(notification)
    }).store(in: &cancellables)

  notificationCenter.post(Notification(name: notificationName))
}

let someLabel = UILabel()

sample {
  class Car {
    var onBatteryChargeChanged: ((Double) -> Void)?
    var kwhInBattery = 50.0 {
      didSet {
        onBatteryChargeChanged?(kwhInBattery)
      }
    }

    let kwhPerKilometer = 0.14

    func drive(kilometers: Double) {
      let kwhNeeded = kilometers * kwhPerKilometer

      assert(kwhNeeded <= kwhInBattery, "Can't make trip, not enough charge in battery")

      kwhInBattery -= kwhNeeded
    }
  }

  let car = Car()
  car.onBatteryChargeChanged = { newCharge in
    someLabel.text = "The car now has \(newCharge)kwh in its battery"
  }
}

sample {
  class Car {
    var kwhInBattery = CurrentValueSubject<Double, Never>(50.0)
    let kwhPerKilometer = 0.14

    func drive(kilometers: Double) {
      let kwhNeeded = kilometers * kwhPerKilometer

      assert(kwhNeeded <= kwhInBattery.value, "Can't make trip, not enough charge in battery")

      kwhInBattery.value -= kwhNeeded
    }
  }

  let car = Car()
  car.kwhInBattery.sink(receiveValue: { newCharge in
    someLabel.text = "The car now has \(newCharge)kwh in its battery"
  })
}

sample {
  class Car {
    @Published var kwhInBattery = 50.0
    let kwhPerKilometer = 0.14

    func drive(kilometers: Double) {
      let kwhNeeded = kilometers * kwhPerKilometer

      assert(kwhNeeded <= kwhInBattery, "Can't make trip, not enough charge in battery")

      kwhInBattery -= kwhNeeded
    }
  }

  let car = Car()
  car.$kwhInBattery.sink(receiveValue: { newCharge in
    someLabel.text = "The car now has \(newCharge)kwh in its battery"
  })
}

sample {
  class Car {
    @Published var kwhInBattery = 50.0
    let kwhPerKilometer = 0.14
  }

  struct CarViewModel {
    var car: Car

    lazy var batterySubject: AnyPublisher<String?, Never> = {
      return car.$kwhInBattery.map({ newCharge in
        return "The car now has \(newCharge)kwh in its battery"
      }).eraseToAnyPublisher()
    }()

    mutating func drive(kilometers: Double) {
      let kwhNeeded = kilometers * car.kwhPerKilometer

      assert(kwhNeeded <= car.kwhInBattery, "Can't make trip, not enough charge in battery")

      car.kwhInBattery -= kwhNeeded
    }
  }

  class CarStatusViewController {
    let label = UILabel()
    let button = UIButton()
    var viewModel: CarViewModel
    var cancellables = Set<AnyCancellable>()

    init(viewModel: CarViewModel) {
      self.viewModel = viewModel
    }

    // setup code goes here

    func setupLabel() {
      viewModel.batterySubject
        .assign(to: \.text, on: label)
        .store(in: &cancellables)
    }

    func buttonTapped() {
      viewModel.drive(kilometers: 10)
    }
  }
}

sample {
  struct CardModel: Decodable {}
  
  class DataProvider {
    @Published var fetchedModels = [CardModel]()

    var currentPage = 0
    var cancellables = Set<AnyCancellable>()

    func fetchNextPage() {
      let url = URL(string: "https://myserver.com/page/\(currentPage)")!
      currentPage += 1

      URLSession.shared.dataTaskPublisher(for: url)
        .tryMap({ [weak self] value -> [CardModel] in
          let jsonDecoder = JSONDecoder()
          let models = try jsonDecoder.decode([CardModel].self, from: value.data)
          let currentModels = self?.fetchedModels ?? []
            
          return currentModels + models
        })
        .replaceError(with: fetchedModels)
        .assign(to: &$fetchedModels)
    }
  }
}
