import UIKit
import PlaygroundSupport
import Combine

PlaygroundPage.current.needsIndefiniteExecution = true

sample {
  class SliderViewController: UIViewController {
    let slider = UISlider()
    let label = UILabel()

    override func viewDidLoad() {
      super.viewDidLoad()

      slider.minimumValue = 0
      slider.maximumValue = 100
      slider.value = 50

      updateLabel()

      let vStack = UIStackView(arrangedSubviews: [label, slider])
      vStack.axis = .vertical
      vStack.spacing = 100

      vStack.translatesAutoresizingMaskIntoConstraints = false

      view.addSubview(vStack)

      NSLayoutConstraint.activate([
        vStack.centerYAnchor.constraint(equalTo: view.centerYAnchor),
        vStack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
        vStack.widthAnchor.constraint(equalToConstant: 200)])

      slider.addTarget(self, action: #selector(updateLabel), for: .valueChanged)
    }

    @objc func updateLabel() {
      label.text = "Slider is at \(slider.value)"
    }
  }

  PlaygroundPage.current.liveView = SliderViewController()
}

var cancellables = Set<AnyCancellable>()

sample {
  let firstNotificationName = Notification.Name("first")
  let secondNotificationName = Notification.Name("second")

  let firstNotification = Notification(name: firstNotificationName)
  let secondNotification = Notification(name: secondNotificationName)

  let first = NotificationCenter.default.publisher(for: firstNotificationName)
  let second = NotificationCenter.default.publisher(for: secondNotificationName)

  let cVal = CurrentValueSubject<String, Never>("hello")

  let zipped = Publishers.Zip(first, second).sink(receiveValue: { val in
    print(val, "zipped")
  }).store(in: &cancellables)

  let merged = Publishers.Merge(first, second).sink(receiveValue: { val in
    print(val, "merged")
  }).store(in: &cancellables)

  let combined = Publishers.CombineLatest(cVal, second).sink(receiveValue: { val in
    print(val, "combined")
  }).store(in: &cancellables)

  print("send first")
  NotificationCenter.default.post(firstNotification)

  cVal.send(completion: .finished)

  print("send second")
  NotificationCenter.default.post(secondNotification)

  print("send third")
  NotificationCenter.default.post(firstNotification)

  print("send fourth")
  NotificationCenter.default.post(secondNotification)
}

sample {
  let left = CurrentValueSubject<Int, Never>(0)
  let right = CurrentValueSubject<Int, Never>(0)

  left.zip(right).sink(receiveValue: { value in
    print(value)
  }).store(in: &cancellables)

  left.value = 1
  left.value = 2
  left.value = 3
  right.value = 1
  right.value = 3
}
