import Combine
import Foundation
import PlaygroundSupport
import UserNotifications
import CoreData
import SwiftUI

PlaygroundPage.current.needsIndefiniteExecution = true

var cancellables = Set<AnyCancellable>()

sample {
  func createFuture() -> Future<Int, Never> {
    return Future { promise in
      promise(.success(Int.random(in: 0..<Int.max)))
    }
  }
  
  createFuture()
    .sink(receiveValue: { value in
      print(value)
    }).store(in: &cancellables)
}

let myURL = URL(string: "https://practicalcombine.com")!

sample {
  func fetchURL(_ url: URL) -> Future<(data: Data, response: URLResponse), URLError> {
    return Future { promise in
      URLSession.shared.dataTask(with: url) { data, response, error in
        if let error = error as? URLError {
          promise(.failure(error))
        }
        
        if let data = data, let response = response {
          promise(.success((data: data, response: response)))
        }
        
        print("RECEIVED RESPONSE")
      }.resume()
    }
  }
  
  let publisher = fetchURL(myURL)
  
  publisher
    .sink(receiveCompletion: { completion in
      print(completion)
    }, receiveValue: { value in
      print(value.data)
    }).store(in: &cancellables)
  
  publisher
    .sink(receiveCompletion: { completion in
      print(completion)
    }, receiveValue: { value in
      print(value.data)
    }).store(in: &cancellables)
}

extension UNUserNotificationCenter {
  func getNotificationSettings() -> Future<UNNotificationSettings, Never> {
    return Future { promise in
      self.getNotificationSettings { settings in
        promise(.success(settings))
      }
    }
  }
  
  func requestAuthorization(options: UNAuthorizationOptions) -> Future<Bool, Error> {
    return Future { promise in
      self.requestAuthorization(options: options) { result, error in
        if let error = error {
          promise(.failure(error))
        } else {
          promise(.success(result))
        }
      }
    }
  }
}

sample {
  UNUserNotificationCenter.current().getNotificationSettings()
    .flatMap({ settings -> AnyPublisher<Bool, Never> in
      switch settings.authorizationStatus {
      case .notDetermined:
        return UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge])
          .replaceError(with: false)
          .eraseToAnyPublisher()
      case .denied:
        return Just(false).eraseToAnyPublisher()
      default:
        return Just(true).eraseToAnyPublisher()
      }
    })
    .receive(on: DispatchQueue.main)
    .sink(receiveValue: { hasPermissions in
      if hasPermissions == false {
        // point user to settings
      } else {
        // we have permission
      }
    }).store(in: &cancellables)
}

sample {
  func createDeferredFuture() -> Deferred<Future<Int, Never>> {
    return Deferred {
      return Future { promise in
        promise(.success(Int.random(in: (1..<Int.max))))
      }
    }
  }

  func createFuture() -> Future<Int, Never> {
    return Future { promise in
      promise(.success(Int.random(in: (1..<Int.max))))
    }
  }

  let manuallyDeferred = Deferred {
    return createFuture()
  }

  let plainFuture = createFuture()

  plainFuture.sink(receiveValue: { value in
    print("plain1", value)
  })

  plainFuture.sink(receiveValue: { value in
    print("plain2", value)
  })

  let deferred = createDeferredFuture()

  deferred.sink(receiveValue: { value in
    print("deferred1", value)
  })

  deferred.sink(receiveValue: { value in
    print("deferred2", value)
  })

  /*manuallyDeferred.sink(receiveValue: { value in
    print("manually deferred", value)
  })*/
}

class User: NSManagedObject {}

extension NSPersistentContainer {
  func fetchUsers(using moc: NSManagedObjectContext, completion: @escaping ([User]) -> Void) {
    moc.perform {
      let request = User.fetchRequest()
      let users = try? moc.fetch(request) as? [User]
      completion(users ?? [])
    }
  }
  
  func fetchUsers(using moc: NSManagedObjectContext) -> Future<[User], Never> {
    return Future { promise in
      moc.perform {
        let request = User.fetchRequest()
        let users = try? moc.fetch(request) as? [User]
        promise(.success(users ?? []))
      }
    }
  }
}

sample {
  struct UsersFetcher {
    let persistentContainer: NSPersistentContainer
    
    func fetchUsers(_ completion: @escaping ([User]) -> Void) {
      persistentContainer.fetchUsers(using: persistentContainer.viewContext, completion: completion)
    }
    
    func fetchUsers() -> Future<[User], Never> {
      return persistentContainer.fetchUsers(using: persistentContainer.viewContext)
    }
  }
}
