import Combine
import Foundation


var cancellables = Set<AnyCancellable>()

sample {
  class DataFetcher {
    private var currentPage = 0
    
    func loadNextPage(_ completion: @escaping (Result<Data, Error>) -> Void) {
      // perform load
      // increment currentPage
      // call completion handler
    }
  }
}

sample {
  class DataFetcher {
    private var currentPage = 0
    
    var dataPublisher = PassthroughSubject<Data, URLError>()
    
    var cancellables = Set<AnyCancellable>()
    
    func loadNextPage() {
      let url = URL(string: "https://practicalcombine.com?page=\(currentPage)")!
      URLSession.shared.dataTaskPublisher(for: url)
        .map(\.data)
        .sink(receiveCompletion: { _ in },
              receiveValue: { [weak self] loadedData in
                self?.dataPublisher.send(loadedData)
                self?.currentPage += 1
              })
        .store(in: &cancellables)
    }
  }
}

sample {
  class DataFetcher {
    private var currentPage = 0
    
    lazy var dataPublisher: AnyPublisher<Data, URLError> = {
      loadRequestPublisher
        .flatMap({ [weak self] _ -> URLSession.DataTaskPublisher in
          let url = URL(string: "https://practicalcombine.com?page=\(self?.currentPage ?? 0)")!
          return URLSession.shared.dataTaskPublisher(for: url)
        })
        .map(\.data)
        .handleEvents(receiveOutput: { [weak self] _ in
          self?.currentPage += 1
        })
        .eraseToAnyPublisher()
    }()
    
    private var loadRequestPublisher = PassthroughSubject<Void, Never>()
    
    func loadNextPage() {
      loadRequestPublisher.send(())
    }
  }
  
  let fetcher = DataFetcher()
  fetcher.dataPublisher
    .sink(receiveCompletion: { _ in },
          receiveValue: { data in
            print(data)
          })
    .store(in: &cancellables)
  
  fetcher.loadNextPage()
  fetcher.loadNextPage()
}

sample {
  struct ApiResponse {
    let hasMorePages: Bool
    let items: [String]
  }
  
  class NetworkObject {
    func loadPage(_ page: Int) -> Just<ApiResponse> {
      if page < 5 {
        return Just(ApiResponse(hasMorePages: true,
                                items: ["Item", "Item"]))
      } else {
        return Just(ApiResponse(hasMorePages: false,
                                items: ["Item", "Item"]))
      }
    }
  }
  
  struct RecursiveFetcher {
    let network: NetworkObject
    
    func loadAllPages() -> AnyPublisher<[String], Never> {
      let pageIndexPublisher = CurrentValueSubject<Int, Never>(0)

        return pageIndexPublisher
          .flatMap({ pageIndex in
            return network.loadPage(pageIndex)
          })
          .handleEvents(receiveOutput: { response in
            if response.hasMorePages {
              pageIndexPublisher.value += 1
            } else {
              pageIndexPublisher.send(completion: .finished)
            }
          })
          .reduce([String](), { collectedStrings, response in
            return response.items + collectedStrings
          })
          .eraseToAnyPublisher()
    }
  }
  
    let fetcher = RecursiveFetcher(network: NetworkObject())
    fetcher.loadAllPages()
      .sink(receiveValue: { strings in
        print(strings)
      })
      .store(in: &cancellables)
}


struct Token {
  let isValid: Bool
}

struct Profile {}

class Authenticator {
  var currentToken = Token(isValid: false)
  
  func refreshToken<S: Subject>(using subject: S) where S.Output == Token {
    self.currentToken = Token(isValid: true)
    subject.send(currentToken)
  }
  
  func tokenSubject() -> CurrentValueSubject<Token, Never> {
    return CurrentValueSubject(currentToken)
  }
}

struct UserApi {
  let authenticator: Authenticator
  
func getProfile() -> AnyPublisher<Data, URLError> {
  let tokenSubject = authenticator.tokenSubject()
  
  return tokenSubject
    .flatMap({ token -> AnyPublisher<Data, URLError> in
      let url: URL = URL(string: "https://www.donnywals.com")!
      
      return URLSession.shared.dataTaskPublisher(for: url)
        .flatMap({ result -> AnyPublisher<Data, URLError> in
          if let httpResponse = result.response as? HTTPURLResponse,
             httpResponse.statusCode == 403 {
            
            self.authenticator.refreshToken(using: tokenSubject)
            return Empty().eraseToAnyPublisher()
          }
          
          return Just(result.data)
            .setFailureType(to: URLError.self)
            .eraseToAnyPublisher()
        })
        .eraseToAnyPublisher()
    })
    .handleEvents(receiveOutput: { _ in
      tokenSubject.send(completion: .finished)
    })
    .eraseToAnyPublisher()
}
}

let api = UserApi(authenticator: Authenticator())
api.getProfile().sink(receiveCompletion: { completion in
  print("completion: \(completion)")
}, receiveValue: { value in
  print("value")
}).store(in: &cancellables)
