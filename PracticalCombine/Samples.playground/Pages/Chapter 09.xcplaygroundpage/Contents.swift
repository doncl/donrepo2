import Combine
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

extension Publisher {
  func customSink(receiveCompletion: @escaping (Subscribers.Completion<Self.Failure>) -> Void,
                  receiveValue: @escaping (Self.Output) -> Void) -> AnyCancellable {

    let sink = Subscribers.CustomSink(receiveCompletion: receiveCompletion, receiveValue: receiveValue)
    self.subscribe(sink)

    return AnyCancellable(sink)
  }
}

extension Subscribers {
  class CustomSink<Input, Failure: Error>: Subscriber {
    let receiveValue: (Input) -> Void
    let receiveCompletion: (Subscribers.Completion<Failure>) -> Void

    var subscription: Subscription?

    init(receiveCompletion: @escaping (Subscribers.Completion<Failure>) -> Void,
         receiveValue: @escaping (Input) -> Void) {

      self.receiveCompletion = receiveCompletion
      self.receiveValue = receiveValue
    }

    func receive(subscription: Subscription) {
      self.subscription = subscription
      subscription.request(.max(1))
    }

    func receive(_ input: Input) -> Subscribers.Demand {
      receiveValue(input)
      return .none
    }

    func receive(completion: Subscribers.Completion<Failure>) {
      receiveCompletion(completion)
    }
  }
}

extension Subscribers.CustomSink: Cancellable {
  func cancel() {
    subscription?.cancel()
    subscription = nil
  }
}

extension Subscriptions {
  class IntSubscription<S: Subscriber>: Subscription where S.Input == Int, S.Failure == Never {
    let numberOfValues: Int
    var currentValue = 0

    var subscriber: S?

    var openDemand = Subscribers.Demand.none

    init(numberOfValues: Int, subscriber: S) {
      self.numberOfValues = numberOfValues
      self.subscriber = subscriber
    }

    func request(_ demand: Subscribers.Demand) {
      openDemand += demand

      while openDemand > 0 && currentValue < numberOfValues {
        if let newDemand = subscriber?.receive(currentValue) {
          openDemand += newDemand
        }

        currentValue += 1
        openDemand -= 1
      }

      if currentValue == numberOfValues {
        subscriber?.receive(completion: .finished)
        cancel()
      }
    }

    func cancel() {
      subscriber = nil
      // we don't have anything to clean up
    }
  }
}

extension Publishers {
  struct IntPublisher: Publisher {
    typealias Output = Int
    typealias Failure = Never

    let numberOfValues: Int

    func receive<S>(subscriber: S)
      where S : Subscriber, Failure == S.Failure, Output == S.Input {

        let subscription = Subscriptions.IntSubscription(numberOfValues: numberOfValues,
                                                         subscriber: subscriber)
        subscriber.receive(subscription: subscription)
    }
  }
}

var cancellables = Set<AnyCancellable>()

sample {
  let publisher = [1, 2, 3].publisher
  let subject = PassthroughSubject<Int, Never>()

  subject
    .sink(receiveValue: { receivedInt in
      print("subject", receivedInt)
    }).store(in: &cancellables)

  publisher
    .subscribe(subject)
    .store(in: &cancellables)
}

sample {
  [1, 2, 3].publisher.customSink(receiveCompletion: { completion in
    print(completion)
  }, receiveValue: { value in
    print(value)
  })
}

sample {
  var cancellables = Set<AnyCancellable>()

  let ints = [1, 2, 3].publisher

  ints
    .sink(receiveValue: { print($0 )})
    .store(in: &cancellables)

  ints
    .sink(receiveValue: { print($0 )})
    .store(in: &cancellables)
}

sample {
  var customPublisher = Publishers.IntPublisher(numberOfValues: 10)
  customPublisher
    .customSink(receiveCompletion: { completion in
      print(completion)
    }, receiveValue: { int in
      print(int)
    })
    .store(in: &cancellables)
}
