import Combine
import Foundation
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

var cancellables = Set<AnyCancellable>()

sample {
  let intSubject = PassthroughSubject<Int, Never>()

  intSubject.sink(receiveValue: { value in
    // print(value)
    // print(Thread.current)
  }).store(in: &cancellables)

  intSubject.send(1)

  DispatchQueue.global().async {
    intSubject.send(2)
  }

  let queue = OperationQueue()
  queue.maxConcurrentOperationCount = 1
  queue.underlyingQueue = DispatchQueue(label: "com.donnywals.queue")

  queue.addOperation {
    intSubject.send(3)
  }
}

sample {
  let intSubject = PassthroughSubject<Int, Never>()

  intSubject
    .receive(on: DispatchQueue.main)
    .sink(receiveValue: { value in
      // print(value)
      // print(Thread.current)
    }).store(in: &cancellables)

  intSubject.send(1)

  DispatchQueue.global().async {
    intSubject.send(2)
  }

  let queue = OperationQueue()
  queue.maxConcurrentOperationCount = 1
  queue.underlyingQueue = DispatchQueue(label: "com.donnywals.queue")

  queue.addOperation {
    intSubject.send(3)
  }
}

sample {
  let intSubject = PassthroughSubject<Int, Never>()

  intSubject
    .subscribe(on: ImmediateScheduler.shared)
    .sink(receiveValue: { value in
      print(value)
      print(Thread.current)
    }).store(in: &cancellables)

  intSubject.send(1)
  intSubject.send(2)
  intSubject.send(3)
}

sample {
  func generateInt() -> Future<Int, Never> {
    return Future { promise in
      promise(.success(Int.random(in: 1...10)))
    }
  }

  generateInt()
    .map({ value in
      sleep(5)
      return value
    })
    .subscribe(on: DispatchQueue.global())
    .sink(receiveValue: { value in
      print(value)
    }).store(in: &cancellables)

  print("hello!")
}

sample {
  URLSession.shared.dataTaskPublisher(for: URL(string: "https://practicalcombine.com")!)
    .subscribe(on: DispatchQueue.main)
    .map({ result in
      print(Thread.current.isMainThread)
    }).sink(receiveCompletion: { _ in }, receiveValue: { value in
      print(Thread.current.isMainThread)
    })
}
