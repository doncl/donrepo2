import Foundation
import Combine
import PlaygroundSupport

PlaygroundPage.current.needsIndefiniteExecution = true

struct MyModel: Decodable {}

let myURL = URL(string: "https://www.donnywals.com/bjfgsdfsg")!

//sample {
//  func fetchURL<T: Decodable>(_ url: URL, completion: @escaping (Result<T, Error>) -> Void) {
//    URLSession.shared.dataTask(with: myURL) { data, response, error in
//      guard let data = data else {
//        if let error = error {
//          completion(.failure(error))
//        }
//
//        assertionFailure("Callback got called with no data and no error. This should never happen.")
//        return
//      }
//
//      do {
//        let decoder = JSONDecoder()
//        let decodedResponse = try decoder.decode(T.self, from: data)
//        completion(.success(decodedResponse))
//      } catch {
//        completion(.failure(error))
//      }
//    }.resume()
//  }
//}

var cancellables = Set<AnyCancellable>()

sample {
  func fetchURL<T: Decodable>(_ url: URL) -> AnyPublisher<T, Error> {
    URLSession.shared.dataTaskPublisher(for: url)
      .tryMap({ result in
        print("\((result.response as? HTTPURLResponse)?.statusCode ?? 0)")
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: result.data)
      })
      .eraseToAnyPublisher()
  }

  //  let publisher: AnyPublisher<MyModel, Error> = fetchURL(myURL)
  //    .share()
  //    .eraseToAnyPublisher()
  //
  //  publisher
  //    .sink(receiveCompletion: { completion in
  //      print(completion)
  //    }, receiveValue: { (model: MyModel) in
  //      print(model)
  //    }).store(in: &cancellables)
  //
  //  publisher
  //    .sink(receiveCompletion: { completion in
  //      print(completion)
  //    }, receiveValue: { (model: MyModel) in
  //      print(model)
  //    }).store(in: &cancellables)
}

sample {
  func fetchURL<T: Decodable>(_ url: URL) -> AnyPublisher<T, Error> {
    URLSession.shared.dataTaskPublisher(for: url)
      .map(\.data)
      .decode(type: T.self, decoder: JSONDecoder())
      .eraseToAnyPublisher()
  }
}

sample {
  struct ErrorModel: Decodable, Error {
    let errorCode: Int
  }

  func fetchURL<T: Decodable>(_ url: URL) -> AnyPublisher<T, Error> {
    URLSession.shared.dataTaskPublisher(for: url)
      .mapError({ $0 as Error })
      .flatMap({ result -> AnyPublisher<T, Error> in
        guard let urlResponse = result.response as? HTTPURLResponse, (200...299).contains(urlResponse.statusCode) else {
          fatalError("We'll handle this later")
        }

        return Just(result.data).decode(type: T.self, decoder: JSONDecoder()).eraseToAnyPublisher()
      }).eraseToAnyPublisher()
  }
}

sample {
  struct APIError: Decodable, Error {
    // fields that model your error
  }

  func fetchURL<T: Decodable>(_ url: URL) -> AnyPublisher<T, Error> {
    URLSession.shared.dataTaskPublisher(for: url)
      .mapError({ $0 as Error })
      .flatMap({ result -> AnyPublisher<T, Error> in
        guard let urlResponse = result.response as? HTTPURLResponse, (200...299).contains(urlResponse.statusCode) else {
          return Just("{}".data(using: .utf8)!)
            .decode(type: APIError.self, decoder: JSONDecoder())
            .tryMap({ errorModel in
              throw errorModel
            })
            .eraseToAnyPublisher()
        }

        return Just(result.data).decode(type: T.self, decoder: JSONDecoder()).eraseToAnyPublisher()
      }).eraseToAnyPublisher()
  }

  //  fetchURL(myURL)
  //    .sink(receiveCompletion: { completion in
  //      if case .failure(let error) = completion,
  //        let apiError = error as? APIError {
  //        print(error)
  //      }
  //    }, receiveValue: { (model: MyModel) in
  //      print(model)
  //    }).store(in: &cancellables)
}

sample {
  struct APIError: Decodable, Error {
    // fields that model your error
  }

  func fetchURL<T: Decodable>(_ url: URL) -> AnyPublisher<T, Error> {
    URLSession.shared.dataTaskPublisher(for: url)
      .tryMap({ result in
        let decoder = JSONDecoder()
        guard let urlResponse = result.response as? HTTPURLResponse, (200...299).contains(urlResponse.statusCode) else {
          let apiError = try decoder.decode(APIError.self, from: result.data)
          throw apiError
        }

        return try decoder.decode(T.self, from: result.data)
      })
      .eraseToAnyPublisher()
  }

  //  fetchURL(myURL)
  //    .sink(receiveCompletion: { completion in
  //      if case .failure(let error) = completion,
  //        let apiError = error as? APIError {
  //        print(error)
  //      }
  //    }, receiveValue: { (model: MyModel) in
  //      print(model)
  //    }).store(in: &cancellables)
}

sample {
  struct APIError: Decodable, Error {
    let statusCode: Int
  }

  func refreshToken() -> AnyPublisher<Bool, Never> {
    // normally you'd have your refresh logic here
    return Just(false).eraseToAnyPublisher()
  }

  func fetchURL<T: Decodable>(_ url: URL) -> AnyPublisher<T, Error> {
    URLSession.shared.dataTaskPublisher(for: url)
      .tryMap({ result in
        let decoder = JSONDecoder()
        guard let urlResponse = result.response as? HTTPURLResponse, (200...299).contains(urlResponse.statusCode) else {
          let apiError = try decoder.decode(APIError.self, from: result.data)
          throw apiError
        }

        return try decoder.decode(T.self, from: result.data)
      })
      .tryCatch({ error -> AnyPublisher<T, Error> in
        guard let apiError = error as? APIError, apiError.statusCode == 401 else {
          throw error
        }

        return refreshToken()
          .tryMap({ success -> AnyPublisher<T, Error> in
            guard success else { throw error }

            return fetchURL(url)
          }).switchToLatest().eraseToAnyPublisher()
      })
      .eraseToAnyPublisher()
  }

  fetchURL(myURL)
    .sink(receiveCompletion: { completion in
      if case .failure(let error) = completion,
        let apiError = error as? APIError {
        print(apiError)
      }
    }, receiveValue: { (model: MyModel) in
      print(model)
    }).store(in: &cancellables)
}

let fullImageURL = URL(string: "https://not-an-image.com")!
let smallImageURL = URL(string: "https://not-an-image.com")!

sample {
  let session = URLSession.shared

  let constrainedConfiguration = URLSessionConfiguration.default
  constrainedConfiguration.allowsConstrainedNetworkAccess = false
  let _ = URLSession(configuration: constrainedConfiguration)

  var constrainedRequest = URLRequest(url: fullImageURL)
  constrainedRequest.allowsConstrainedNetworkAccess = false

  let normalRequest = URLRequest(url: smallImageURL)

  URLSession.shared
    .dataTaskPublisher(for: constrainedRequest)
    .tryCatch({ error -> URLSession.DataTaskPublisher in
      guard error.networkUnavailableReason == .constrained else {
        throw error
      }

      return session.dataTaskPublisher(for: normalRequest)
    })
    .sink(receiveCompletion: { completion in
      // handle completion
    }, receiveValue: { result in
      // handle received data
    })
}

enum SectionType: String, Decodable {
  case featured, favorites, curated
}

struct Event: Decodable, Hashable {
  let id = UUID()
}

struct HomePageSection {
  let events: [Event]
  let sectionType: SectionType

  static func featured(events: [Event]) -> HomePageSection {
    return HomePageSection(events: events, sectionType: .featured)
  }

  static func favorites(events: [Event]) -> HomePageSection {
    return HomePageSection(events: events, sectionType: .favorites)
  }

  static func curated(events: [Event]) -> HomePageSection {
    return HomePageSection(events: events, sectionType: .curated)
  }
}

sample {
  let featuredContentURL = URL(string: "https://donnywals.com")!
  let curatedContentURL = URL(string: "https://donnywals.com")!
  let remoteFavoritesURL = URL(string: "https://donnywals.com")!

  let featuredPublisher = URLSession.shared.dataTaskPublisher(for: featuredContentURL)
    .map({ $0.data })
    .decode(type: [Event].self, decoder: JSONDecoder())
    .replaceError(with: [Event]())
    .map({ HomePageSection.featured(events: $0) })
    .eraseToAnyPublisher()

  let curatedPublisher = URLSession.shared.dataTaskPublisher(for: curatedContentURL)
    .map({ $0.data })
    .decode(type: [Event].self, decoder: JSONDecoder())
    .replaceError(with: [Event]())
    .map({ HomePageSection.curated(events: $0) })
    .eraseToAnyPublisher()

  class LocalFavorites {
    static func fetchAll() -> AnyPublisher<[Event], Never> {
      return Just([]).eraseToAnyPublisher()
    }
  }

  let localFavoritesPublisher = LocalFavorites.fetchAll()

  let remoteFavoritesPublisher = URLSession.shared.dataTaskPublisher(for: curatedContentURL)
    .map({ $0.data })
    .decode(type: [Event].self, decoder: JSONDecoder())
    .replaceError(with: [Event]())
    .eraseToAnyPublisher()

  let favoritesPublisher = Publishers.Zip(localFavoritesPublisher, remoteFavoritesPublisher)
    .map({ favorites -> HomePageSection in
      let uniqueFavorites = Set(favorites.0 + favorites.1)
      return HomePageSection.favorites(events: Array(uniqueFavorites))
    })
    .eraseToAnyPublisher()

  let homePagePublisher = Publishers.Merge3(featuredPublisher, curatedPublisher, favoritesPublisher)

  homePagePublisher
    .sink(receiveValue: { section in
      switch section.sectionType {
      case .featured:
        // render featured section
        print("featured loaded")
      case .curated:
        // render curated section
        print("curated loaded")
      case .favorites:
        // render favorites section
        print("favorites loaded")
      }
    }).store(in: &cancellables)
}
