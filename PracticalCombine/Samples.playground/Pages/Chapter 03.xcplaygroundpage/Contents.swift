import Combine
import UIKit
import PlaygroundSupport
PlaygroundPage.current.needsIndefiniteExecution = true

var cancellables = Set<AnyCancellable>()
let someURL = URL(string: "https://www.donnywals.com")!
struct User: Decodable {
  let name: String
}
let userNameLabel = UILabel()

sample {
  let myLabel = UILabel()

  [1, 2, 3].publisher
    .sink(receiveValue: { int in
      myLabel.text = "Current value: \(int)"
    })
}

sample {
  let myLabel = UILabel()

  let p = [1, 2, 3].publisher.map({ int in
    return "Current value: \(int)"
  })

  p.sink(receiveValue: { string in
    myLabel.text = string
  })
}

sample {
  // URLSession.DataTaskPublisher
  let dataTaskPublisher = URLSession.shared.dataTaskPublisher(for: someURL)

  // Publishers.Map<URLSession.DataTaskPublisher, Data>
  let mappedPublisher = dataTaskPublisher
    .map({ response in
      return response.data
    })
}

sample {
  let dataTaskPublisher = URLSession.shared.dataTaskPublisher(for: someURL)
    .retry(1)
    .map({ $0.data })
    .decode(type: User.self, decoder: JSONDecoder())
    .map({ $0.name })
    .replaceError(with: "unkown")
    .assign(to: \.text, on: userNameLabel)
}

sample {
  let result = ["one", "2", "three", "4", "5"].compactMap({ Int($0) })
  print(result) // [2, 4, 5]
}

sample {
  ["one", "2", "three", "4", "5"].publisher
    .compactMap({ Int($0) })
    .sink(receiveValue: { int in
      print(int)
    })
}

sample {
  ["one", "2", "three", "4", "5"].publisher
    .map({ Int($0) })
    .replaceNil(with: 0)
    .compactMap({ $0 })
    .sink(receiveValue: { int in
      print(int)
    })

  // output: 0, 2, 0, 4, 5
}

//sample {
//  var baseURL = URL(string: "https://www.donnywals.com")!
//
//  ["/", "/the-blog", "/speaking", "/newsletter"].publisher
//    .setFailureType(to: URLError.self)
//    .flatMap({ path -> URLSession.DataTaskPublisher in
//      let url = baseURL.appendingPathComponent(path)
//      return URLSession.shared.dataTaskPublisher(for: url)
//    })
//    .sink(receiveCompletion: { completion in
//      print("Completed with: \(completion)")
//    }, receiveValue: { result in
//      print(result)
//    }).store(in: &cancellables)
//}

var randomizedURL: URL {
  return someURL.appendingPathComponent("?id=\(UUID().uuidString)")
}

sample {
  [1, 2, 3].publisher
    .print()
    .flatMap(maxPublishers: .max(1), { int in
      return Array(repeating: int, count: 2).publisher
    })
    .sink(receiveValue: { value in
      print("got: \(value)")
    })
}

sample {
  let timerPublisher = Timer.publish(every: 0.1, on: .main, in: .common).autoconnect()
  timerPublisher
    //.setFailureType(to: URLError.self)
    //    .flatMap(maxPublishers: .max(1), { _ -> URLSession.DataTaskPublisher in
    //      return URLSession.shared.dataTaskPublisher(for: randomizedURL)
    //    })
    .print()
    .flatMap(maxPublishers: .max(1), { _ -> Timer.TimerPublisher in
      let t = Timer.publish(every: 1, on: .main, in: .common)
      t.connect()
      return t
    })
    .sink(receiveCompletion: { completion in
      print("received completion: \(completion)")
    }, receiveValue: { value in
      print("received value: \(value)")
    }).store(in: &cancellables)

}

sample {
  enum MyError: Error {
    case outOfBounds
  }

  [1, 2, 3].publisher
    .tryMap({ int in
      guard int < 3 else {
        throw MyError.outOfBounds
      }

      return int * 2
    })
    .sink(receiveCompletion: { completion in
      print(completion)
    }, receiveValue: { val in
      print(val)
    })
}

extension Publisher where Output == String, Failure == Never {
  func toURLSessionDataTask(baseURL: URL) -> AnyPublisher<URLSession.DataTaskPublisher.Output, URLError> {
    if #available(iOS 14, *) {
      return self
        .flatMap({ path -> URLSession.DataTaskPublisher in
          let url = baseURL.appendingPathComponent(path)
          return URLSession.shared.dataTaskPublisher(for: url)
        })
        .eraseToAnyPublisher()
    } else {
      return self
        .setFailureType(to: URLError.self)
        .flatMap({ path -> URLSession.DataTaskPublisher in
          let url = baseURL.appendingPathComponent(path)
          return URLSession.shared.dataTaskPublisher(for: url)
        })
        .eraseToAnyPublisher()
    }
  }
}

sample {
  var baseURL = URL(string: "https://www.donnywals.com")!

  ["/", "/the-blog", "/speaking", "/newsletter"].publisher
    .toURLSessionDataTask(baseURL: baseURL)
    .sink(receiveCompletion: { completion in
      print("Completed with: \(completion)")
    }, receiveValue: { result in
      print(result)
    }).store(in: &cancellables)
}
