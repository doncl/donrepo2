import Foundation
import Combine

struct SomeModel: Codable {
  let name: String
}

sample {
  let arr = [1, 2, 3].map { $0 * 2 }
  print(arr)
}

sample {
  let myUrl = URL(string: "https://www.donnywals.com")!

  func requestData(_ completion: @escaping (Result<Data, Error>) -> Void) {
    URLSession.shared.dataTask(with: myUrl) { data, response, error in
      if let error = error {
        completion(.failure(error))
        return
      }

      guard let data = data else {
        preconditionFailure("If there is no error, data should be present...")
      }

      completion(.success(data))
    }.resume()
  }
}

sample {
  let myUrl = URL(string: "https://www.donnywals.com")!

  func requestData() -> AnyPublisher<Data, URLError> {
    URLSession.shared.dataTaskPublisher(for: myUrl)
      .map(\.data)
      .eraseToAnyPublisher()
  }
}

sample {
  let myUrl = URL(string: "https://www.donnywals.com")!

  func requestData() -> AnyPublisher<Data, URLError> {
    URLSession.shared.dataTaskPublisher(for: myUrl)
      .map(\.data)
      .eraseToAnyPublisher()
  }

  requestData()
    .decode(type: SomeModel.self, decoder: JSONDecoder())
    .map { $0.name }
    .sink(receiveCompletion: { _ in },
          receiveValue: { print($0) })
}
