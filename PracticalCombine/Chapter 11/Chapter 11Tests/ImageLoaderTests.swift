//
//  ImageLoaderTests.swift
//  Chapter 11
//
//  Created by Donny Wals on 04/05/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import XCTest
import Combine
import UIKit
@testable import Chapter11

class ImageLoaderTests: XCTestCase {
  
  var cancellables = Set<AnyCancellable>()

  override func setUp() {
    cancellables = Set<AnyCancellable>()
  }
  
  func testImageLoaderClearsImagesOnMemoryWarning() {
    // setup
    let notificationCenter = NotificationCenter()
    let imageLoader = ImageLoader(notificationCenter)
    
    // store a dummy image
    let image = UIImage(systemName: "house")
    let url = URL(string: "https://fake.url/house")!
    imageLoader.images[url] = image
    XCTAssertNotNil(imageLoader.images[url])
    
    // send memory warning
    let memoryWarning = UIApplication.didReceiveMemoryWarningNotification
    notificationCenter.post(Notification(name: memoryWarning))
    
    // verify that the images are now gone
    XCTAssertNil(imageLoader.images[url])
  }
  
  func testImageLoaderLoadsImageFromNetwork() {
    // setup
    let mockNetwork = MockImageNetworkProvider()
    let notificationCenter = NotificationCenter()
    let imageLoader = ImageLoader(notificationCenter, network: mockNetwork)
    let url = URL(string: "https://fake.url/house")!
    
    let result = awaitCompletion(for: imageLoader.loadImage(at: url))
    XCTAssertNoThrow(try result.get())
    XCTAssertEqual(mockNetwork.wasLoadURLCalled, true)
    XCTAssertNotNil(imageLoader.images[url])
  }
  
  func testImageLoaderUsesCachedImageIfAvailable() {
    // setup
    let mockNetwork = MockImageNetworkProvider()
    let notificationCenter = NotificationCenter()
    let imageLoader = ImageLoader(notificationCenter, network: mockNetwork)
    let url = URL(string: "https://fake.url/house")!
    imageLoader.images[url] = UIImage(systemName: "house")
    
    let result = awaitCompletion(for: imageLoader.loadImage(at: url))
    XCTAssertNoThrow(try result.get())
    XCTAssertEqual(mockNetwork.wasLoadURLCalled, false)
    XCTAssertNotNil(imageLoader.images[url])
  }
}

class MockImageNetworkProvider: ImageNetworking {
  var wasLoadURLCalled = false
  
  func loadURL(_ url: URL) -> AnyPublisher<Data, Error> {
    wasLoadURLCalled = true
    
    let data = UIImage(systemName: "house")!.pngData()!
    return Just(data)
      .setFailureType(to: Error.self)
      .eraseToAnyPublisher()
  }
}
