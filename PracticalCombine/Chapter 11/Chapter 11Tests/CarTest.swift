//
//  CarTest.swift
//  Chapter 11Tests
//
//  Created by Donny Wals on 27/04/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import XCTest
import Combine
@testable import Chapter11

class CarTest: XCTestCase {
  var car: Car!
  var cancellables: Set<AnyCancellable>!
  
  override func setUp() {
    car = Car()
    cancellables = []
  }
  
  func testKwhBatteryIsPublisher() {
    let newValue: Double = 10.0
    var expectedValues = [car.kwhInBattery, newValue]
    
    let receivedAllValues = expectation(description: "all values received")
    
    car.$kwhInBattery.sink(receiveValue: { value in
      guard  let expectedValue = expectedValues.first else {
        XCTFail("The publisher emitted more values than expected.")
        return
      }
      
      guard expectedValue == value else {
        XCTFail("Expected received value \(value) to match first expected value \(expectedValue)")
        return
      }
      
      expectedValues = Array(expectedValues.dropFirst())
      
      if expectedValues.isEmpty {
        receivedAllValues.fulfill()
      }
    }).store(in: &cancellables)
    
    car.kwhInBattery = newValue
    
    waitForExpectations(timeout: 1, handler: nil)
  }
}
