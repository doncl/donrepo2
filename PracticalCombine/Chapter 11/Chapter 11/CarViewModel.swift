//
//  CarViewModel.swift
//  Chapter 11
//
//  Created by Donny Wals on 27/04/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import Combine

public struct CarViewModel {
  var car: Car

  public lazy var batterySubject: AnyPublisher<String?, Never> = {
    return car.$kwhInBattery.map({ newCharge in
      return "The car now has \(newCharge)kwh in its battery"
    }).eraseToAnyPublisher()
  }()

  public mutating func drive(kilometers: Double) {
    let kwhNeeded = kilometers * car.kwhPerKilometer

    assert(kwhNeeded <= car.kwhInBattery, "Can't make trip, not enough charge in battery")

    car.kwhInBattery -= kwhNeeded
  }
}
