//
//  ImageLoader.swift
//  Chapter 11
//
//  Created by Donny Wals on 04/05/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import Foundation
import Combine
import UIKit

protocol ImageNetworking {
  func loadURL(_ url: URL) -> AnyPublisher<Data, Error>
}

class ImageNetworkProvider: ImageNetworking {
  func loadURL(_ url: URL) -> AnyPublisher<Data, Error> {
    return URLSession.shared.dataTaskPublisher(for: url)
      .mapError({$0 as Error})
      .map(\.data)
      .eraseToAnyPublisher()
  }
}

class ImageLoader {
  var images = [URL: UIImage]()
  var cancellables = Set<AnyCancellable>()
  
  let network: ImageNetworking
  
  init(_ notificationCenter: NotificationCenter = NotificationCenter.default,
       network: ImageNetworking = ImageNetworkProvider()) {
    self.network = network
    
    let notification = UIApplication.didReceiveMemoryWarningNotification
    notificationCenter.publisher(for: notification)
      .sink(receiveValue: { [weak self] _ in
        self?.images = [URL: UIImage]()
      }).store(in: &cancellables)
  }
  
  func loadImage(at url: URL) -> AnyPublisher<UIImage, Error> {
    if let image = images[url] {
      return Just(image).setFailureType(to: Error.self).eraseToAnyPublisher()
    }
    
    return network.loadURL(url)
      .tryMap({ data in
        guard let image = UIImage(data: data) else {
          throw ImageLoaderError.invalidData
        }
        
        return image
      })
      .handleEvents(receiveOutput: { [weak self] image in
        self?.images[url] = image
      })
      .eraseToAnyPublisher()
  }
}

enum ImageLoaderError: Error {
  case invalidData
}
