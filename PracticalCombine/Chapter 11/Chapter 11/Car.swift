//
//  Car.swift
//  Chapter 11
//
//  Created by Donny Wals on 27/04/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import Combine

public class Car {
  @Published public var kwhInBattery = 50.0
  let kwhPerKilometer = 0.14
}
