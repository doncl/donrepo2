//
//  CombinedViewController.swift
//  Chapter 5
//
//  Created by Wals, Donny on 01/03/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import UIKit
import Combine

class CombinedViewController: UIViewController {
  let firstNameTextField = UITextField()
  let lastNameTextField = UITextField()
  let addressTextField = UITextField()
  let zipCodeTextField = UITextField()
  let countryTextField = UITextField()

  let fullNameLabel = UILabel()
  let addressLabel = UILabel()
  let countryLabel = UILabel()

  @Published var firstName = ""
  @Published var lastName = ""
  @Published var address = ""
  @Published var zipCode = ""
  @Published var country = ""

  var cancellables = Set<AnyCancellable>()

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .systemBackground
    firstNameTextField.borderStyle = .roundedRect
    lastNameTextField.borderStyle = .roundedRect
    addressTextField.borderStyle = .roundedRect
    zipCodeTextField.borderStyle = .roundedRect
    countryTextField.borderStyle = .roundedRect

    firstNameTextField.placeholder = "First Name"
    lastNameTextField.placeholder = "Last Name"
    addressTextField.placeholder = "Address"
    zipCodeTextField.placeholder = "Zip Code"
    countryTextField.placeholder = "Country"

    firstNameTextField.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
    lastNameTextField.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
    addressTextField.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
    zipCodeTextField.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)
    countryTextField.addTarget(self, action: #selector(textFieldChanged(_:)), for: .editingChanged)

    let nameViews = [firstNameTextField, lastNameTextField]
    let nameStack = UIStackView(arrangedSubviews: nameViews)
    nameStack.axis = .horizontal
    nameStack.distribution = .fillEqually
    nameStack.spacing = 8

    let formViews = [nameStack, addressTextField, zipCodeTextField, countryTextField,
                     fullNameLabel, addressLabel, countryLabel]

    let formStack = UIStackView(arrangedSubviews: formViews)
    formStack.axis = .vertical
    formStack.spacing = 8

    view.addSubview(formStack)

    formStack.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate([
      formStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      formStack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 8),
      formStack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -8)
    ])

    $firstName
      .combineLatest($lastName)
      .map({ combined in
        return "\(combined.0) \(combined.1)"
      })
      .assign(to: \.text, on: fullNameLabel)
      .store(in: &cancellables)

    $address
      .combineLatest($zipCode)
      .map({ combined in
        return "\(combined.0), \(combined.1)"
      })
      .assign(to: \.text, on: addressLabel)
      .store(in: &cancellables)

    $country
      .map({ $0 })
      .assign(to: \.text, on: countryLabel)
      .store(in: &cancellables)
  }

  @objc func textFieldChanged(_ textField: UITextField) {
    switch textField{
    case firstNameTextField:
      firstName = firstNameTextField.text ?? ""
    case lastNameTextField:
      lastName = lastNameTextField.text ?? ""
    case addressTextField:
      address = addressTextField.text ?? ""
    case zipCodeTextField:
      zipCode = zipCodeTextField.text ?? ""
    case countryTextField:
      country = countryTextField.text ?? ""
    default: return
    }
  }
}
