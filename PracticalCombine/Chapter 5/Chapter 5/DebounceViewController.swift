//
//  DebounceViewController.swift
//  Chapter 5
//
//  Created by Wals, Donny on 25/02/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import UIKit
import Combine

class DebounceViewController: UIViewController {
  let textField = UITextField()
  let label = UILabel()

  @Published var searchQuery: String?

  var cancellables = Set<AnyCancellable>()

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .systemBackground
    textField.borderStyle = .roundedRect

    let vStack = UIStackView(arrangedSubviews: [textField, label])
    view.addSubview(vStack)

    vStack.translatesAutoresizingMaskIntoConstraints = false
    vStack.axis = .vertical
    vStack.spacing = 10

    vStack.translatesAutoresizingMaskIntoConstraints = false

    view.addSubview(vStack)

    NSLayoutConstraint.activate([
      vStack.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      vStack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      vStack.widthAnchor.constraint(equalToConstant: 200)])

    textField.addTarget(self, action: #selector(textChanged), for: .editingChanged)
    $searchQuery
      .throttle(for: 1, scheduler: DispatchQueue.main, latest: false)
      .filter({ ($0 ?? "").count > 2 })
      .removeDuplicates()
      .print()
      .assign(to: \.text, on: label)
      .store(in: &cancellables)
  }

  @objc func textChanged() {
    searchQuery = textField.text
  }
}
