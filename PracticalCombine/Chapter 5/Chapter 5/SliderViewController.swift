//
//  ViewController.swift
//  Chapter 5
//
//  Created by Wals, Donny on 24/02/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import UIKit
import Combine

class SliderViewController: UIViewController {
  let slider = UISlider()
  let label = UILabel()

  @Published var sliderValue: Float = 50

  var cancellables = Set<AnyCancellable>()

  override func viewDidLoad() {
    super.viewDidLoad()

    view.backgroundColor = .systemBackground

    slider.minimumValue = 0
    slider.maximumValue = 100

    let vStack = UIStackView(arrangedSubviews: [label, slider])
    vStack.axis = .vertical
    vStack.spacing = 10

    vStack.translatesAutoresizingMaskIntoConstraints = false

    view.addSubview(vStack)

    NSLayoutConstraint.activate([
      vStack.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      vStack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      vStack.widthAnchor.constraint(equalToConstant: 200)])

    $sliderValue
      .map({ value in
        "Slider is at \(value)"
      })
      .assign(to: \.text, on: label)
      .store(in: &cancellables)

    $sliderValue
      .assign(to: \.value, on: slider)
      .store(in: &cancellables)

    slider.addTarget(self, action: #selector(updateLabel), for: .valueChanged)
  }

  @objc func updateLabel() {
    sliderValue = slider.value
  }
}

import SwiftUI

struct ExampleView: View {
  @State var sliderValue: Float = 50

  var body: some View {
    VStack {
      Text("Slider is at \(sliderValue)")
      Slider(value: $sliderValue, in: (1...100))
    }
  }
}
