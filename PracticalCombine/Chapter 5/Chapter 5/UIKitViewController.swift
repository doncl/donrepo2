//
//  UIKitViewController.swift
//  Chapter 5
//
//  Created by Wals, Donny on 24/02/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import UIKit

class UIKitViewController: UIViewController {

  let slider = UISlider()
  let label = UILabel()

  var sliderValue: Float = 50 {
    didSet {
      slider.value = sliderValue
      label.text = "Slider is at \(sliderValue)"
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    slider.minimumValue = 0
    slider.maximumValue = 100
    slider.value = 50

    let vStack = UIStackView(arrangedSubviews: [label, slider])
    vStack.axis = .vertical
    vStack.spacing = 100

    vStack.translatesAutoresizingMaskIntoConstraints = false

    view.addSubview(vStack)

    NSLayoutConstraint.activate([
      vStack.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      vStack.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      vStack.widthAnchor.constraint(equalToConstant: 200)])

    slider.addTarget(self, action: #selector(updateLabel), for: .valueChanged)
  }

  @objc func updateLabel() {
    sliderValue = slider.value
  }
}

