//
//  NetworkingViewController.swift
//  Chapter 10
//
//  Created by Wals, Donny on 15/03/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import Foundation
import UIKit
import Combine

class NetworkingViewController: UIViewController {
  var cancellables = Set<AnyCancellable>()

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = .systemBackground

    let button = UIButton(type: .system)
    button.setTitle("Initiate network calls", for: .normal)

    view.addSubview(button)
    button.translatesAutoresizingMaskIntoConstraints = false

    NSLayoutConstraint.activate([
      button.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      button.centerYAnchor.constraint(equalTo: view.centerYAnchor)])

    button.addTarget(self, action: #selector(loadHomePage), for: .touchUpInside)
  }

  @objc func loadHomePage() {
    let featuredContentURL = URL(string: "https://donnywals.com")!
    let curatedContentURL = URL(string: "https://donnywals.com")!
    let remoteFavoritesURL = URL(string: "https://donnywals.com")!

    let featuredPublisher = URLSession.shared.dataTaskPublisher(for: featuredContentURL)
      .map({ $0.data })
      .decode(type: [Event].self, decoder: JSONDecoder())
      .delay(for: 0.8, scheduler: DispatchQueue.main)
      .map({ HomePageSection.featured(events: $0) })
      .lane("Featured publisher")
      .eraseToAnyPublisher()

    let curatedPublisher = URLSession.shared.dataTaskPublisher(for: curatedContentURL)
      .map({ $0.data })
      .decode(type: [Event].self, decoder: JSONDecoder())
      .delay(for: 1.2, scheduler: DispatchQueue.main)
      .replaceError(with: [Event]())
      .setFailureType(to: Error.self)
      .map({ HomePageSection.curated(events: $0) })
      .lane("Curated publisher")
      .eraseToAnyPublisher()

    let localFavoritesPublisher = LocalFavorites.fetchAll()
      .lane("Local favorites publisher")

    let remoteFavoritesPublisher = URLSession.shared.dataTaskPublisher(for: curatedContentURL)
      .map({ $0.data })
      .decode(type: [Event].self, decoder: JSONDecoder())
      .delay(for: 0.3, scheduler: DispatchQueue.main)
      .replaceError(with: [Event]())
      .lane("Remote favorites publisher")
      .eraseToAnyPublisher()

    let favoritesPublisher = Publishers.Zip(localFavoritesPublisher, remoteFavoritesPublisher)
      .map({ favorites -> HomePageSection in
        let uniqueFavorites = Set(favorites.0 + favorites.1)
        return HomePageSection.favorites(events: Array(uniqueFavorites))
      })
      .setFailureType(to: Error.self)
      .lane("Favorites publisher")
      .eraseToAnyPublisher()

    let homePagePublisher = Publishers.Merge3(featuredPublisher, curatedPublisher, favoritesPublisher)

    homePagePublisher
      .lane("Homepage publisher")
      .sink(receiveCompletion: { _ in }, receiveValue: { section in
        switch section.sectionType {
        case .featured:
          // render featured section
          print("featured loaded")
        case .curated:
          // render curated section
          print("curated loaded")
        case .favorites:
          // render favorites section
          print("favorites loaded")
        }
      }).store(in: &cancellables)
  }
}
