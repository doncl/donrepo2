//
//  Models.swift
//  Chapter 10
//
//  Created by Wals, Donny on 15/03/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import Foundation
import Combine

enum SectionType: String, Decodable {
  case featured, favorites, curated
}

struct Event: Decodable, Hashable {
  let id = UUID()
}

struct HomePageSection {
  let events: [Event]
  let sectionType: SectionType

  static func featured(events: [Event]) -> HomePageSection {
    return HomePageSection(events: events, sectionType: .featured)
  }

  static func favorites(events: [Event]) -> HomePageSection {
    return HomePageSection(events: events, sectionType: .favorites)
  }

  static func curated(events: [Event]) -> HomePageSection {
    return HomePageSection(events: events, sectionType: .curated)
  }
}

class LocalFavorites {
  static func fetchAll() -> AnyPublisher<[Event], Never> {
    return Just([]).eraseToAnyPublisher()
  }
}
