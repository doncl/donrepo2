//
//  MainViewController.swift
//  Chapter 9
//
//  Created by Wals, Donny on 08/03/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import UIKit
import Combine

class MainViewController: UIViewController {
  let slider = UISlider()
  var cancellables = Set<AnyCancellable>()

  override func viewDidLoad() {
    view.addSubview(slider)
    view.backgroundColor = .systemBackground
    slider.translatesAutoresizingMaskIntoConstraints = false

    slider.minimumValue = 0
    slider.maximumValue = 100

    NSLayoutConstraint.activate([
      slider.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.9),
      slider.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      slider.centerXAnchor.constraint(equalTo: view.centerXAnchor)])

    slider
      .publisher(for: .valueChanged)
      .debounce(for: 0.2, scheduler: DispatchQueue.main)
      .sink(receiveValue: { control in
        guard let slider = control as? UISlider
          else { return }

        print(slider.value)
      }).store(in: &cancellables)
  }
}
