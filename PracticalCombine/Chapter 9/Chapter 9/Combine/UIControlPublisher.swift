//
//  UIControlPublisher.swift
//  Chapter 9
//
//  Created by Wals, Donny on 08/03/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import Combine
import UIKit

extension UIControl {
  struct EventPublisher: Publisher {
    typealias Output = UIControl
    typealias Failure = Never

    let control: UIControl
    let controlEvent: UIControl.Event

    func receive<S>(subscriber: S) where S : Subscriber, Failure == S.Failure, Output == S.Input {
      let subscription = EventSubscription(control: control, event: controlEvent, subscriber: subscriber)
      subscriber.receive(subscription: subscription)
    }
  }
}

extension UIControl {
  fileprivate class EventSubscription<S: Subscriber>: Subscription where S.Input == UIControl, S.Failure == Never {
    let control: UIControl
    let event: UIControl.Event
    var subscriber: S?

    var currentDemand = Subscribers.Demand.none

    init(control: UIControl, event: UIControl.Event, subscriber: S) {
      self.control = control
      self.event = event
      self.subscriber = subscriber

      control.addTarget(self,
                        action: #selector(eventOccured),
                        for: event)
    }

    func request(_ demand: Subscribers.Demand) {
      currentDemand += demand
    }

    func cancel() {
      subscriber = nil
      control.removeTarget(self,
                           action: #selector(eventOccured),
                           for: event)
    }

    @objc func eventOccured() {
      if currentDemand > 0 {
        currentDemand += subscriber?.receive(control) ?? .none
        currentDemand -= 1
      }
    }
  }
}
