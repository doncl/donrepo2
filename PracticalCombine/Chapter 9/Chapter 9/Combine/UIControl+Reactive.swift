//
//  UIControl+Reactive.swift
//  Chapter 9
//
//  Created by Wals, Donny on 08/03/2020.
//  Copyright © 2020 Donny Wals. All rights reserved.
//

import UIKit
import Combine

extension UIControl {
  func publisher(for event: UIControl.Event) -> UIControl.EventPublisher {
    return UIControl.EventPublisher(control: self, controlEvent: event)
  }
}
