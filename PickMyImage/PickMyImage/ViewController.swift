//
//  ViewController.swift
//  PickMyImage
//
//  Created by Don Clore on 2/25/22.
//

import UIKit
import PhotosUI

class ViewController: UIViewController {
  
  var dataSource: UICollectionViewDiffableDataSource<Section, Item>!
  enum Section {
      case main
  }
  
  var items: [Item] = []
  
  lazy var cv: UICollectionView = {
    let layout = configureLayout()
    let coll = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
    return coll
  }()
  
  var cellRegistration: UICollectionView.CellRegistration<Cell, Item>!
  
  
  let group: DispatchGroup = DispatchGroup()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    [cv,].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      view.addSubview($0)
    }
    
    view.sendSubviewToBack(cv)
    
    self.cellRegistration =  UICollectionView.CellRegistration<Cell, Item> { (cell, indexPath, item) in
      cell.item = item
    }

    
    NSLayoutConstraint.activate([
      cv.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      cv.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      cv.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      cv.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
    
    dataSource = UICollectionViewDiffableDataSource<Section, Item>(collectionView: cv, cellProvider: { (collectionView, indexPath, item) -> UICollectionViewCell? in
      return collectionView.dequeueConfiguredReusableCell(using: self.cellRegistration, for: indexPath, item: item)
    })
  }
  
  private func applyInitialData() {
      var snapshot = NSDiffableDataSourceSnapshot<Section, Item>()
      snapshot.appendSections([.main])
      snapshot.appendItems(items, toSection: .main)
    
      dataSource.apply(snapshot, animatingDifferences: false)
  }

  @IBAction func pickeMeTouched(_ sender: UIButton) {
    // Create configuration for photo picker
     var configuration = PHPickerConfiguration()
     
     // Specify type of media to be filtered or picked. Default is all
    configuration.filter = .any(of: [.images, .videos, .livePhotos])
     
     // For unlimited selections use 0. Default is 1
     configuration.selectionLimit = 3
     
     // Create instance of PHPickerViewController
     let picker = PHPickerViewController(configuration: configuration)
     picker.delegate = self
     present(picker, animated: true)
  }
  
  
  private func configureLayout() -> UICollectionViewLayout {
      let config = UICollectionLayoutListConfiguration(appearance: .insetGrouped)
    
      return UICollectionViewCompositionalLayout.list(using: config)
  }
}

extension ViewController: PHPickerViewControllerDelegate {
  func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {
    dismiss(animated: true)
    guard !results.isEmpty else {
      return
    }
    for result in results {
      group.enter()
      result.itemProvider.loadObject(ofClass: UIImage.self) { [weak self] image, error in
        DispatchQueue.main.async {
          guard let self = self else { return }
          guard let image = image as? UIImage, error == nil else {
            return
          }
          let item = Item(image: image)
          self.items.append(item)
          DispatchQueue.main.async {
            self.group.leave()
          }
        }
      }
    }
    group.notify(queue: .main) { [weak self] in
      guard let self = self else {return}
      self.applyInitialData()
    }
  }
}

struct CellConfig: UIContentConfiguration, Hashable {
  var image: UIImage?
  
  func makeContentView() -> UIView & UIContentView {
    return CellView(configuration: self)
  }
  
  func updated(for state: UIConfigurationState) -> CellConfig {
    guard let _ = state as? UICellConfigurationState else {
      return self
    }
    
    let updatedConfiguration = self
    return updatedConfiguration
  }
}

class CellView: UIView, UIContentView {
  let imageView: UIImageView = UIImageView()
  
  private var currentConfiguration: CellConfig!
  var configuration: UIContentConfiguration {
    get {
        currentConfiguration
    }
    set {
        // Make sure the given configuration is of type CellConfig
        guard let newConfiguration = newValue as? CellConfig else {
            return
        }
        
        // Apply the new configuration to SFSymbolVerticalContentView
        // also update currentConfiguration to newConfiguration
        apply(configuration: newConfiguration)
    }
  }
  
  
  init(configuration: CellConfig) {
    super.init(frame: .zero)
    
    self.configuration = configuration
    [imageView,].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      addSubview(imageView)
    }
    NSLayoutConstraint.activate([
      imageView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
      imageView.leftAnchor.constraint(equalTo: layoutMarginsGuide.leftAnchor),
      imageView.rightAnchor.constraint(equalTo: layoutMarginsGuide.rightAnchor),
      imageView.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor),
      imageView.heightAnchor.constraint(equalToConstant: 200),
    ])
    imageView.clipsToBounds = true
    imageView.contentMode = UIView.ContentMode.scaleAspectFill
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  
  private func apply(configuration: CellConfig) {
  
    // Only apply configuration if new configuration and current configuration are not the same
    guard currentConfiguration != configuration else {
        return
    }
    
    // Replace current configuration with new configuration
    currentConfiguration = configuration
  
    imageView.image = configuration.image
      
  }
  
  
}


struct Item: Hashable {
  var id: UUID = UUID()
  var image: UIImage
}



class Cell: UICollectionViewListCell {
  var item: Item?
  
  override func updateConfiguration(using state: UICellConfigurationState) {
    var newConfiguration = CellConfig().updated(for: state)
    newConfiguration.image = item?.image
    
    contentConfiguration = newConfiguration
  }
}
