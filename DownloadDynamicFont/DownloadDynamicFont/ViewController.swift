//
//  ViewController.swift
//  DownloadDynamicFont
//
//  Created by Don Clore on 6/16/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit
import CoreText

class ViewController: UIViewController {
  
  @IBOutlet var boldLabel: UILabel!
  
  @IBOutlet var lightLabel: UILabel!
  
  @IBOutlet var regularLabel: UILabel!
  
  let q = DispatchQueue(label: "fontQ")
  
  let boldURL: URL = URL(string: "https://maven-fonts.s3.amazonaws.com/gangstergrotesk-bold.ttf")!
  let lightURL: URL = URL(string: "https://maven-fonts.s3.amazonaws.com/gangstergrotesk-light.ttf")!
  let regularURL: URL = URL(string: "https://maven-fonts.s3.amazonaws.com/gangstergrotesk-regular.ttf")!
  
  override func viewDidLoad() {
    super.viewDidLoad()
            
    loadFont(url: boldURL, label: boldLabel)
    loadFont(url: lightURL, label: lightLabel)
    loadFont(url: regularURL, label: regularLabel)
  }


}

extension ViewController {
  
  @discardableResult
  func loadFont(url: URL, label: UILabel) -> UIFont? {
    let fontFileName = url.lastPathComponent
    guard !fontFileName.isEmpty else {
      return nil
    }
    print(fontFileName)
    
    q.async { 
      let downloadTask = URLSession.shared.downloadTask(with: url) { urlOrNil, responseOrNil, errorOrNil in
        guard let fileURL = urlOrNil else {
          return
        }
        
        do {
        
          let documentsURL = try
             FileManager.default.url(for: .documentDirectory,
                                            in: .userDomainMask,
                                            appropriateFor: nil,
                                            create: false)
          let savedURL = documentsURL.appendingPathComponent(fontFileName)
          if !FileManager.default.fileExists(atPath: savedURL.path) {
            try FileManager.default.moveItem(at: fileURL, to: savedURL)
          }
          
          DispatchQueue.main.async {
            guard let fontDataProvider = CGDataProvider(url: savedURL as CFURL) else {
              return
            }
            guard let newFont = CGFont(fontDataProvider) else {
              return
            }
            guard let newFontName = newFont.postScriptName as String?  else {
              return
            }
            CTFontManagerRegisterGraphicsFont(newFont, nil)
            label.font = UIFont(name: newFontName, size: 50.0)
          }
          
        } catch let error {
            print ("file error: \(error)")
        }
      }

      downloadTask.resume()
    }
    return nil
  }
}

