//
//  VideoFromImage.swift
//  VideoFromImage
//
//  Created by Don Clore on 2/5/22.
//

import UIKit
import AVFoundation
import CoreVideo
import VideoToolbox

enum VideoMakerError: Error {
  case cantaddinput
  case cantmakepixelbuffer
  case failedtostartwriting
  case failedappendbuffer
}


class VideoFromImage {
  let media_queue = DispatchQueue(label: "movieQ")
  
  static let shared: VideoFromImage = VideoFromImage()
  
  private init() {}
  
  var error: Error?
  
  private func getOuputURL() -> URL {
    let docDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    let file = docDir.appendingPathComponent("outputVideo.MOV")
    
    return file
  }
  
  func makeMovie(fromImage image: UIImage) {
    let outputURL = getOuputURL()
    let canvasSize = image.size
            
    do {
      if FileManager.default.fileExists(atPath: outputURL.path) {
        try FileManager.default.removeItem(at: outputURL)
      }
      
      let videoWriter = try AVAssetWriter(outputURL: outputURL, fileType: AVFileType.mov)
      let videoSettings: [String: AnyObject] = [
        AVVideoCodecKey: AVVideoCodecType.h264 as AnyObject,
        AVVideoWidthKey: canvasSize.width as AnyObject,
        AVVideoHeightKey: canvasSize.height as AnyObject,
        //AVVideoScalingModeKey: AVVideoScalingModeResizeAspectFill as AnyObject,
      ]
      
      let assetWriterInput = AVAssetWriterInput(mediaType: AVMediaType.video, outputSettings: videoSettings)
      guard videoWriter.canAdd(assetWriterInput) else {
        throw VideoMakerError.cantaddinput
      }
      videoWriter.add(assetWriterInput)
      
      let sourceBufferAttributes: [String: Any] = [
        String(kCVPixelBufferPixelFormatTypeKey): NSNumber(value: kCVPixelFormatType_32ARGB),
        (kCVPixelBufferWidthKey as String): Float(canvasSize.width),
        (kCVPixelBufferHeightKey as String): Float(canvasSize.height),
//        String(kCVPixelBufferCGImageCompatibilityKey): NSNumber(booleanLiteral: true),
//        String(kCVPixelBufferCGBitmapContextCompatibilityKey): NSNumber(booleanLiteral: true),
        
      ]
      
      let pixelBufferAdaptor = AVAssetWriterInputPixelBufferAdaptor(assetWriterInput: assetWriterInput, sourcePixelBufferAttributes: sourceBufferAttributes)
      
      guard videoWriter.startWriting() else {
        throw VideoMakerError.failedtostartwriting
      }
      
      videoWriter.startSession(atSourceTime: CMTime.zero)
      
      assetWriterInput.requestMediaDataWhenReady(on: media_queue) { [weak self] in
        guard let self = self else { return }
        
        var frameCount: Int = 0
        var remainingCount: Int = 10
        
        while remainingCount > 0 {
          while assetWriterInput.isReadyForMoreMediaData {
              
            let presentationTime = CMTime(value: Int64(frameCount * 30), timescale: 30)
            
            if !self.appendPixelBufferFor(image: image, pixelBufferAdaptor: pixelBufferAdaptor, presentationTime: presentationTime) {
              self.error = VideoMakerError.failedappendbuffer
              break
            }
            
            frameCount += 1
            remainingCount -= 1
          }
        }

        
        assetWriterInput.markAsFinished()
        videoWriter.finishWriting {
          if let error = videoWriter.error {
            print("error - \(error)")
          } else {
            print("outputURL = \(outputURL)")
          }
        }
      }
    } catch let error {
      print("error - \(error)")
    }
  }
  
  func appendPixelBufferFor(image: UIImage, pixelBufferAdaptor: AVAssetWriterInputPixelBufferAdaptor, presentationTime: CMTime) -> Bool {
      var appendSucceeded = false
      
      autoreleasepool {
          if  let pixelBufferPool = pixelBufferAdaptor.pixelBufferPool {
              let pixelBufferPointer = UnsafeMutablePointer<CVPixelBuffer?>.allocate(capacity: 1)
              let status: CVReturn = CVPixelBufferPoolCreatePixelBuffer(
                  kCFAllocatorDefault,
                  pixelBufferPool,
                  pixelBufferPointer
              )
              
              if let pixelBuffer = pixelBufferPointer.pointee, status == 0 {
                  fillPixelBufferFromImage(image, pixelBuffer: pixelBuffer)
                  
                  appendSucceeded = pixelBufferAdaptor.append(
                      pixelBuffer,
                      withPresentationTime: presentationTime
                  )
                  
                  pixelBufferPointer.deinitialize(count: 1)
              } else {
                  NSLog("error: Failed to allocate pixel buffer from pool")
              }
              
              pixelBufferPointer.deallocate()
          }
      }
      
      return appendSucceeded
  }

  
  func fillPixelBufferFromImage(_ image: UIImage, pixelBuffer: CVPixelBuffer) {
      CVPixelBufferLockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
      
      let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer)
      let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
      let context = CGContext(
          data: pixelData,
          width: Int(image.size.width),
          height: Int(image.size.height),
          bitsPerComponent: 8,
          bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer),
          space: rgbColorSpace,
          bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue
      )
      
      context?.draw(image.cgImage!, in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
      
      CVPixelBufferUnlockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
  }
}

extension UIImage {
    /**
     Resizes the image to width x height and converts it to an RGB CVPixelBuffer.
     */
    public func pixelBuffer(width: Int, height: Int) -> CVPixelBuffer? {
        return pixelBuffer(width: width, height: height,
                           pixelFormatType: kCVPixelFormatType_32ARGB,
                           colorSpace: CGColorSpaceCreateDeviceRGB(),
                           alphaInfo: .noneSkipFirst)
    }
    
    /**
     Resizes the image to width x height and converts it to a grayscale CVPixelBuffer.
     */
    public func pixelBufferGray(width: Int, height: Int) -> CVPixelBuffer? {
        return pixelBuffer(width: width, height: height,
                           pixelFormatType: kCVPixelFormatType_OneComponent8,
                           colorSpace: CGColorSpaceCreateDeviceGray(),
                           alphaInfo: .none)
    }
    
    func pixelBuffer(width: Int, height: Int, pixelFormatType: OSType,
                     colorSpace: CGColorSpace, alphaInfo: CGImageAlphaInfo) -> CVPixelBuffer? {
        var maybePixelBuffer: CVPixelBuffer?
        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue,
                     kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue]
        let status = CVPixelBufferCreate(kCFAllocatorDefault,
                                         width,
                                         height,
                                         pixelFormatType,
                                         attrs as CFDictionary,
                                         &maybePixelBuffer)
        
        guard status == kCVReturnSuccess, let pixelBuffer = maybePixelBuffer else {
            return nil
        }
        
        CVPixelBufferLockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
        let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer)
        
        guard let context = CGContext(data: pixelData,
                                      width: width,
                                      height: height,
                                      bitsPerComponent: 8,
                                      bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer),
                                      space: colorSpace,
                                      bitmapInfo: alphaInfo.rawValue)
            else {
                return nil
        }
        
        UIGraphicsPushContext(context)
        context.translateBy(x: 0, y: CGFloat(height))
        context.scaleBy(x: 1, y: -1)
        self.draw(in: CGRect(x: 0, y: 0, width: width, height: height))
        UIGraphicsPopContext()
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer, CVPixelBufferLockFlags(rawValue: 0))
        return pixelBuffer
    }
}

extension UIImage {
    /**
     Creates a new UIImage from a CVPixelBuffer.
     */
    public convenience init?(pixelBuffer: CVPixelBuffer) {
      var cgImage: CGImage?
      VTCreateCGImageFromCVPixelBuffer(pixelBuffer, options: nil, imageOut: &cgImage)
        
      if let cgImage = cgImage {
          self.init(cgImage: cgImage)
      } else {
          return nil
      }
    }
    
    /**
     Creates a new UIImage from a CVPixelBuffer, using Core Image.
     */
    public convenience init?(pixelBuffer: CVPixelBuffer, context: CIContext) {
        let ciImage = CIImage(cvPixelBuffer: pixelBuffer)
        let rect = CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(pixelBuffer),
                          height: CVPixelBufferGetHeight(pixelBuffer))
        if let cgImage = context.createCGImage(ciImage, from: rect) {
            self.init(cgImage: cgImage)
        } else {
            return nil
        }
    }
}
