//
//  ViewController.swift
//  VideoFromImage
//
//  Created by Don Clore on 2/4/22.
//

import UIKit
import AVFoundation
import CoreVideo

class ViewController: UIViewController {
  let q = DispatchQueue(label: "movieQ")
  let exportingQ = DispatchQueue(label: "exportingQ")

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view.
  }


  @IBAction func buttonTapped(_ sender: UIButton) {
    print("\(#function)")
    
    let start = Date()
    let image = UIImage(named: "library.jpg")!
    //let image = UIImage(named: "blue.jpg")!
    VideoFromImage.shared.makeMovie(fromImage: image)
    let elapsed = Date().timeIntervalSince(start)
    print("Elapsed = \(elapsed) seconds")
  }
}

