//
//  FauxLaunchScreen.swift
//  Sportsy
//
//  Created by Don Clore on 4/30/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class FauxLaunchScreen: UIViewController {
  let imageView: UIImageView = UIImageView(image: UIImage(named: "launchImage"))  

  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = UIColor.white
    imageView.contentMode = UIView.ContentMode.scaleAspectFit
    imageView.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(imageView)
    
    NSLayoutConstraint.activate([
      imageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
      imageView.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
      imageView.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor),
      imageView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
    ])
  }
}
