//
//  ViewController.swift
//  Sportsy
//
//  Created by Don Clore on 4/30/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  let fauxLaunch: FauxLaunchScreen = FauxLaunchScreen()
  var subscriptionKeys: [String] = []
   
  @IBOutlet var startUpdatingLocationsButton: UIButton!
  
  
  lazy var activity: UIActivityIndicatorView = {
    let a = UIActivityIndicatorView(style: .large)
    a.translatesAutoresizingMaskIntoConstraints = false
    return a
  }()
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    doTheFauxLaunchScreen()
    self.view.addSubview(self.activity)
    NSLayoutConstraint.activate([
      self.activity.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
      self.activity.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
    ])
  }
  
  @IBAction func startUpdatingLocationsButtonPressed(_ sender: UIButton) {
    LocationManager.shared.startUpdatingLocations()
  }
}

extension ViewController {
  private func doTheFauxLaunchScreen() {
    addChild(fauxLaunch)
    view.addSubview(fauxLaunch.view)
    fauxLaunch.didMove(toParent: self)
    fauxLaunch.view.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
      fauxLaunch.view.topAnchor.constraint(equalTo: view.topAnchor),
      fauxLaunch.view.leftAnchor.constraint(equalTo: view.leftAnchor),
      fauxLaunch.view.rightAnchor.constraint(equalTo: view.rightAnchor),
      fauxLaunch.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
    ])

    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
      let xform = CGAffineTransform(scaleX: 0.1, y: 0.1).concatenating(CGAffineTransform(rotationAngle: CGFloat.pi * 3))
      UIView.animate(withDuration: 0.400, delay: 0, options: [.curveEaseInOut], animations: { [weak self] in
        guard let self = self else { return }
        self.fauxLaunch.view.alpha = 0
        self.fauxLaunch.imageView.alpha = 0
        self.fauxLaunch.view.transform = xform
      }, completion: { [weak self] _ in
        guard let self = self else { return }
        self.fauxLaunch.willMove(toParent: nil)
        self.fauxLaunch.view.removeFromSuperview()
        self.fauxLaunch.removeFromParent()
        self.doCrudeLoginDlg()
      })
    })
  }
  
  private func doCrudeLoginDlg() {
    let ac = UIAlertController(title: "Crude faux login dialog", message: "Gimme all your secrets", preferredStyle: .alert)
    ac.addTextField(configurationHandler: { textField in
      textField.placeholder = "Email Address or Username"
    })
    
    ac.addTextField(configurationHandler: { textField in
      textField.isSecureTextEntry = true
      textField.placeholder = "Password"
    })

    let action = UIAlertAction(title: "Submit", style: .default, handler: {[unowned self] _ in
      guard let username = ac.textFields![0].text, let password = ac.textFields![1].text else {
         return  // In a real world, there'd be validation, and disabling of submit button,
                 // and a real custom dialog.  This is just to communicate the intent.
      }
      self.activity.isHidden = false
      self.view.bringSubviewToFront(self.activity)
      self.activity.startAnimating()
      AuthManager.shared.login(usernameOrPassword: username, password: password, success: { [unowned self] in
        self.activity.stopAnimating()
        let successDlg = UIAlertController(title: "SUCCESS", message: "Nice", preferredStyle: .alert)
        self.present(successDlg, animated: true, completion: {
          DispatchQueue.main.asyncAfter(deadline: .now() + 2.0, execute: {
            
            self.dismiss(animated: true, completion: { [weak self] in
              guard let self = self else { return }
              self.connectWebSocket()
              LocationManager.shared.askForAuthorization()
              LocationManager.shared.startMonitoringVisits()
              DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: { [weak self] in
                guard let self = self else { return }
                self.startUpdatingLocationsButton.isEnabled = true
              })
            })
          })
        })
      }, failure: { error in
        let errDlg = UIAlertController(title: "ERROR", message: error, preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
        errDlg.addAction(ok)
        self.present(errDlg, animated: true, completion: nil)
      })
    })
     
     ac.addAction(action)
     present(ac, animated: true)
  }
  
  
}

extension ViewController {
  func connectWebSocket() {
     NotificationCenter.default.addObserver(self,
       selector: #selector(ViewController.webSocketOpened(_:)),
       name: SportsySocketClient.socketOpened, object: nil)
    
    // SUBSCRIBE TO A BUNCH OF OTHER THINGS>
    subscriptionKeys = SportsySocketClient.shared.getSubscriptionKeys()
  }
  
  @objc func webSocketOpened(_ note: Notification) {
    // Do something useful
  }
}


