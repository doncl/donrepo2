//
//  LocationManager.swift
//  Sportsy
//
//  Created by Don Clore on 4/30/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation
import CoreLocation
import UserNotifications

class LocationManager: NSObject {
  static let shared = LocationManager()
  
  private let center = UNUserNotificationCenter.current()
  private let clLocMgr = CLLocationManager()
  let geoCoder = CLGeocoder()
  
  private override init() {}
  
  func askForAuthorization() {
    clLocMgr.requestAlwaysAuthorization()
    center.requestAuthorization(options: [.alert, .sound]) { granted, error in
      // We don't really need this for this POC?  The idea is we might want to display banners
      // with location.  Just to show that this works, for reals.
      // If they refuse, fine.
    }
  }
  
  func startMonitoringVisits() {
    clLocMgr.startMonitoringVisits()
    clLocMgr.delegate = self
  }
  
  func startUpdatingLocations() {
    clLocMgr.distanceFilter = 35
    clLocMgr.allowsBackgroundLocationUpdates = true
    clLocMgr.startUpdatingLocation()
  }
  
}

extension LocationManager: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didVisit visit: CLVisit) {
    // create CLLocation from the coordinates of CLVisit
    let clLocation = CLLocation(latitude: visit.coordinate.latitude, longitude: visit.coordinate.longitude)    
    // Get location description
     geoCoder.reverseGeocodeLocation(clLocation) { placemarks, _ in
       if let place = placemarks?.first {
         let description = "\(place)"
         self.newVisitReceived(visit, description: description)
       }
     }
  }

  func newVisitReceived(_ visit: CLVisit, description: String) {
    let location = Location(visit: visit, descriptionString: description)
    LocationsStorage.shared.saveLocationOnDisk(location)
    
    let content = UNMutableNotificationContent()
    content.title = "New VISIT!"
    content.body = location.description
    content.sound = UNNotificationSound.default
    
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
    let request = UNNotificationRequest(identifier: location.dateString, content: content, trigger: trigger)
    
    center.add(request, withCompletionHandler: nil)
  }
  
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let location = locations.first else {
      return
    }
    
    geoCoder.reverseGeocodeLocation(location) { placemarks, _ in
      if let place = placemarks?.first {
        let description = "Fake visit: \(place)"
        
        let fakeVisit = FakeVisit(
          coordinates: location.coordinate,
          arrivalDate: Date(),
          departureDate: Date())
        self.newVisitReceived(fakeVisit, description: description)
      }
    }
  }
}

// Shamelessly stolen from Wenderlich tutorial
final class FakeVisit: CLVisit {
  private let myCoordinates: CLLocationCoordinate2D
  private let myArrivalDate: Date
  private let myDepartureDate: Date

  override var coordinate: CLLocationCoordinate2D {
    return myCoordinates
  }
  
  override var arrivalDate: Date {
    return myArrivalDate
  }
  
  override var departureDate: Date {
    return myDepartureDate
  }
  
  init(coordinates: CLLocationCoordinate2D, arrivalDate: Date, departureDate: Date) {
    myCoordinates = coordinates
    myArrivalDate = arrivalDate
    myDepartureDate = departureDate
    super.init()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}
