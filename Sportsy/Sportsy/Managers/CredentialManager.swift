//
//  CredentialManager.swift
//  Sportsy
//
//  Created by Don Clore on 4/30/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

struct CredentialManager {
  struct Credentials: Codable {
    let username: String
    let password: String
  }
  
  static let shared = CredentialManager()
  
  private init() {}
  
  func serializeCredentials(username: String, password: String) -> Data? {
    let creds = Credentials(username: username, password: password)
    let coder = JSONEncoder()
    
    do {
      return try coder.encode(creds)
    } catch (let error) {
      print("error = \(error.localizedDescription)")
      return nil
    }
  }
  
  func saveToKeychain(token: String) {
    let wrapper = KeychainWrapper()
    wrapper.mySetObject(token, forKey: kSecValueData)
    wrapper.writeToKeychain()
  }
  
  func clearAuthToken() {
    let wrapper = KeychainWrapper()
    wrapper.mySetObject("", forKey: kSecValueData)
    wrapper.writeToKeychain()
  }
  
  func getToken() -> String? {
    let wrapper = KeychainWrapper()
    return wrapper.myObject(forKey: kSecValueData) as? String
  }
}
