//
//  AuthManager.swift
//  Sportsy
//
//  Created by Don Clore on 4/30/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

struct AuthManager {
  private init() {}
  static let shared = AuthManager()
    
  func login(usernameOrPassword: String, password: String,
             success: @escaping () -> (),
             failure: @escaping (String) -> ()) {
    
    guard let data = CredentialManager.shared.serializeCredentials(username: usernameOrPassword, password: password) else {
      failure("Could not serialize the supplied credentials")
      return
    }
    Net.sharedInstance.makeRestfulCall(verb: "POST", uri: "https://placeholderloginportal",
                                       additionalHeaders: nil,
                                       postBodyData: data,
                                       success: { token in
                                        
      CredentialManager.shared.saveToKeychain(token: token)
      success()
    }, failure: { error in
      failure(error)
    })
  }
}
