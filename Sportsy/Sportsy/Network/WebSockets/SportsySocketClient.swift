//
//  SportsySocketClient.swift
//  Sportsy
//
//  Created by Don Clore on 4/30/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation
import UIKit
import Network

@objc class SportsySocketClient: NSObject {
  
  static let socketOpened = NSNotification.Name(rawValue: "sportsywebsocketopened")
  
  static let shared = SportsySocketClient()
    
  var subscriptionURL : URL?
  var reconnecting : Bool = false
  var socket: SRWebSocket?
  var deliberateDisconnection = false
  static let initialReconnectDelay : Int64 = 1
  static let maxReconnectDelay : Int64 = 64
  var currentReconnectDelay : Int64 =  1
  var open : Bool {
    guard let socket = socket else {
      return false
    }
    return socket.readyState == SRReadyState.OPEN
  }
  var reachabilityDown = false
  
  static let networkChanged = Notification.Name("network_changed")
  
  var connected : Bool {
    get {
      guard let _ = socket else {
        return false
      }
      return true
    }
  }
  var lastError: Int = 0

  private override init() {
    super.init()

      NotificationCenter.default.addObserver(self,
                                             selector: #selector(SportsySocketClient.networkChanged(_:)),
                                             name: SportsySocketClient.networkChanged, object: nil)
  }
 
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
}

//MARK: Reachability and networking handling
extension SportsySocketClient {

  
  @objc func networkChanged(_ note : NSNotification) {
    
  }
}

//MARK: class subscription prefix methods
extension SportsySocketClient {
  func getSubscriptionKeys() -> [String] {
    return [] // Whatever sportsy needs to do here.
  }
}

//MARK: Subscribing and sending.
extension SportsySocketClient {

  fileprivate func delay(_ delaySeconds : Int64, delayFunc : @escaping () -> ()) {
    let triggerTime = (Int64(NSEC_PER_SEC) * delaySeconds)
    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(triggerTime) / Double(NSEC_PER_SEC), execute: {
      delayFunc()
    })
  }

  fileprivate func reconnectWithExponentialBackOff() {
    delay(currentReconnectDelay, delayFunc: {
      self.connect(reconnecting: true)
      self.currentReconnectDelay = self.currentReconnectDelay * 2

      // Don't let the reconnect delay get too crazy large.
      if self.currentReconnectDelay > SportsySocketClient.maxReconnectDelay {
        self.currentReconnectDelay = SportsySocketClient.initialReconnectDelay
      }
    })
  }
  
  func connect(reconnecting : Bool = false) {

    self.reconnecting = reconnecting

    let subscriptionURLString = NetworkConstants.getWebsocketSubscriptionURL()
    subscriptionURL = URL(string: subscriptionURLString)
    if let subscriptionURL = subscriptionURL {
      
      if reconnecting || socket != nil {
        cleanupSocket()
      }
      
      if socket == nil {
        socket = SRWebSocket(url: subscriptionURL)
        socket?.delegate = self
      }
      socket!.open()
    }
  }
  
  fileprivate func webSocketNotification(text: String) {
    guard let _ = text.data(using: .utf8) else {
      print("Failed to turn websocketnoficiation text into Data object, text = \(text)")
      return
    }
    
    // TODO:  Decode the JSON (assuming it's JSON) notification, find out which ones have been subscribed to, and
    // fire off NotificationCenter notifications to the subscribers.
    
//    NotificationEventResponse.jsonDecode(data, success: { notification in
//      guard let subscriptionKeys = notification.subscriptionKeys else {
//        Global.log.error("Nil subscription keys on notification \(text)")
//        return
//      }
//      DispatchQueue.main.async {
//        for subscriptionKey in subscriptionKeys {
//          let notificationName = Notification.Name(subscriptionKey)
//          NotificationCenter.default.post(name: notificationName, object: notification)
//        }
//      }
//
//    }, failure: { error in
//      Global.log.error("Failed to deserialize web socket notification from text \(text) err = \(error)")
//      return
//    })
//
  }

  func disconnect() {
    deliberateDisconnection = true
    cleanupSocket()
  }
  
  fileprivate func cleanupSocket() {
    if let socket = socket {
      socket.delegate = nil
      socket.close()
    }
    socket = nil
  }

  func subscribe(toKey keyName: String,
    completion: ((Notification.Name) -> Void)? = nil) {
    
    guard let socket = socket else {
      print("Web Socket not initialized")
      return
    }
    
    if open {
      let msg = "Subscribe:\(keyName)"
      socket.send(msg)
    }
    
    if let completion = completion {
      let key = Notification.Name(keyName)
      completion(key)
    }
  }

  func unsubscribe(fromKey keyName: String,
    completion: ((Notification.Name) -> Void)? = nil) {
    
    guard let socket = socket else {
      print("Web Socket not initialized")
      return
    }
    if open {
      let msg = "Unsubscribe:\(keyName)"
      socket.send(msg)
    }
    
    if let completion = completion {
      let key = Notification.Name(keyName)
      completion(key)
    }
  }

  func subscribeAllUserNotifications() {
    // Subscribe to all kinds of things.
    subscribeNotificationConsumed()
  }
    
  fileprivate func subscribeNotificationConsumed(){

    guard let socket = socket else {
      print("Web Socket not initialized")
      return
    }

    guard let token = CredentialManager.shared.getToken() else {
      print("Either Web Socket not intialized or user not logged in")
      return
    }
    
    
    let notificationConsumedSubscription = "notificationConsumed:\(token)"
    let msg = "subscribing with \(notificationConsumedSubscription)"
    print(msg)
    
    socket.send(notificationConsumedSubscription)
  }
}

//MARK: SRWebSocketDelegate
extension SportsySocketClient : SRWebSocketDelegate {
  func webSocket(_ webSocket: SRWebSocket!, didReceiveMessage message: Any!) {
    if let messageString = message as? String {
      webSocketNotification(text: messageString)
    }
  }

  func webSocketDidOpen(_ webSocket: SRWebSocket!) {
    let subscriptionURLString = subscriptionURL?.absoluteString
    print("Web Socket connected to \(subscriptionURLString as Optional)")
    DispatchQueue.main.async {
      NotificationCenter.default.post(name: SportsySocketClient.socketOpened,
        object: self.reachabilityDown)
      self.reachabilityDown = false
      self.currentReconnectDelay = SportsySocketClient.initialReconnectDelay
      self.subscribeAllUserNotifications()
    }
  }

  func webSocket(_ webSocket: SRWebSocket!, didFailWithError error: Error!) {
    print("Web socket failure desc = \(error.localizedDescription)")
  }

  func webSocket(_ webSocket: SRWebSocket!, didCloseWithCode code: Int, reason: String!,
                 wasClean: Bool) {

    if false == wasClean {
      lastError = code
      let msg = "Error on disconnecting from Web Socket = \(code)"
      print(msg)
      deliberateDisconnection = false
      cleanupSocket()
    }
    
    if deliberateDisconnection {
      let msg = "Web Socket successfully disconnected"
      print(msg)
      currentReconnectDelay = SportsySocketClient.initialReconnectDelay
    } else {
      let msg = "Web Socket unintentionally disconnecting, reconnecting with " +
                "delay of \(self.currentReconnectDelay) seconds"
    
      print(msg)
    
      reconnectWithExponentialBackOff()
    }
  }
  
  func webSocket(_ webSocket: SRWebSocket!, didReceivePong pongPayload: Data!) {
    print("Received websocket Pong")
  }

  func webSocketShouldConvertTextFrame(toString webSocket: SRWebSocket!) -> Bool {
    return true
  }
}




