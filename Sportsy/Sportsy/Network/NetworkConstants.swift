//
//  NetworkConstants.swift
//  Sportsy
//
//  Created by Don Clore on 4/30/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

struct NetworkConstants {
  static let webSocketHost = "fauxWebSockets"
  
  static func getWebsocketSubscriptionURL() -> String {
    return "wss://\(webSocketHost)/notifications"
  }
}
