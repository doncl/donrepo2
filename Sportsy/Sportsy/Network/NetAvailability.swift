//
//  NetAvailability.swift
//  Sportsy
//
//  Created by Don Clore on 4/30/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation
import Network

struct NetAvailability {
  var webSocketHostNetworkMonitor: NWPathMonitor?
  
  static let shared = NetAvailability()
  
  private init() {}
  
  mutating func setupNetworkMonitor() {
    
    if webSocketHostNetworkMonitor == nil {
      webSocketHostNetworkMonitor = NWPathMonitor()
      webSocketHostNetworkMonitor!.pathUpdateHandler = { path in
        NotificationCenter.default.post(name: SportsySocketClient.networkChanged, object: nil)
      }
      
      let queue = DispatchQueue.global(qos: .background)
      webSocketHostNetworkMonitor?.start(queue: queue)
    }  
  }
}
