//
//  FauxNet.swift
//  Sportsy
//
//  Created by Don Clore on 4/30/20.
//  Copyright © 2020 Beer Barrel Poker Studios, Inc. All rights reserved.
//

import Foundation

class FauxNet: InterWebs {
  func makeRestfulCall(verb: String, uri: String,
                       additionalHeaders: [String : String]?,
                       postBodyData: Data?,
                       success: @escaping (String) -> (), failure: @escaping (String) -> ()) {
    
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.900, execute: {
      success(UUID().uuidString)  // fake token.
    })
  }
}
