// ReSharper disable once CheckNamespace

using CSharpAutoSuggest.CandidateAdditions;
using CSharpAutoSuggest.Entities;

namespace CSharpAutoSuggest
{
    public partial class AutoSuggestService
    {
        static Trie<LabelEntity> labelsTrie = new Trie<LabelEntity>();
        static Trie<ArtistEntity> artistsTrie = new Trie<ArtistEntity>();
        static Trie<ReleaseEntity> releasesTrie = new Trie<ReleaseEntity>();
        
        
        public AutoSuggestResponse GetAutoSuggest(string prefix)
        {
            // NOTE TO CANDIDATE:  THIS IS WHERE YOU DO THE ACTUAL WORK.
            // The goal here is to do something with the prefix passed in, comparing it to the
            // labels, artist, and releases that are loaded from disk, and construct a 3 part result
            // set (an AutoSuggestResponse object), that comprises reasonable suggestions returned,
            // given this prefix.
            var labels = labelsTrie.GetMatchingResults(prefix);
            var artists = artistsTrie.GetMatchingResults(prefix);
            var releases = releasesTrie.GetMatchingResults(prefix);
            return new AutoSuggestResponse
            {
                Labels = labels,
                Artists = artists,
                Releases = releases,
            };
        }
        
        
        private static void ProcessEntities(LabelList labels, ArtistList artists, ReleasesList releases)
        {
            foreach (var label in labels.Labels)
            {
                if (label.Name != null)
                {
                    labelsTrie.Insert(label.Name, label);
                }
                
                // TODO:  Lots of other ways to insert the label, and have multiple entries in the trie.  This is bare
                // minimum.
            }

            foreach (var artist in artists.Artists)
            {
                if (artist.Name != null)
                {
                    artistsTrie.Insert(artist.Name, artist);
                }
                // TODO:  Lots of other ways to insert the artist, and have multiple entries in the trie.  This is bare
                // minimum.                
            }

            foreach (var release in releases.Releases)
            {
                if (release.Title != null)
                {
                    releasesTrie.Insert(release.Title, release);
                }
            }
        }
    }
}