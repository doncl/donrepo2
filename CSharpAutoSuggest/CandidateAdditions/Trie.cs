using System;
using System.Collections.Generic;
using System.Linq;

namespace CSharpAutoSuggest.CandidateAdditions
{
    public class Trie<T> where T: class
    {
        private TrieNode<T> root = new TrieNode<T>(key: null, parent: null, body: null);
        
        public void Insert(string key, T body)
        {
            var newKey = key.Trim('\n', ' ').ToLower();
            var current = root;
            foreach (var character in newKey)
            {
                var element = character.ToString();
                if (!current.Children.ContainsKey(element))
                {
                    var newChild = new TrieNode<T>(key: element, parent: current, body: body);
                    current.Children.Add(element, newChild);
                }

                current = current.Children[element];
            }

            current.IsTerminal = true;
        }

        public List<T> GetMatchingResults(string prefix)
        {
            var newPrefix = prefix.ToLower();
            var current = root;
            foreach (var c in newPrefix)
            {
                var element = c.ToString();
                if (!current.Children.ContainsKey(element))
                {
                    return Enumerable.Empty<T>().ToList();
                }

                current = current.Children[element];
            }

            return GetMatchingResultsAfterNode(newPrefix, current);
        }

        private List<T> GetMatchingResultsAfterNode(string prefix, TrieNode<T> node)
        {
            var results = new List<T>();
            if (node.IsTerminal)
            {
                if (node.Body != null)
                {
                    results.Add(node.Body);
                }
            }

            foreach (var child in node.Children.Values.ToList())
            {
                var subPrefix = prefix;
                subPrefix = $"{prefix}{child.Key}";
                var subValues = GetMatchingResultsAfterNode(subPrefix, child);
                results.AddRange(subValues);
            }

            return results;
        }
    }
}