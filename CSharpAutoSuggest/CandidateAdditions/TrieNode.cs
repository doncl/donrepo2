using System.Collections.Generic;

namespace CSharpAutoSuggest.CandidateAdditions
{
    public class TrieNode<T> where T: class
    {
        public string Key { get; set; }
        public TrieNode<T> Parent { get; set; }
        public Dictionary<string, TrieNode<T>> Children { get; set; }
        public T Body { get; set; }
        public bool IsTerminal { get; set; }

        public TrieNode(string key, TrieNode<T> parent, T body)
        {
            this.Key = key;
            this.Parent = parent;
            this.Body = body;
            this.Children = new Dictionary<string, TrieNode<T>>();
        }
    }
}