//
//  ContentView.swift
//  HudsonAlignmentGuide
//
//  Created by Don Clore on 2/18/21.
//

import SwiftUI

struct ContentView: View {
  var body: some View {
    VStack(alignment: .leading) {
      ForEach(0..<10) { position in
        Text("Number \(position)")
          .alignmentGuide(HorizontalAlignment.leading) { d in
            return CGFloat(position) * -10
          }
          .border(Color.black)
      }
    }
    .background(Color.red)
    .frame(width: 400, height: 400)
    .background(Color.blue)
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
      ContentView()
  }
}
