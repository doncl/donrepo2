//
//  HudsonAlignmentGuideApp.swift
//  HudsonAlignmentGuide
//
//  Created by Don Clore on 2/18/21.
//

import SwiftUI

@main
struct HudsonAlignmentGuideApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
