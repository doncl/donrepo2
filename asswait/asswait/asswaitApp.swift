//
//  asswaitApp.swift
//  asswait
//
//  Created by Don Clore on 10/22/21.
//

import SwiftUI

@main
struct asswaitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
