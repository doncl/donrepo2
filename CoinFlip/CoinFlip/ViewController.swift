//
//  ViewController.swift
//  CoinFlip
//
//  Created by Don Clore on 7/7/21.
//

import UIKit

class ViewController: UIViewController {
  struct TranInfo {
    let duration: TimeInterval
    let repeats: Float
  }
  let img = UIImageView(image: UIImage(named: "coin")!)
  
  var currentIndex: Int = 0
  
  let trans: [TranInfo] = [
    TranInfo (duration: 0.1, repeats: 5),
    TranInfo (duration: 0.2, repeats: 3),
    TranInfo (duration: 0.3, repeats: 2),
    TranInfo (duration: 0.5, repeats: 1),
  ]
  

  override func viewDidLoad() {
    super.viewDidLoad()
    
    img.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(img)
    
    NSLayoutConstraint.activate([
      img.centerXAnchor.constraint(equalTo: view.centerXAnchor),
      img.centerYAnchor.constraint(equalTo: view.centerYAnchor),
      img.widthAnchor.constraint(equalToConstant: 200),
      img.heightAnchor.constraint(equalTo: img.widthAnchor),
    ])
  }

  private func simpleSpinTransition() {
    let t = CATransition()
    t.startProgress = 0
    t.endProgress = 1.0
    t.type = CATransitionType(rawValue: "flip")
    t.subtype = CATransitionSubtype(rawValue: "fromRight")
    t.duration = 0.2
    t.repeatCount = 10
    t.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
    t.delegate = self
    
    img.layer.add(t, forKey: "trans0")
  }
  
  private func chainedSpinTransition(forIndex index: Int) {
    guard index < trans.count else {
      return
    }
    let traninfo = trans[index]
    let t = CATransition()
    t.startProgress = 0
    t.endProgress = 1.0
    t.type = CATransitionType(rawValue: "flip")
    t.subtype = CATransitionSubtype(rawValue: "fromRight")
    t.duration = traninfo.duration
    t.repeatCount = traninfo.repeats
    t.delegate = self
    
    img.layer.add(t, forKey: "transition")
  }

  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    //simpleSpinTransition()
    chainedSpinTransition(forIndex: currentIndex)
  }
}

extension ViewController: CAAnimationDelegate {
  func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
    if flag {
      currentIndex += 1
      chainedSpinTransition(forIndex: currentIndex)
    }
  }
}

