//
//  ViewController.swift
//  Blurme
//
//  Created by Don Clore on 3/18/22.
//

import UIKit

class ViewController: UIViewController {
  let img: UIImageView = UIImageView(image: UIImage(named: "grater")!)
  
  lazy var blur: UIVisualEffectView = {
    let blurEffect = UIBlurEffect(style: .systemThinMaterialLight)
    let blurEffectView = UIVisualEffectView(effect: blurEffect)
    blurEffectView.alpha = 0.9
    blurEffectView.clipsToBounds = true
    return blurEffectView
  }()
  

  var blurLeft: NSLayoutConstraint = NSLayoutConstraint()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    img.contentMode = UIView.ContentMode.scaleAspectFill

    [img, blur,].forEach {
      $0.translatesAutoresizingMaskIntoConstraints = false
      view.addSubview($0)
    }
        
    img.clipsToBounds = true
    
    blurLeft = blur.leftAnchor.constraint(equalTo: img.leftAnchor, constant: 0)
    
    NSLayoutConstraint.activate([
      img.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 100),
      img.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 50),
      img.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: -50),
      img.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -100),
      
      blurLeft,
      blur.topAnchor.constraint(equalTo: img.topAnchor),
      blur.rightAnchor.constraint(equalTo: img.rightAnchor),
      blur.bottomAnchor.constraint(equalTo: img.bottomAnchor),
    ])
  }

  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    view.layoutIfNeeded()
    let width = img.bounds.width
    self.blurLeft.constant = width
    UIView.animate(withDuration: 6, delay: 0, options: [.curveLinear], animations: {
      self.view.layoutIfNeeded()
    }, completion: nil)
  }

}

