//
//  AttemptToPlaceAndDrawApp.swift
//  AttemptToPlaceAndDraw
//
//  Created by Don Clore on 9/26/21.
//

import SwiftUI

@main
struct AttemptToPlaceAndDrawApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
