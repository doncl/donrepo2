//
//  ContentView.swift
//  AttemptToPlaceAndDraw
//
//  Created by Don Clore on 9/26/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
