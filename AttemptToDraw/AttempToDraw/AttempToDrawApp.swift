//
//  AttempToDrawApp.swift
//  AttempToDraw
//
//  Created by Don Clore on 9/20/21.
//

import SwiftUI

@main
struct AttempToDrawApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
