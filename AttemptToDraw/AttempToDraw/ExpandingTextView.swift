//
//  WrappedTextView.swift
//  AttempToDraw
//
//  Created by Don Clore on 9/26/21.
//

import SwiftUI

struct ExpandingTextView: View {
	@Binding var text: String
	let minHeight: CGFloat = 32
	let minWidth: CGFloat = 160.0
	@State private var textViewHeight: CGFloat?
	@State private var textViewWidth: CGFloat?

	var body: some View {
		WrappedTextView(text: $text, textDidChange: self.textDidChange)
			.frame(width: textViewWidth ?? minWidth, height: textViewHeight ?? minHeight)
			.border(Color.black, width: 2.0)
	}

	private func textDidChange(_ textView: UITextView) {
		let fixedHeight = textView.frame.height
		let newSize = textView.sizeThatFits(CGSize(width: CGFloat.greatestFiniteMagnitude, height: fixedHeight))
		let newHeight = newSize.height
		let newWidth = newSize.width
		
		textViewWidth = newWidth
		textViewHeight = newHeight

		
		print("\(#function) - width = \(newWidth), height = \(newHeight)")
	}
}


private struct WrappedTextView: UIViewRepresentable {
	typealias UIViewType = UITextView
	
	@Binding var text: String
	let textDidChange: (UITextView) -> Void

	func makeUIView(context: Context) -> UITextView {
		let view = UITextView()
		view.isEditable = true
		view.delegate = context.coordinator
		view.autocorrectionType = .no
		view.autocapitalizationType = .none
		view.keyboardType = .alphabet
		view.textAlignment = .center
		view.font = UIFont.preferredFont(forTextStyle: .largeTitle)
		return view
	}

	func updateUIView(_ uiView: UITextView, context: Context) {
		uiView.text = self.text
		DispatchQueue.main.async {
			self.textDidChange(uiView)
		}
	}

	func makeCoordinator() -> Coordinator {
		return Coordinator(text: $text, textDidChange: textDidChange)
	}

	class Coordinator: NSObject, UITextViewDelegate {
		@Binding var text: String
		let textDidChange: (UITextView) -> Void

		init(text: Binding<String>, textDidChange: @escaping (UITextView) -> Void) {
			self._text = text
			self.textDidChange = textDidChange
		}

		func textViewDidChange(_ textView: UITextView) {
			self.text = textView.text
			self.textDidChange(textView)
		}
	}
}

struct ExpandingTextView_Previews: PreviewProvider {
	@State static private var text: String = "Hello, WrappedText!"

    static var previews: some View {
		ExpandingTextView(text: $text)
    }
}
