//
//  ContentView.swift
//  AttempToDraw
//
//  Created by Don Clore on 9/20/21.
//

import SwiftUI
import UIKit

struct ContentView: View {
	@State private var text: String = "Hello, TextEditor"
	
    var body: some View {		
		ExpandingTextView(text: $text)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}


