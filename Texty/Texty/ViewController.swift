//
//  ViewController.swift
//  Texty
//
//  Created by Don Clore on 3/30/22.
//

import UIKit

class ViewController: UIViewController {
  
  let texty: Texty = Texty()

  override func viewDidLoad() {
    super.viewDidLoad()
    
    texty.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(texty)
    
    NSLayoutConstraint.activate([
      texty.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -24),
      texty.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 24),
      texty.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -24),
      texty.heightAnchor.constraint(equalToConstant: 180),
    ])
    
    let string = "Macbook Pro\nLG 4k 27\" Display\nMX Master & MX Keys\nAnother line of text\n"

    texty.text = string
    texty.title = "BITE NAME"
  }
}

