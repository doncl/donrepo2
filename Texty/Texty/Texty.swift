//
//  Texty.swift
//  Texty
//
//  Created by Don Clore on 3/30/22.
//

import UIKit

class Texty: UIView {
  var text: String = "" {
    didSet {
      guard let tl = layer as? TextyLayer else {
        return
      }
      tl.text = text
    }
  }
  
  var title: String = "" {
    didSet {
      guard let tl = layer as? TextyLayer else {
        return
      }
      tl.title = title
    }
  }
  
  
  override class var layerClass: AnyClass {
    return TextyLayer.self
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    guard let tl = layer as? TextyLayer else {
      return
    }

    tl.textLayer.frame = tl.bounds
    tl.layoutSublayers()
  }
}


class TextyLayer: CALayer {
  struct Constants {
    static let topBarHeight: CGFloat = 48.0
    static let backColor: CGColor = UIColor(red: 44 / 255, green: 44 / 255, blue: 46 / 225, alpha: 1.0).cgColor
    static let textColor: CGColor = UIColor.white.cgColor
    static let orange: CGColor = UIColor(red: 255 / 255, green: 107 / 255, blue: 74 / 255, alpha: 1.0).cgColor
    static let blue: CGColor = UIColor(red: 100 / 255, green: 48 / 255, blue: 255 / 255, alpha: 1).cgColor
    static let gutter: CGFloat = 32.0
    static let leftBarGutter: CGFloat = 10
    static let rightBarGutter: CGFloat = 42
    static let bottomBarHeight: CGFloat = 6
    static let topBarToText: CGFloat = 16
    static let textVertPad: CGFloat = 8
    static let titleGutter: CGFloat = 12
  }
  
  let titleLayer: CATextLayer = CATextLayer()
  let textLayer: CATextLayer = CATextLayer()
  let background: CALayer = CALayer()
  let topBar: CAGradientLayer = CAGradientLayer()
  let bottomBar: CAGradientLayer = CAGradientLayer()
  
  override var frame: CGRect {
    didSet {
      textLayer.frame = bounds
    }
  }
  
  var title: String = "" {
    didSet {
      titleLayer.isWrapped = false
      titleLayer.shouldRasterize = true
      titleLayer.rasterizationScale = UIScreen.main.scale
      titleLayer.backgroundColor = UIColor.clear.cgColor
      titleLayer.alignmentMode = CATextLayerAlignmentMode.left
      titleLayer.contentsGravity = .left
      titleLayer.needsDisplayOnBoundsChange = true
      titleLayer.isGeometryFlipped = true
      titleLayer.masksToBounds = true
      let attributes: [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.foregroundColor: UIColor.white,
        NSAttributedString.Key.font: UIFont.systemFont(ofSize: 28, weight: .black).boldItalic()
      ]
      self.titleAttrText = NSAttributedString(string: title, attributes: attributes)
      titleLayer.string = titleAttrText
    }
  }
  
  private let attrs: [NSAttributedString.Key: Any] = [
    NSAttributedString.Key.foregroundColor: UIColor.white,
    NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .semibold),
  ]
  
  private var attrText: NSAttributedString = NSAttributedString(string: "")
  private var titleAttrText: NSAttributedString = NSAttributedString(string: "")
          
  var text: String =  "" {
    didSet {
      textLayer.isWrapped = true
      textLayer.shouldRasterize = true
      textLayer.rasterizationScale = UIScreen.main.scale
      textLayer.backgroundColor = UIColor.clear.cgColor
      textLayer.alignmentMode = CATextLayerAlignmentMode.left
      textLayer.contentsGravity = .left
      textLayer.needsDisplayOnBoundsChange = true
      textLayer.isGeometryFlipped = true
      textLayer.masksToBounds = true
      self.attrText = NSAttributedString(string: text, attributes: attrs)
      textLayer.string = self.attrText
    }
  }
  
  override init() {
    super.init()
    backgroundColor = UIColor.clear.cgColor
    background.backgroundColor = Constants.backColor
    addSublayer(background)
    addSublayer(topBar)
    topBar.addSublayer(titleLayer)
    background.addSublayer(textLayer)
    addSublayer(bottomBar)
    
    [topBar, bottomBar].forEach {
      $0.colors = [Constants.blue, Constants.orange]
      $0.transform = CATransform3DMakeRotation(CGFloat.pi / 2, 0, 0, 1)
    }
    
    titleLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func layoutSublayers() {
    let topBarFrame = CGRect(x: Constants.leftBarGutter, y: 0,
                             width: bounds.width - (Constants.leftBarGutter + Constants.rightBarGutter),
                             height: Constants.topBarHeight)
    
    topBar.frame = topBarFrame
    var y = Constants.topBarHeight / 2
    
    let backFrame: CGRect = CGRect(x: 0, y: y, width: bounds.width, height: bounds.height - y)
    background.frame = backFrame
    var width = bounds.width
    var size = getSizeByLabelMeasuring(width: width, newAttrStr: attrText)
    var x = Constants.gutter
    y = topBarFrame.height / 2 + Constants.topBarToText
    textLayer.frame = CGRect(x: x, y: y, width: size.width, height: size.height + Constants.textVertPad)
    bottomBar.frame = CGRect(x: 0, y: bounds.height - Constants.bottomBarHeight, width: bounds.width, height: Constants.bottomBarHeight)
    
    size = getSizeByLabelMeasuring(width: bounds.width, newAttrStr: titleAttrText)
    x = Constants.topBarHeight / 2 - size.height / 2
    y = topBarFrame.width - size.width - Constants.titleGutter
    width = size.width
    let height = size.height
    let titleFrame = CGRect(x: x, y: y, width: height, height: width)
    titleLayer.frame = titleFrame
  }
  
  
  private func getSizeByLabelMeasuring(width: CGFloat, newAttrStr: NSAttributedString) -> CGSize {
    let horzPad: CGFloat = Constants.gutter
    let label: UILabel = UILabel()
    label.numberOfLines = 0

    label.attributedText = newAttrStr
        
    let initialSize: CGSize = CGSize(width: width - (horzPad * 2), height: CGFloat.greatestFiniteMagnitude)
    let sizeThatFits: CGSize = label.sizeThatFits(initialSize)
    return sizeThatFits
  }
}

extension NSAttributedString {
  var pointSize: CGFloat {
    guard let font  = attribute(NSAttributedString.Key.font, at: 0, effectiveRange: nil) as? UIFont else {
      return 0
    }
    return font.pointSize

  }
}

extension UIFont {
  func withTraits(traits:UIFontDescriptor.SymbolicTraits) -> UIFont {
    let descriptor = fontDescriptor.withSymbolicTraits(traits)
    return UIFont(descriptor: descriptor!, size: 0) //size 0 means keep the size as it is
  }
  
  func boldItalic() -> UIFont {
    return withTraits(traits: [.traitBold, .traitItalic])
  }

  func bold() -> UIFont {
    return withTraits(traits: .traitBold)
  }

  func italic() -> UIFont {
    return withTraits(traits: .traitItalic)
  }
}
